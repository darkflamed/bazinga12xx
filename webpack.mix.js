const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*mix.options({
    purifyCss: true
});*/

mix.js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/moment.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   //.sourceMaps()
   .version();

mix.js('resources/assets/js/pus/pus.js', 'public/js')
    .sass('resources/assets/sass/pus/pus.scss', 'public/css')
    //.sourceMaps()
    .version();