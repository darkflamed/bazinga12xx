@extends('global.main')

@section('more-js')

@stop

@section('more-css')

@stop

@section('title')
    @lang('titles.class_change')
@stop

@section('description')

@stop

@section('keywords')

@stop

@section('content')
    <article id="content" class="well padding0 margin0">
        <div data-sitehead="" class="sitehead-account">
            <h1 class="margin0 hidden-xs">@lang('titles.class_change')</h1>
        </div>
        <section class="sec-user">
            <div class="form">
                <div class="margin0 row">
                    <div class="col-xs-12">
					<span class="col-xs-12">
						<span class="point frost col-xs-12"><h4 style="color: #088A29;"></h4></span>
						<span class="col-xs-12" style="margin-bottom: 25px;">
							@lang('message.class_change_message')
						</span>
                        @if (isset($errors) && count($errors) > 0)
                            <div class="alert alert-danger col-xs-12">
                                @lang('message.problem_with_input')<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
						<div class="col-xs-12">
                            <form class="form-horizontal" role="form" method="post" action="/account/classchange">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
								<span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="character">@lang('fields.character')@lang('fields.colon')</label>
									<div class="col-sm-6 col-xs-8"><select class="form-control" style="font-size: 12px !important; padding: 5px 2px; color: #d3c2ae !important;" id="character" name="character">
                                            @foreach($Characters as $char)
                                                <option value="{{$char->strUserId}}">{{$char->strUserId}}</option>
                                            @endforeach
                                        </select></div></span>
								<span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="class">@lang('fields.payment_type')@lang('fields.colon')</label>
									<div class="col-sm-6 col-xs-8"><select class="form-control" style="font-size: 12px !important; padding: 5px 2px;color: #d3c2ae !important;" id="payment" name="payment">
                                            <option value="0">750 Knight Cash</option>
                                            <option value="1">15,000 National Points</option>
                                        </select></div></span>
								<span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="class">@lang('fields.payment_type')@lang('fields.colon')</label>
									<div class="col-sm-6 col-xs-8"><select class="form-control" style="font-size: 12px !important; padding: 5px 2px;color: #d3c2ae !important;" id="class" name="class">
                                            @if($Nation == 1)
                                                <option value="11">-&gt;Orc Warrior</option>
                                                <option value="12">-&gt;Orc Rogue</option>
                                                <option value="13">-&gt;Orc Magician</option>
                                                <option value="14">-&gt;Orc Priest-Taurek</option>
                                                <option value="15">-&gt;Orc Priest-Puri</option>
                                            @elseif($Nation == 2)
                                                <option value="21">&gt;Human Warrior-Barbarian</option>
                                                <option value="22">&gt;Human Warrior-Male</option>
                                                <option value="23">&gt;Human Warrior-Female</option>
                                                <option value="24">&gt;Human Rogue-Male</option>
                                                <option value="25">&gt;Human Rogue-Female</option>
                                                <option value="26">&gt;Human Magician-Male</option>
                                                <option value="27">&gt;Human Magician-Female</option>
                                                <option value="28">&gt;Human Priest-Male</option>
                                                <option value="29">&gt;Human Priest-Female</option>
                                            @else
                                                <option value="0">Please Create a Character</option>
                                            @endif
                                        </select></div></span>
								<span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="class">Secret PIN:</label>
									<div class="col-sm-6 col-xs-8"><input class="form-control" style="font-size: 12px !important; padding: 5px 2px;color: #d3c2ae !important;" id="pin" name="pin">
                                    </div></span>
                                <span class="form-group" style="border-spacing: 5px 6px;"><button class="btn btn-primary-dark col-xs-offset-5" type="submit" name="submit">Change Class</button></span>
                            </form>
                        </div>
					</span>
                    </div>
                </div>
            </div>
        </section>
    </article>
@stop