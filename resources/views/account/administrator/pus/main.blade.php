@extends('global.main')

@section('javascript')
    <script type="text/javascript">
        let currentItem = 0;

        $(document).ready(function() {
            $('#itemselect').on('change', function () {
                $('#item-' + currentItem).hide();
                currentItem = $(this).val();
                $('#item-' + currentItem).show();
            });
        });

        function Delete(id) {
            if(window.confirm("Are you sure you want to delete this item?")) {
                location.href='/account/administrator/pus/items/delete/'+id;
            }
        }
    </script>
@stop

@section('css')

@stop

@section('title')
    Manage Powerup Store
@stop

@section('content')
<div class="container container-main">
    <article id="content" class="padding0 margin0">
        <div data-sitehead="" class="sitehead-account">
            <h1 class="margin0 hidden-xs">Manage Powerup Store</h1>
        </div>
        <section class="sec-user">
            <div class="form">
                <div class="margin0 row">
                    <div class="col-xs-12">
					<span class="col-xs-12">
                        @if (Session::has('Success'))
                            <div class="success alert-success col-xs-12" style="text-align: center;">
                                <h4>
                                <ul>
                                        <li>{{ Session::get('Success') }}</li>
                                </ul>
                                </h4>
                            </div>
                        @endif

                        @if (isset($errors) && count($errors) > 0)
                            <div class="alert alert-danger col-xs-12">
                                @lang('message.problem_with_input')<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="col-xs-12">

                            <div class="col-sm-offset-2 col-sm-7">
                                <select id="itemselect" class="form-control" style="font-size: 12px !important; padding: 5px 2px; margin-top: 20px;margin-bottom: 20px;color: #d3c2ae !important;" id="class" name="item">
                                    <option value="0">New Item</option>
                                    @foreach($Items as $item)
                                        <option value="{{$item->id}}" @if(old('id') == $item->id) selected @endif>{{$item->name}} - {{$item->num}} - {{$item->price}}KC</option>
                                    @endforeach
                                </select>
                            </div>

                            <form id="item-0" class="form-horizontal col-xs-offset-2" role="form" method="post" action="{{url('account/administrator/pus/items')}}" @if(old('id') != 0) hidden @endif>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="id" value="0">

                                <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="num">Num:</label>
									<div class="col-xs-8"><input class="col-xs-8 form-control" maxlength="9" name="num"
                                                                 value="@if(old('id') == 0){{ old('num') }}@endif"></div></span>
                                <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="name">Name:</label>
									<div class="col-xs-8"><input class="col-xs-8 form-control" maxlength="50" name="name"
                                                                 value="@if(old('id') == 0){{ old('name') }}@endif"></div></span>
                                <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="price">Price:</label>
									<div class="col-xs-8"><input class="col-xs-8 form-control" name="price"
                                                                 value="@if(old('id') == 0){{ old('price') }}@else 0 @endif"></div></span>
                                <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="count">Count:</label>
									<div class="col-xs-8"><input class="col-xs-8 form-control" maxlength="10" type="number" name="count"
                                                                 value="@if(old('id') == 0){{ old('count') }}@else 1 @endif"></div></span>
                                <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="expiredays">Expire Days:</label>
									<div class="col-xs-8"><input class="col-xs-8 form-control" maxlength="10" type="number" name="expiredays"
                                                                 value="@if(old('id') == 0){{ old('expiredays') }}@else 0 @endif"></div></span>
                                <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="desc1">Description_1:</label>
									<div class="col-xs-8"><textarea class="col-xs-8 form-control" maxlength="250" name="desc1">@if(old('id') == 0){{ old('desc1') }}@endif</textarea></div></span>
                                <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="desc2">Description_2:</label>
									<div class="col-xs-8"><textarea class="col-xs-8 form-control" maxlength="250" name="desc2">@if(old('id') == 0){{ old('desc2') }}@endif</textarea></div></span>
                                <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="category">Category:</label>
									<div class="col-xs-8">
                                        <select class="form-control" style="border-spacing: 5px 2px; color:#FFFFFF !important; font-size: 12px !important;" name="category">
                                            @foreach($Categories as $category)
                                                <option value="{{$category->id}}" @if(old('id') == 0 && old('category') == $category->id) selected @endif>{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </span>
                                <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="hotitem">Hot Item:</label>
									<div class="col-xs-8">
                                        <select class="form-control" style="border-spacing: 5px 2px; color:#FFFFFF !important; font-size: 12px !important;" name="hotitem">
                                            @for($i = 0; $i < 6; $i++)
                                                <option value="{{$i}}" @if(old('id') == 0 && old('hotitem') == $i) selected @endif>{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </span>
                                <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="newitem">New Item:</label>
									<div class="col-xs-8">
                                        <select class="form-control" style="border-spacing: 5px 2px; color:#FFFFFF !important; font-size: 12px !important;" name="newitem">
                                            @for($i = 0; $i < 6; $i++)
                                                <option value="{{$i}}" @if(old('id') == 0 && old('newitem') == $i) selected @endif>{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </span>
                                <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="recomitem">Recommended Item:</label>
									<div class="col-xs-8">
                                        <select class="form-control" style="border-spacing: 5px 2px; color:#FFFFFF !important; font-size: 12px !important;" name="recomitem">
                                            @for($i = 0; $i < 6; $i++)
                                                <option value="{{$i}}" @if(old('id') == 0 && old('recomitem') == $i) selected @endif>{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </span>
                                <span class="form-group" style="border-spacing: 5px 6px;">
                                    <button class="btn btn-main col-xs-offset-4" type="submit" name="submit">Submit</button>
                                </span>
                            </form>

                            @foreach($Items as $item)
                            <form id="item-{{$item->id}}" class="form-horizontal col-xs-offset-2" role="form" method="post" action="{{url('account/administrator/pus/items')}}" @if(old('id') != $item->id || empty(old('id'))) hidden @endif>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="id" value="{{$item->id}}">

                                <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="num">Num:</label>
									<div class="col-xs-8"><input class="col-xs-8 form-control" maxlength="9" name="num" value="{{$item->num}}"></div></span>
                                <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="name">Name:</label>
									<div class="col-xs-8"><input class="col-xs-8 form-control" maxlength="50" name="name" value="{{$item->name}}"></div></span>
                                <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="price">Price:</label>
									<div class="col-xs-8"><input class="col-xs-8 form-control" name="price" value="{{$item->price}}"></div></span>
                                <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="count">Count:</label>
									<div class="col-xs-8"><input class="col-xs-8 form-control" maxlength="10" type="number" name="count" value="{{$item->count}}"></div></span>
                                <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="expiredays">Expire Days:</label>
									<div class="col-xs-8"><input class="col-xs-8 form-control" maxlength="10" type="number" name="expiredays" value="{{$item->expire_days}}"></div></span>
                                <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="desc1">Description_1:</label>
									<div class="col-xs-8"><textarea class="col-xs-8 form-control" maxlength="250" name="desc1">{{$item->desc_1}}</textarea></div></span>
                                <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="desc2">Description_2:</label>
									<div class="col-xs-8"><textarea class="col-xs-8 form-control" maxlength="250" name="desc2">{{$item->desc_2}}</textarea></div></span>
                                <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="category">Category:</label>
									<div class="col-xs-8">
                                        <select class="form-control" style="border-spacing: 5px 2px; color:#FFFFFF !important; font-size: 12px !important;" name="category">
                                            @foreach($Categories as $category)
                                                <option value="{{$category->id}}"
                                                        @if($category->id == $item->category_id)
                                                            selected
                                                        @endif >{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </span>
                                <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="hotitem">Hot Item:</label>
									<div class="col-xs-8">
                                        <select class="form-control" style="border-spacing: 5px 2px; color:#FFFFFF !important; font-size: 12px !important;" name="hotitem">
                                            @for($i = 0; $i < 5; $i++)
                                                <option value="{{$i}}"
                                                        @if($i == $item->hot_item)
                                                        selected
                                                        @endif >{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </span>
                                <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="newitem">New Item:</label>
									<div class="col-xs-8">
                                        <select class="form-control" style="border-spacing: 5px 2px; color:#FFFFFF !important; font-size: 12px !important;" name="newitem">
                                            @for($i = 0; $i < 5; $i++)
                                                <option value="{{$i}}"
                                                        @if($i == $item->new_item)
                                                        selected
                                                        @endif >{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </span>
                                <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-6" for="recomitem">Recommended Item:</label>
									<div class="col-xs-8">
                                        <select class="form-control" style="border-spacing: 5px 2px; color:#FFFFFF !important; font-size: 12px !important;" name="recomitem">
                                            @for($i = 0; $i < 5; $i++)
                                                <option value="{{$i}}"
                                                        @if($i == $item->recommending_item)
                                                        selected
                                                        @endif >{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </span>
                                <span class="form-group" style="border-spacing: 5px 6px;">
                                    <button class="btn btn-danger col-xs-offset-1" type="button" name="delete" onClick="Delete({{$item->id}});"
                                    style="color: #FFF !important;">Delete</button>
                                    <button class="btn btn-main col-xs-offset-2" type="submit" name="submit">Submit</button>
                                </span>
                            </form>
                            @endforeach
                        </div>
					</span>
                    </div>
                </div>
            </div>
        </section>
    </article>
</div>
@stop