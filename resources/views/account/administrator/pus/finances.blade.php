@extends('global.main')

@section('javascript')
    <script type="text/javascript" src="{{ asset('/js/icons.js') }}"></script>
    <script   src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"   integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E="   crossorigin="anonymous"></script>

    <script type="text/javascript">
        var CurrentGroup = 0;

        $(document).ready(function() {
            $( "#tabs" ).tabs();
        });

        function showItemGroup($group) {
            $('#ItemGroup-'+CurrentGroup).hide();
            CurrentGroup = $group;
            $('#ItemGroup-'+$group).show();
        }
    </script>
@stop

@section('css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
@stop

@section('title')
    Manage Finances
@stop

@section('content')
    <style>
        .ui-tabs-active {
            background: none repeat scroll 0 0 #342f2b !important;
        }

        .ui-widget-content .ui-state-default {
            background: none repeat scroll 0 0 #1d1712;
        }
        .ui-tabs-tab a:hover {
            color: #9f7401 !important;
        }
        .ui-tabs-tab a:visited {
            color: #9f7401 !important;
        }
        .ui-tabs-tab a:active {
            color: #9f7401 !important;
        }
        .ui-tabs-tab a:focus {
            color: #9f7401 !important;
        }
    </style>

    <article id="content" class="well padding0 margin0">
        <div data-sitehead="" class="sitehead-account">
            <h1 class="margin0 hidden-xs">Manage Powerup Store Finances</h1>
        </div>
        <section class="sec-user">
            <table width="100%">
            <tbody>
                <tr>
                    <td>
                        <table style="margin: 1%">
                            <tbody>
                            <tr>
                                <td colspan="2"><div class="textredlg">General Information</div></td>
                            </tr>
                            <tr>
                                <td><b>Gross Income</b></td>
                                <td>${{number_format($TransactionSummary->gross_income, 2)}}</td>
                            </tr>
                            <tr>
                                <td><b>Fees</b></td>
                                <td>${{number_format($TransactionSummary->total_fees, 2)}}</td>
                            </tr>
                            <tr>
                                <td><b>Net Income</b></td>
                                <td>${{number_format(($TransactionSummary->gross_income - $TransactionSummary->total_fees), 2)}}</td>
                            </tr>
                            <tr>
                                <td><b>Number of Transactions</b></td>
                                <td>{{$TransactionSummary->num_transactions}}</td>
                            </tr>
                            <tr>
                                <td colspan="2"><div class="textredlg">Available Funds</div></td>
                            </tr>
                            <tr>
                                <td><b>Justin Mercer</b></td>
                                <td>$
                                    <?php
                                        $gross = ($TransactionSummary->gross_income - $TransactionSummary->total_fees)*.50;
                                        foreach($Transactions as $trans)
                                            if($trans->status == 4 && $trans->account_id = 1) $gross -= $trans->payment_gross;
                                        echo number_format($gross, 2);
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td><b>Christopher Minardi</b></td>
                                <td>$<?php
                                    $gross = ($TransactionSummary->gross_income - $TransactionSummary->total_fees)*.20;
                                    foreach($Transactions as $trans)
                                        if($trans->status == 4 && $trans->account_id = 0) $gross -= $trans->payment_gross;
                                    echo number_format($gross, 2);
                                    ?></td>
                            </tr>
                            <tr>
                                <td><b>Pacnik</b></td>
                                <td>$<?php
                                    $gross = ($TransactionSummary->gross_income - $TransactionSummary->total_fees)*.20;
                                    foreach($Transactions as $trans)
                                        if($trans->status == 4 && $trans->account_id = 0) $gross -= $trans->payment_gross;
                                    echo number_format($gross, 2);
                                    ?></td>
                            </tr>
                            <tr>
                                <td><b>Sheldon Cooper</b></td>
                                <td>$<?php
                                    $gross = ($TransactionSummary->gross_income - $TransactionSummary->total_fees)*.10;
                                    foreach($Transactions as $trans)
                                        if($trans->status == 4 && $trans->account_id = 0) $gross -= $trans->payment_gross;
                                    echo number_format($gross, 2);
                                    ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td>
                        <div id="tabs" style="margin-left: auto; margin-right: auto; width: 100%;">
                            <ul style="height: 28px">
                                <li style="float: left;"><a href="#tabs-1"><div style="padding: 6px 12px; font-size: 12px;">User Transactions</div></a></li>
                                <li style="float: left;"><a href="#tabs-2"><div style="padding: 6px 12px; font-size: 12px;">Bills</div></a></li>
                                <li style="float: left;"><a href="#tabs-3"><div style="padding: 6px 12px; font-size: 12px;">Money Requests</div></a></li>
                            </ul>

                            <div id="tabs-1" style="width: 100%; min-height: 300px;">
                                <table class="table table-striped col-xs-12" style="border: solid 2px rgba(0, 0, 0, 0.8);">
                                    <thead>
                                    <tr bgcolor="#25120B" class="textwhite">
                                        <td width="5%" align="center"></td>
                                        <td width="20%" align="center">AccountID</td>
                                        <td width="20%" align="center">Name</td>
                                        <td width="20%" align="center">Provider</td>
                                        <td width="10%" align="center">Profit</td>
                                        <td width="5%" align="center">KC</td>
                                        <td width="20%" align="center">Transaction ID</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($Transactions as $index => $trans)
                                        @if(!$trans->is_bill && $trans->status == 3)
                                        <tr>
                                            <td align="center"> <font color="#e7dbc3">{{$index + 1}}</font></td>
                                            <td align="center">{{$trans->strAccountID}}</td>
                                            <td align="center">{{$trans->first_name}} {{$trans->last_name}}</td>
                                            <td align="center">{{$trans->provider}}</td>
                                            <td align="center">${{number_format($trans->payment_gross - $trans->payment_fee, 2)}}</td>
                                            <td align="center">{{$trans->kc_amount}}</td>
                                            <td align="center">{{$trans->txn_id}}</td>
                                        </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div id="tabs-2" style="width: 100%; min-height: 300px;">
                                <table class="table table-striped col-xs-12" style="border: solid 2px rgba(0, 0, 0, 0.8);">
                                    <thead>
                                    <tr bgcolor="#25120B" class="textwhite">
                                        <td width="5%" align="center"></td>
                                        <td width="20%" align="center">AccountID</td>
                                        <td width="20%" align="center">Purpose</td>
                                        <td width="10%" align="center">Amount</td>
                                        <td width="20%" align="center">Transaction ID (optional)</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($Transactions as $index => $trans)
                                        @if($trans->is_bill && $trans->status == 3)
                                            <tr>
                                                <td align="center"> <font color="#e7dbc3">{{$index + 1}}</font></td>
                                                <td align="center">{{$trans->strAccountID}}</td>
                                                <td align="center">{{$trans->provider}}</td>
                                                <td align="center">${{number_format($trans->payment_gross, 2)}}</td>
                                                <td align="center">{{$trans->txn_id}}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div id="tabs-3" style="width: 100%; min-height: 300px;">
                                <table class="table table-striped col-xs-12" style="border: solid 2px rgba(0, 0, 0, 0.8);">
                                    <thead>
                                    <tr bgcolor="#25120B" class="textwhite">
                                        <td width="5%" align="center"></td>
                                        <td width="20%" align="center">AccountID</td>
                                        <td width="20%" align="center">Name</td>
                                        <td width="10%" align="center">Amount</td>
                                        <td width="20%" align="center">Status</td>
                                        <td width="20%" align="center">Notes</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($Transactions as $index => $trans)
                                        @if($trans->status == 4)
                                            <tr>
                                                <td align="center"> <font color="#e7dbc3">{{$index + 1}}</font></td>
                                                <td align="center">{{$trans->strAccountID}}</td>
                                                <td align="center"></td>
                                                <td align="center">${{number_format($trans->payment_gross, 2)}}</td>
                                                <td align="center">{{$trans->provider}}</td>
                                                <td align="center"></td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
            </table>
        </section>
    </article>
@stop