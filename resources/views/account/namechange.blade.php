@extends('global.main')

@section('more-js')

@stop

@section('more-css')

@stop

@section('title')
    Name Change
@stop

@section('description')

@stop

@section('keywords')

@stop

@section('content')
    <article id="content" class="well padding0 margin0">
        <div data-sitehead="" class="sitehead-account">
            <h1 class="margin0 hidden-xs">Name Change</h1>
        </div>
        <section class="sec-user">
            <div class="form">
                <div class="margin0 row">
                    <div class="col-xs-12">
					<span class="col-xs-12">
						<span class="point frost col-xs-12"><h4 style="color: #088A29;"></h4></span>
						<span class="col-xs-12" style="margin-bottom: 25px;">
							<div class=""><font color="gold">You must be <b>OFFLINE</b> when you attempt to perform name change.</font><br></div>
						</span>
                        @if (isset($errors) && count($errors) > 0)
                            <div class="alert alert-danger col-xs-12">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
						<div class="col-xs-12">
                            <form class="form-horizontal" role="form" method="post" action="/account/changename">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
								<span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-4" for="class">Payment Type:</label>
									<div class="col-xs-6"><select class="form-control" style="font-size: 12px !important; padding: 5px 2px;color: #d3c2ae !important;" id="payment" name="payment">
                                            <option value="0">500 Knight Cash</option>
                                            <option value="1">10,000 National Points</option>
                                        </select></div></span>
								<span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-4" for="strchars">Character:</label>
									<div class="col-xs-6"><select class="form-control" style="font-size: 12px !important; padding: 5px 2px; color: #d3c2ae !important;" id="strchars" name="strchars">
                                            @foreach($Characters as $char)
                                                <option value="{{$char->strUserId}}">{{$char->strUserId}}</option>
                                            @endforeach
                                        </select></div></span>
								<span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-4" for="newname">New Name:</label>
									<div class="col-xs-6"><input class="form-control" maxlength="21" style="font-size: 12px !important; padding: 5px 2px; color: #d3c2ae !important;" id="newname" name="newname">
                                    </div></span>
								<span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-4" for="class">Secret PIN:</label>
									<div class="col-sm-6 col-xs-8"><input class="form-control" style="font-size: 12px !important; padding: 5px 2px;color: #d3c2ae !important;" id="pin" name="pin">
                                    </div></span>
                                <span class="form-group" style="border-spacing: 5px 6px;"><button class="btn btn-primary-dark col-xs-offset-4" type="submit" name="submit">Change Name</button></span>
                            </form>
                        </div>
					</span>
                    </div>
                </div>
            </div>
        </section>
    </article>
@stop