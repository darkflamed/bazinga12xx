@extends('global.main')

@section('more-js')

@stop

@section('more-css')

@stop

@section('title')
    Gender Change
@stop

@section('description')

@stop

@section('keywords')

@stop

@section('content')
    <div class="container container-main">
        <span class="col-xs-12">
            <span class="col-xs-12" style="margin-bottom: 25px;">
                <h2>@lang('message.gender_change', ['username' => Auth::user()->strAccountID])</h2>
            </span>
            @if (isset($errors) && count($errors) > 0)
                <div class="alert alert-danger col-xs-12">
                    @lang('message.problem_with_input')<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="col-xs-12">
                <div class=""><font color="gold">You must be <b>OFFLINE</b> when you attempt to perform gender change.</font><br></div>
                <form class="form-horizontal" role="form" method="post" action="/account/changegender">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-3" for="character">Character:</label>
                        <div class="col-xs-9"><select class="form-control" style="font-size: 12px !important; padding: 5px 2px; color: #d3c2ae !important;" id="character" name="character">
                                @foreach($Characters as $char)
                                    <option value="{{$char->strUserId}}">{{$char->strUserId}}</option>
                                @endforeach
                            </select></div></span>
                    <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-3" for="barbarian">Change to Barbarian?</label>
                        <div class="col-xs-9">
                            <input style="display: block !important;" type="checkbox" id="barbarian" name="barbarian" value="checked">
                        </div></span>
                    <span class="form-group" style="border-spacing: 5px 6px;"><button class="btn btn-main col-xs-offset-4" type="submit" name="submit">Change Gender</button></span>
                </form>
            </div>
        </span>
    </div>
@stop