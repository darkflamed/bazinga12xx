@extends('global.main')

@section('more-js')

@stop

@section('more-css')

@stop

@section('title')
    Account
@stop

@section('description')

@stop

@section('keywords')

@stop

@section('content')

<div class="container container-main">
    <table class="table">
        <thead>
        <tr><td>
                <h4>Account Management</h4>
                @if (isset($errors) && count($errors) > 0)
                    <div class="alert alert-danger col-xs-12">
                        <ul class="list-unstyled">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </td></tr>
        </thead>
        <tbody>
        <tr>
            <td class="row">
                <div class="col-xs-6 padding-10">
                    <div class="col-xs-12">
                        <div class="well well-main">
                            <h4 class="text-center">Account Information</h4>
                            <h5>
                                <ul class="list-unstyled list-group row">
                                    <li class="list-group-item col-xs-6">Account User ID:</li>
                                    <li class="list-group-item col-xs-6">{{Auth::user()->strAccountID}}</li>
                                    <li class="list-group-item col-xs-6">Date Created:</li>
                                    <li class="list-group-item col-xs-6">{{Carbon\Carbon::parse(Auth::user()->created_at)->format('M d, Y')}}</li>
                                    <li class="list-group-item col-xs-6">Email Address:</li>
                                    <li class="list-group-item col-xs-6">{{Auth::user()->email}}</li>
                                    <li class="list-group-item col-xs-6">Knight Cash:</li>
                                    <li class="list-group-item col-xs-6">{{Auth::user()->nKnightCash}}</li>
                                </ul>
                            </h5>
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-bottom: 20px;">
                        <div class="well well-main">
                            <a href="{{ url('/account/recharge') }}"><img class="img-responsive center-block" src="{{ asset('images/pus/recharge.png') }}"></a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 padding-10">
                    <div class="well well-main">
                        <h4 class="text-center">Account Functions</h4>
                        <h5>
                            <ul class="list-unstyled list-group row">
                                <li class="list-group-item col-xs-6"><a href="{{ url('/account/forgotpin') }}">Forgot PIN</a></li>
                                <li class="list-group-item col-xs-6"><a href="{{ url('/account/changepin') }}">Change PIN</a></li>
                                <li class="list-group-item col-xs-6"><a href="{{ url('/account/changepassword') }}">Change Password</a></li>
                                <li class="list-group-item col-xs-6"><a href="{{ url('/account/seal_unseal') }}">Seal/Unseal Items</a></li>
                                <li class="list-group-item col-xs-6"><a href="{{ url('/account/clan_nation_change') }}">Clan Nation Change</a></li>
                                <li class="list-group-item col-xs-6"><a href="{{ url('/account/clan_leader_change') }}">Clan Leader Change</a></li>
                            </ul>
                        </h5>
                        @if(Auth::user()->is_admin == true)
                            <h4 class="text-center">GM Functions</h4>
                            <h5>
                                <ul class="list-unstyled list-group row">
                                    <li class="list-group-item col-xs-6"><a href="#"></a></li>
                                    <li class="list-group-item col-xs-6"><a href="#"></a></li>
                                </ul>
                            </h5>
                        @endif
                        @if(Auth::user()->is_super_admin == true)
                        <h4 class="text-center">Administrator Functions</h4>
                        <h5>
                            <ul class="list-unstyled list-group row">
                                <li class="list-group-item col-xs-6"><a href="{{url('account/administrator/pus')}}">Modify PUS</a></li>
                                <li class="list-group-item col-xs-6"><a href="#">Modify News</a></li>
                            </ul>
                        </h5>
                        @endif
                    </div>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>
@stop