@extends('global.main')

@section('javascript')
    <script src="https://www.paypalobjects.com/api/checkout.js" data-version-4></script>
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            'oAuthToken' => $oAuth
        ]) !!};

        window.axios.defaults.headers.common = {
            'X-CSRF-TOKEN': window.Laravel.csrfToken,
            'X-Requested-With': 'XMLHttpRequest',
            'Authorization': 'Bearer ' + window.Laravel.oAuthToken,
            'Accept': 'application/json',
        };

        $(function() {
            @if(filter_var(env('PAYPAL_SANDBOXED', 'false'), FILTER_VALIDATE_BOOLEAN) == true)
                window.Checkout.renderPaypalButton('sandbox');
            @else
                window.Checkout.renderPaypalButton('production');
            @endif
        });
    </script>
@stop

@section('css')

@stop

@section('title')
    Recharge Knight Cash
@stop

@section('content')
    <div class="container container-main">
        <div class="col-xs-12">
            <span class="col-xs-12 text-center" style="margin-bottom: 25px;">
                <h2>RECHARGE</h2>
            </span>
            <div class="col-xs-12">
                <form lass="form-horizontal" id="RechargeSubmit" action="#" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <h3>Please select Knight Cash package:</h3>
                    <fieldset class="form-group">
                        <select class="form-control" name="KCSelect" id="KCSelect">
                            {!! $KCOptions !!}
                        </select>
                    </fieldset>
                    <h3>Please select service:</h3>
                    <div class="row" style="padding-bottom: 20px;">
                        <div class="col-xs-5 col-xs-offset-1">
                            <center><div id="paypal-button-container"></div></center>
                        </div>
                        <div class="col-xs-5">
                            <center><img onClick="window.Checkout.paygolCreatePayment()" onmouseover="this.style.cursor='pointer'" src="{{ asset('images/pus/paygol.png') }}" border="0" alt="Make payments with Paygol: the easiest way!"></center>
                        </div>
                        <div class="col-xs-12">
                            <div id="debugDiv"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop