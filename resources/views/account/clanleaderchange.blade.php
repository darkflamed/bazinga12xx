@extends('global.main')

@section('css')

@stop

@section('javascript')

@stop

@section('title')
    Clan Leader Change
@stop

@section('content')
    <div class="container container-main">
        <span class="col-xs-12">
            <span class="col-xs-12" style="margin-bottom: 25px;">
                <h2>Please select a character give up leadership of clan:</h2>
				<br><h3><font color="orange">This will cost 250 Knight Cash</font></h3>
            </span>
            @if (isset($errors) && count($errors) > 0)
                <div class="alert alert-danger col-xs-12">
                    @lang('message.problem_with_input')<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="col-md-8 col-md-offset-2">
                <form class="form-horizontal" role="form" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <span class="form-group" style="border-spacing: 5px 6px;">
                        <select class="form-control" name="character">
                        @foreach($Characters as $char)
                                <option value="{{ $char->id }}">{{ $char->strUserId }}</option>
                            @endforeach
                        </select>
                    </span>
                    <span class="form-group" style="border-spacing: 5px 6px;">
                        <select class="form-control" name="chief_character">
                        @foreach($ClanCharacters as $char)
                            @if($char->id != $Characters->first()->id)
                                <option value="{{ $char->id }}">{{ $char->strUserId }}</option>
                            @endif
                        @endforeach
                        </select>
                    </span>
                    <span class="form-group" style="border-spacing: 5px 2px;">
                        <label class="col-xs-3" for="class">Secret PIN:</label>
                        <div class="col-sm-6 col-xs-8">
                            <input class="form-control" style="font-size: 12px !important; padding: 5px 2px;color: #d3c2ae !important;" id="pin" name="pin">
                        </div>
                    </span>
                    <span class="form-group" style="border-spacing: 5px 6px;"><button class="btn btn-main col-xs-offset-4" type="submit" name="submit">Change Leader</button></span>
                </form>
            </div>
        </span>
    </div>
@stop