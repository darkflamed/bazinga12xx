@extends('global.main')

@section('javascript')

@stop

@section('css')

@stop

@section('title')
    Change PIN
@stop

@section('content')
    <div class="container container-main">
        <span class="col-xs-12">
            <span class="col-xs-12" style="margin-bottom: 25px;">
                <h2>Please input PIN information</h2>
            </span>
            @if (isset($errors) && count($errors) > 0)
                <div class="alert alert-danger col-xs-12">
                    Problem with input<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="col-xs-12">
                <form class="form-horizontal" role="form" method="post" action="{{url('account/changepin')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-3" for="pin">Old PIN</label>
                        <div class="col-xs-6">
                            <input class="form-control" type="password" id="pin" name="pin" maxlength="28" value="{{old('pin')}}">
                        </div>
                    </span>
                    <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-3" for="newpin">New PIN</label>
                        <div class="col-xs-6">
                            <input class="form-control" type="password" id="newpin" name="newpin" maxlength="28" value="{{old('newpin')}}">
                        </div>
                    </span>
                    <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-3" for="newpin_confirmation">New PIN Confirmation</label>
                        <div class="col-xs-6">
                            <input class="form-control" type="password" id="newpin_confirmation" name="newpin_confirmation" maxlength="28" value="{{old('newpin_confirmation')}}">
                        </div>
                    </span>
                    <span class="form-group" style="border-spacing: 5px 6px;"><button class="btn btn-main col-xs-offset-5" type="submit" name="submit">Change PIN</button></span>
                </form>
            </div>
        </span>
    </div>
@stop