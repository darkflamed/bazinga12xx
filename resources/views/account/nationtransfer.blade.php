@extends('global.main')

@section('more-js')

@stop

@section('more-css')

@stop

@section('title')
    Nation Transfer
@stop

@section('description')

@stop

@section('keywords')

@stop

@section('content')
    <div class="container container-main">
        <span class="col-xs-12">
            <span class="col-xs-12" style="margin-bottom: 25px;">
                <h2>@lang('message.please_input_account_details', ['username' => Auth::user()->strAccountID])</h2>
            </span>
            @if (isset($errors) && count($errors) > 0)
                <div class="alert alert-danger col-xs-12">
                    @lang('message.problem_with_input')<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="col-xs-12">
                <font color="orange">Please select your desired option for NT:</font>
                <form class="form-horizontal" role="form" method="post" action="/account/nationtransfer">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <span class="form-group" style="border-spacing: 5px 6px;">250 KC&nbsp;&nbsp;&nbsp;<input class="col-xs-offset-4" type="radio" name="payment" value="kc" checked="">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Free (Every 7 days)&nbsp;&nbsp;&nbsp;<input type="radio" name="payment" value="free"></span>
                    <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-3" for="class">Secret PIN:</label>
                        <div class="col-sm-6 col-xs-8"><input class="form-control" style="font-size: 12px !important; padding: 5px 2px;color: #d3c2ae !important;" id="pin" name="pin">
                        </div></span>
                    <span class="form-group" style="border-spacing: 5px 6px;"><button class="btn btn-main col-xs-offset-4" type="submit" name="submit">Nation Transfer</button></span>
                </form>
            </div>
        </span>
    </div>
@stop