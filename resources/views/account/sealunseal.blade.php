@extends('global.main')

@section('javascript')
    <script>
        window.CharacterInfo = {!! $CharacterInfo !!};
        window.SealedItems = {!! $SealedItems !!}
        window.Items.Initialize();
        window.Items.getCharacterItems('{{ url('/') }}');
        window.AddSeal = [];
        window.RemoveSeal = [];

        $( document ).ready(function() {
            $('#SealForm').on('submit', function(e){
                e.preventDefault();
                let CharInfo = '';
                for(let key in window.CharacterInfo) {
                    if (!CharacterInfo.hasOwnProperty(key)) continue;
                    let char = window.CharacterInfo[key];

                    CharInfo += key;
                    CharInfo += ',';
                    CharInfo += char.last_updated;
                    CharInfo += '|';
                }

                axios.post('{{url('/account/seal_unseal')}}', {
                    add: window.AddSeal,
                    remove: window.RemoveSeal,
                    pin: $('#pin').val(),
                    last_updated: CharInfo
                })
                .then(function (response) {
                    if(response.data.error) {
                        $('#ErrorBox').show();
                        $('#ErrorBox > ul').html('<li>'+response.data.message+'</li>');
                        if(response.data.reload)
                            location.reload();
                    } else {
                        location.reload();
                    }
                    return false;
                })
                .catch(function (error) {
                    console.log(error);
                    return false;
                });
                return false;
            });
        });

    </script>
@stop

@section('css')

@stop

@section('title')
    Seal/Unseal
@stop

@section('content')
    <style>
        .item-sealed {
            background: rgba(137,58,54,0.4);
            width: 47px;
            height: 47px;
        }
    </style>
    <div id="CharacterContainer" class="container container-main">
        <span class="col-xs-12">
            <span class="col-xs-12" style="margin-bottom: 25px;">
                <h2>Seal/Unseal Items</h2>
            </span>
            <div class="row">
                <div id="CharacterInventoryData" class="col-xs-12"></div>
                <div class="col-xs-12" style="margin-top: 20px;">
                    <form id="SealForm" class="form-horizontal" role="form">
                    <div class="form-group">
                        <div id="ErrorBox" class="alert alert-danger col-xs-12" style="display:none;">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>

                            </ul>
                        </div>
                        <label for="pin" class="col-md-4 control-label">PIN</label>
                        <div class="col-md-4">
                            <input id="pin" type="password" class="form-control" name="pin" required>
                        </div>
                        <div class="col-xs-12" style="margin-top: 20px;">
                            <center><button class="btn btn-main">Save Changes</button></center>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </span>

    </div>
    <div id="ItemInfo"></div>
@stop