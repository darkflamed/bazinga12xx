@extends('global.main')

@section('javascript')

@stop

@section('css')

@stop

@section('title')
    Password Change
@stop

@section('content')
    <div class="container container-main">
        <span class="col-xs-12">
            <span class="col-xs-12" style="margin-bottom: 25px;">
                <h2>@lang('message.please_input_account_details', ['username' => Auth::user()->strAccountID])</h2>
            </span>
            @if (isset($errors) && count($errors) > 0)
                <div class="alert alert-danger col-xs-12">
                    @lang('message.problem_with_input')<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="col-xs-12">
                <form class="form-horizontal" role="form" method="post" action="{{url('account/changepassword')}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-3" for="password">@lang('fields.old_password')</label>
                        <div class="col-xs-6">
                            <input class="form-control" type="password" id="password" name="password" maxlength="28" value="{{old('password')}}">
                        </div>
                    </span>
                    <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-3" for="newpassword">@lang('fields.new_password')</label>
                        <div class="col-xs-6">
                            <input class="form-control" type="password" id="newpassword" name="newpassword" maxlength="28" value="{{old('newpassword')}}">
                        </div>
                    </span>
                    <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-3" for="newpassword_confirmation">@lang('fields.confirm_new_password')</label>
                        <div class="col-xs-6">
                            <input class="form-control" type="password" id="newpassword_confirmation" name="newpassword_confirmation" maxlength="28" value="{{old('newpassword_confirmation')}}">
                        </div>
                    </span>
                    <span class="form-group" style="border-spacing: 5px 2px;"><label class="col-xs-3" for="pin">@lang('fields.secret_pin')</label>
                        <div class="col-xs-6">
                            <input class="form-control" type="password" id="pin" name="pin" maxlength="9" value="{{old('pin')}}">
                        </div></span>
                    <span class="form-group" style="border-spacing: 5px 6px;"><button class="btn btn-main col-xs-offset-5" type="submit" name="submit">@lang('titles.change_password')</button></span>
                </form>
            </div>
        </span>
    </div>
@stop