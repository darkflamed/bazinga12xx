<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
    <title>BazingaKO PUS</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge; IE=11; IE=10;" />
    <meta http-equiv="MSThemeCompatible" content="Yes" />
    <link href="{{ asset(mix('css/pus.css')) }}" rel="stylesheet">
</head>
    <body style="background: #2f3222 url({{asset('images/pus/pus_background.jpg')}}) no-repeat left top; width: 982px; height: 702px; border:2px solid #2f3222;">
    <div class="main_area">
        <div class="border_shadow content_area">
            <div id="Content">
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1">
                        <div class="panel panel-powerup">
                            <div class="panel-heading">Login</div>
                            <div class="panel-body">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/pus/login') }}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="mgid" value="{{ is_null($request->mgid) ? old('mgid') : $request->mgid }}">
                                    <input type="hidden" name="strAccountID" value="{{ is_null($request->mgid) ? old('mgid') : $request->mgid }}">
                                    <input type="hidden" name="mgmc" value="{{ is_null($request->mgmc) ? old('mgmc') : $request->mgmc }}">
                                    <input type="hidden" name="param" value="{{ is_null($request->param) ? old('param') : $request->param }}">

                                    <div class="form-group{{ $errors->has('strAccountID') ? ' has-error' : '' }}">
                                        <div class="col-md-6">
                                            @if ($errors->has('strAccountID'))
                                                <span class="help-block">
                                                    <strong>{!!  $errors->first('strAccountID') !!}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" class="col-md-4 control-label">Password</label>

                                        <div class="col-md-6">
                                            <input id="password" type="password" class="form-control" name="password" required autofocus>

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-4">
                                            <button type="submit" class="btn btn-default">
                                                Login
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="rightnav no-margin"></div>
    </div>
    </body>
</html>