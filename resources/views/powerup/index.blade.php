@extends('powerup.main')

@section('content')
    <body style="background: #2f3222 url({{asset('images/pus/pus_background.jpg')}}) no-repeat left top; width: 982px; height: 702px; border:2px solid #2f3222;">

    @include('powerup.topnav')
    <div class="main_area">
        <div class="border_shadow content_area">
            <div id="Content">
                @include('powerup.content.main')
            </div>
        </div>

        <div class="rightnav no-margin">
            <div class="border_shadow kc_box_outer">
                <div class="kc_box_inner">
                    <div class="knight_cash_display">
                        Knight Cash: <span id="KnightCash">{{$KnightCash}}</span>
                        <hr>
                        Free Inventory Slots: <span id="FreeSlots">{{$FreeSlots}}</span>
                    </div>
                </div>
            </div>

            <a href="#"><img class="recharge_image" width="227" height="49" src="{{asset('images/pus/recharge.png')}}" alt="Recharge Knight Cash" onClick="window.Main.getRechargeForm()" /></a>

            <div class="bottom_button_group">
                <a class="bottom_button" href="#"><img width="54" height="79" src="{{asset('images/pus/button_my_shop.jpg')}}" alt="My Shop" /></a>
                <a class="bottom_button" href="#"><img width="54" height="79" src="{{asset('images/pus/button_suggested.jpg')}}" alt="Suggested" /></a>
                <a class="bottom_button" href="#"><img width="54" height="79" src="{{asset('images/pus/button_shopping_cart.jpg')}}" alt="Shopping Cart" onClick="window.Main.getCart()" /></a>
                <a class="bottom_button" href="#"><img width="54" height="79" src="{{asset('images/pus/button_cash_inventory.jpg')}}" alt="Cash Inventory" /></a>

                <a href="#"><img class="promotion_button" width="220" height="30" src="{{asset('images/pus/button_bundle.jpg')}}" alt="Bundle" /></a>
                <a href="#"><img class="promotion_button" width="220" height="30" src="{{asset('images/pus/button_on_sale.jpg')}}" alt="On Sale" /></a>
            </div>
        </div>
    </div>

    <div class="modal_storage" hidden>
        @include('powerup.popups.itemdetail')
        @include('powerup.popups.message')
    </div>
    </body>
@stop