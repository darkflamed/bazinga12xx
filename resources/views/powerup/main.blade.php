<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
@include('powerup.header')
@yield('content')

<script>
    window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            'oAuthToken' => $oAuth
        ]) !!};
</script>
@if(!Auth::guest())
<script src="//{{ Request::getHost() }}:6001/socket.io/socket.io.js"></script>
@endif
<script src="{{ asset(mix('/js/pus.js')) }}" ></script>
@if(!Auth::guest())
    <script>
        if(typeof window.Echo !== 'undefined') {
            window.Echo.private('knightcash.{{Auth::user()->id}}')
                .listen('KnightCashBroadcast', function (e) {
                    $('#KnightCash').html(e.KnightCash);
                });
        }
    </script>
@endif
@yield('more-js')

<script>
    $(document).ready(function() {
        window.Initialize.run();
    });
</script>
</html>