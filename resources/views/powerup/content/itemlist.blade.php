<div class="itemlist_main_content">
    <div class="itemlist_top_nav">
        <div class="dropdown form-group">
            <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">
                <div id="dropdown-type-text">
                    @if($ListCount == 10)
                        Default (10)
                    @else
                        Show {{$ListCount}}
                    @endif
                </div><span class="caret"></span></button>
            <ul class="dropdown-menu dropdown-listcount" aria-labelledby="Count">
                <li><a href="#" data-value="10">Default (10)</a></li>
                <li><a href="#" data-value="20">Show 20</a></li>
                <li><a href="#" data-value="30">Show 30</a></li>
            </ul>
        </div>

        <div class="float-right">
            <div class="itemlist_top_nav_buttons">
                <button class="btn btn-brown" onClick="window.Cart.itemlistBuyItems()">Buy</button>
                <button class="btn btn-brown" onClick="window.Cart.itemlistCartItems()">Cart</button>
                <button class="btn btn-green" onClick="window.Main.resetCheckBoxes()">Reset</button>
            </div>

            <div class="dropdown form-group">
                <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">
                    <div id="dropdown-type-text">Ordered By</div><span class="caret"></span></button>
                <ul class="dropdown-menu" aria-labelledby="OrderedBy">
                    <li><a href="#" data-value="1">Ordered By</a></li>
                    <li><a href="#" data-value="1">Item Name</a></li>
                    <li><a href="#" data-value="1">Popularity</a></li>
                    <li><a href="#" data-value="1">Price (Ascending)</a></li>
                    <li><a href="#" data-value="1">Price (Descending)</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="itemlist_listed_content">
        <div id="SearchPage" class="hidden">{{$SearchPage}}</div>
        <div id="style-1" class="scrollbar content-scrollbar">
            <div class="force-overflow">
            <table id="ListedItemsTable">
                <thead>
                    <tr>
                        <th></th>
                        <th>Item</th>
                        <th>Name</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($StoreItems as $item)
                <tr>
                    <td><input class="ListedItem" value="{{$item->num}}" type="checkbox"></td>
                    <td onClick="Main.getItemDetail({{$item->num}})"><img src="{{asset('images/itemicons/'.Template::getIconID($item->icon_id))}}"></td>
                    <td onClick="Main.getItemDetail({{$item->num}})">{{$item->name}}</td>
                    <td onClick="Main.getItemDetail({{$item->num}})">{{$item->price}} KC</td>
                </tr>
                @endforeach
                </tbody>
            </table>
            </div>
        </div>

        <div class="itemlist_page_buttons">
            <center>

                <button class="btn btn-green" onClick="
                        @if($SearchPage == 'true')
                            window.Main.getStoreSearch('{{$SearchText}}',
                        @else
                            window.Main.getStoreCategory({{$Category}},
                        @endif
                {{$Page-1 <= 0 ? 1 : $Page-1}})">&#60; Prev</button>

                @for($i=0; $i<$TotalPages; $i++)
                    <a href="#" onClick="
                        @if($SearchPage == 'true')
                            window.Main.getStoreSearch('{{$SearchText}}',
                        @else
                            window.Main.getStoreCategory({{$Category}},
                        @endif
                    {{$i+1}})"
                    @if($Page == ($i+1))
                        class="current"
                    @endif
                    >{{$i+1}}</a>
                @endfor

                <button class="btn btn-green" onClick="
                    @if($SearchPage == 'true')
                        window.Main.getStoreSearch('{{$SearchText}}',
                    @else
                        window.Main.getStoreCategory({{$Category}},
                    @endif
                {{$Page+1 > $TotalPages ? $TotalPages : $Page+1}})">Next &#62;</button>

            </center>
        </div>
    </div>
</div>