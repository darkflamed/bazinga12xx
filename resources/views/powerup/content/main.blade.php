<div id="style-1" class="scrollbar content-scrollbar">
<div class="content_main force-overflow">

    <div class="content_banner border_shadow">
        <img width="437" height="127" src="{{asset('images/pus/banner.png')}}" alt="Cash Inventory" />
    </div>

    <div class="weekly_hot_items border_shadow">
        <div style="width: 190px; height: 125px;">
            <span><strong>Weekly Hot Items</strong><button class="btn btn-green float-right">Sort</button></span>
            <hr class="weekly_hot_hr">
            <ul class="weekly_hot_list">
                @foreach($New as $key => $nw)
                    @if($key < 5)
                        <li><span>{{$key+1}}</span>{{$nw->name}}</li>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>

    <br>

    <hr class="border_shadow main_hr">

    <div class="display_box">
        <span>New Items</span>
        <div class="display_box_inner_contenet">
            <div class="display_box_top">
                <div class="display_box_top_icon">
                    <img class="display_box_corner_icon" src="{{asset('images/pus/icon_new_64.gif')}}">
                    <div class="display_box_icon_background">
                        <img class="display_box_icon" src="{{asset('images/itemicons/'.Template::getIconID($New[0]->icon_id))}}">
                    </div>
                </div>
                <table class="display_box_top_info">
                    <tr>
                        <td class="display_box_top_name">{{$New[0]->name}}</td><td class="display_box_top_price">{{$New[0]->price}} KC</td>
                    </tr>
                    <tr class="display_box_top_description">
                        <td colspan="2">{{$New[0]->desc_1}}</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button class="align_bottom btn btn-brown-small" type="button" onClick="window.Main.getItemDetail({{$New[0]->num}})">Buy</button>
                            <button class="align_bottom btn btn-green-small" type="button" onClick="window.Cart.addToCart({{$New[0]->num}}, 1)">Cart</button>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="display_box_bottom">
                @foreach($New as $key => $nw)
                    @if($key != 0 && $key < 4)
                <div class="display_box_bottom_content border_shadow">
                    <img src="{{asset('images/itemicons/'.Template::getIconID($nw->icon_id))}}">
                    <span>{{$nw->name}}</span>
                    <div class="price">{{$nw->price}} KC</div>
                    <div>
                        <button class="align_bottom btn btn-brown-small" type="button" onClick="window.Main.getItemDetail({{$nw->num}})">Buy</button>
                        <button class="align_bottom btn btn-green-small" type="button" onClick="window.Cart.addToCart({{$nw->num}}, 1)">Cart</button>
                    </div>
                </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>

    <div class="display_box">
        <span>Recommended Items</span>
        <div class="display_box_inner_contenet">
            <div class="display_box_top">
                <div class="display_box_top_icon">
                    <img class="display_box_corner_icon" src="{{asset('images/pus/icon_best_64.gif')}}">
                    <div class="display_box_icon_background">
                        <img class="display_box_icon" src="{{asset('images/itemicons/'.Template::getIconID($Recommended[0]->icon_id))}}">
                    </div>
                </div>
                <table class="display_box_top_info">
                    <tr>
                        <td class="display_box_top_name">{{$Recommended[0]->name}}</td><td class="display_box_top_price">{{$Recommended[0]->price}} KC</td>
                    </tr>
                    <tr class="display_box_top_description">
                        <td colspan="2">{{$Recommended[0]->desc_1}}</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button class="align_bottom btn btn-brown-small" type="button" onClick="window.Main.getItemDetail({{$Recommended[0]->num}})">Buy</button>
                            <button class="align_bottom btn btn-green-small" type="button" onClick="window.Cart.addToCart({{$Recommended[0]->num}}, 1)">Cart</button>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="display_box_bottom">
                @foreach($Recommended as $key => $nw)
                    @if($key != 0 && $key < 4)
                        <div class="display_box_bottom_content border_shadow">
                            <img src="{{asset('images/itemicons/'.Template::getIconID($nw->icon_id))}}">
                            <span>{{$nw->name}}</span>
                            <div class="price">{{$nw->price}} KC</div>
                            <div>
                                <button class="align_bottom btn btn-brown-small" type="button" onClick="window.Main.getItemDetail({{$nw->num}})">Buy</button>
                                <button class="align_bottom btn btn-green-small" type="button" onClick="window.Cart.addToCart({{$nw->num}}, 1)">Cart</button>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>
</div>