<table class="popup_item_detail_content">
    <tr>
        <td><img src="{{asset('images/itemicons/'.Template::getIconID($Item->icon_id))}}"></td>
        <td>
            <div class="popup_item_detail_info_top">
                <font color="grey">Item:</font> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$Item->name}}
            </div>
            <hr>
            <div class="popup_item_detail_info_top">
                <font color="grey">Quantity:</font>
                <div id="BuyItemQuantity" class="hidden">1</div>
                <div class="dropdown form-group">
                    <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">
                        <div id="dropdown-type-text">1</div><span class="caret"></span></button>
                    <ul class="dropdown-menu dropdown-item-detail-quantity" aria-labelledby="Quantity">
                        @for($i=0; $i<10;$i++)
                        <li><a href="#" data-value="{{$i+1}}">{{$i+1}}</a></li>
                        @endfor
                    </ul>
                </div>
            </div>
            <hr>
            <div class="popup_item_detail_info_top">
                <font color="grey">Selling Price:</font> <span class="price">{{$Item->price}} KC</span>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div>
                <hr>
                <div class="popup_item_detail_description">
                    <div><strong>Item Description</strong></div>
                    <p>
                        {{$Item->desc_1}}
                    </p>
                    <div><strong>How to Use Item</strong></div>
                    <p>
                        {{$Item->desc_2}}
                    </p>
                </div>
                <div>
                    @if($Item->expire_days > 0)
                        <font color="#006400">This item will expire in {{$Item->expire_days}} days</font>
                    @endif
                </div>
            </div>
            <hr>
            <div class="popup_item_detail_buttons">
                <button class="align_bottom btn btn-brown-small" onClick="window.Cart.purchaseBuyItem({{$Item->num}})">Buy</button>
                <button class="align_bottom btn btn-green-small" onClick="window.Dialog.closeDialog('dialog-item-detail'); window.Cart.addToCart({{$Item->num}}, $('#BuyItemQuantity').html() );">Cart</button>
                <button class="align_bottom btn btn-green" onClick="window.Dialog.closeDialog('dialog-item-detail')">Cancel</button>
            </div>
        </td>
    </tr>
</table>