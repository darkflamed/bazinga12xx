<div class="cart_main">
    <div id="style-1" class="scrollbar content-scrollbar">
        <div class="content_main force-overflow">
            <div class="cart_top_nav">
                <font color="yellow">Shopping Cart</font> Shopping cart can hold maximum 50. (Store for a week)
                <div class="float-right">
                    <button class="btn btn-brown">Uncheck</button>
                    <button class="btn btn-brown" onClick="window.Cart.cartDeleteItems()">Delete Item</button>
                </div>
            </div>

            <div class="cart_listed_items">
                <table>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Item</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($Cart as $item)
                        <tr>
                            <td><input class="CartItem" value="{{$item->id}},{{$item->quantity}}" type="checkbox"></td>
                            <td><img src="{{asset('images/itemicons/'.$item->attributes->icon)}}"></td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->price}} KC</td>
                            <td>
                                <div>{{$item->quantity}} Quantity</div>
                                <div>
                                    <select id="qty-select-{{ $item->id }}" class="cart_select_menu">
                                        @for($i=0; $i<10;$i++)
                                            <option value="{{$i+1}}"
                                            @if($i+1 == $item->quantity)
                                                selected="selected"
                                            @endif
                                            >{{$i+1}}</option>
                                        @endfor
                                    </select>
                                    <button class="btn btn-brown-small" onClick="window.Cart.cartModifyItem({{$item->id}}, $('#qty-select-{{ $item->id }}').val())">Modify</button></div>
                            </td>
                            <td>{{$item->price * $item->quantity}} KC</td>
                        </tr>
                    @endforeach
                        <tr class="cart_purchase_total">
                            <td colspan="5"><div class="float-right">Total purchasing amount &nbsp;: &nbsp;</div></td>
                            <td>{{$CartTotal}} KC</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <center><div class="cart_bottom_nav">
                <button class="btn btn-brown">Back</button>
                <button class="btn btn-brown" onClick="window.Cart.cartBuyItems()">Buy</button>
            </div></center>
        </div>
    </div>
</div>