
<div id="Content">
    <div class="col-xs-12">
        <div class="panel panel-powerup">
            <div class="panel-heading"></div>
            <div class="panel-body" style="font-size: 1.5em; overflow-y: scroll; height: 540px;">
                <p></p>
                <p>Step 1: Go to URL: https://knightonline-myko.com</p>
                <img src="{{ asset('images/pus/recharge_step_1.png') }}"><br><br>
                <p>Step 2: Login to your User Panel area.</p>
                <img src="{{ asset('images/pus/recharge_step_2.jpg') }}"><br><br>
                <p>Step 3: Click on "Buy Knight Cash" TAB.</p>
                <img src="{{ asset('images/pus/recharge_step_3.jpg') }}"><br><br>
                <p>Step 4: Select your KC option, payment method & complete payment</p></p>
                <img src="{{ asset('images/pus/recharge_step_4.jpg') }}"><br><br>
                <p><font color="green">Finish: You will receive your Knight Cash instantly after successful payment.</font></p></font>
                <p></p>
            </div>
        </div>
    </div>
</div>