
<div id="Content">
    <div class="col-xs-12">
        <div class="panel panel-powerup">
            <div class="panel-heading"><img class="img-responsive center-block" src="{{ asset('images/pus/button_select_language.png') }}"></div>
            <div class="panel-body">
                <div class="col-xs-12">
                    <a href="#" onClick="window.Main.getEnglishRecharge()"><img class="img-responsive center-block" src="{{ asset('images/pus/button_english.png') }}"></a>
                </div>
                <div class="col-xs-12">
                    <a href="#" onClick="window.Main.getSpanishRecharge()"><img class="img-responsive center-block" src="{{ asset('images/pus/button_spanish.png') }}"></a>
                </div>
                <div class="col-xs-12">
                    <a href="#" onClick="window.Main.getTurkishRecharge()"><img class="img-responsive center-block" src="{{ asset('images/pus/button_turkish.png') }}"></a>
                </div>
            </div>
        </div>
    </div>
</div>