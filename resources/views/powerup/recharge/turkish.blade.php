
<div id="Content">
    <div class="col-xs-12">
        <div class="panel panel-powerup">
            <div class="panel-heading"></div>
            <div class="panel-body" style="font-size: 1.5em; overflow-y: scroll; height: 540px;">
                <p></p>
                <p>Adım 1: Açık URL: https://knightonline-myko.com</p>
                <img src="{{ asset('images/pus/recharge_step_1.png') }}"><br><br>
                <p>Adım 2: Bazinga üyeliğinizle panelde giriş yapın ve User Panel'de BUY Knight Cash linkine tıklayın.</p>
                <img src="{{ asset('images/pus/recharge_step_2.jpg') }}"><br><br>
                <p>Adım 3: Terhic edilen KC miktarını seçip aşağıda resimde gösterilen PAYGOL ikonuna tıklayın.</p>
                <img src="{{ asset('images/pus/recharge_step_3.jpg') }}"><br><br>
                <p>Adım 4:PayGol sitesi açıldığında aşağıdaki ödeme yöntemlerinden biri ile KC satın alabilirsiniz. <br>Bizim tavsiye ettiğimiz yöntem Credit Card.<br></p></p>
                <img src="{{ asset('images/pus/recharge_step_4.jpg') }}"><br><br>
                <p><font color="green">Ödeme yaptıktan sonra KC otomatik olarak saniyeler içinde hesabınıza yüklenir.</font></p></font>
                <p></p>
            </div>
        </div>
    </div>
</div>