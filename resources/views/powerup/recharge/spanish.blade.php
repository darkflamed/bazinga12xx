
<div id="Content">
    <div class="col-xs-12">
        <div class="panel panel-powerup">
            <div class="panel-heading"></div>
            <div class="panel-body" style="font-size: 1.5em; overflow-y: scroll; height: 540px;">
                <p></p>
                <p>Paso 1: Anda al URL: https://knightonline-myko.com</p>
                <img src="{{ asset('images/pus/recharge_step_1.png') }}"><br><br>
                <p>Paso 2: Logea a tu Panel de Usuario.</p>
                <img src="{{ asset('images/pus/recharge_step_2.jpg') }}"><br><br>
                <p>Paso 3: Click en "Buy Knight Cash"</p>
                <img src="{{ asset('images/pus/recharge_step_3.jpg') }}"><br><br>
                <p>Paso 4: Selecciona tu opción de KC, método de pago y completa el pago.</p></p>
                <img src="{{ asset('images/pus/recharge_step_4.jpg') }}"><br><br>
                <p><font color="green">Completo: Usted recibirá su Knight Cash instantáneamente después del pago.</font></p></font>
                <p></p>
            </div>
        </div>
    </div>
</div>