<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
    @include('powerup.header')

    <body style="background: #2f3222 url({{asset('images/pus/pus_background.jpg')}}) no-repeat left top; width: 982px; height: 702px; border:2px solid #2f3222;">
    <div style="width: 982px; height: 702px; background-color: rgba(15, 15, 15, 0.8); margin-top: -150px; margin-left: 0px;">
        <div class="container" style="margin-top: 150px;">
            <div class="col-xs-12" style="color: #7a0d08; font-size: 4.5em; margin-top: 200px;">
                <img style="margin-left: 180px;" src="{{ asset('images/pus/reopen.png') }}">
            </div>
        </div>
    </div>
    </body>
</html>