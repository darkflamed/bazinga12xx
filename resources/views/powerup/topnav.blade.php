<div class="nav-top">
    <div class="topnav_container">

        <div class="nav_search border_shadow">
            <form id="ItemSearchForm" class="itemsearch" action="{{'pus/search'}}" method="post">

                <div class="dropdown form-group">
                    <button class="btn btn-secondary dropdown-toggle" name="button" type="button" data-toggle="dropdown" value="0">
                        <div id="dropdown-type-text">All</div><span class="caret"></span></button>
                    <ul class="dropdown-menu dropdown-searchcategory" aria-labelledby="ItemType">
                        <li><a href="#" data-value="0">All</a></li>
                        <li><a href="#" data-value="1">Buffs</a></li>
                        <li><a href="#" data-value="2">Attributes</a></li>
                        <li><a href="#" data-value="3">Special</a></li>
                        <li><a href="#" data-value="4">Premium</a></li>
                        <li><a href="#" data-value="5">Knight Cash</a></li>
                    </ul>
                </div>

                <label><input class="radio_button" type="radio" name="namedesc" checked value="0">Name</label>
                <label><input class="radio_button" type="radio" name="namedesc" value="1">Description</label>

                <input id="ItemInput" type="text" class="form-control" name="searchText">

                <button class="btn btn-secondary search_button" onClick="window.Main.SearchPowerupStore()" type="button">Search</button>
            </form>
        </div>


        <div class="nav_categories border_shadow">
            <ul class="categories_list">
                <li><a href="#" onClick="window.Main.getStoreCategory(1)">Buffs</a></li>
                <li><a href="#" onClick="window.Main.getStoreCategory(2)">Attributes</a></li>
                <li><a href="#" onClick="window.Main.getStoreCategory(5)">Special</a></li>
                <li><a href="#" onClick="window.Main.getStoreCategory(3)">Premium</a></li>
                <li><a href="#" onClick="window.Main.getStoreCategory(4)">Knight Cash</a></li>
            </ul>
        </div>

    </div>
    <img class="logo_image" src="{{asset('images/pus/nav_logo.png')}}" width="213" height="79" alt="Powerup Store" onClick="window.Main.getHome()" />
</div>