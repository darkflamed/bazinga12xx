@extends('global.main')

@section('javascript')

@stop

@section('css')

@stop

@section('title')
    Terms and Conditions
@stop

@section('content')

    <div class="container container-main">
        <table class="table">
            <thead>
            <tr><td><h4>TERMS OF USE</h4></td></tr>
            </thead>
            <tbody>
            <tr>
                <td class="row">
                    <div class="well well-main">
                        <p>
                            By accessing or using http://knightonline-myko.com (the "Site") and affiliated services (the "Services") that belongs to KO-MyKO, you (the "User") agree to comply with the terms and conditions governing the User's use of any areas of the Site and affiliated services as set forth below.
                        </p>
                        <h3>PURCHASES</h3>
                        <p>
                            Purchases on our KO-MyKO minecraft server are completely voluntary. Before you purchase anything please
                            note that you are not allowed to, and can not charge back as your payment will already be used to
                            support server project. If you attempt to open dispute on paypal your gaming account will be
                            suspended. In your attempt of trying to dispute transaction this section of user agreements will
                            be shown to paypal review specialists along with other articles of user agreements. If you already
                            purchased something and your account has been suspended, reason for your account suspension is violating game
                            rules and (or) user agreements. Please note that if you have purchased something, you have no immunity and
                            you must obey ingame rules and user agreements. KO-MyKO Server vouch that your account will not in any
                            case be suspended falsly or with harmful or fraud intentions. If your account has been suspended
                            it's been justified.
                        </p>
                        <h3>IN-GAME ACCOUNT</h3>
                        <p>
                            You may register a regular account and password for the service for free. You are responsible for all activity under your account, associated accounts, and passwords. The Site is NOT responsible for unauthorised access to your account, and any loss of virtual items associated with it.
                        </p>
                        <h3>ACCESS TO THE SITE AND THE SERVICES</h3>
                        <p>
                            KO-MyKO provides free and unlimited access to the Site and the Services.
                        </p>
                        <h3>SUBMISSION</h3>
                        <p>
                            KO-MyKO does not assume any obligation with respect to any Submission and no confidential or fiduciary understanding or relationship is established by the Site's receipt or acceptance of any submission. All submissions become the exclusive property of the Site and its affiliates. The Site and its affiliates may use any submission without restriction and the User shall not be entitled to any compensation.
                        </p>
                        <h3>USE OF SITE</h3>
                        <p>
                            This Site or any portion of the Site as well as the Services may not be reproduced, duplicated, copied, sold, resold, or otherwise exploited for any commercial purpose except as expressly permitted by KO-MyKO. KO-MyKO reserves the right to refuse service in its discretion, without limitation, if KO-MyKO believes that User conduct violates applicable law or is harmful to the interests of KO-MyKO, other users of the Site and the Services or its affiliates.
                        </p>
                        <h3>Third-Party Content</h3>
                        <p>
                            Neither KO-MyKO, nor its affiliates, nor any of their respective officers, directors, employees, or agents, nor any third party, including any Provider/Affiliate, or any other User of the Site and Services, guarantees the accuracy, completeness, or usefulness of any content, nor its merchantability or fitness for any particular purpose. In some instances, the content available through the Site may represent the opinions and judgments of Providers/Affiliates or Users. KO-MyKO and its affiliates do not endorse and shall not be responsible for the accuracy or reliability of any opinion, advice, or statement made on the Site and the Services by anyone other than authorised KO-MyKO employees. Under no circumstances shall KO-MyKO, or its affiliates, or any of their respective officers, directors, employees, or agents be liable for any loss, damage or harm caused by a User's reliance on information obtained through the Site and the Services. It is the responsibility of the User to evaluate the information, opinion, advice, or other Content available through this Site.
                        </p>
                        <h3>Disclaimers and Limitation of Liability</h3>
                        <p>
                            User agrees that use of the Site and the Services is at the User's sole risk. Neither KO-MyKO, nor its affiliates, nor any of their respective officers, directors, employees, agents, third-party content providers, merchants, sponsors, licensors or the like (collectively, "Providers"), warrant that the Site and the Services will be uninterrupted or error-free; nor do they make any warranty as to the results that may be obtained from the use of the Site and the Services, or as to the accuracy, reliability, or currency of any information content, service, or merchandise provided through this Site. THE SITE AND THE SERVICES ARE PROVIDED BY KO-MyKO ON AN "AS IS" AND "AS AVAILABLE" BASIS. THE SITE MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED, AS TO THE OPERATION OF THE SITE, THE INFORMATION, CONTENT, MATERIALS OR PRODUCTS, INCLUDED ON THIS SITE. TO THE FULL EXTENT PERMISSIBLE BY APPLICABLE LAW, THE SITE DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. KO-MyKO W ILL NOT BE LIABLE FOR ANY DAMAGES OF ANY KIND ARISING FROM THE USE OF THE SITE AND THE SERVICES, INCLUDING BUT NOT LIMITED TO DIRECT, INDIRECT, INCIDENTAL, PUNITIVE AND CONSEQUENTIAL DAMAGES. Under no circumstances shall KO-MyKO or any other party involved in creating, producing, or distributing the Site and the Services be liable for any direct, indirect, incidental, special, or consequential damages that result from the use of or inability to use the Site and the Services, including but not limited to reliance by the User on any information obtained from the Site or that result from mistakes, omissions, interruptions, deletion of files or email, errors, defects, viruses, delays in operation or transmission, or any failure of performance, whether or not resulting from acts of God, communications failure, theft, destruction, or unauthorised access to the Site's records, programs, or services. The user hereby acknowledges that these disclaimers and limitation on liability shall apply to all content, merchandise, and services available through the Site and the Services. In states that do not allow the exclusion of limitation or limitation of liability for consequential or incidental damages, the User agrees that liability in such states shall be limited to the fullest extent permitted by applicable law.
                        </p>
                        <h3>TERMINATION OF SERVICE</h3>
                        <p>
                            KO-MyKO reserves the right, in its sole discretion, to change, suspend, limit, or discontinue any aspect of the Service and the Services at any time. KO-MyKO may suspend or terminate any User's access to all or part of the Site and the Services, without notice, for any conduct that KO-MyKO, in its sole discretion, believes is in violation of these terms and conditions.
                        </p>

                        <h3>ACKNOWLEDGEMENT</h3>
                        <p>
                            By accessing or using the Site and the Services, THE USER AGREES TO BE BOUND BY THESE TERMS AND CONDITIONS, INCLUDING DISCLAIMERS. KO-MyKO reserves the right to make changes to the Site and these terms and conditions, including disclaimers, at any time. IF YOU DO NOT AGREE TO THE PROVISIONS OF THIS AGREEMENT OR ARE NOT SATISFIED WITH THE SERVICE, YOUR SOLE AND EXCLUSIVE REMEDY IS TO DISCONTINUE YOUR USE OF THE SERVICE.
                        </p>
                        <h3>PRIVACY STATEMENT</h3>
                        <p>
                            Certain user information collected through the use of this website is automatically stored for reference. We track such information to perform internal research on our users' demographic interests and behaviour and to better understand, protect and serve our community of users. Payment or any other financial information is NEVER submitted, disclosed or stored on the Site and is bound to Terms and Conditions and Privacy Policy of our respective partners and/or payment processors. Basic user information (such as IP address, logs for using website interface and account management) may be disclosed to our partners in mutual efforts to counter potential illegal activities. KO-MyKO makes significant effort to protect..
                        </p>
                        <h3>FINISH</h3>
                        <p>
                            If you acknowledge and agree to our ToS we kindly welcome you to our community :)
                        </p>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

@stop