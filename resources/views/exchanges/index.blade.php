@extends('global.main')

@section('javascript')
    <script>
        window.Items.Initialize();
    </script>
@stop

@section('css')

@stop

@section('title')

@stop

@section('content')
    <article id="ExchangeContainer" class="container container-main">
        <center><div id="title"><h1>EXCHANGE LIST<p></p><span></span></h1></div></center>
        <div class="col-xs-12">
            <div class="input-group col-xs-6" style="margin-bottom: 4px;">
                <input id="Search" type="text" class="form-control input-sm" onkeyup="window.Items.searchTable('ExchangeTable')" placeholder="Search..." style="background-color: rgba(25, 25, 25, 0.31); color: #e7dbc3" />
            </div>
        </div>
        <div class="col-xs-6" style="height: 500px; overflow-y: scroll;">
            <table id="ExchangeTable" class="table table-striped col-xs-12">
                <tbody>
                <?php $npcs = array();?>
                @foreach($Exchanges as $index => $exc)
                    @if(!in_array($exc->nNpcNum . '|' . $exc->nOriginItemNum1, $npcs))
                        <?php array_push($npcs, $exc->nNpcNum . '|' . $exc->nOriginItemNum1)?>
                        <tr onmousedown="window.Items.getExchangeItems('{{ url('/') }}', {{ $exc->nNpcNum }}, {{ $exc->nOriginItemNum1 }})" style="cursor: pointer;">
                            <td align="center" style="color: #e7dbc3">{{ $exc->strNpcName }}</td>
                            <td align="center" style="text-align: center; color: #a6e687; font-size: 1em;">{{ $exc->nOriginItemNum1 === 0 || is_null($exc->strName) ? $exc->strNote : $exc->strName }}</td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-xs-6">
            <center><h1>EXCHANGE ITEMS<p></p><span></span></h1></center>
            <div id="Items" class="col-xs-12"></div>
        </div>
    </article>
    <div id="ItemInfo"></div>
@stop