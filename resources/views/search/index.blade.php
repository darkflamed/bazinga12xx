@extends('global.main')

@section('javascript')
    <script type="text/javascript">
        $(function() {
            $( "#test" ).autocomplete({
                source: "/json_search",
                method: "POST",
                minLength: 2,
                select: function(event, ui) {
                    selected = true;
                    window.location = "/character/" + ui.item.value;
                }
            });


            $.widget( "custom.catcomplete", $.ui.autocomplete, {
                _create: function() {
                    this._super();
                    this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
                },
                _renderMenu: function( ul, items ) {
                    var that = this,
                        currentCategory = "";
                    $.each( items, function( index, item ) {
                        var li;
                        if ( item.category != currentCategory ) {
                            ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
                            currentCategory = item.category;
                        }
                        li = that._renderItemData( ul, item );
                        if ( item.category ) {
                            li.attr( "aria-label", item.category + " : " + item.label );
                        }
                    });
                }
            });

            $( "#searchbox" ).catcomplete({
                delay: 0,
                source: "/json_search",
                method: "POST",
                minLength: 2,
                select: function(event, ui) {
                    selected = true;
                    window.location = "/"+ ui.item.category.toLowerCase() +"/" + ui.item.value;
                }
            });
        });

        function searchChar()
        {
            window.location = "/character/" + $("#tags").val(ui.item.value);
        }
    </script>
@stop

@section('css')

@stop

@section('title')

@stop

@section('content')


    <div class="container container-main">
        <div class="row">
            <div class="col-md-12">
                <h2>Search for User or Clan</h2>
                <div id="custom-search-input">
                    <div class="input-group col-md-12">
                        <input id="searchbox" type="text" class="form-control input-lg" placeholder="Search..." />
                        <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="button">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop