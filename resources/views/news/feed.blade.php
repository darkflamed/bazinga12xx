@extends('global.main')

@section('javascript')

@stop

@section('css')

@stop

@section('title')
    News
@stop

@section('content')
<div class="container container-main">
    <table class="table">
        <thead>
        <tr><td><h4>NEWS FEED</h4></td></tr>
        </thead>
        <tbody>
            <tr valign="middle">
                <td>
                    @foreach($NewsArticles as $Article)
                        <a href="{{ url('news/'.strtolower(str_replace(' ', '-', $Article->title))) }}">
                        <div class="well well-news row">
                            <div class="col-xs-4">
                                <img src="{{asset($Article->image)}}" class="img-responsive" alt="article_image" />
                            </div>

                            <div class="col-xs-8">
                            <span class="col-xs-12 well-news-title">
                                <table><tr>
                                    <td width="100%">{{$Article->title}}</td>
                                    <td class="well-news-title-date">{{Carbon\Carbon::parse($Article->created_at)->format('M d, Y')}}</td>
                                </tr></table>
                            </span>
                                <span class="col-xs-12 well-news-text"><p>{{$Article->short_text}}</p></span>
                            </div>
                        </div>
                        </a>
                    @endforeach
                </td>
            </tr>
        </tbody>
    </table>
</div>
@stop