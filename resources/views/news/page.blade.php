@extends('global.main')

@section('javascript')

@stop

@section('css')

@stop

@section('title')

@stop

@section('content')

<div class="container container-main">
    <table class="table">
        <thead>
        <tr><td><h4>{{ $News->title }}</h4></td></tr>
        </thead>
        <tbody>
        <tr valign="middle">
            <td>
                <div class="well well-news row">
                    <div class="col-xs-12">
                        <span class="col-xs-12 well-news-title">
                            <table><tr>
                                <td width="100%"></td>
                                <td class="well-news-title-date">{{Carbon\Carbon::parse($News->created_at)->format('M d, Y')}}</td>
                            </tr></table>
                        </span>

                        <span class="col-xs-12 well-news-text">
                            <div class="col-xs-4 thumbnail" style="margin: 15px;"><img src="{{asset($News->image)}}" class="img-responsive" alt="article_image" /></div>
                            <p>{{$News->long_text}}</p>
                        </span>
                    </div>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>

@stop