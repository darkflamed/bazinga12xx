@extends('global.main')

@section('javascript')

@stop

@section('css')

@stop

@section('title')

@stop

@section('content')

<div class="container container-main">
    <div class="col-xs-12">
        <div id="RankingsTabs">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#ALLTIME" data-toggle="tab">ALL TIME</a>
                </li>
                <li>
                    <a href="#MONTHLY" data-toggle="tab">MONTHLY</a>
                </li>
                <li>
                    <a href="#CLAN" data-toggle="tab">CLAN</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="ALLTIME">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#ALLTIMEKARUS" data-toggle="tab">KARUS</a>
                        </li>
                        <li>
                            <a href="#ALLTIMEELMORAD" data-toggle="tab">ELMORAD</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="ALLTIMEKARUS">
                            <div id="UserRankings" class="col-xs-12">
                                <table class="table table-striped">
                                    <thead>
                                    <tr><td colspan="4">User Rankings</td></tr>
                                    <tr><td width="25px"></td><td width="25px"></td><td colspan="2"></td></tr>
                                    </thead>
                                    <tbody>
                                    <tr valign="middle"></tr>
                                    <?php
                                    if($KarusUserRankings->count() < 100) {
                                        $k = 100 - $KarusUserRankings->count();
                                        for($i = 0; $i < $k; $i++) {
                                            $Character = New App\Character;
                                            $Character->Nation = 1;
                                            $Character->strUserID = '';
                                            $Character->LoyaltyMonthly = 0;

                                            $KarusUserRankings->push($Character);
                                        }
                                    }
                                    ?>
                                    @foreach($KarusUserRankings as $index => $ranking)
                                        <?php $ranknum = $index+1;
                                            if($ranknum == 1) $rank = 1;
                                            elseif($ranknum > 1 && $ranknum <= 4) $rank =  2;
                                            elseif($ranknum > 3 && $ranknum <= 9) $rank =  3;
                                            elseif($ranknum > 9 && $ranknum <= 25) $rank =  4;
                                            elseif($ranknum > 25 && $ranknum <= 50) $rank =  5;
                                            elseif($ranknum > 39) $rank =  6;
                                            ?>

                                        <tr align="right" valign="middle">
                                            <td align="center" id="sRankPos">{{$index+1}}&nbsp;</td>

                                            <td id="sRankPos"><img src="{{ asset('images/symbol/'.$ranking->Nation.'.gif')}}"></td>

                                            <td align="left" id="sRankName"><img src="{{asset('/images/symbol/k00'.$rank.'.gif')}}">
                                                <a href="{{url('character/'.$ranking->strUserId)}}" style="color:#988e80;">{{$ranking->strUserId}}</a></td>
                                            <td align="left" id="sRankLoyalty">{{number_format($ranking->Loyalty)}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="ALLTIMEELMORAD">
                            <div id="UserRankings" class="col-xs-12">
                                <table class="table table-striped">
                                    <thead>
                                    <tr><td colspan="4">User Rankings</td></tr>
                                    <tr><td width="25px"></td><td width="25px"></td><td colspan="2"></td></tr>
                                    </thead>
                                    <tbody>
                                    <tr valign="middle"></tr>
                                    <?php
                                    if($ElmoradUserRankings->count() < 100) {
                                        $k = 100 - $ElmoradUserRankings->count();
                                        for($i = 0; $i < $k; $i++) {
                                            $Character = New App\Character;
                                            $Character->Nation = 2;
                                            $Character->strUserID = '';
                                            $Character->LoyaltyMonthly = 0;

                                            $ElmoradUserRankings->push($Character);
                                        }
                                    }
                                    ?>
                                    @foreach($ElmoradUserRankings as $index => $ranking)
                                        <?php $ranknum = $index+1;
                                        if($ranknum == 1) $rank = 1;
                                        elseif($ranknum > 1 && $ranknum <= 4) $rank =  2;
                                        elseif($ranknum > 3 && $ranknum <= 9) $rank =  3;
                                        elseif($ranknum > 9 && $ranknum <= 25) $rank =  4;
                                        elseif($ranknum > 25 && $ranknum <= 50) $rank =  5;
                                        elseif($ranknum > 39) $rank =  6;
                                        ?>

                                        <tr align="right" valign="middle">
                                            <td align="center" id="sRankPos">{{$index+1}}&nbsp;</td>

                                            <td id="sRankPos"><img src="{{ asset('images/symbol/'.$ranking->Nation.'.gif')}}"></td>

                                            <td align="left" id="sRankName"><img src="{{asset('/images/symbol/k00'.$rank.'.gif')}}">
                                                <a href="{{url('character/'.$ranking->strUserId)}}" style="color:#988e80;">{{$ranking->strUserId}}</a></td>
                                            <td align="left" id="sRankLoyalty">{{number_format($ranking->Loyalty)}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="MONTHLY">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#MONTHLYKARUS" data-toggle="tab">KARUS</a>
                        </li>
                        <li>
                            <a href="#MONTHLYELMORAD" data-toggle="tab">ELMORAD</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="MONTHLYKARUS" class="tab-pane active col-xs-12">
                            <table class="table table-striped">
                                <thead>
                                <tr><td colspan="4">Monthly Rankings</td></tr>
                                <tr><td width="25px"></td><td width="25px"></td><td colspan="2"></td></tr>
                                </thead>
                                <tbody>
                                <tr valign="middle"></tr>
                                <?php
                                if($KarusMonthlyRankings->count() < 100) {
                                    $k = 100 - $KarusMonthlyRankings->count();
                                    for($i = 0; $i < $k; $i++) {
                                        $Character = New App\Character;
                                        $Character->Nation = 1;
                                        $Character->strUserID = '';
                                        $Character->LoyaltyMonthly = 0;

                                        $KarusMonthlyRankings->push($Character);
                                    }
                                }
                                ?>
                                @foreach($KarusMonthlyRankings as $index => $ranking)
                                    <?php $ranknum = $index+1;
                                    if($ranknum == 1) $rank = 1;
                                    elseif($ranknum > 1 && $ranknum <= 4) $rank =  2;
                                    elseif($ranknum > 3 && $ranknum <= 9) $rank =  3;
                                    elseif($ranknum > 9 && $ranknum <= 25) $rank =  4;
                                    elseif($ranknum > 25 && $ranknum <= 50) $rank =  5;
                                    elseif($ranknum > 39) $rank =  6;
                                    ?>

                                    <tr align="right" valign="middle">
                                        <td align="center" id="sRankPos">{{$index+1}}&nbsp;</td>

                                        <td id="sRankPos"><img src="{{ asset('images/symbol/'.$ranking->Nation.'.gif')}}"></td>

                                        <td align="left" id="sRankName"><img src="{{asset('/images/symbol/00'.$rank.'org.gif')}}">
                                            <a href="{{url('character/'.$ranking->strUserId)}}" style="color:#988e80;">{{$ranking->strUserId}}</a></td>
                                        <td align="left" id="sRankLoyalty">{{number_format($ranking->LoyaltyMonthly)}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div id="MONTHLYELMORAD" class="tab-pane col-xs-12">
                            <table class="table table-striped">
                                <thead>
                                <tr><td colspan="4">Monthly Rankings</td></tr>
                                <tr><td width="25px"></td><td width="25px"></td><td colspan="2"></td></tr>
                                </thead>
                                <tbody>
                                <tr valign="middle"></tr>
                                <?php
                                if($ElmoradMonthlyRankings->count() < 100) {
                                    $k = 100 - $ElmoradMonthlyRankings->count();
                                    for($i = 0; $i < $k; $i++) {
                                        $Character = New App\Character;
                                        $Character->Nation = 2;
                                        $Character->strUserID = '';
                                        $Character->LoyaltyMonthly = 0;

                                        $ElmoradMonthlyRankings->push($Character);
                                    }
                                }
                                ?>
                                @foreach($ElmoradMonthlyRankings as $index => $ranking)
                                    <?php $ranknum = $index+1;
                                    if($ranknum == 1) $rank = 1;
                                    elseif($ranknum > 1 && $ranknum <= 4) $rank =  2;
                                    elseif($ranknum > 3 && $ranknum <= 9) $rank =  3;
                                    elseif($ranknum > 9 && $ranknum <= 25) $rank =  4;
                                    elseif($ranknum > 25 && $ranknum <= 50) $rank =  5;
                                    elseif($ranknum > 39) $rank =  6;
                                    ?>

                                    <tr align="right" valign="middle">
                                        <td align="center" id="sRankPos">{{$index+1}}&nbsp;</td>

                                        <td id="sRankPos"><img src="{{ asset('images/symbol/'.$ranking->Nation.'.gif')}}"></td>

                                        <td align="left" id="sRankName"><img src="{{asset('/images/symbol/00'.$rank.'org.gif')}}">
                                            <a href="{{url('character/'.$ranking->strUserId)}}" style="color:#988e80;">{{$ranking->strUserId}}</a></td>
                                        <td align="left" id="sRankLoyalty">{{number_format($ranking->LoyaltyMonthly)}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="CLAN">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#CLANKARUS" data-toggle="tab">KARUS</a>
                        </li>
                        <li>
                            <a href="#CLANELMORAD" data-toggle="tab">ELMORAD</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="CLANKARUS" class="tab-pane active col-sm-12">
                            <table class="table table-striped">
                                <thead>
                                <tr><td colspan="4">Clan Rankings</td></tr>
                                <tr><td width="25px"></td><td width="25px"></td><td colspan="2"></td></tr>
                                </thead>
                                <tbody><tr valign="middle">
                                </tr>
                                <?php
                                if($KarusClanRankings->count() < 100) {
                                    $k = 100 - $KarusClanRankings->count();
                                    for($i = 0; $i < $k; $i++) {
                                        $Clan = New App\Clan;
                                        $Clan->Nation = 1;
                                        $Clan->Ranking = 0;
                                        $Clan->IDNum = 0;
                                        $Clan->IDName = '';
                                        $Clan->Points = 0;

                                        $KarusClanRankings->push($Clan);
                                    }
                                }
                                ?>
                                @foreach($KarusClanRankings as $index => $ranking)
                                    <tr align="right" valign="middle">
                                        <td align="center" id="sRankPos">{{$index+1}}&nbsp;</td>

                                        <td id="sRankPos"><img src="{{ asset('images/symbol/'.$ranking->Nation.'.gif')}}"></td>

                                        <td align="left" id="sRankName"><img src="{{ asset('images/symbol/'.Template::getClanRanking($ranking->Points))}}">
                                            <a href="{{url('clan/'.rtrim($ranking->IDName))}}" style="color:#6c645a;">{{$ranking->IDName}}</a></td>
                                        <td align="left" id="sRankLoyaltyc">{{number_format($ranking->Points)}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div id="CLANELMORAD" class="tab-pane col-sm-12">
                            <table class="table table-striped">
                                <thead>
                                <tr><td colspan="4">Clan Rankings</td></tr>
                                <tr><td width="25px"></td><td width="25px"></td><td colspan="2"></td></tr>
                                </thead>
                                <tbody><tr valign="middle">
                                </tr>
                                <?php
                                if($ElmoradClanRankings->count() < 100) {
                                    $k = 100 - $ElmoradClanRankings->count();
                                    for($i = 0; $i < $k; $i++) {
                                        $Clan = New App\Clan;
                                        $Clan->Nation = 2;
                                        $Clan->Ranking = 0;
                                        $Clan->IDNum = 0;
                                        $Clan->IDName = '';
                                        $Clan->Points = 0;

                                        $ElmoradClanRankings->push($Clan);
                                    }
                                }
                                ?>
                                @foreach($ElmoradClanRankings as $index => $ranking)
                                    <tr align="right" valign="middle">
                                        <td align="center" id="sRankPos">{{$index+1}}&nbsp;</td>

                                        <td id="sRankPos"><img src="{{ asset('images/symbol/'.$ranking->Nation.'.gif')}}"></td>

                                        <td align="left" id="sRankName"><img src="{{ asset('images/symbol/'.Template::getClanRanking($ranking->Points))}}">
                                            <a href="{{url('clan/'.rtrim($ranking->IDName))}}" style="color:#6c645a;">{{$ranking->IDName}}</a></td>
                                        <td align="left" id="sRankLoyaltyc">{{number_format($ranking->Points)}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop