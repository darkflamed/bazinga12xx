@extends('global.main')

@section('javascript')

@stop

@section('css')

@stop

@section('title')

@stop

@section('content')

    <div class="container container-main">
        <table class="table">
            <thead>
            <tr><td><h4>Downloads</h4></td></tr>
            </thead>
            <tbody>
            <tr>
                <td class="row">

                    <div class="well well-main">
                        <h4 class="text-center">Links</h4>
                        <h5>
                            <ul class="list-unstyled list-group row">
                                <li class="list-group-item col-xs-6">
                                    <div class="well well-download">
                                        <a href="#"><img src="{{ asset('images/mediafire.png') }}" ></a>
                                    </div>
                                </li>
                                @foreach($Downloads as $download)
                                    <li class="list-group-item col-xs-6">
                                        <div class="well well-download">
                                            <a href="{{ $download->link }}"><img src="{{ asset('images/mediafire.png') }}" ></a>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </h5>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

@stop