@extends('global.main')

@section('javascript')
    <script>
        window.Items.Initialize();
    </script>
@stop

@section('css')

@stop

@section('title')

@stop

@section('content')
    <article id="DropContainer" class="container container-main">
        <center><div id="title"><h1>DROP LIST<p></p><span></span></h1></div></center>
        <div class="col-xs-12">
            <div class="input-group col-xs-6" style="margin-bottom: 4px;">
                <input id="Search" type="text" class="form-control input-sm" onkeyup="window.Items.searchTable('MonsterTable')" placeholder="Search..." style="background-color: rgba(25, 25, 25, 0.31); color: #e7dbc3" />
            </div>
        </div>
        <div class="col-xs-6" style="height: 500px; overflow-y: scroll;">
            <table id="MonsterTable" class="table table-striped col-xs-12">
                <tbody>
                @foreach($Monsters as $index => $mon)
                    <tr onmousedown="window.Items.getMonsterItems('{{ url('/') }}', {{ $mon->sItem }})" style="cursor: pointer;">
                        <td align="center"> <font color="#e7dbc3">{{ Template::getZoneName($mon->ZoneID) }}</font></td>
                        <td align="center" style="text-align: center; color: #a6e687; font-size: 1em;">{{$mon->strName}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-xs-6">
            <center><h1>DROPS<p></p><span></span></h1></center>
            <div id="Items" class="col-xs-12"></div>
        </div>
    </article>
    <div id="ItemInfo"></div>
@stop