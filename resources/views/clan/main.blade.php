@extends('global.main')

@section('more-js')

@stop

@section('more-css')

@stop

@section('title')
    @lang('titles.clan_information')
@stop

@section('description')

@stop

@section('keywords')

@stop

@section('content')
    <article id="content" class="container container-main">
        <div class="container_3 account_sub_header">
            <div class="grad">
                <center><div id="title"><h1>{{$Knights->IDName}}<p></p><span></span></h1></div></center>
            </div>
        </div>
        <div class="padding20">
            <table class="table table-striped col-xs-12">
                <thead>
                <tr bgcolor="#25120B" class="textwhite">
                    <td width="5%" align="center"></td>
                    <td width="5%" align="center"></td>
                    <!--<td width="5%" align="center"></td>-->
                    <td width="20%" align="center">Username</td>
                    <td width="20%" align="center">@lang('fields.class')</td>
                    <td width="5%" align="center">@lang('fields.level')</td>
                    <td width="20%" align="center">Points</td>
                    <td width="20%" align="center">Rank</td>
                </tr>
                </thead>
                <tbody>
                @foreach($Users as $index => $char)
                    <tr>
                        <td align="center"> <font color="#e7dbc3">{{$index + 1}}</font></td>
                        <td align="center"><img src="/img/{{$char->Nation}}.gif"></td>
                        <!--<td align="center"><img src="/img/symbol/k001.gif"></td>-->
                        <td align="center"><a href="{{url('character/'.$char->strUserId)}}">{{$char->strUserId}}</a></td>
                        <td align="center"><font color="#e7dbc3">{{Template::getClassName($char->Class)}}</font></td>
                        <td align="center"><font color="#e7dbc3">{{$char->Level}}</font></td>
                        <td align="center"><font color="#a6e687">{{number_format($char->Loyalty)}}</font></td>
                        <td align="center"><font color="#e7dbc3">
                            <?php if($char->Fame == 1) echo 'Leader';
                            else if($char->Fame == 0) echo 'Assistant';
                            else echo 'Member'; ?>
                        </font></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </article>
@stop