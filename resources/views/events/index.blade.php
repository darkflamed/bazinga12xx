@extends('global.main')

@section('javascript')

@stop

@section('css')

@stop

@section('title')

@stop

@section('content')
    <div class="container container-main">
        <table class="table">
            <thead>
            <tr><td><h2>EVENTS</h2></td></tr>
            </thead>
            <tbody>
            <tr>
                <td class="row">
                    <div class="col-md-4" style="padding: 10px;">
                        <a href="{{ url('events/bifrost') }}"><img class="img-responsive" src="{{ asset('images/events/bifrost.jpg') }}"></a>
                    </div>
                    <div class="col-md-4" style="padding: 10px;">
                        <a href="{{ url('events/bdw') }}"><img class="img-responsive" src="{{ asset('images/events/BDW.jpg') }}"></a>
                    </div>
                    <div class="col-md-4" style="padding: 10px;">
                        <a href="{{ url('events/csw') }}"><img class="img-responsive" src="{{ asset('images/events/CSW.jpg') }}"></a>
                    </div>
                    <div class="col-md-4" style="padding: 10px;">
                        <a href="{{ url('events/crcz') }}"><img class="img-responsive" src="{{ asset('images/events/CRCZ.jpg') }}"></a>
                    </div>
                    <div class="col-md-4" style="padding: 10px;">
                        <a href="{{ url('events/crhl') }}"><img class="img-responsive" src="{{ asset('images/events/CRHL.jpg') }}"></a>
                    </div>
                    <div class="col-md-4" style="padding: 10px;">
                        <a href="{{ url('events/crlns') }}"><img class="img-responsive" src="{{ asset('images/events/CRLNS.jpg') }}"></a>
                    </div>
                    <div class="col-md-4" style="padding: 10px;">
                        <a href="{{ url('events/death_match') }}"><img class="img-responsive" src="{{ asset('images/events/death_match.jpg') }}"></a>
                    </div>
                    <div class="col-md-4" style="padding: 10px;">
                        <a href="{{ url('events/forgotten_temple') }}"><img class="img-responsive" src="{{ asset('images/events/forgotten_temple.jpg') }}"></a>
                    </div>
                    <div class="col-md-4" style="padding: 10px;">
                        <a href="{{ url('events/juraid') }}"><img class="img-responsive" src="{{ asset('images/events/juraid.jpg') }}"></a>
                    </div>
                    <div class="col-md-4" style="padding: 10px;">
                        <a href="{{ url('events/lunar') }}"><img class="img-responsive" src="{{ asset('images/events/lunar.jpg') }}"></a>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

@stop