@extends('global.main')

@section('javascript')

@stop

@section('css')

@stop

@section('title')
    Forgotten Temple
@stop

@section('content')

    <div class="container container-main">
        <table class="table">
            <thead>
            <tr><td><h4>FORGOTTEN TEMPLE – OPENS AT 68 LVL (FT)</h4></td></tr>
            </thead>
            <tbody>
            <tr>
                <td class="row">
                    <div class="well well-main">
                        <h3>
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;40 players maximum are allowed to join the Forgotten Temple. Once the registration period starts, the fastest players that will type REGISTER in normal chat, will receive system message saying they have registered succesfully. 15 minutes after registration period, 40 players will be teleported in a dungeon, there, various types of monsters appear every few minutes, and power of the monsters increases continuously. Reward is distributed only if you survive at the Temple till the end.
                                <br><br><span style="font-size: 1.2em">Objective:</span> Join any of the 5 parties in the dungeon, together defeat all monsters and don't die till the end.
                                <br><br><span style="font-size: 1.2em">Reward:</span> 1x <a href="{{ url('exchanges') }}">Blue Treasure Chest</a>
                            </p>
                        </h3>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

@stop