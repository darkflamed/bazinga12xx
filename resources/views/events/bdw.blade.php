@extends('global.main')

@section('javascript')

@stop

@section('css')

@stop

@section('title')
    BDW
@stop

@section('content')

    <div class="container container-main">
        <table class="table">
            <thead>
            <tr><td><h4>BORDER DEFENSE WAR (BDW)</h4></td></tr>
            </thead>
            <tbody>
            <tr>
                <td class="row">

                    <div class="well well-main">
                        <h3>
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;Groups of 8 players per nation will have a chance to fight on border's of their realms. Once the registration period starts, the fastest players that will type REGISTER in normal chat, will receive system message saying they have registered succesfully. 15 minutes after registration period, groups of 8 Karus and 8 El Morad players will be teleported into a zone where they will fight opossing nation's parties.
                                <br><br><span style="font-size: 1.2em">Objective:</span> In 45 minutes achieve more kills then your enemy or destroy the center and enemy's monument or achieve 60 kills for instant victory.
                                <br><br><span style="font-size: 1.2em">Reward:</span> Winners: 200 National Points, 5M EXP, <a href="{{ url('exchanges') }}">Red Treasure Chest</a>, EXP Flash
                                <br>Losers: 5M EXP
                            </p>
                        </h3>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

@stop