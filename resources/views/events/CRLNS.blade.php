@extends('global.main')

@section('javascript')

@stop

@section('css')

@stop

@section('title')
    Collection Race Last Nation Standing
@stop

@section('content')

    <div class="container container-main">
        <table class="table">
            <thead>
            <tr><td><h4>COLLECTION RACE: LAST NATION STANDING – RONARK LAND BASE (CR:RLB)</h4></td></tr>
            </thead>
            <tbody>
            <tr>
                <td class="row">

                    <div class="well well-main">
                        <h3>
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;When the time is right, Ronark Land Base zone will open its gates for nation of El Morad and Karus. Two nations will collide in an endless battle, fighting for supremacy over the new land of RLB. Prepare yourself for a race where you'll be competing on two different levels, individual, and national !
                                <br><br><span style="font-size: 1.2em">Objective 1 (nation):</span> In order to win the land of RLB, your nation will have to slay all citizens of the opposing nation, wether it be by slaying them completely or by drasticaly reducing their numbers by the time the entrances close.
                                <br><br><span style="font-size: 1.2em">Reward 1:</span> Citizens of a winning nation that stay alive during this fierce battle and do not get killed more then 8 times will receive: - 3x Abyss Gem, Gold Coin, EXP Flash, 200 National Points and 10M EXP.
                                <br><br><span style="font-size: 1.2em">Objective 2 (individual):</span> Each citizen fighting in this fierce battle also enters in a race … a race for collecting kills. If you kill 50 enemy players by the time zone closes you will obtain a random item from a huge list of rewards.
                                <br><br><span style="font-size: 1.2em">Reward 2:</span> <a href="{{ url('exchanges') }}">COLLECTION RACE RANDOM ITEM</a>
                            </p>
                        </h3>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

@stop