@extends('global.main')

@section('javascript')

@stop

@section('css')

@stop

@section('title')
    Bifrost
@stop

@section('content')

    <div class="container container-main">
        <table class="table">
            <thead>
            <tr><td><h4>BIFROST – OPENS AT 70 LVL (BF)</h4></td></tr>
            </thead>
            <tbody>
            <tr>
                <td class="row">

                    <div class="well well-main">
                        <h3>
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;Bifrost crack in the center of colony zone expands twice per week allowing the waring nations to conquer the monument. Whomever destroys the monument, crach will give him advantage to enter the realm of Bifrost with a head start thus allowing them to hunt for pricy fragments without interuptions of the enemy nation. After a while the nation that failed to defeat the monument will be able to enter the real of bifrost. The battle continoues in another realm. Choose to collect priceless fragments or choose to hunt your enemies. Choice is yours.
                                <br><br><span style="font-size: 1.2em">Objective:</span> When event starts, you have to destroy the monument in order to join. After monument is destroyed you can teleport in realm of Bifrost and hunt for 2 hours, or even less time if your nation failed to destroy the monument first. The gates to the realm are in center of Colony Zone.
                                <br><br><span style="font-size: 1.2em">Reward:</span> Various fragments obtained from monsters; Ultima Boss drops.
                            </p>
                        </h3>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

@stop