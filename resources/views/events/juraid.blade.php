@extends('global.main')

@section('javascript')

@stop

@section('css')

@stop

@section('title')
    Juraid
@stop

@section('content')

    <div class="container container-main">
        <table class="table">
            <thead>
            <tr><td><h4>JURAID DUNGEON – OPENS AT 70+ LVL (JD)</h4></td></tr>
            </thead>
            <tbody>
            <tr>
                <td class="row">

                    <div class="well well-main">
                        <h3>
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;Most dedicated heroes of highest levels can join this event. Once the registration period starts, the fastest players that will type REGISTER in normal chat, will receive system message saying they have registered succesfully. 15 minutes after registration period, 8 Karus players and 8 El Morad players will be teleported into fierce dungeon where they will fight for the ultimate reward, the silvery gem. This event will be available only after level 70 cap.
                                <br><br><span style="font-size: 1.2em">Objective:</span> Defeat 4 stages of monsters faster then the enemy party, defeat the Deva Bird boss before the enemy. If you fail to defeat Deva Bird boss in 50 minutes, the nation with most kills achieved will win the battle.
                                <br><br><span style="font-size: 1.2em">Reward:</span> Various Gems from monsters, Silvery Gem for winners, Black Gem for losers.
                            </p>
                        </h3>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

@stop