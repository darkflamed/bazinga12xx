@extends('global.main')

@section('javascript')

@stop

@section('css')

@stop

@section('title')
    Death Match
@stop

@section('content')

    <div class="container container-main">
        <table class="table">
            <thead>
            <tr><td><h4>DEATH MATCH (DM)</h4></td></tr>
            </thead>
            <tbody>
            <tr>
                <td class="row">
                    <div class="well well-main">
                        <h3>
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;For those that are most thirsty for blood. Once a day God of chaos will open a zone where only pure skill will matter. He will disquise player's name where nobody will recognize anyone, everyone will stand alone in true massacre and test of skill. All players will be receiving and dealing same amount of damage no matter their defense and attack power, all players will have the same Health. You can only use 2 skills that are located under ''Basic Skills Tab''. In this zone you can not use light feet as a rogue or any other skills. You can use health potions, swift skill, speed potions and the [DM] Basic Skills. You can use any weapon you desire but you can get a special weapon for death match event only, located at the NPC near teleport gates in moradon. With this weapon you will achieve best results. You can join this event through teleport gates or by typing REGISTER.
                                <br><br><span style="font-size: 1.2em">Objective:</span> Achieve best killing score before zone closes.
                                <br><br><span style="font-size: 1.2em">Reward:</span> 1x <a href="{{ url('exchanges') }}">Death Match Cross</a>
                            </p>
                        </h3>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

@stop