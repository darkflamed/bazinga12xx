@extends('global.main')

@section('javascript')

@stop

@section('css')

@stop

@section('title')
    Collection Race Colony Zone
@stop

@section('content')

    <div class="container container-main">
        <table class="table">
            <thead>
            <tr><td><h4>COLLECTION RACE: COLONY ZONE (CR:CZ)</h4></td></tr>
            </thead>
            <tbody>
            <tr>
                <td class="row">
                    <div class="well well-main">
                        <h3>
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;God of chaos often gets hungry for blood. He does not discriminate, be Karus or El Morad you will fight for his pleasure. By spilling enough blood in his name he will award you with a random reward from a huge list of well built items. When the time is right he will announce a race, a mercyles battle in main battle grounds of Colony Zone.
                                <br><br><span style="font-size: 1.2em">Objective:</span> Each citizen fighting in this fierce battle enters in a race … a race for collecting kills. If you kill 50 enemy players by the time race ends, you will obtain a random item from a huge list of rewards.
                                <br><br><span style="font-size: 1.2em">Reward:</span> <a href="{{ url('exchanges') }}">COLLECTION RACE RANDOM ITEM</a>
                            </p>
                        </h3>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

@stop