@extends('global.main')

@section('javascript')

@stop

@section('css')

@stop

@section('title')
    Lunar War
@stop

@section('content')

    <div class="container container-main">
        <table class="table">
            <thead>
            <tr><td><h4>LUNAR WAR (LW)</h4></td></tr>
            </thead>
            <tbody>
            <tr>
                <td class="row">

                    <div class="well well-main">
                        <h3>
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;Twice a week warriors of Karus and El Morad will be able to enter Lunar zone through the  Lunar Valley. Zone is a bridge between the hostile nations. Battle in this zone is most fierce at weekends when masses of players colide and fight each other for a chance to enter the defeated nation land and occupy them.
                                <br><br><span style="font-size: 1.2em">Objective:</span> The battle lasts for 45 minutes, after 45 minutes the nation with most kills will have a chance to invade the enemy's nation for additional 45 minutes. If a nation defeat both warders and keeper in under 45 minutes, the winning nation have a bonus time in invasion.
                                <br><br><span style="font-size: 1.2em">Reward:</span> Knight's Medals droping from NPCs in invasion
                            </p>
                        </h3>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

@stop