@extends('global.main')

@section('javascript')

@stop

@section('css')

@stop

@section('title')
    Castle Siege War
@stop

@section('content')

    <div class="container container-main">
        <table class="table">
            <thead>
            <tr><td><h4>CASTLE SIEGE WAR – OPENS AT 68 LVL (CSW)</h4></td></tr>
            </thead>
            <tbody>
            <tr>
                <td class="row">
                    <div class="well well-main">
                        <h3>
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;Once a week zone of Delos will transform into battlefield for those who wish power and wealth. The strongest, most organized and most cunning clans will be able to fight a battle for Castle of Delos.
                                <br><br><span style="font-size: 1.2em">Objective:</span> Destroy the center artifact in Castle of Delos and hold ownership until the war ends. Clan owning the artifact before time runs out will be the new owner of castle for at least a week upon unsuccessfull defense.
                                <br><br><span style="font-size: 1.2em">Reward:</span> 1x Ownership of delos treasury, <a href="{{ url('exchanges') }}">White Treasure Chest</a>, 800 National points and 100 Knight Cash for each member of the winning clan
                            </p>
                        </h3>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

@stop