@extends('global.main')

@section('javascript')

@stop

@section('css')

@stop

@section('title')
    Collection Race Holy Land
@stop

@section('content')

    <div class="container container-main">
        <table class="table">
            <thead>
            <tr><td><h4>COLLECTION RACE: HOLY LAND – RONARK ZONE (CR:HL)</h4></td></tr>
            </thead>
            <tbody>
            <tr>
                <td class="row">

                    <div class="well well-main">
                        <h3>
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;Holy spirits of the fallen creatures from realm of the living can produce enormous energy. An energy so powerful that can open a land which is lost in time. For a short period of time you will be able to join this land. Once again you'll be competing for two objectives.
                                <br><br><span style="font-size: 1.2em">Objective 1:</span> Each citizen fighting in this fierce battle enters in a race … a race for collecting kills. If you kill 50 enemy players by the time zone closes you will obtain a random item from a huge list of rewards.
                                <br><br><span style="font-size: 1.2em">Reward 1:</span> <a href="{{ url('exchanges') }}">COLLECTION RACE RANDOM ITEM</a>
                                <br><br><span style="font-size: 1.2em">Objective 2:</span> Instead of fighting with the enemy nation you can choose to put Holy spirits out of their misery. Hunt down the spirits ! For every 5 trophies you will receive 5x cursed coin that you can sell to any npc for 100k coins each. Easy and fast money making but don't get to greedy the enemy is watching !
                                <br><br><span style="font-size: 1.2em">Reward 2:</span> 5x Cursed Coin per 5 mobs killed (party / solo repeatable, fast respawn)
                            </p>
                        </h3>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

@stop