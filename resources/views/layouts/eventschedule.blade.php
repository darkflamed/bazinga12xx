<script>
    nextevent = '';
    times = [ '1:00', '3:00', '4:00', '11:00', '14:00', '15:00', '16:00', '17:00',
        '19:00', '20:00', '21:00', '22:00', '22:50'];
    days = [ 'mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun' ];
    schedule =  {
        mon: [
            {active: true, tooltip: 'Forgotten Temple', classname: 'FT', text: 'FT', starttime: '1:00', endtime: '1:45'},
            {active: false},
            {active: true, tooltip: 'Border Defense War', classname: 'BDW', text: 'BDW', starttime: '4:00', endtime: '4:45'},
            {active: true, tooltip: 'Forgotten Temple', classname: 'FT', text: 'FT', starttime: '11:00', endtime: '11:45'},
            {active: false},
            {active: true, tooltip: 'Forgotten Temple', classname: 'FT', text: 'FT', starttime: '15:00', endtime: '15:45'},
            {active: true, tooltip: 'Border Defense War', classname: 'BDW', text: 'BDW', starttime: '16:00', endtime: '16:45'},
            {active: false},
            {active: false},
            {active: true, tooltip: 'Border Defense War', classname: 'BDW', text: 'BDW', starttime: '20:00', endtime: '20:45'},
            {active: true, tooltip: 'Collection Race; Colony Zone', classname: 'CR', text: 'CR;CZ', starttime: '21:00', endtime: '21:45'},
            {active: true, tooltip: 'Death Match', classname: 'DM', text: 'DM', starttime: '22:00', endtime: '22:45'},
            {active: false},
        ],
        tue: [
            {active: true, tooltip: 'Forgotten Temple', classname: 'FT', text: 'FT', starttime: '1:00', endtime: '1:45'},
            {active: false},
            {active: true, tooltip: 'Border Defense War', classname: 'BDW', text: 'BDW', starttime: '4:00', endtime: '4:45'},
            {active: true, tooltip: 'Forgotten Temple', classname: 'FT', text: 'FT', starttime: '11:00', endtime: '11:45'},
            {active: false},
            {active: true, tooltip: 'Forgotten Temple', classname: 'FT', text: 'FT', starttime: '15:00', endtime: '15:45'},
            {active: true, tooltip: 'Border Defense War', classname: 'BDW', text: 'BDW', starttime: '16:00', endtime: '16:45'},
            {active: false},
            {active: false},
            {active: true, tooltip: 'Border Defense War', classname: 'BDW', text: 'BDW', starttime: '20:00', endtime: '20:45'},
            {active: true, tooltip: 'Collection Race; Holy Land Ronark Zone', classname: 'CR', text: 'CR;HL', starttime: '21:00', endtime: '21:45'},
            {active: true, tooltip: 'Death Match', classname: 'DM', text: 'DM', starttime: '22:00', endtime: '22:45'},
            {active: false},
        ],
        wed: [
            {active: true, tooltip: 'Forgotten Temple', classname: 'FT', text: 'FT', starttime: '1:00', endtime: '1:45'},
            {active: false},
            {active: true, tooltip: 'Border Defense War', classname: 'BDW', text: 'BDW', starttime: '4:00', endtime: '4:45'},
            {active: true, tooltip: 'Forgotten Temple', classname: 'FT', text: 'FT', starttime: '11:00', endtime: '11:45'},
            {active: true, tooltip: 'Collection Race; Holy Land Ronark Zone', classname: 'CR', text: 'CR;HL', starttime: '14:00', endtime: '14:45'},
            {active: true, tooltip: 'Forgotten Temple', classname: 'FT', text: 'FT', starttime: '15:00', endtime: '15:45'},
            {active: true, tooltip: 'Border Defense War', classname: 'BDW', text: 'BDW', starttime: '16:00', endtime: '16:45'},
            {active: false},
            {active: false},
            {active: true, tooltip: 'Border Defense War', classname: 'BDW', text: 'BDW', starttime: '20:00', endtime: '20:45'},
            {active: true, tooltip: 'Collection Race; Last Nation Standing - Ronark Land Base', classname: 'CR', text: 'CR;RLB', starttime: '21:00', endtime: '21:45'},
            {active: true, tooltip: 'Death Match', classname: 'DM', text: 'DM', starttime: '22:00', endtime: '22:45'},
            {active: false},
        ],
        thu: [
            {active: true, tooltip: 'Forgotten Temple', classname: 'FT', text: 'FT', starttime: '1:00', endtime: '1:45'},
            {active: true, tooltip: 'Collection Race; Last Nation Standing - Ronark Land Base', classname: 'CR', text: 'CR;RLB', starttime: '3:00', endtime: '3:45'},
            {active: true, tooltip: 'Border Defense War', classname: 'BDW', text: 'BDW', starttime: '4:00', endtime: '4:45'},
            {active: true, tooltip: 'Forgotten Temple', classname: 'FT', text: 'FT', starttime: '11:00', endtime: '11:45'},
            {active: false},
            {active: true, tooltip: 'Forgotten Temple', classname: 'FT', text: 'FT', starttime: '15:00', endtime: '15:45'},
            {active: true, tooltip: 'Border Defense War', classname: 'BDW', text: 'BDW', starttime: '16:00', endtime: '16:45'},
            {active: false},
            {active: false},
            {active: true, tooltip: 'Border Defense War', classname: 'BDW', text: 'BDW', starttime: '20:00', endtime: '20:45'},
            {active: true, tooltip: 'Collection Race; Colony Zone', classname: 'CR', text: 'CR;CZ', starttime: '21:00', endtime: '21:45'},
            {active: true, tooltip: 'Death Match', classname: 'DM', text: 'DM', starttime: '22:00', endtime: '22:45'},
            {active: false},
        ],
        fri: [
            {active: true, tooltip: 'Forgotten Temple', classname: 'FT', text: 'FT', starttime: '1:00', endtime: '1:45'},
            {active: false},
            {active: true, tooltip: 'Border Defense War', classname: 'BDW', text: 'BDW', starttime: '4:00', endtime: '4:45'},
            {active: true, tooltip: 'Forgotten Temple', classname: 'FT', text: 'FT', starttime: '11:00', endtime: '11:45'},
            {active: false},
            {active: true, tooltip: 'Forgotten Temple', classname: 'FT', text: 'FT', starttime: '15:00', endtime: '15:45'},
            {active: true, tooltip: 'Border Defense War', classname: 'BDW', text: 'BDW', starttime: '16:00', endtime: '16:45'},
            {active: false},
            {active: true, tooltip: 'Lunar War', classname: 'LW', text: 'LUNAR', starttime: '19:00', endtime: '20:45'},
            {active: true, tooltip: 'Border Defense War', classname: 'BDW', text: 'BDW', starttime: '20:00', endtime: '20:45'},
            {active: true, tooltip: 'Collection Race; Holy Land Ronark Zone', classname: 'CR', text: 'CR;HL', starttime: '21:00', endtime: '21:45'},
            {active: true, tooltip: 'Juraid', classname: 'JR', text: 'JURAID', starttime: '22:00', endtime: '22:45'},
            {active: false},
        ],
        sat: [
            {active: true, tooltip: 'Forgotten Temple', classname: 'FT', text: 'FT', starttime: '1:00', endtime: '1:45'},
            {active: false},
            {active: true, tooltip: 'Border Defense War', classname: 'BDW', text: 'BDW', starttime: '4:00', endtime: '4:45'},
            {active: true, tooltip: 'Forgotten Temple', classname: 'FT', text: 'FT', starttime: '11:00', endtime: '11:45'},
            {active: true, tooltip: 'Collection Race; Holy Land Ronark Zone', classname: 'CR', text: 'CR;HL', starttime: '14:00', endtime: '14:45'},
            {active: true, tooltip: 'Forgotten Temple', classname: 'FT', text: 'FT', starttime: '15:00', endtime: '15:45'},
            {active: true, tooltip: 'Border Defense War', classname: 'BDW', text: 'BDW', starttime: '16:00', endtime: '16:45'},
            {active: true, tooltip: 'Castle Siege War', classname: 'CSW', text: 'CSW', starttime: '17:00', endtime: '18:10'},
            {active: true, tooltip: 'Lunar War', classname: 'LW', text: 'LUNAR', starttime: '19:00', endtime: '20:45'},
            {active: true, tooltip: 'Border Defense War', classname: 'BDW', text: 'BDW', starttime: '20:00', endtime: '20:45'},
            {active: true, tooltip: 'Collection Race; Last Nation Standing - Ronark Land Base', classname: 'CR', text: 'CR;RLB', starttime: '21:00', endtime: '21:45'},
            {active: true, tooltip: 'Juraid', classname: 'JR', text: 'JURAID', starttime: '22:00', endtime: '22:45'},
            {active: true, tooltip: 'Bifrost', classname: 'BF', text: 'BF', starttime: '22:50', endtime: '00:50'},
        ],
        sun: [
            {active: true, tooltip: 'Forgotten Temple', classname: 'FT', text: 'FT', starttime: '1:00', endtime: '1:45'},
            {active: true, tooltip: 'Collection Race; Last Nation Standing - Ronark Land Base', classname: 'CR', text: 'CR;RLB', starttime: '3:00', endtime: '3:45'},
            {active: true, tooltip: 'Border Defense War', classname: 'BDW', text: 'BDW', starttime: '4:00', endtime: '4:45'},
            {active: true, tooltip: 'Forgotten Temple', classname: 'FT', text: 'FT', starttime: '11:00', endtime: '11:45'},
            {active: false},
            {active: true, tooltip: 'Forgotten Temple', classname: 'FT', text: 'FT', starttime: '15:00', endtime: '15:45'},
            {active: true, tooltip: 'Border Defense War', classname: 'BDW', text: 'BDW', starttime: '16:00', endtime: '16:45'},
            {active: false},
            {active: false},
            {active: true, tooltip: 'Border Defense War', classname: 'BDW', text: 'BDW', starttime: '20:00', endtime: '20:45'},
            {active: true, tooltip: 'Collection Race; Colony Zone', classname: 'CR', text: 'CR;CZ', starttime: '21:00', endtime: '21:45'},
            {active: true, tooltip: 'Death Match', classname: 'DM', text: 'DM', starttime: '22:00', endtime: '22:45'},
            {active: true, tooltip: 'Bifrost', classname: 'BF', text: 'BF', starttime: '22:50', endtime: '00:50'},
        ],
    };

    function getNextEvent() {
        let Time = moment().tz('Europe/Ljubljana').format('HH:mm');
        let NextEventTime = moment().tz('Europe/Ljubljana');
        let DayOfWeek = moment().tz('Europe/Ljubljana').format('dddd').substr(0, 3).toLowerCase();
        let TimeMinutes = convertToMinutes(Time);
        let NextEvent = null;

        console.log(DayOfWeek);

        schedule[DayOfWeek].forEach(function(event) {
            if(event.active && TimeMinutes < convertToMinutes(event.starttime) && NextEvent === null) {
                NextEventTime.add(convertToMinutes(event.starttime) - TimeMinutes, 'minutes');
                NextEvent = event.tooltip + ' ' + moment().tz('Europe/Ljubljana').countdown(NextEventTime, ~0x002 && ~0x001).toString();
            }
        });

        if(NextEvent === null) {
            DayOfWeek = moment().tz('Europe/Ljubljana').add(1, 'days').format('dddd').substr(0, 3).toLowerCase();
            schedule[DayOfWeek].forEach(function(event) {
                if(event.active && NextEvent === null) {
                    NextEventTime.add(1, 'days').startOf('day');
                    NextEventTime.add(convertToMinutes(event.starttime), 'minutes');
                    NextEvent = event.tooltip + ' ' + moment().tz('Europe/Ljubljana').countdown(NextEventTime, ~0x002 && ~0x001).toString();
                }
            });
        }

        $('.nextevent').html('~ Next Event ~ <br>' + NextEvent);
    }

    function convertToMinutes(Time) {
        let Arr = Time.split(':');
        return (parseInt(Arr[0])*60)+parseInt(Arr[1]);
    }

    $( document ).ready(function() {
        getNextEvent();
        $('.timezone').html('Server Time: ' + moment().tz('Europe/Ljubljana').format('HH:mm'));
        let Table = $('.table-eventschedule tbody');
        $('.table-eventschedule thead tr td').attr('colspan', times.length+1);

        let append = '<tr><td></td>';
        times.forEach(function(time) {
            append += '<td>'+time+'</td>';
        });
        append += '</tr>';
        Table.append(append);

        days.forEach(function(day) {
            let append = '<tr><td>'+day.toUpperCase()+'</td>';
            schedule[day].forEach(function(event) {
                if(event.active)
                    append += '<td class="'+event.classname+'"><div data-toggle="tooltip" data-placement="top" title="'+event.tooltip+'&#10; Start: '+event.starttime+' - End: '+event.endtime+'">' + event.text + '</div></td>';
                else
                    append += '<td></td>';
            });
            append += '</tr>';
            Table.append(append);
        });

        $('[data-toggle="tooltip"]').tooltip();
    });

</script>