@extends('global.main')

@section('javascript')
<script src="{{ asset(mix('js/moment.js')) }}"></script>
<script>
    $( document ).ready(function() {
        $.ajaxSetup({ cache: true });
        $.getScript('//connect.facebook.net/en_US/sdk.js', function(){
            FB.init({
                appId: '{your-app-id}',
                version: 'v2.7' // or v2.1, v2.2, v2.3, ...
            });
            $('#loginbutton,#feedbutton').removeAttr('disabled');
            FB.getLoginStatus(updateStatusCallback);
        });

        $( window ).resize(function() {
            $('#FacebookContainer').html('<div class="fb-page" data-href="https://www.facebook.com/bazingako" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/bazingako" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/bazingako">Bazinga KO</a></blockquote></div>');
            FB.XFBML.parse();
        });

        $('.cCLAN').click(function(e){
            e.preventDefault();
            $('.cUSER').parent().removeClass("active");
            $('.cCLAN').parent().addClass("active");
            $('#CLAN').addClass('active');
            $('#USER').removeClass('active');
        });

        $('.cUSER').click(function(e){
            e.preventDefault();
            $('.cCLAN').parent().removeClass("active");
            $('.cUSER').parent().addClass("active");
            $('#USER').addClass('active');
            $('#CLAN').removeClass('active');
        });
    });
</script>
@include('layouts.eventschedule')
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
@stop

@section('css')

@stop

@section('title')

@stop

@section('content')
    <div class="container">
        <center>
            <img src="{{asset('images/komyko.png')}}" style="margin-top: -15px;" class="col-xs-6 col-xs-offset-3">
        </center>
    </div>
    <div class="container container-main">
        <div class="row">
			<div class="col-xs-12">
				<h3><font color="silver"><center>KO-MyKO BETA [15.12.2017 - 7 PM GMT+1]</center></font></h3></br><center><img src="http://gen.sendtric.com/countdown/cap2bbdv1p"></br></center><br>
			</div>
            <div class="col-md-8">
                <div class="col-xs-12">
                    <div class="col-xs-12" style="overflow-x: auto">
                        <table class="table table-eventschedule">
                            <thead>
                            <tr>
                                <td>
                                    <div class="eventtitle">Event Schedule</div>
                                    <div class="timezone"></div>
                                    <div class="nextevent"></div>
                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr valign="middle"></tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="col-xs-6 progress-main">
                        <span>Karus {{ is_null($Online) ? 0 : ($Online->KARUS / ($Online->KARUS + $Online->ELMORAD)) * 100  }}%</span>
                        <div class="progress vertical">
                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"
                                 style="width: {{ is_null($Online) ? 0 : ($Online->KARUS / ($Online->KARUS + $Online->ELMORAD)) * 100  }}%;"></div>
                        </div>
                    </div>
                    <div class="col-xs-6 progress-main">
                        <span>Elmorad {{ is_null($Online) ? 0 : ($Online->ELMORAD / ($Online->KARUS + $Online->ELMORAD)) * 100  }}%</span>
                        <div class="progress vertical">
                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"
                                 style="width: {{ is_null($Online) ? 0 : ($Online->ELMORAD / ($Online->KARUS + $Online->ELMORAD)) * 100  }}%;"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-xs-12">
                <div id="RankingsTabs">
                    <div class="tab-content">
                        <div class="tab-pane active" id="USER">
                            <div id="SubRankingsTabs" class="col-xs-12">
                                <ul class="nav nav-tabs inline">
                                    <li class="active">
                                        <a class="cUSER" href="#USER" data-toggle="tab">USER</a>
                                    </li>
                                    <li>
                                        <a class="cCLAN" href="#CLAN" data-toggle="tab">CLAN</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs inline">
                                    <li class="active">
                                        <a href="#USERKARUS" data-toggle="tab">KARUS</a>
                                    </li>
                                    <li>
                                        <a href="#USERELMO" data-toggle="tab">ELMORAD</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="USERKARUS">
                                        <div id="UserRankings" class="col-xs-12">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr><td colspan="4">User Rankings</td></tr>
                                                <tr><td width="25px"></td><td width="25px"></td><td colspan="2"></td></tr>
                                                </thead>
                                                <tbody>
                                                <tr valign="middle"></tr>
                                                <?php
                                                if($UserRankingsKarus->count() < 10) {
                                                    $k = 10 - $UserRankingsKarus->count();
                                                    for($i = 0; $i < $k; $i++) {
                                                        $Character = New App\Character;
                                                        $Character->Nation = 1;
                                                        $Character->strUserID = 'Open';
                                                        $Character->Loyalty = 0;

                                                        $UserRankingsKarus->push($Character);
                                                    }
                                                }
                                                ?>
                                                @foreach($UserRankingsKarus as $index => $ranking)
                                                    <?php switch($index+1){
                                                        case 1: $rank = 1; break;
                                                        case 2:case 3:case 4:$rank =  2; break;
                                                        case 5:case 6:case 7:case 8:case 9: $rank =  3;break;
                                                        case 10: $rank =  4; break; } ?>

                                                    <tr align="right" valign="middle">
                                                        <td align="center" id="sRankPos">{{$index+1}}&nbsp;</td>

                                                        <td id="sRankPos"><img src="{{ asset('images/symbol/'.$ranking->Nation.'.gif')}}"></td>

                                                        <td align="left" id="sRankName"><img src="{{asset('/images/symbol/k00'.$rank.'.gif')}}">
                                                            <a href="{{url('character/'.$ranking->strUserId)}}" style="color:#988e80;">{{$ranking->strUserId}}</a></td>
                                                        <td align="left" id="sRankLoyalty">{{number_format($ranking->Loyalty)}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="USERELMO">
                                        <div id="UserRankings" class="col-xs-12">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr><td colspan="4">User Rankings</td></tr>
                                                <tr><td width="25px"></td><td width="25px"></td><td colspan="2"></td></tr>
                                                </thead>
                                                <tbody>
                                                <tr valign="middle"></tr>
                                                <?php
                                                if($UserRankingsElmo->count() < 10) {
                                                    $k = 10 - $UserRankingsElmo->count();
                                                    for($i = 0; $i < $k; $i++) {
                                                        $Character = New App\Character;
                                                        $Character->Nation = 2;
                                                        $Character->strUserID = 'Open';
                                                        $Character->Loyalty = 0;

                                                        $UserRankingsElmo->push($Character);
                                                    }
                                                }
                                                ?>
                                                @foreach($UserRankingsElmo as $index => $ranking)
                                                    <?php switch($index+1){
                                                        case 1: $rank = 1; break;
                                                        case 2:case 3:case 4:$rank =  2; break;
                                                        case 5:case 6:case 7:case 8:case 9: $rank =  3;break;
                                                        case 10: $rank =  4; break; } ?>

                                                    <tr align="right" valign="middle">
                                                        <td align="center" id="sRankPos">{{$index+1}}&nbsp;</td>

                                                        <td id="sRankPos"><img src="{{ asset('images/symbol/'.$ranking->Nation.'.gif')}}"></td>

                                                        <td align="left" id="sRankName"><img src="{{asset('/images/symbol/k00'.$rank.'.gif')}}">
                                                            <a href="{{url('character/'.$ranking->strUserId)}}" style="color:#988e80;">{{$ranking->strUserId}}</a></td>
                                                        <td align="left" id="sRankLoyalty">{{number_format($ranking->Loyalty)}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="CLAN">
                            <div id="SubRankingsTabs" class="col-xs-12">
                                <ul class="nav nav-tabs inline">
                                    <li class="active">
                                        <a class="cUSER" href="#USER" data-toggle="tab">USER</a>
                                    </li>
                                    <li>
                                        <a class="cCLAN" href="#CLAN" data-toggle="tab">CLAN</a>
                                    </li>
                                </ul>
                                <ul class="nav nav-tabs inline">
                                    <li class="active">
                                        <a href="#CLANKARUS" data-toggle="tab">KARUS</a>
                                    </li>
                                    <li>
                                        <a href="#CLANELMO" data-toggle="tab">ELMORAD</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="CLANKARUS">
                                        <div id="ClanRankings" class="col-sm-12">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr><td colspan="4">Clan Rankings</td></tr>
                                                <tr><td width="25px"></td><td width="25px"></td><td colspan="2"></td></tr>
                                                </thead>
                                                <tbody><tr valign="middle">
                                                </tr>
                                                @foreach($KarusClanRankings as $index => $ranking)
                                                    <tr align="right" valign="middle">
                                                        <td align="center" id="sRankPos">{{$index+1}}&nbsp;</td>
                                                        <td id="sRankPos"><img src="{{ asset('images/symbol/'.$ranking->Nation.'.gif')}}"></td>
                                                        <td align="left" id="sRankName"><img src="{{ asset('images/symbol/'.Template::getClanRanking($ranking->Points))}}">
                                                            <a href="{{url('clan/'.rtrim($ranking->IDName))}}" style="color:#6c645a;">{{$ranking->IDName}}</a></td>
                                                        <td align="left" id="sRankLoyaltyc">{{number_format($ranking->Points)}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="CLANELMO">
                                        <div id="ClanRankings" class="col-sm-12">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr><td colspan="4">Clan Rankings</td></tr>
                                                <tr><td width="25px"></td><td width="25px"></td><td colspan="2"></td></tr>
                                                </thead>
                                                <tbody><tr valign="middle">
                                                </tr>
                                                @foreach($ElmoClanRankings as $index => $ranking)
                                                    <tr align="right" valign="middle">
                                                        <td align="center" id="sRankPos">{{$index+1}}&nbsp;</td>

                                                        <td id="sRankPos"><img src="{{ asset('images/symbol/'.$ranking->Nation.'.gif')}}"></td>

                                                        <td align="left" id="sRankName"><img src="{{ asset('images/symbol/'.Template::getClanRanking($ranking->Points))}}">
                                                            <a href="{{url('clan/'.rtrim($ranking->IDName))}}" style="color:#6c645a;">{{$ranking->IDName}}</a></td>
                                                        <td align="left" id="sRankLoyaltyc">{{number_format($ranking->Points)}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-8 col-xs-12">
                <div class="col-xs-12">
                    <div id="NoticeBoard" class="notice_board container">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a  href="#1" data-toggle="tab">ALL</a>
                            </li>
                            <li>
                                <a href="#2" data-toggle="tab">NEWS</a>
                            </li>
                            <li>
                                <a href="#3" data-toggle="tab">GM LIST</a>
                            </li>
                            <li>
                                <a href="#4" data-toggle="tab">UPDATES</a>
                            </li>
                        </ul>

                        <div class="tab-content ">
                            <div class="tab-pane active" id="1">
                                <h3>Welcome to KO-MyKO. KO-MyKO is a 70 lvl cap, farm server with a great market orianted system, active events & strong PvP action. If you have enjoyed first versions of Knight Online our server is the right address for your nostalgia of Breth, Piana and newbie action.</h3>
                            </div>
                            <div class="tab-pane" id="2">
                                <h3> Welcome to KO-MyKO News section. For news please visit our FORUM <a href="http://forum.knightonline-myko.com/index.php?/forum/13-announcements/">(CLICK HERE)</a> or follow us on FACEBOOK <a href="http://facebook.com/bazingako">(CLICK HERE)</a></h3>
                            </div>
                            <div class="tab-pane" id="3">
                                <div class="col-xs-12" style="height: 250px; overflow-y: scroll; padding-top: 8px;">
                                    <table class="table table-striped">
                                    <tbody>
                                    @foreach($GMList as $gm)
                                        <tr align="right" valign="middle">
                                            <td width="25"><img src="{{ asset('images/symbol/'.$gm->Nation.'.gif')}}"></td>
                                            <td align="left">
                                                <a href="{{url('character/'.$gm->strUserId)}}" style="color:#988e80;">{{$gm->strUserId}}</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="4">
                                <h3>To follow our updates please check our FaceBook or Forums.</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

			
            <div id="FacebookContainer" class="col-lg-4 col-xs-12">
                <center>
                <div class="fb-page" data-href="https://www.facebook.com/bazingako" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/bazingako" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/bazingako">Bazinga KO</a></blockquote></div>
                </center>
            </div>

        </div>
    </div>
@stop