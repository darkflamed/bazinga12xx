@extends('global.main')

@section('content')
    <div class="container container-main">
        <div class="row">
            <div class="col-xs-12">
                <h2>Your account has been registered, you must confirm your email before using this account.</h2><br>
                <br><h2>Hesabınız kaydedilmiştir, hesabınızı kullanmaya başlamadan önce emailinizi onaylamanız gerekmektedir.</h2><br>
                <br><h2>Tu cuenta ha sido registrada, debes confirmar tu email antes de usar esta cuenta.</h2><br>
            </div>
        </div>
    </div>
@endsection