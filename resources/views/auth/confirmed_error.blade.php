@extends('global.main')

@section('content')
    <div class="container container-main">
        <div class="row">
            <div class="col-xs-12">
                <h2><span class="help-block"><strong>Confirmation token is invalid.</strong></span></h2>
            </div>
        </div>
    </div>
@endsection