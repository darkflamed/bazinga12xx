@extends('global.main')

@section('javascript')
    <script>
        $(window).on('load', function() {
            window.setCountrySelector("#country");
        });
    </script>
@endsection

@section('content')
<div class="container container-main">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('accountid') ? ' has-error' : '' }}">
                            <label for="accountid" class="col-md-4 control-label">Username</label>

                            <div class="col-md-6">
                                <input id="accountid" type="text" class="form-control" name="accountid" value="{{ old('accountid') }}" required autofocus>

                                @if ($errors->has('accountid'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('accountid') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('pin') ? ' has-error' : '' }}">
                            <label for="pin" class="col-md-4 control-label">Secret PIN</label>

                            <div class="col-md-6">
                                <input id="pin" type="password" class="form-control" name="pin" required>

                                @if ($errors->has('pin'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pin') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="pin-confirm" class="col-md-4 control-label">Confirm Secret PIN</label>

                            <div class="col-md-6">
                                <input id="pin-confirm" type="password" class="form-control" name="pin_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                            <label for="country" class="col-md-4 control-label">Country</label>

                            <div class="col-md-6">
                                <input id="country" class="form-control" name="country" required>
                                <input type="hidden" id="country_code" name="country_code" />

                                @if ($errors->has('country'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <input class="checkbox-inline" type="checkbox" name="tos" {{ is_null(old('tos')) ? '' : 'checked' }}>
                                <label for="tos" class="control-label">&nbsp;I Agree to the <a href="{{ url('tac') }}">Terms and Conditions</a></label>

                                @if ($errors->has('tos'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tos') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                {!! app('captcha')->display() !!}

                                @if ($errors->has('captcha'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('captcha') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-main">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
