@extends('global.main')

@section('content')
    <div class="container container-main">
        <div class="row">
            <div class="col-xs-12">
                <h2>Your account at email: {{ $user->email }} has been confirmed.</h2>
            </div>
        </div>
    </div>
@endsection