@extends('emails.main')

@section('Preview_Text')
    {{ $preview_text }}
@endsection

@section('Header')
    {{ $header }}
@endsection

@section('Body')
    {{ $body }}
@endsection

@isset($button_text)
    @section('Button_Text')
        {{ $button_text }}
    @endsection
@endisset

@isset($button_link)
    @section('Button_Link')
        {{ $button_link }}
    @endsection
@endisset