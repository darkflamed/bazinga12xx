<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
@include('global.header')
<body>
<div id="app">
    @include('global.nav')
    @yield('content')
    @include('global.footer')
</div>

<!-- Scripts -->
@if(!Auth::guest() && false)
<script src="http://{{ Request::getHost() }}:6001/socket.io/socket.io.js"></script>
@endif

<script src="{{ asset(mix('js/app.js')) }}"></script>

@if(!Auth::guest() && false)
    <script>
        window.Echo.private('knightcash.{{Auth::user()->id}}')
            .listen('KnightCashBroadcast', (e) => {
                $('#KnightCash').html(e.KnightCash);
            });
    </script>
@endif

@yield('javascript')
</body>
</html>
