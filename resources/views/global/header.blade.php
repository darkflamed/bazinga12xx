<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title') KO-MyKO</title>

    <!-- Styles -->
    <link href="{{ asset(mix('css/app.css')) }}" rel="stylesheet">
    @yield('css')
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}" type="shortcut icon" />

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => '',
            'oAuthToken' => !Auth::guest() ? Auth::user()->createToken('PowerupStore')->accessToken : ''
        ]) !!};

        let xhr = new XMLHttpRequest();
        xhr.open('GET', '//{{ Request::getHost() }}/getCSRF');
        xhr.onload = function() {
            if (xhr.status === 200)
               window.Laravel.csrfToken = xhr.responseText;
        };
        xhr.send();
    </script>

</head>