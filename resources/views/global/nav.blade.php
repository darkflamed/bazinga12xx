<div class="navbar-top container-fluid">
    <div class="container">
        <a class="navbar-brand hidden-xs" href="{{ url('/') }}">
            KO-MyKO
        </a>

        @if(!Auth::guest())
        <div class="pull-left hidden-lg hidden-md hidden-sm" style="list-style: none; margin-top: 4px;">
                <a href="{{ url('account') }}">
                    {{ Auth::user()->strAccountID }} | {{ Auth::user()->nKnightCash }} KC
                </a>
        </div>
        @endif

        <ul class="pull-right" style="list-style: none">
            @if (Auth::guest())
                <li>
                    <a href="{{ route('login') }}"><span class="glyphicon glyphicon-user"></span> Login</a>
                    <a href="{{ route('register') }}"><span class="glyphicon glyphicon-lock"></span> Register</a>
                </li>
            @else
                <li>
                    <a class="hidden-xs" href="{{ url('account') }}">
                        {{ Auth::user()->strAccountID }} | <span id="KnightCash">{{ Auth::user()->nKnightCash }}</span> KC
                    </a>
                    <a style="display: inline-block;"
                       href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                        <span class="glyphicon glyphicon-log-out"></span> Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            @endif
        </ul>
    </div>
</div>

<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand visible-xs" href="{{ url('/') }}">
                KO-MyKO
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav navbar-left">
                &nbsp;<li class="dropdown">
                    <a href="{{url('account/recharge')}}">GET KC</a>
                </li>
                <li class="dropdown">
                    <a href="{{url('rankings')}}">RANKINGS</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">GUIDES</a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{url('droplist')}}">Drop List</a></li>
                        <li><a href="{{url('questlist')}}">Quests</a></li>
                        <li><a href="{{url('exchanges')}}">Exchanges</a></li>
                        <li><a href="{{url('events')}}">Events</a></li>
                        <li><a href="{{url('upgrade_rates')}}">Upgrade Rates</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="{{url('search')}}">SEARCH</a>
                </li>
                <li class="dropdown">
                    <a href="{{url('downloads')}}">DOWNLOADS</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">COMMUNITY</a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="https://facebook.com/bazingako">Facebook</a></li>
                        <li><a href="https://Forum.knightonline-myko.com">Forums</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>