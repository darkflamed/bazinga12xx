<div class="container-fluid container-footer">
    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="list-inline">
                        <li><a href="{{ url('tac') }}">Terms and Conditions</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
</div>