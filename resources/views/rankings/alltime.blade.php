@extends('global.main')

@section('more-js')

@stop

@section('more-css')

@stop

@section('title')
    All Time Rankings
@stop

@section('description')

@stop

@section('keywords')

@stop

@section('content')
    <div class="container">
        <div class="col-xs-8 col-xs-offset-2">
            <div class="col-xs-6"><a href="{{url('rankings')}}"><h3>Monthly Rankings</h3></a></div>
            <div class="col-xs-6"><a href="{{url('rankings/clan')}}"><h3>Clan Rankings</h3></a></div>
        </div>
    </div>
    <article id="content" class="well padding0 margin0">
        <div class="container_3 account_sub_header">
            <div class="grad">
                <center><div id="title"><h1>ALL TIME RANKINGS<p></p><span></span></h1></div></center>
            </div>
        </div>
        <div class="padding20">
            <table class="table table-striped col-xs-12">
                <thead>
                <tr bgcolor="#25120B" class="textwhite">
                    <td width="5%" align="center"></td>
                    <td width="5%" align="center"></td>
                    <!--<td width="5%" align="center"></td>-->
                    <td width="20%" align="center">UserName</td>
                    <td width="20%" align="center">Class</td>
                    <td width="5%" align="center">Lv</td>
                    <td width="20%" align="center">Clan</td>
                    <td width="20%" align="center">Points</td>
                </tr>
                </thead>
                <tbody>
                @foreach($Users as $index => $char)
                    <tr>
                        <td align="center"> <font color="#e7dbc3">{{$index + 1}}</font></td>
                        <td align="center"><img src="/img/{{$char->Nation}}.gif"></td>
                        <!--<td align="center"><img src="/img/symbol/k001.gif"></td>-->
                        <td align="center"><a href="{{url('character/'.$char->strUserId)}}">{{$char->strUserId}}</a></td>
                        <td align="center"><font color="#e7dbc3">{{Template::getClassName($char->Class)}}</font></td>
                        <td align="center"><font color="#e7dbc3">{{$char->Level}}</font></td>
                        <td align="center"><a href="{{url('clan/'.$char->IDNum)}}">{{$char->IDName}}</a></td>
                        <td align="center"><font color="#a6e687">{{number_format($char->Loyalty)}}</font></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </article>
@stop