<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>In-Game Ranking KO-MyKO</title>
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}" type="shortcut icon" />
</head>

<style>
    body {
        padding: 0px;
        margin: 0px;
        font-weight: bold;
        cursor: default;
        font-size: 16px;
    }
    .container {
        background: #0f1105 url('{{asset('images/ingame_rank.jpg')}}') no-repeat center center;
        width: 886px;
        height: 414px;
        font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
        position: absolute;
    }

    .Karus {
        top: 68px;
        left: 39px;
    }

    .Elmorad {
        top: 68px;
        left: 469px;
    }

    .Rankings {
        color: #e7ffe0;
        position: absolute;
        font-size: 12px;
        background-color: rgba(0, 0, 0, 0.8);
        -webkit-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
        -moz-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
        box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
    }

    .bottom-text {
        color: #fffe91;
        position: absolute;
        top: 381px;
        left: 67px;
        background-color: rgba(0, 0, 0, 0.8);
        border: 1px solid #aa8c70;
        padding: 3px;
    }

    .character-info {
        color: #e7ffe0;
        position: absolute;
        top: 353px;
        left: 39px;
        background-color: rgba(0, 0, 0, 0.8);
    }

    thead > tr {
        color: #ffffff;
    }

    td {
        border: 1px solid #aa8c70;
    }
</style>

<body>
<div class="container">
    <div class="Karus Rankings">
        <table cellspacing="0">
            <thead>
            <tr>
                <td height="22px" width="26px" align="center">#</td>
                <td width="122px">Character</td>
                <td width="132px">Clan</td>
                <td width="88px">NP</td>
            </tr>
            </thead>
            <tbody>
            <?php
            if($KarusUserRankings->count() < 10) {
                $k = 10 - $KarusUserRankings->count();
                for($i = 0; $i < $k; $i++) {
                    $Character = New App\Character;
                    $Character->Nation = 1;
                    $Character->strUserID = '';
                    $Character->LoyaltyMonthly = 0;

                    $KarusUserRankings->push($Character);
                }
            }
            ?>
            @foreach($KarusUserRankings as $index => $ranking)
                <tr valign="middle">
                    <td height="21px" align="center" id="sRankPos">{{$index+1}}&nbsp;</td>
                    <td>{{$ranking->strUserId}}</td>
                    <td align="left" id="sRankLoyalty">{{$ranking->IDName}}</td>
                    <td align="left" id="sRankLoyalty">{{number_format($ranking->LoyaltyToday)}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="Elmorad Rankings">
        <table cellspacing="0">
            <thead>
            <tr>
                <td height="22px" width="26px" align="center">#</td>
                <td width="122px">Character</td>
                <td width="132px">Clan</td>
                <td width="88px">NP</td>
            </tr>
            </thead>
            <tbody>
            <?php
            if($ElmoradUserRankings->count() < 10) {
                $k = 10 - $ElmoradUserRankings->count();
                for($i = 0; $i < $k; $i++) {
                    $Character = New App\Character;
                    $Character->Nation = 1;
                    $Character->strUserID = '';
                    $Character->LoyaltyMonthly = 0;

                    $ElmoradUserRankings->push($Character);
                }
            }
            ?>
            @foreach($ElmoradUserRankings as $index => $ranking)
                <tr valign="middle">
                    <td height="21px" align="center" id="sRankPos">{{$index+1}}&nbsp;</td>
                    <td>{{$ranking->strUserId}}</td>
                    <td align="left" id="sRankLoyalty">{{$ranking->IDName}}</td>
                    <td align="left" id="sRankLoyalty">{{number_format($ranking->LoyaltyToday)}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="character-info">
        <table cellspacing="0">
            <tbody>
                <tr valign="middle">
                    <td height="21px" width="231px">&nbsp;{{is_null($User)? '' : $User->strUserId}}</td>
                    <td width="364px">&nbsp;{{is_null($User)? '' : $User->IDName}}</td>
                    <td width="207px">&nbsp;{{is_null($User)? '' : number_format($User->LoyaltyToday)}}</td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="bottom-text">
        * Every 2 hours Top 1 PKers will receive a small reward - Leaderboard resets every day at midnight *
    </div>
</div>
</body>
</html>