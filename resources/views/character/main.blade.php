@extends('global.main')

@section('javascript')
    @if(Auth::check() && Auth::user()->is_admin)
        <script>
            window.Items.Initialize();

            function banHardware() {
                window.location = "{{ url('/ban/hardware/'.$User->id) }}";}

            function banAccount() {
                window.location = "{{ url('/ban/account/'.$User->id) }}";}

            function banCharacter() {
                window.location = "{{ url('/ban/character/'.$Character->id) }}";}

            function muteCharacter() {
                window.location = "{{ url('/mute/character/'.$Character->id) }}";}

            function unbanHardware() {
                window.location = "{{ url('/unban/hardware/'.$User->id) }}";}

            function unbanAccount() {
                window.location = "{{ url('/unban/account/'.$User->id) }}";}

            function unbanCharacter() {
                window.location = "{{ url('/unban/character/'.$Character->id) }}";}

            function unmuteCharacter() {
                window.location = "{{ url('/unmute/character/'.$Character->id) }}";}
        </script>
    @endif
@stop

@section('css')

@stop

@section('title')
    Character Information
@stop

@section('content')

<div class="container container-main">
    <table class="table table-character col-xs-12">
        <thead>
        <tr><td><h4>{{$Character->strUserId}} <img src="/images/{{$Character->Online}}.png" alt="({{$Character->Online}})" title="{{$Character->Online}}"></h4></td></tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <div class="well well-main row">
                    <div class="col-xs-6">
                    <h5>
                        <ul class="list-unstyled list-group row">
                            <li class="list-group-item col-xs-6"><b>@lang('fields.clan')</b></li>
                            <li class="list-group-item col-xs-6">@if(!is_null($Character->Knights))<a href="{{url('clan/'.$Character->Knights->IDName)}}">{{$Character->Knights->IDName}}</a>@else None @endif</li>
                            <li class="list-group-item col-xs-6"><b>@lang('fields.nation')</b></li>
                            <li class="list-group-item col-xs-6">{{Template::getNationName($Character->Nation)}}</li>
                            <li class="list-group-item col-xs-6"><b>@lang('fields.class')</b></li>
                            <li class="list-group-item col-xs-6">{{Template::getClassName($Character->Class)}}</li>
                            <li class="list-group-item col-xs-6"><b>@lang('fields.level')</b></li>
                            <li class="list-group-item col-xs-6">{{$Character->Level}}</li>
                            <li class="list-group-item col-xs-6"><b>@lang('fields.national_points')</b></li>
                            <li class="list-group-item col-xs-6">{{number_format($Character->Loyalty)}}</li>
                            <li class="list-group-item col-xs-6"><b>@lang('fields.monthly_np')</b></li>
                            <li class="list-group-item col-xs-6">{{number_format($Character->LoyaltyMonthly)}}</li>
                            <li class="list-group-item col-xs-6"><b>Country Flag</b></li>
                            <li class="list-group-item col-xs-6"><div class="country-select"><div class="flag {{$Flag}}"><div class="arrow"></div></div></div></li>
                            <li class="list-group-item col-xs-6"><b>@lang('fields.location')</b></li>
                            <li class="list-group-item col-xs-6">{{Template::getZoneName($Character->Zone)}} ({{$Character->PX}}, {{$Character->PZ}})</li>

                            <li class="list-group-item col-xs-12"> </li>

                            <li class="list-group-item col-xs-6"><b>@lang('fields.stats')</b></li>
                            <li class="list-group-item col-xs-6"> </li>

                            <li class="list-group-item col-xs-6"> </li>

                            <li class="list-group-item col-xs-3"><b><font color="#ffbf09">STR</font></b></li>
                            <li class="list-group-item col-xs-3">{{$Character->Strong}}</li>
                            <li class="list-group-item col-xs-3"><b><font color="#ffbf09">DEX</font></b></li>
                            <li class="list-group-item col-xs-3">{{$Character->Dex}}</li>
                            <li class="list-group-item col-xs-3"><b><font color="#ffbf09">INT</font></b></li>
                            <li class="list-group-item col-xs-3">{{$Character->Intel}}</li>
                            <li class="list-group-item col-xs-3"><b><font color="#ffbf09">HP</font></b></li>
                            <li class="list-group-item col-xs-3">{{$Character->Sta}}</li>
                            <li class="list-group-item col-xs-3"><b><font color="#ffbf09">MP</font></b></li>
                            <li class="list-group-item col-xs-3">{{$Character->Cha}}</li>
                        </ul>
                    </h5>
                    </div>

                    @if(Auth::check() && Auth::user()->is_admin)
                        <div class="col-xs-6">
                            <table class="inventory" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                <?php for ($i = 0; $i < 14; $i++) {
                                    $pItem = $pEquipped->get($i);
                                    if (is_null($pItem) || $pItem->GetNum() <= 0)
                                    {
                                        echo '<td width="45" height="45">&nbsp;</td>';
                                    } else { ?>
                                            <td width="45" height="45" onMouseOver="window.Items.showPopup(this, '{{$pItem->GetNum()}}');" onMouseOut="window.Items.hidePopup();">
                                <img src="{{ asset('images/itemicons/itemicon_'.$pItem->getIconID().'.jpg') }}" onMouseOver="window.Items.showBorder(this);" onMouseOut="window.Items.hideBorder(this);"
                                     width="43" height="43" style="border: 0px dotted white">
                                </td>
                                <?php }
                                    if(($i+1)%3 == 0) echo '</tr><tr>';
                                    }
                                    ?>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-xs-6">
                            <table class="inventory-bag" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <?php for ($i = 0; $i < 28; $i++) {
                                    $pItem = $pEquipped->get($i+14);
                                    if (is_null($pItem) || $pItem->GetNum() <= 0)
                                    {
                                        echo '<td width="43" height="43">&nbsp;</td>';
                                    } else { ?>
                                    <td width="43" height="43" onMouseOver="window.Items.showPopup(this, '{{$pItem->GetNum()}}');" onMouseOut="window.Items.hidePopup();">
                                        <img src="{{ asset('images/itemicons/itemicon_'.$pItem->getIconID().'.jpg') }}" onMouseOver="window.Items.showBorder(this);" onMouseOut="window.Items.hideBorder(this);"
                                             width="43" height="43" style="border: 0px dotted white">
                                    </td>
                                    <?php }
                                    if(($i+1)%7 == 0) echo '</tr><tr>';
                                    }
                                    ?>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="col-xs-6">
                            <img class="img-responsive center-block" src="{{ asset('images/characterclass/' . Template::getNationName($Character->Nation) . '_' . Template::getClassName($Character->Class) . '.png') }}">
                        </div>
                    @endif

                    <div class="col-xs-12">
                        @if(Auth::check() && Auth::user()->is_admin)
                            <span style="width: 100%; text-align: center;">
                                <h4>
                                    @if(!$isHardwareBanned)
                                        <button onClick="banHardware()" type="button" class="btn btn-main">Hardware Ban</button> &nbsp;&nbsp;&nbsp;&nbsp;
                                    @else
                                        <button onClick="unbanHardware()" type="button" class="btn btn-main">Hardware Un-Ban</button> &nbsp;&nbsp;&nbsp;&nbsp;
                                    @endif

                                    @if(!$isAccountBanned)
                                        <button onClick="banAccount()" type="button" class="btn btn-main">Ban Account</button> &nbsp;&nbsp;&nbsp;&nbsp;
                                    @else
                                        <button onClick="unbanAccount()" type="button" class="btn btn-main">Un-Ban Account</button> &nbsp;&nbsp;&nbsp;&nbsp;
                                    @endif

                                    @if($Character->Authority == 255)
                                        <button onClick="unbanCharacter()" type="button" class="btn btn-main">Un-Ban Character</button> &nbsp;&nbsp;&nbsp;&nbsp;
                                    @else
                                        <button onClick="banCharacter()" type="button" class="btn btn-main">Ban Character</button> &nbsp;&nbsp;&nbsp;&nbsp;
                                    @endif

                                    @if($Character->Authority == 11)
                                        <button onClick="unmuteCharacter()" type="button" class="btn btn-main">Un-Mute Character</button> &nbsp;&nbsp;&nbsp;&nbsp;
                                    @else
                                        <button onClick="muteCharacter()" type="button" class="btn btn-main">Mute Character</button> &nbsp;&nbsp;&nbsp;&nbsp;
                                    @endif
                                </h4>
                            </span><br><br>
                        @endif
                        <span style="width: 100%; text-align: center;"><h4>{{Template::getZoneName($Character->Zone)}} ({{$Character->PX}}, {{$Character->PZ}})</h4></span><br><br>
                    </div>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    @if(Auth::check() && Auth::user()->is_admin)
        {!! $presetItemData !!}
    @endif
</div>

@stop