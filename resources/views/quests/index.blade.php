@extends('global.main')

@section('javascript')
    <script>
        var popup;

        function getPosition(e)
        {
            var cursor = {x:0, y:0};
            cursor.x = (window.Event) ? e.pageX : event.clientX + (document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft);
            cursor.y = (window.Event) ? e.pageY : event.clientY + (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);

            return cursor;
        }

        function movePopup(e)
        {
            if (popup == null || popup == 'undefined')
                return;

            var cursor = getPosition(e);
            popup.style.left = (cursor.x + 10) + 'px';
            popup.style.top = (cursor.y + 10) + 'px';
        }

        function showBorder(element)
        {
            element.style.border = '1px dotted white';
        }

        function hideBorder(element)
        {
            element.style.border = '1px solid transparent';
        }

        function showPopup(element, i)
        {
            if (popup != null)
                hidePopup();

            popup = document.getElementById('item' + i);
            if(popup == null) return;
            popup.style.position = 'absolute';
            popup.style.display = 'block';
        }

        function hidePopup()
        {
            popup.style.display = 'none';
            popup.style.position = 'relative';
            popup = null;
        }

        if (window.Event && document.captureEvents && Event.MOUSEMOVE)
        {
            document.captureEvents(Event.MOUSEMOVE);
        }
        document.onmousemove = movePopup;
    </script>
@stop

@section('css')

@stop

@section('title')

@stop

@section('content')
    <div class="container container-main">
        <center><div id="title"><h1>QUEST LIST<p></p><span></span></h1></div></center>
        <table class="table table-striped">
            <thead>
            <tr>
                <td>Name</td>
                <td>Type</td>
                <td>Kills</td>
                <td>Zone</td>
                <td>NP</td>
                <td>XP</td>
                <td>MinLV</td>
                <td>MaxLV</td>
                <td>Repeat?</td>
                <td>Reward Items</td>
            </tr>
            </thead>
            <tbody><tr valign="middle">
            </tr>
            @foreach($Quests as $index => $quest)
                <tr align="right" valign="middle">
                    <td align="center">{{$quest->QuestName}}&nbsp;</td>
                    <td align="center">{{$quest->QuestInfo}}&nbsp;</td>
                    <td align="center">{{$quest->QuestObjectCount1}}&nbsp;</td>
                    <td align="center">{{$quest->QuestZone}}&nbsp;</td>
                    <td align="center">{{$quest->QuestRewardNP}}&nbsp;</td>
                    <td align="center">{{$quest->QuestRewardXP}}&nbsp;</td>
                    <td align="center">{{$quest->QuestMinLevel}}&nbsp;</td>
                    <td align="center">{{$quest->QuestMaxLevel}}&nbsp;</td>
                    <td align="center">{{$quest->QuestRepeatable == 1 ? 'True' : 'False'}}&nbsp;</td>
                    <td align="center">

                        @for($i=1; $i<6; $i++)
                            @if($quest['QuestRewardItem'.$i] > 0)
                                <img class="img-responsive" src="{{ asset('images/itemicons/itemicon_'.$pEquipped->Get($pEquipped->Find($quest['QuestRewardItem'.$i]))->GetIconID().'.jpg') }}"  onMouseOver="showBorder(this); showPopup(this, '{{$quest['QuestRewardItem'.$i]}}');" onMouseOut="hideBorder(this); hidePopup();"
                                     style="border: 0px dotted white; float:left; width: 30px; height: 30px;">
                            @endif
                        @endfor
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    {!! $presetItemData !!}
@stop
