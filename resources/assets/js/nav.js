let $ = require('jquery');

$( document ).ready(function() {
    $('ul.nav li.dropdown').on({
        mouseenter: function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
        },
        mouseleave: function () {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
        }
    });
});