class Items {
    static Initialize()
    {
        if (window.Event && document.captureEvents && Event.MOUSEMOVE)
        {
            document.captureEvents(Event.MOUSEMOVE);
        }
        document.onmousemove = window.Items.movePopup;
    }

    static getMonsterItems(url, sItem)
    {
        window.spinner.spin();
        $('#DropContainer').append(window.spinner.el);

        $.getJSON(  url + '/json_monster_drops/' + sItem, function( data ) {
            $('#Items').html('');
            window.Items.getItemArray(url, data, window.Items.loadMonsterItems);
        });
    }

    static loadMonsterItems(url, itemarray, options)
    {
        if(itemarray !== null) {
            window.Items.renderPresetItemData(url, itemarray);
            window.Items.renderItemIconList(url, itemarray);
        } else {
            alert('ItemArray is NULL!');
            window.ExchangeItemWait = false;
            window.spinner.stop();
        }
    }

    static getExchangeItems(url, npcNum, nExchangeItem)
    {
        window.spinner.spin();
        $('#ExchangeContainer').append(window.spinner.el);

        if(window.ExchangeItemWait === false) {
            window.ExchangeItemWait = true;

            $.getJSON(url + '/json_exchange_items/' + npcNum + '/' + nExchangeItem, function (data) {
                $('#Items').html('');
                window.Items.getItemArray(url, data, window.Items.loadExchangeItems);
            });
        }
    }

    static loadExchangeItems(url, itemarray, options)
    {
        if(itemarray !== null) {
            window.Items.renderPresetItemData(url, itemarray);
            window.Items.renderItemIconList(url, itemarray);
        } else {
            alert('ItemArray is NULL!');
            window.ExchangeItemWait = false;
            window.spinner.stop();
        }
    }

    static getCharacterItems(url)
    {
        window.spinner.spin();
        $('#CharacterContainer').append(window.spinner.el);

        for(let key in window.CharacterInfo) {
            if (!CharacterInfo.hasOwnProperty(key)) continue;
            let char = window.CharacterInfo[key];

            $('#CharacterInventoryData').append('<div id="CharacterInventoryData-'+key+'" class="col-xs-12"></div>');
            window.Items.getItemArray(url, char.items, window.Items.loadCharacterItems, key);
        }
    }

    static loadCharacterItems(url, itemarray, options)
    {
        if(itemarray !== null) {
            window.Items.renderPresetItemData(url, itemarray);
            window.Items.renderItemIconGear(url, itemarray, options);
        } else {
            alert('ItemArray is NULL!');
            window.ExchangeItemWait = false;
            window.spinner.stop();
        }
    }

    static getItemArray(url, ItemList, func, options)
    {
        if(ItemList.constructor !== Array) { alert(ItemList.constructor + ' Not an Array!'); return false; }

        axios.post(url + '/json_get_items', {
            items : ItemList.join('|')
        })
        .then(function (response) {
            func(url, response.data, options);
        })
        .catch(function (error) {
            console.log(error);
            return false;
        });

        return true;
    }

    static renderPresetItemData(url, ItemArray)
    {
        if(ItemArray.constructor !== Array) { alert(ItemArray.constructor + ' Not an Array!'); return; }

        let ItemData = '';

        ItemArray.forEach(function(Item){
            if(!(document.getElementById('#item' + Item.Num))) {
                let ItemKind = window.Items.getItemTypeTwo(Item.ItemType, Item.Num);

                ItemData += '<div id="item'+Item.Num+'" class="itemInfo" align="left">'
                    + '<center>'
                    + '<div style="font-size: 14px;" id="itemType-'+ ItemKind +'">'+Item.strName+'</div>'
                    + '<div style="font-size: 12px;" id="itemType-'+ ItemKind +'">'+window.Items.getItemTypeName(ItemKind)+'</div>'
                    + '<div style="font-size: 11px;">'+window.Items.getItemKindName(ItemKind)+'</div>'
                    + '</center><br /><div style="font-size: 11px;">'
                    + window.Items.getItemExtendedInfo(Item)
                    + window.Items.getItemBonuses(Item)
                    + window.Items.getItemSetBonuses(Item)
                    + window.Items.getReqStats(Item)
                    + '<br /><center>' + Item.strDescription + '</center>'
                    + '</div></div>';
            }
        });

        $('#ItemInfo').append(ItemData);
    }

    static renderItemIconGear(url, ItemArray, character)
    {
        if(ItemArray.constructor !== Array) { alert(ItemArray.constructor + ' Not an Array!'); return; }

        let IconData = '<center><h3>'+character+'</h3></center>';
        IconData += '<table class="inventory float-left" cellspacing="0" cellpadding="0"><tbody>';

        for(let i=0; i<42; i++){
            let _item = window.CharacterInfo[character].items[i];
            let _serial = window.CharacterInfo[character].serials[i];

            if(i === 14)
                IconData += '</tbody></table><table class="inventory-bag float-left" cellspacing="0" cellpadding="0"><tbody>';

            if(i < 14) {
                if (i === 0 || i % 3 === 0) IconData += '<tr>';
            } else if(i === 14)
                IconData += '<tr>';
            else
                if(i === 0 || i % 7 === 0) IconData += '<tr>';

            if(_item === null || typeof _item === 'undefined' || _item === 0) {
                IconData += '<td style="width: 47px; height: 47px;">&nbsp;</td>';
            } else {
                let Item = null;
                ItemArray.forEach(function(key){
                    if(key.Num == _item) {
                        Item = key;
                    }
                });
                if(Item !== null && typeof Item !== 'undefined') {
                    IconData += '<td><div style="width: 47px; height: 47px; border: 1px dotted transparent; cursor: pointer;'
                        + 'background: url(\'' + url + '/images/itemicons/' + window.Items.getIconID(Item.IconID) + '\') 1px 0px no-repeat;"'
                        + ' onMouseOver="window.Items.showBorder(this); window.Items.showPopup(this, \'' + Item.Num + '\');"'
                        + ' onMouseOut="window.Items.hideBorder(this); window.Items.hidePopup();"'
                        + ' onClick="window.Items.toggleSeal(this, \''+character+'\', \''+_item+'\', \''+_serial+'\')">';

                    window.SealedItems.forEach(function(item){
                       if(_serial == item.ItemSerial)
                           IconData += '<div class="item-sealed"><span class="glyphicon glyphicon-certificate" style="top: 26px; left: 2px; font-size: 9px; cursor: pointer; position: relative;">SEALED</span></div>';
                    });

                    IconData += '</div></td>';
                }
            }

            if(i === 13)
                IconData += '<td i="'+i+'" style="width: 47px; height: 47px;">&nbsp;</td>';

            if(i < 14) {
                if ((i + 1) % 3 === 0) {
                    IconData += '</tr>';
                }
            } else
                if((i+1) % 7 === 0) IconData += '</tr>';
        }

        IconData += '</tbody></table>';

        //IconData = IconData.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');

        $('#CharacterInventoryData-'+character).append(IconData);

        window.ExchangeItemWait = false;
        window.spinner.stop();
    }

    static toggleSeal(element, character, num, serial)
    {
        if(!$(element).find('div').hasClass('item-sealed')) { // ADD
            let found = false;
            window.SealedItems.forEach(function(item){
                if(serial == item.ItemSerial) found = true;
            });

            window.RemoveSeal.forEach(function(item, index){
               if(serial == item.Serial) window.RemoveSeal.splice(index);
            });

            if(!found) {
                window.AddSeal.forEach(function(seal){
                    if(seal.Serial == serial) {
                        $(element).html('<div class="item-sealed"><span class="glyphicon glyphicon-certificate" style="top: 26px; left: 2px; font-size: 9px; cursor: pointer; position: relative;">SEALED</span></div>');
                        return;
                    }
                });

                window.AddSeal.push(
                    {
                        'Character': character,
                        'Num': num,
                        'Serial': serial,
                    }
                );
            }
            $(element).html('<div class="item-sealed"><span class="glyphicon glyphicon-certificate" style="top: 26px; left: 2px; font-size: 9px; cursor: pointer; position: relative;">SEALED</span></div>');
        } else { //REMOVE
            let found = false;
            window.SealedItems.forEach(function(item, index){
                if(serial == item.ItemSerial) {
                    found = true;
                }
            });

            window.AddSeal.forEach(function(item, index){
                if(item.Serial == serial)
                    window.AddSeal.splice(index);
            });

            window.RemoveSeal.forEach(function(item, index){
                if(item.Serial == serial) {
                    $(element).html('');
                    return;
                }
            });

            if(found)
                window.RemoveSeal.push(
                    {
                        'Character': character,
                        'Num': num,
                        'Serial': serial,
                    }
                );
            $(element).html('');
        }
    }

    static renderItemIconList(url, ItemArray)
    {
        if(ItemArray.constructor !== Array) { alert(ItemArray.constructor + ' Not an Array!'); return; }

        let IconData = '';

        ItemArray.forEach(function(Item){
            let IconID = window.Items.getIconID(Item.IconID);
            IconData +=
                '<img src="'+url+'/images/itemicons/' + window.Items.getIconID(Item.IconID) + '" onMouseOver="window.Items.showBorder(this); window.Items.showPopup(this, ' + Item.Num + ');" onMouseOut="window.Items.hideBorder(this); window.Items.hidePopup();" \
                        width="43" height="43" style="border: 0px dotted white; margin: 2px;">';
        });

        $('#Items').html(IconData);

        window.ExchangeItemWait = false;
        window.spinner.stop();
    }

    static getItemTypeTwo(Type, Num)
    {
        if (Type === null || Num.toString().substr(-3, 3) === '000')
            Type = 6;

        return Type;
    }

    static getIconID(IconID)
    {
        let Icon = 'itemicon_';
        if(IconID === null || typeof IconID === 'undefined') {
            IconID = '00000000';
        }
        Icon += IconID.toString().length < 9 ?
            IconID.toString().substr(0, 1) + '_' + IconID.toString().substr(1, 4) + '_' + IconID.toString().substr(5, 2) + '_' + IconID.toString().substr(7, 1) + '.jpg' :
            IconID.toString().substr(0, 2) + '_' + IconID.toString().substr(2, 4) + '_' + IconID.toString().substr(6, 2) + '_' + IconID.toString().substr(8, 1) + '.jpg';
        return Icon;
    }

    static getItemTypeName(Type)
    {
        switch(Type){
            case 0:
                return "(Upgrade Item)<br />";
            case 1:
                return "(Magic Item)<br />";
            case 2:
                return "(Rare Item)<br />";
            case 3:
                return "(Craft Item)<br />";
            case 4:
                return "(Unique Item)<br />";
            case 5:
                return "(Upgrade Item)<br />";
            case 6:
                return "";
            default:
                return "";
        }
    }

    static getItemKindName(Kind)
    {
        switch (Kind)
        {
            case 11:
                return 'DAGGER';
            case 21:
                return 'SWORD';
            case 22:
                return '2HSWORD';
            case 31:
                return 'AXE';
            case 32:
                return '2HAXE';
            case 41:
                return 'MACE';
            case 42:
                return '2HMACE';
            case 51:
                return 'SPEAR';
            case 52:
                return '2HSPEAR';
            case 60:
                return 'SHIELD';
            case 70:
                return 'BOW';
            case 71:
                return 'XBOW';
            case 91:
                return 'EARRING';
            case 92:
                return 'NECKLACE';
            case 93:
                return 'RING';
            case 94:
                return 'BELT';
            case 95: // quest items
                return '';
            case 97: // misc
                return '';
            case 98: // scrolls
                return '';
            case 110:
                return 'STAFF';
            case 210:
                return 'WARRIOR_ARMOR';
            case 220:
                return 'ROGUE_ARMOR';
            case 230:
                return 'MAGE_ARMOR';
            case 240:
                return 'PRIEST_ARMOR';
            case 255: // misc
                return '';
            default:
                return '';
        }
    }

    static getItemExtendedInfo(Item)
    {
        let ExtendedInfo = '';

        // weapons
        if ((Item.Kind >= 11 && Item.Kind <= 71 && Item.Kind !== 60) || Item.Kind === 110)
        {
            if(typeof Item.Damage === 'undefined') Item.Damage = 0;
            ExtendedInfo += 'Attack Power : '+Item.Damage.toString()+'<br />';

            if(typeof Item.Delay === 'undefined') Item.Delay = 0;
            ExtendedInfo += 'Attack Speed : '+(Item.Delay < 110 ? 'FAST' : (Item.Delay > 150 ? 'VERY_SLOW' : 'SLOW'))+'<br />';

            if(typeof Item.Range === 'undefined') Item.Range = 0;
            ExtendedInfo += 'Effective Range : '+(Item.Range/10).toFixed(2).toString()+'<br />';
        }

        // weapons, armors, misc
        if(typeof Item.Weight === 'undefined') Item.Weight = 0;
        if (((Item.Kind >= 11 && Item.Kind <= 71) || Item.Kind === 110) || (Item.Kind >= 210 && Item.Kind <= 240) || (Item.Kind === 97 || Item.Kind === 255))
            ExtendedInfo += 'Weight : '+(Item.Weight/10).toFixed(2).toString()+'<br />';


        // no longer misc
        if(typeof Item.Duration === 'undefined') Item.Duration = 0;
        if (((Item.Kind >= 11 && Item.Kind <= 71) || Item.Kind === 110) || (Item.Kind >= 210 && Item.Kind <= 240))
            ExtendedInfo += 'Max Durability : '+Item.Duration+'<br />';

        // armors
        if(typeof Item.Ac === 'undefined') Item.Ac = 0;
        if ((Item.Kind >= 210 && Item.Kind <= 240) || Item.Kind === 60 || Item.Ac > 0)
            ExtendedInfo += 'Defense Ability : '+Item.Ac+'<br />';

        return ExtendedInfo;
    }

    static getItemBonuses(Item)
    {
        let BonusInfo = '';

        if (Item.FireDamage > 0 || Item.IceDamage > 0 || Item.LightningDamage > 0 || Item.PoisonDamage > 0
            || Item.StrB > 0 || Item.StaB > 0 || Item.DexB > 0 || Item.IntelB > 0 || Item.ChaB > 0
            || Item.DaggerAc > 0 || Item.SwordAc > 0 || Item.MaceAc > 0 || Item.AxeAc > 0 || Item.SpearAc > 0 || Item.BowAc > 0
            || Item.HPDrain > 0 || Item.MPDamage > 0 || Item.MPDrain > 0 || Item.MaxHpB > 0 || Item.MaxMpB > 0
            || Item.FireR > 0 || Item.ColdR > 0 || Item.LightningR > 0 || Item.MagicR > 0 || Item.PoisonR > 0 || Item.CurseR > 0)
        {
            if (typeof Item.FireDamage !== 'undefined' && Item.FireDamage > 0)
                BonusInfo += 'Fire Damage : '+Item.FireDamage+'<br />';

            if (typeof Item.IceDamage !== 'undefined' && Item.IceDamage > 0)
                BonusInfo += 'Ice Damage : '+Item.IceDamage+'<br />';

            if (typeof Item.LightningDamage !== 'undefined' && Item.LightningDamage > 0)
                BonusInfo += 'Lightning Damage : '+Item.LightningDamage+'<br />';

            if (typeof Item.PoisonDamage !== 'undefined' && Item.PoisonDamage > 0)
                BonusInfo += 'Poison Damage : '+Item.PoisonDamage+'<br />';

            if (typeof Item.DaggerAc !== 'undefined' && Item.DaggerAc > 0)
                BonusInfo += 'Defense Ability (Dagger) : '+Item.DaggerAc+'<br />';

            if (typeof Item.SwordAc !== 'undefined' && Item.SwordAc > 0)
                BonusInfo += 'Defense Ability (Sword) : '+Item.SwordAc+'<br />';

            if (typeof Item.MaceAc !== 'undefined' && Item.MaceAc > 0)
                BonusInfo += 'Defense Ability (Club) : '+Item.MaceAc+'<br />';

            if (typeof Item.AxeAc !== 'undefined' && Item.AxeAc > 0)
                BonusInfo += 'Defense Ability (Ax) : '+Item.AxeAc+'<br />';

            if (typeof Item.SpearAc !== 'undefined' && Item.SpearAc > 0)
                BonusInfo += 'Defense Ability (Spear) : '+Item.SpearAc+'<br />';

            if (typeof Item.BowAc !== 'undefined' && Item.BowAc > 0)
                BonusInfo += 'Defense Ability (Bow) : '+Item.BowAc+'<br />';

            if (typeof Item.HPDrain !== 'undefined' && Item.HPDrain > 0)
                BonusInfo += 'HP Absorbed : '+Item.HPDrain+'<br />';

            if (typeof Item.MPDrain !== 'undefined' && Item.MPDrain > 0)
                BonusInfo += 'MP Absorbed : '+Item.MPDrain+'<br />';

            if (typeof Item.MPDamage !== 'undefined' && Item.MPDamage > 0)
                BonusInfo += 'MP Damage : '+Item.MPDamage+'<br />';

            if (typeof Item.MaxHpB !== 'undefined' && Item.MaxHpB > 0)
                BonusInfo += 'HP Bonus : '+Item.MaxHpB+'<br />';

            if (typeof Item.MaxMpB !== 'undefined' && Item.MaxMpB > 0)
                BonusInfo += 'MP Bonus : '+Item.MaxMpB+'<br />';

            if (typeof Item.StrB !== 'undefined' && Item.StrB > 0)
                BonusInfo += 'Strength Bonus : '+Item.StrB+'<br />';

            if (typeof Item.StaB !== 'undefined' && Item.StaB > 0)
                BonusInfo += 'Health Bonus : '+Item.StaB+'<br />';

            if (typeof Item.DexB !== 'undefined' && Item.DexB > 0)
                BonusInfo += 'Dexterity Bonus : '+Item.DexB+'<br />';

            if (typeof Item.IntelB !== 'undefined' && Item.IntelB > 0)
                BonusInfo += 'Intelligence Bonus : '+Item.IntelB+'<br />';

            if (typeof Item.ChaB !== 'undefined' && Item.ChaB > 0)
                BonusInfo += 'Magic Power Bonus : '+Item.ChaB+'<br />';

            if (typeof Item.FireR !== 'undefined' && Item.FireR > 0)
                BonusInfo += 'Resistance to Flame : '+Item.FireR+'<br />';

            if (typeof Item.ColdR !== 'undefined' && Item.ColdR > 0)
                BonusInfo += 'Resistance to Glacier : '+Item.ColdR+'<br />';

            if (typeof Item.LightningR !== 'undefined' && Item.LightningR > 0)
                BonusInfo += 'Resistance to Lightning : '+Item.LightningR+'<br />';

            if (typeof Item.MagicR !== 'undefined' && Item.MagicR > 0)
                BonusInfo += 'Resistance to Magic : '+Item.MagicR+'<br />';

            if (typeof Item.PoisonR !== 'undefined' && Item.PoisonR > 0)
                BonusInfo += 'Resistance to Poison : '+Item.PoisonR+'<br />';

            if (typeof Item.CurseR !== 'undefined' && Item.CurseR > 0)
                BonusInfo += 'Resistance to Curse : '+Item.CurseR+'<br />';

            BonusInfo = '<div id="itemBonuses">'+BonusInfo+'</div>';
        }

        return BonusInfo;
    }

    static getItemSetBonuses(Item)
    {
        let SetBonuses = '';

        if (typeof Item.ACBonus !== 'undefined' && Item.ACBonus > 0)
            SetBonuses += 'Defense Bonus: '+Item.ACBonus+'<br />';
        if (typeof Item.HPBonus !== 'undefined' && Item.HPBonus > 0)
            SetBonuses += 'HP Bonus: '+Item.HPBonus+'<br />';
        if (typeof Item.XPBonusPercent !== 'undefined' && Item.XPBonusPercent > 0)
            SetBonuses += 'XP Bonus Percent: %'+ (Item.XPBonusPercent * 100) +'<br />';
        if (typeof Item.CoinBonusPercent !== 'undefined' && Item.CoinBonusPercent > 0)
            SetBonuses += 'Coin Bonus Percent: %'+ (Item.CoinBonusPercent * 100) +'<br />';
        if (typeof Item.APBonusPercent !== 'undefined' && Item.APBonusPercent > 0)
            SetBonuses += 'Attack Power Bonus: %'+Item.APBonusPercent+'<br />';
        if (typeof Item.APBonusClassPercent !== 'undefined' && Item.APBonusClassPercent > 0)
            SetBonuses += 'Class Attack Power Bonus: %'+Item.APBonusClassPercent+'<br />';
        if (typeof Item.ACBonusClassPercent !== 'undefined' && Item.ACBonusClassPercent > 0)
            SetBonuses += 'Class Defense Power Bonus: %'+Item.ACBonusClassPercent+'<br />';

        return SetBonuses;
    }

    static getReqStats(Item)
    {
        let ReqStats = '';

        if (typeof Item.ReqStr !== 'undefined' && Item.ReqStr > 0)
            ReqStats += 'Required Strength : '+Item.ReqStr+'<br />';

        if (typeof Item.ReqSta !== 'undefined' && Item.ReqSta > 0)
            ReqStats += 'Required Health : '+Item.ReqSta+'<br />';

        if (typeof Item.ReqDex !== 'undefined' && Item.ReqDex > 0)
            ReqStats += 'Required Dexterity : '+Item.ReqDex+'<br />';

        if (typeof Item.ReqIntel !== 'undefined' && Item.ReqIntel > 0)
            ReqStats += 'Required Intelligence : '+Item.ReqIntel+'<br />';

        if (typeof Item.ReqCha !== 'undefined' && Item.ReqCha > 0)
            ReqStats += 'Required Magic Power : '+Item.ReqCha+'<br />';

        return ReqStats;
    }

    static getPosition(e)
    {
        window.cursorPOS = {x:0, y:0};
        window.cursorPOS.x = (window.Event) ? e.pageX : event.clientX + (document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft);
        window.cursorPOS.y = (window.Event) ? e.pageY : event.clientY + (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);

        return window.cursorPOS;
    }

    static movePopup(e)
    {
        if (window.popup === null || typeof window.popup === 'undefined')
            return;

        let cursor = window.Items.getPosition(e);
        window.popup.style.left = (cursor.x + 10) + 'px';
        window.popup.style.top = (cursor.y + 10) + 'px';
    }

    static showBorder(element)
    {
        element.style.border = '1px dotted white';
    }

    static hideBorder(element)
    {
        element.style.border = '1px solid transparent';
    }

    static showPopup(element, i)
    {
        if (window.popup !== null)
            window.Items.hidePopup();

        window.popup = document.getElementById('item' + i);
        if(window.popup === null) return;
        window.popup.style.position = 'absolute';
        window.popup.style.display = 'block';
    }

    static hidePopup()
    {
        window.popup.style.display = 'none';
        window.popup.style.position = 'relative';
        window.popup = null;
    }

    static searchTable(Name) {
        // Declare variables
        let input, filter, table, tr, td, i;
        input = document.getElementById("Search");
        filter = input.value.toUpperCase();
        table = document.getElementById(Name);
        tr = table.getElementsByTagName("tr");

        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                    continue;
                }
            }

            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                    continue;
                }
            }

            tr[i].style.display = "none";
        }
    }
}

export default Items;