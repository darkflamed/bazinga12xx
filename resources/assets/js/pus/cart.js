class Cart {
    static addToCart(num)
    {
        addToCart(num, 1);
    }

    static addToCart(num, qty)
    {
        axios.post('api/pus/cart/add', {
            items : num+','+qty
        })
            .then(function (response) {
                window.Dialog.showPopupMessage('Item Added to Cart', 'Item has '+response.data+' added to cart.', 'Ok');
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    static purchaseBuyItem(num)
    {
        axios.post('api/pus/cart/buy', {
            items : num+','+$('#BuyItemQuantity').html()
        })
            .then(function (response) {
                window.Dialog.showPopupMessage('Purchasing Item', response.data, 'Ok');
                window.Main.getKnightCash();
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    static cartDeleteItems()
    {
        let Items = [];

        $('.CartItem').each(function(index) {
            if($(this).is(':checked')) {
                Items.push($(this).val());
            }
        });

        axios.post('api/pus/cart/delete', {
            items : Items.join('|')
        })
            .then(function (response) {
                window.Main.getCart();
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    static cartBuyItems()
    {
        let Items = [];

        $('.CartItem').each(function(index) {
            if($(this).is(':checked')) {
                Items.push($(this).val());
            }
        });

        axios.post('api/pus/cart/buy', {
            items : Items.join('|'),
            cartbuy : '1'
        })
            .then(function (response) {
                window.Dialog.showPopupMessage('Purchasing Items', response.data, 'Ok');
                window.Main.getKnightCash();
                window.Main.getCart();
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    static cartModifyItem(num, qty)
    {
        axios.get('api/pus/cart/modify/'+num+'/'+qty)
            .then(function (response) {
                if(response.data == 'success')
                    window.Main.getCart();
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    static itemlistBuyItems()
    {
        let Items = [];

        $('.ListedItem').each(function(index) {
            if($(this).is(':checked')) {
                Items.push($(this).val() + ',1');
            }
        });

        axios.post('api/pus/cart/buy', {
            items : Items.join('|')
        })
            .then(function (response) {
                window.Dialog.showPopupMessage('Purchasing Items', response.data, 'Ok');
                window.Main.getKnightCash();
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    static itemlistCartItems()
    {
        let Items = [];

        $('.ListedItem').each(function(index) {
            if($(this).is(':checked')) {
                Items.push($(this).val() + ',1');
            }
        });

        if(Items.length <= 0) { alert('Please check items to add to the cart!'); return; }

        axios.post('api/pus/cart/add', {
            items : Items.join('|')
        })
            .then(function (response) {
                window.Dialog.showPopupMessage('Items Added to Cart', 'Items have '+response.data+' added to cart.', 'Ok');
            })
            .catch(function (error) {
                console.log(error);
            });

        $('.ListedItem').each(function(index) {
            $(this).prop('checked', false);
        });
    }
}

export default Cart;