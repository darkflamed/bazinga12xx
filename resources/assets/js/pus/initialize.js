class Initialize {

    static run() {
        $(".dropdown-menu li").on('click', 'a', function(){
            $(this).parents(".dropdown").find('.btn').html('<div id="dropdown-type-text">' + $(this).text() + ' </div><span class="caret"></span>');
            $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
        });

        $(".dropdown-listcount li").on('click', 'a', function(){
            ListCount = $(this).data('value');
            if($('#SearchPage').html() === 'true')
                window.Main.getStoreSearch(SearchText, NowPage);
            else
                window.Main.getStoreCategory(Category, NowPage);

        });

        $(".dropdown-searchcategory li").on('click', 'a', function(){
            let Category = $(this).data('value');
            $('#ItemSearchCategory').val( Category );
        });

        $(".dropdown-item-detail-quantity li").on('click', 'a', function(){
            let Quantity = $(this).data('value');
            $('#BuyItemQuantity').html(Quantity);
        });

        /*
         *  Overrides for submit forms
         */

        $('#ItemSearchForm').on("submit", function(event) {
            window.Main.SearchPowerupStore();
            return false;
        });

        $('#RechargeSubmit').on("submit", function(event) {
            //document.body.style.cursor = 'wait';
            axios.post('api/pus/recharge', {
                KCSelect: $('#KCSelect').val(),
                Service: $('#Service').val()
            })
                .then(function (response) {
                    $('#Content').html(response.data);
                    window.Initialize.run();
                    window.Dialog.closeDialog('dialog-recharge');
                })
                .catch(function (error) {
                    console.log(error);
                });

            return false;
        });
    }
}

export default Initialize;