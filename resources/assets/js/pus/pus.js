window.$ = window.jQuery = require('jquery');
import 'jquery-ui/ui/widgets/dialog';

import Initialize from './initialize.js';
window.Initialize = Initialize;

import Main from './main.js';
window.Main = Main;

import Dialog from './dialog.js';
window.Dialog = Dialog;

import Cart from './cart.js';
window.Cart = Cart;

/**
* Global Variables for the Powerup Store
*/

window.ListCount = 10;
window.SortType = 0;
window.Category = 0;
window.NowPage = 1;
window.SearchText = "";
window.SearchCategory = 0;
window.CheckoutJSLoaded = false;

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */
require('es6-promise').polyfill();
window.axios = require('axios');

window.axios.defaults.headers.common = {
    'X-CSRF-TOKEN': window.Laravel.csrfToken,
    'X-Requested-With': 'XMLHttpRequest',
    'Authorization': 'Bearer ' + window.Laravel.oAuthToken,
    'Accept': 'application/json',
};

import Echo from 'laravel-echo'

if(typeof(io) !== 'undefined') {
    window.Echo = new Echo({
        broadcaster: 'socket.io',
        host: window.location.hostname + ':6001',
        auth: {
            headers: {
                Authorization: 'Bearer ' + window.Laravel.oAuthToken,
            },
        },
    });
}
