class Main {

    static getHome()
    {
        axios.get('api/pus/main')
            .then(function (response) {
                $('#Content').html(response.data);
                window.Initialize.run();
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    static getCart()
    {
        axios.get('api/pus/cart')
            .then(function (response) {
                $('#Content').html(response.data);
                window.Initialize.run();
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    static getItemDetail(num) {
        axios.get("api/pus/itemdetail/" + num)
            .then(function (response) {
                $("#ItemDetailContent").html(response.data), $("#dialog-item-detail").dialog({
                    minWidth: 460,
                    minHeight: 410,
                    open: function(event, ui) {
                        window.Initialize.run();
                    }
                })
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    static getStoreCategory(category, nowPage) {
        nowPage = typeof nowPage !== 'undefined' ? nowPage : 1;

        axios.post('api/pus/category', {
            category: category,
            nowPage: nowPage,
            sortType: window.SortType,
            listCount: window.ListCount
        })
            .then(function (response) {
                $('#Content').html(response.data);
                window.Initialize.run();
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    static getStoreSearch(searchText, nowPage)
    {
        nowPage = typeof nowPage !== 'undefined' ? nowPage : 1;

        axios.post('api/pus/search', {
            searchText : searchText,
            nowPage : nowPage,
            sortType : SortType,
            listCount : ListCount
        })
            .then(function (response) {
                $('#Content').html(response.data);
                window.Initialize.run();
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    static SearchPowerupStore()
    {
        let URL = 'api/' + $('#ItemSearchForm').attr('action');
        let NameDesc = $('#ItemSearchForm').find('input[name="namedesc"]:checked').val();
        let SearchText = $('#ItemInput').val();

        axios.post(URL, {
            nameDesc: NameDesc,
            searchText: SearchText,
            category: window.SearchCategory,
            nowPage: window.NowPage,
            sortType: window.SortType,
            listCount: window.ListCount
        })
            .then(function (response) {
                $('#Content').html(response.data);
                window.Initialize.run();
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    static resetCheckBoxes()
    {
        $('input:checkbox').removeAttr('checked');
    }

    static getKnightCash() {
        axios.get('api/pus/knightcash')
            .then(function (response) {
                $("#KnightCash").html(response.data.nKnightCash);
                $("#FreeSlots").html(response.data.Slots);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    static getRechargeForm() {
        axios.get('api/pus/recharge')
            .then(function (response) {
                $("#Content").html(response.data);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    static getEnglishRecharge() {
        axios.get('api/pus/recharge/english')
            .then(function (response) {
                $("#Content").html(response.data);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    static getSpanishRecharge() {
        axios.get('api/pus/recharge/spanish')
            .then(function (response) {
                $("#Content").html(response.data);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    static getTurkishRecharge() {
        axios.get('api/pus/recharge/turkish')
            .then(function (response) {
                $("#Content").html(response.data);
            })
            .catch(function (error) {
                console.log(error);
            });
    }
}

export default Main;