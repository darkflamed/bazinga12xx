class Dialog {

    static closeDialog(dialog)
    {
        $('#'+dialog).dialog("close");
    }

    static showPopupMessage(title, content, button)
    {
        let DialogMessage = $('#dialog-message');

        DialogMessage.attr('title', title);
        $('#dialog-message-text').html(content);
        $('#dialog-message-button').html(button);

        DialogMessage.dialog({
            minWidth: 460,
            minHeight: 180
        });
    }

    static showRechargeDialog()
    {
        $( "#dialog-recharge" ).dialog({
            minWidth: 460,
            minHeight: 340
        });
    }
}

export default Dialog;