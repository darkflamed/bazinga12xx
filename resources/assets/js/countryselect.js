let $ = require('jquery');
let cs = require('country-select-js');

$( document ).ready(function() {
    window.setCountrySelector = function(selector) {
        $(selector).countrySelect({
            'responsiveDropdown': true
        });
    };
});