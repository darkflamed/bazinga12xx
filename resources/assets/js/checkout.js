class Checkout {
    static renderPaypalButton(environment) {
        paypal.Button.render({

            env: environment, // sandbox | production

            // Specify the style of the button

            style: {
                label: 'generic',
                size: 'medium',    // small | medium | large | responsive
                shape: 'rect',     // pill | rect
                color: 'black',     // gold | blue | silver | black
                fundingicons: true, // optional
                branding: true, // optional
                tagline: false
            },

            // payment() is called when the button is clicked
            payment: function () {
                // Make a call to your server to set up the payment
                let paymentData = {
                    Service: 0,
                    KCSelect: $('#KCSelect').val()
                };

                return axios.post('/api/recharge/paypal/payment/create', paymentData)
                    .then(function (res) {
                        return res.data.id;
                    });
            },

            // onAuthorize() is called when the buyer approves the payment
            onAuthorize: function (data, actions) {

                // Set up the data you need to pass to your server
                let authorizeData = {
                    paymentID: data.paymentID,
                    payerID: data.payerID
                };

                // Make a call to your server to execute the payment
                return axios.post('/api/recharge/paypal/payment/execute', authorizeData)
                    .then(function (res) {
                        $('#Content').html(res.data);
                        window.alert('Payment Complete!');
                    });
            }

        }, '#paypal-button-container');
    }

    static paygolCreatePayment()
    {
        console.log(window.Laravel);
        axios.post('/api/recharge/paygol/payment/create', {KCSelect: $('#KCSelect').val()})
            .then(function (response) {
                let url = 'https://www.paygol.com/pay?';
                url += 'pg_serviceid=' + response.data.pg_serviceid;
                url += '&pg_currency=' + response.data.pg_currency;
                url += '&pg_name=' + response.data.pg_name;
                url += '&pg_price=' + response.data.pg_price;
                url += '&pg_custom=' + response.data.pg_custom;
                url += '&pg_return_url=' + response.data.pg_return_url;
                url += '&pg_cancel_url=' + response.data.pg_cancel_url;

                window.location = url;
            })
            .catch(function (error) {
                console.log(error);
            });
    }
}

export default Checkout;