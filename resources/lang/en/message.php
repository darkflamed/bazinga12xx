<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Message Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'please_input_account_details' => 'Please input account details',
    'gender_change' => 'Gender Change',
    'problem_with_input' => 'There was a problem with your input'

];