<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Titles Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'change_password' => 'Change Password',
    'clan_information' => 'Clan Details'
];