<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Fields Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'clan' => 'Clan',
    'nation' => 'Nation',
    'class' => 'Class',
    'level' => 'Level',
    'national_points' => 'National Points',
    'monthly_np' => 'Monthly NP',
    'location' => 'Location',
    'stats' => 'Stats',
    'character_info' => 'Character Info',
    'old_password' => 'Old Password',
    'new_password' => 'New Password',
    'confirm_new_password' => 'Confirm New Password',
    'secret_pin' => 'Secret PIN',
];