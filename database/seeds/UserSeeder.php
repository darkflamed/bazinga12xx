<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('TB_USER')->insert([
            'email' => 'darkflame9013@gmail.com',
            'password' => '2c014fd23813d241eb05765a4974d920',
            'strAccountID' => 'Darkflamed',
            'is_super_admin' => true,
            'is_admin' => true,
            'verified' => true,
            'pin' => 1234,
            'nKnightCash' => 1000000
        ]);

        DB::table('TB_USER')->insert([
            'email' => 'ziga.pacnik@gmail.com',
            'password' => '4ead5155ee1e3882037fe4781904b023',
            'strAccountID' => 'paczor1',
            'is_super_admin' => true,
            'is_admin' => true,
            'verified' => true,
            'pin' => 1234,
            'nKnightCash' => 1000000
        ]);

        DB::table('TB_USER')->insert([
            'email' => 'karusleader@localhost.com',
            'password' => '4ead5155ee1e3882037fe4781904b023',
            'strAccountID' => 'zigolo1',
            'is_super_admin' => true,
            'is_admin' => true,
            'verified' => true,
            'pin' => 1234,
            'nKnightCash' => 1000000
        ]);

        DB::table('TB_USER')->insert([
            'email' => 'humanleader@localhost.com',
            'password' => '4ead5155ee1e3882037fe4781904b023',
            'strAccountID' => 'zigolo2',
            'is_super_admin' => true,
            'is_admin' => true,
            'verified' => true,
            'pin' => 1234,
            'nKnightCash' => 1000000
        ]);

        DB::table('TB_USER')->insert([
            'email' => 'klemko1@localhost.com',
            'password' => '4ead5155ee1e3882037fe4781904b023',
            'strAccountID' => 'klemko1',
            'is_super_admin' => true,
            'is_admin' => true,
            'verified' => true,
            'pin' => 1234,
            'nKnightCash' => 1000000
        ]);
    }
}
