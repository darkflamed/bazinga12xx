<?php

use Illuminate\Database\Seeder;

class PowerupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('store_items')->insert([
            'name' => 'Trina\'s Piece',
            'num' => 700002000,
            'icon_id' => '70000200',
            'price' => 400,
            'event_price' => 0,
            'desc_1' => 'Increase upgrade success rate',
            'desc_2' => '',
            'category_id' => 5,
            'hot_item' => 1,
            'new_item' => 1,
            'recommending_item' => 1
        ]);

        DB::table('store_items')->insert([
            'name' => 'Silver Premium',
            'num' => 700051000,
            'icon_id' => '700051000',
            'price' => 800,
            'event_price' => 0,
            'desc_1' => 'Description on login screen',
            'desc_2' => '',
            'category_id' => 6,
            'hot_item' => 2,
            'new_item' => 2,
            'recommending_item' => 2
        ]);

        DB::table('store_items')->insert([
            'name' => 'Priest Cap +3',
            'num' => 281003073,
            'icon_id' => '28100123',
            'price' => 800,
            'event_price' => 0,
            'desc_1' => 'Priest Weapon Defense Armor',
            'desc_2' => '',
            'category_id' => 3,
            'hot_item' => 2,
            'new_item' => 2,
            'recommending_item' => 2
        ]);

        DB::table('store_items')->insert([
            'name' => 'Shard +1',
            'num' => 111211001,
            'icon_id' => '11121000',
            'price' => 800,
            'event_price' => 0,
            'desc_1' => 'Rogue Melle Weapon',
            'desc_2' => '',
            'category_id' => 4,
            'hot_item' => 0,
            'new_item' => 0,
            'recommending_item' => 0
        ]);

        DB::table('store_items')->insert([
            'name' => 'Shard +2',
            'num' => 111211002,
            'icon_id' => '11121000',
            'price' => 800,
            'event_price' => 0,
            'desc_1' => 'Rogue Melle Weapon',
            'desc_2' => '',
            'category_id' => 2,
            'hot_item' => 0,
            'new_item' => 0,
            'recommending_item' => 0
        ]);

        DB::table('store_items')->insert([
            'name' => 'Shard +3',
            'num' => 111211003,
            'icon_id' => '11121000',
            'price' => 800,
            'event_price' => 0,
            'desc_1' => 'Rogue Melle Weapon',
            'desc_2' => '',
            'category_id' => 1,
            'hot_item' => 0,
            'new_item' => 0,
            'recommending_item' => 0
        ]);

        DB::table('store_items')->insert([
            'name' => 'Shard +4',
            'num' => 111211004,
            'icon_id' => '11121000',
            'price' => 800,
            'event_price' => 0,
            'desc_1' => 'Rogue Melle Weapon',
            'desc_2' => '',
            'category_id' => 1,
            'hot_item' => 0,
            'new_item' => 0,
            'recommending_item' => 0
        ]);

        DB::table('store_items')->insert([
            'name' => 'Shard +5',
            'num' => 111211005,
            'icon_id' => '11121000',
            'price' => 800,
            'event_price' => 0,
            'desc_1' => 'Rogue Melle Weapon',
            'desc_2' => '',
            'category_id' => 2,
            'hot_item' => 0,
            'new_item' => 0,
            'recommending_item' => 0
        ]);

        DB::table('store_items')->insert([
            'name' => 'Shard +6',
            'num' => 111211006,
            'icon_id' => '11121000',
            'price' => 800,
            'event_price' => 0,
            'desc_1' => 'Rogue Melle Weapon',
            'desc_2' => '',
            'category_id' => 2,
            'hot_item' => 0,
            'new_item' => 0,
            'recommending_item' => 0
        ]);

        DB::table('store_items')->insert([
            'name' => 'Shard +7',
            'num' => 111211007,
            'icon_id' => '11121000',
            'price' => 800,
            'event_price' => 0,
            'desc_1' => 'Rogue Melle Weapon',
            'desc_2' => '',
            'category_id' => 2,
            'hot_item' => 0,
            'new_item' => 0,
            'recommending_item' => 0
        ]);

        DB::table('store_items')->insert([
            'name' => 'Shard +8',
            'num' => 111211008,
            'icon_id' => '11121000',
            'price' => 800,
            'event_price' => 0,
            'desc_1' => 'Rogue Melle Weapon',
            'desc_2' => '',
            'category_id' => 2,
            'hot_item' => 0,
            'new_item' => 0,
            'recommending_item' => 0
        ]);

        DB::table('store_items')->insert([
            'name' => 'Raptor +1',
            'num' => 156240041,
            'icon_id' => '15621000',
            'price' => 800,
            'event_price' => 0,
            'desc_1' => 'Warrior Weapon',
            'desc_2' => '',
            'category_id' => 2,
            'hot_item' => 0,
            'new_item' => 0,
            'recommending_item' => 0
        ]);

        DB::table('store_items')->insert([
            'name' => 'Raptor +2',
            'num' => 156240042,
            'icon_id' => '15621000',
            'price' => 800,
            'event_price' => 0,
            'desc_1' => 'Warrior Weapon',
            'desc_2' => '',
            'category_id' => 2,
            'hot_item' => 0,
            'new_item' => 0,
            'recommending_item' => 0
        ]);

        DB::table('store_items')->insert([
            'name' => 'Raptor +3',
            'num' => 156240043,
            'icon_id' => '15621000',
            'price' => 800,
            'event_price' => 0,
            'desc_1' => 'Warrior Weapon',
            'desc_2' => '',
            'category_id' => 2,
            'hot_item' => 0,
            'new_item' => 0,
            'recommending_item' => 0
        ]);

        DB::table('store_items')->insert([
            'name' => 'Raptor +4',
            'num' => 156240044,
            'icon_id' => '15621000',
            'price' => 800,
            'event_price' => 0,
            'desc_1' => 'Warrior Weapon',
            'desc_2' => '',
            'category_id' => 2,
            'hot_item' => 0,
            'new_item' => 0,
            'recommending_item' => 0
        ]);

        DB::table('store_items')->insert([
            'name' => 'Raptor +5',
            'num' => 156240045,
            'icon_id' => '15621000',
            'price' => 800,
            'event_price' => 0,
            'desc_1' => 'Warrior Weapon',
            'desc_2' => '',
            'category_id' => 2,
            'hot_item' => 0,
            'new_item' => 0,
            'recommending_item' => 0
        ]);

        DB::table('store_items')->insert([
            'name' => 'Raptor +6',
            'num' => 156240046,
            'icon_id' => '15621000',
            'price' => 800,
            'event_price' => 0,
            'desc_1' => 'Warrior Weapon',
            'desc_2' => '',
            'category_id' => 2,
            'hot_item' => 0,
            'new_item' => 0,
            'recommending_item' => 0
        ]);

        DB::table('store_items')->insert([
            'name' => 'Raptor +7',
            'num' => 156240047,
            'icon_id' => '15621000',
            'price' => 800,
            'event_price' => 0,
            'desc_1' => 'Warrior Weapon',
            'desc_2' => '',
            'category_id' => 2,
            'hot_item' => 0,
            'new_item' => 0,
            'recommending_item' => 0
        ]);

        DB::table('store_items')->insert([
            'name' => 'Raptor +8',
            'num' => 156240048,
            'icon_id' => '15621000',
            'price' => 800,
            'event_price' => 0,
            'desc_1' => 'Warrior Weapon',
            'desc_2' => '',
            'category_id' => 2,
            'hot_item' => 0,
            'new_item' => 0,
            'recommending_item' => 0
        ]);

        DB::table('store_items')->insert([
            'name' => 'Iron Bow +1',
            'num' => 168440001,
            'icon_id' => '16841000',
            'price' => 800,
            'event_price' => 0,
            'desc_1' => 'Rogue Range Weapon',
            'desc_2' => '',
            'category_id' => 2,
            'hot_item' => 0,
            'new_item' => 0,
            'recommending_item' => 0
        ]);

        DB::table('store_items')->insert([
            'name' => 'Iron Bow +2',
            'num' => 168440002,
            'icon_id' => '16841000',
            'price' => 800,
            'event_price' => 0,
            'desc_1' => 'Rogue Range Weapon',
            'desc_2' => '',
            'category_id' => 2,
            'hot_item' => 0,
            'new_item' => 0,
            'recommending_item' => 0
        ]);

        DB::table('store_items')->insert([
            'name' => 'Iron Bow +3',
            'num' => 168440003,
            'icon_id' => '16841000',
            'price' => 800,
            'event_price' => 0,
            'desc_1' => 'Rogue Range Weapon',
            'desc_2' => '',
            'category_id' => 2,
            'hot_item' => 0,
            'new_item' => 0,
            'recommending_item' => 0
        ]);

        DB::table('store_categories')->insert([
            'id' => '1',
            'name' => 'Cospre'
        ]);

        DB::table('store_categories')->insert([
            'id' => '2',
            'name' => 'Weapon/Shield'
        ]);
        DB::table('store_categories')->insert([
            'id' => '3',
            'name' => 'Armor'
        ]);
        DB::table('store_categories')->insert([
            'id' => '4',
            'name' => 'Defense'
        ]);
        DB::table('store_categories')->insert([
            'id' => '5',
            'name' => 'Upgrade'
        ]);
        DB::table('store_categories')->insert([
            'id' => '6',
            'name' => 'Misc/Premium'
        ]);
    }
}
