<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TB_USER', function (Blueprint $table) {
            $table->string('password_pus')->nullable();
            $table->integer('inventory_slots')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TB_USER', function (Blueprint $table) {
            $table->dropColumn('password_pus');
            $table->dropColumn('inventory_slots');
        });
    }
}
