<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHardwareLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('HardwareLog', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id');
            $table->string('HWID', 32);
            $table->timestamps();
        });

        Schema::table('HardwareLog', function(Blueprint $table){
            $sql = 'create unique index "unique_account_HWID" on "HardwareLog" ("account_id", "HWID") WITH (IGNORE_DUP_KEY = ON)';
            DB::connection()->getPdo()->exec($sql);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('HardwareLog');
    }
}
