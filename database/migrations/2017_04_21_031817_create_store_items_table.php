<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedBigInteger('num');
            $table->unsignedInteger('icon_id');
            $table->integer('price')->default(999999);
            $table->integer('event_price')->default(0);
            $table->integer('count')->default(1);
            $table->integer('expire_days')->default(0);
            $table->string('desc_1');
            $table->string('desc_2');
            $table->integer('limit_total')->default(0);
            $table->integer('category_id');
            $table->integer('hot_item')->default(0);
            $table->integer('new_item')->default(0);
            $table->integer('recommending_item')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_items');
    }
}
