<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSoacsLoginStoredProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            /****** Object:  StoredProcedure [dbo].[SOACS_LOGIN]    Script Date: ".Carbon\Carbon::now()." ******/
            
            ALTER PROCEDURE [dbo].[SOACS_LOGIN]
                @AccountID varchar(21),
                @Password varchar(18),
                @IpAddress	varchar(50),
                @HwID		varchar(50),
                @nRet		smallint	OUTPUT
            AS
            
            SET ANSI_NULLS ON
            SET QUOTED_IDENTIFIER ON
            
            if(exists(select * from HardwareBanList where ltrim(rtrim(HWID)) = ltrim(rtrim(@HwID))))
            begin
                -- Blocked HardwareID
                SET @nRet = 4
                RETURN
            end
            
            DECLARE @Nation tinyint, @CharNum smallint, @account_id int
            DECLARE @HashedPassword varchar(32)
            DECLARE @pwd varchar(32)
            
            SET @Nation = 0
            SET @CharNum = 0
            SET @pwd = null
            SET @HashedPassword = null
            SET @account_id = 0
            
            SELECT @pwd = password, @account_id = id FROM [dbo].[TB_USER] WHERE strAccountID = @AccountID
            SET @HashedPassword = CONVERT(VARCHAR(32), HashBytes('MD5', CONCAT('b@z', @Password, '1n6a')), 2)
            IF @pwd IS null
            BEGIN
             SET @nRet = 0
             RETURN
            END
            ELSE IF @pwd <> @HashedPassword
            BEGIN
             SET @nRet = 0
             RETURN
            END
            --gift status control
            Declare @giftcount int,@Status int,@level int,@charID varchar(21)
            SELECT @giftcount = Count(strAccountID) FROM USER_GIFT_STATUS WHERE strAccountID= @AccountID
            IF @giftcount>0
            BEGIN
            --SELECT @Status = [Status] FROM USER_GIFT_STATUS WHERE strAccountID= @AccountID
            --SELECT @charID = strCharID1 FROM ACCOUNT_CHAR where strAccountID = @AccountID
            --SELECT @level = [Level] FROM USERDATA where strUserId = @charID
            --IF @Status=1 and @level > 64 and @level < 70
            --BEGIN
            --INSERT INTO PREMIUM_SERVICE VALUES (@AccountID,1,3,getdate())
            --END
            --END
            --ELSE
            --BEGIN
            INSERT INTO PREMIUM_SERVICE VALUES (@AccountID,1,3,getdate())
            INSERT INTO USER_GIFT_STATUS VALUES (@AccountID,1,getdate())
            END
            --gift control end here
            --CurrentUserdan Silelim
            delete from currentuser where straccountid = @AccountID
            --Currentuser bitis
            ---Login bilgilerini kaydet
            insert into HardwareLog (account_id, HWID, updated_at, created_at) values (@account_id, ltrim(rtrim(@HwID)), getdate(), getdate())
            --Login bilgileri bitis
            SELECT @Nation = bNation, @CharNum = bCharNum FROM ACCOUNT_CHAR WHERE strAccountID = @AccountID
            IF @@ROWCOUNT = 0
            BEGIN
             SET @nRet = 1
             RETURN
            END
            IF @CharNum >= 0
            BEGIN
             SET @nRet = 1
             RETURN
            END
            ELSE
            BEGIN
             SET @nRet = @Nation+1
             RETURN
            END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
