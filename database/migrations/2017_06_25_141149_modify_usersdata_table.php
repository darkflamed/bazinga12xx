<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUsersdataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $keyExists = DB::select(DB::raw("SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_NAME='PK_UserData'"));

        if(sizeof($keyExists) > 0) {
            Schema::table('USERDATA', function (Blueprint $table) {
                $table->dropPrimary('PK_UserData');
            });
        }

        Schema::table('USERDATA', function (Blueprint $table) {
            //$table->increments('id');
            //$table->integer('account_id')->default(1);
            //$table->foreign('account_id')->references('id')->on('TB_USER');
            $table->unique('strUserId');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('USERDATA', function (Blueprint $table) {
            $table->dropColumn('id');
            $table->dropColumn('account_id');
        });
    }
}
