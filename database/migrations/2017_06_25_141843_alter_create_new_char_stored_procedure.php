<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCreateNewCharStoredProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("
            /****** Object:  StoredProcedure [dbo].[CREATE_NEW_CHAR]    Script Date: ".Carbon\Carbon::now()." ******/
            
            ALTER PROCEDURE [dbo].[CREATE_NEW_CHAR]  
              
            @nRet        smallint OUTPUT,  
            @AccountID     char(21),  
            @index         tinyint,  
            @CharID    varchar(21),  
            @Race          tinyint,
            @Class     smallint,
            @Hair         tinyint,  
            @Face         tinyint,  
            @Str         tinyint,  
            @Sta         tinyint,  
            @Dex         tinyint,  
            @Intel         tinyint,  
            @Cha         tinyint  
              
            AS  
            
            SET ANSI_NULLS ON
            SET QUOTED_IDENTIFIER ON
            
            -- Declare loop counter, and character var  
            DECLARE @i int, @c int  
            DECLARE @account_id int
              
            -- Set the loop counter to 1 (to start at the first character in the string; SUBSTRING() isn't zero-indexed from memory)  
            SET @i = 1  
              
            -- Loop until we've hit the end of the character name  
            WHILE (@i <= LEN(@CharID))  
            BEGIN  
                -- Get the character we're up to, then convert it to its numerical ASCII code  
                SET @c = ASCII(SUBSTRING(@CharID, @i, 1))  
                  
                -- Start checking  
                IF (@c NOT BETWEEN ASCII('a') AND ASCII('z')      -- allow a-z  
                    AND @c NOT BETWEEN ASCII('A') AND ASCII('Z')  -- allow A-Z  
                    AND @c NOT BETWEEN ASCII('0') AND ASCII('9')  -- allow 0-9  
                    )    -- allow other characters: _ and :  
                BEGIN  
                      SET @nRet = 5 -- if I recall, this is the invalid char name response? Don't really remember though...!  
                      RETURN  
                END  
              
                -- Increase the loop counter by one to move to the next character in the character's name.  
                SET @i = @i + 1  
            END  
              
              
            DECLARE @Row tinyint, @Nation tinyint, @Zone tinyint, @PosX int, @PosZ int  
                SET @Row = 0    SET @Nation = 0  SET @Zone = 21  SET @PosX = 81510 SET @PosZ = 44460  
              
                SELECT @Nation = bNation, @Row = bCharNum FROM ACCOUNT_CHAR WHERE strAccountID = @AccountID  
                  
                IF @Row >= 5    SET @nRet =  1  
                  
                IF @Nation = 1 AND @Race > 10    SET @nRet = 2  
                ELSE IF @Nation = 2 AND @Race < 10    SET @nRet = 2  
                ELSE IF @Nation <>1 AND @Nation <> 2    SET @nRet = 2  
              
                IF @nRet > 0  
                    RETURN  
                  
                 SELECT @Row = COUNT(*) FROM USERDATA WHERE strUserId = ltrim(rtrim(@CharID))  
                IF @Row > 0  
                BEGIN  
                    SET @nRet =  3  
                    RETURN  
                END  
              
            --  SET @Zone = @Nation  
                SET @Zone=21  
                SELECT @PosX = InitX, @PosZ = InitZ FROM ZONE_INFO WHERE ZoneNo = @Zone  
                
                
                -- Zenocide 11/1/09 Illigal Name block  
              
            IF  
               @CharID like 'GM_%'
            or @CharID like '%professor%'
            or @CharID like '%DISABLE%'
            or @CharID like '%mers%'
            or @CharID like 'Adm%'
            or @CharID like '%Moderator%'
            or @CharID like '%GameMaster%'
            or @CharID like '%OldSchoolKO%'
            or @CharID like '%Defauld%'
            or @CharID like '%Humanpriest%'
            or @CharID like '%Champion%'
            or @CharID like '%castiel%'
            or @CharID like '%Slade%'
            or @CharID like '%Ferruzo%'
            or @CharID like '%Admin%'
            or @CharID like '%Draco%'
            or @CharID like '%Drako%'
            or @CharID like '%Dr4ko%'
            or @CharID like '%Drak0%'
            or @CharID like '%Dr4k0%'
            or @CharID like '%Zephyr%'
              
            BEGIN  
                    SET @nRet = 5  
                   RETURN  
                END  
                
            SELECT @account_id = id FROM TB_USER WHERE strAccountID = @AccountID
                
            BEGIN TRAN  
              
                IF @index = 0  
                    UPDATE ACCOUNT_CHAR SET strCharID1 = @CharID, bCharNum = bCharNum + 1 WHERE strAccountID = @AccountID  
                IF @index = 0
                    INSERT INTO PREMIUM_SERVICE VALUES (@AccountID,1,3,getdate())
                ELSE IF @index = 1  
                    UPDATE ACCOUNT_CHAR SET strCharID2 = @CharID, bCharNum = bCharNum + 1 WHERE strAccountID = @AccountID  
                ELSE IF @index = 2  
                    UPDATE ACCOUNT_CHAR SET strCharID3 = @CharID, bCharNum = bCharNum + 1 WHERE strAccountID = @AccountID  
                ELSE IF @index = 3  
                    UPDATE ACCOUNT_CHAR SET strCharID4 = @CharID, bCharNum = bCharNum + 1 WHERE strAccountID = @AccountID  
                ELSE IF @index = 4  
                    UPDATE ACCOUNT_CHAR SET strCharID5 = @CharID, bCharNum = bCharNum + 1 WHERE strAccountID = @AccountID  
              
                if @Nation = 1 begin -- karus
                
                    INSERT INTO USERDATA (account_id, strUserId, Nation, Race, Class, HairColor, Face, Strong, Sta, Dex, Intel, Cha, Zone, PX, PZ ,Knights,Fame,sRace)  
                    VALUES     (@account_id, @CharID, @Nation, @Race, @Class, @Hair, @Face, @Str, @Sta, @Dex, @Intel, @Cha, @Zone, @PosX, @PosZ ,1,5,@Race)  
                end
            
                else begin -- elmorad
                
                    INSERT INTO USERDATA (account_id, strUserId, Nation, Race, Class, HairColor, Face, Strong, Sta, Dex, Intel, Cha, Zone, PX, PZ ,Knights,fame,sRace)  
                    VALUES     (@account_id, @CharID, @Nation, @Race, @Class, @Hair, @Face, @Str, @Sta, @Dex, @Intel, @Cha, @Zone, @PosX, @PosZ ,2,5,@Race)  
                end
              
                exec baslangicitem @CharID 
                  
                IF @@ERROR <> 0  
                BEGIN      
                    ROLLBACK TRAN  
                    SET @nRet =  4  
                    RETURN  
                END  
                  
            COMMIT TRAN  
            SET @nRet =  0  ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
