<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id');
            $table->integer('character_id');
            $table->string('client_ip', 45)->default('');
            $table->tinyInteger('kc_option');
            $table->integer('kc_amount');
            $table->decimal('kc_price', 8, 2);
            $table->tinyInteger('provider');
            $table->string('transaction_id');
            $table->string('country');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->decimal('payment_gross', 8, 2);
            $table->decimal('payment_fee', 8, 2);
            $table->string('payment_status');
            $table->string('payment_method');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_transactions');
    }
}
