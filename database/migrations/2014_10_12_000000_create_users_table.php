<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('TB_USER');

        Schema::create('TB_USER', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('email_token')->nullable();
            $table->tinyInteger('verified')->default(0);
            $table->boolean('is_admin')->default(false);
            $table->boolean('is_super_admin')->default(false);
            $table->string('country_code')->default('us');
            $table->integer('pin');
            $table->rememberToken();
            $table->timestamps();

            $table->string('strAccountID', 21);
            $table->string('strSocNo', 20)->nullable();
            $table->integer('idays')->default(0);
            $table->tinyInteger('strAuthority')->default(1);
            $table->string('strIP', 15)->nullable();
            $table->tinyInteger('Premiumtype')->default(1);
            $table->dateTime('PremiumDays')->default(DB::raw('(getdate()+(3))'));
            $table->string('IP', 21)->nullable();
            $table->string('Key', 21)->nullable();
            $table->integer('giris')->nullable();
            $table->integer('nKnightCash')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TB_USER');
    }
}
