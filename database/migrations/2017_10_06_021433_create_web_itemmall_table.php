<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebItemmallTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('WEB_ITEMMALL');

        Schema::create('WEB_ITEMMALL', function (Blueprint $table) {
            $table->increments('id');
            $table->string('strAccountID', 21);
            $table->string('strCharID', 21)->default('');
            $table->smallInteger('ServerNo')->default(0);
            $table->integer('ItemID');
            $table->smallInteger('ItemCount');
            $table->integer('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('WEB_ITEMMALL');
    }
}
