<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['CacheFilter:2']], function () { // 2 Minute Caching pages
    Route::get('/', 'HomeController@index');
    Route::any('/ingame/rankings', 'RankingsController@ingame');
});

Route::group(['middleware' => ['CacheFilter:15']], function () { // 15 Minute Caching pages
    Route::get('/rankings', 'RankingsController@index');
    Route::get('/news', 'NewsController@index');
    Route::get('/news/{Article}', 'NewsController@getPage');
    Route::get('/downloads', 'DownloadsController@index');
    Route::get('/upgrade_rates', 'HomeController@getUpgradeRates');
    Route::get('/search', 'SearchController@index');
    Route::get('/tac', 'HomeController@getTAC');

    Route::get('/events', 'EventController@index');
    Route::get('/events/bifrost', 'EventController@bifrost');
    Route::get('/events/crlns', 'EventController@crlns');
    Route::get('/events/crhl', 'EventController@crhl');
    Route::get('/events/crcz', 'EventController@crcz');
    Route::get('/events/lunar', 'EventController@lunar');
    Route::get('/events/forgotten_temple', 'EventController@forgotten_temple');
    Route::get('/events/csw', 'EventController@csw');
    Route::get('/events/juraid', 'EventController@juraid');
    Route::get('/events/death_match', 'EventController@death_match');
    Route::get('/events/bdw', 'EventController@bdw');

    Route::get('/character/{CharacterName}', 'CharacterController@showCharacter');
    Route::get('/clan/{ClanName}', 'CharacterController@showClan');

    Route::any('/droplist', 'DropListController@index');
    Route::get('/questlist', 'QuestController@index');
    Route::get('/exchanges', 'ExchangesController@index');
});

Route::post('/json_get_items', 'ItemController@getItems');
Route::get('/json_search', 'SearchController@jsonSearch');
Route::get('/json_monster_drops/{sItem}', 'DropListController@getItemData');
Route::get('/json_exchange_items/{nIndex}/{item}', 'ExchangesController@getItemData');
Route::get('/getCSRF', function(){
    return json_encode(csrf_token());
});

Route::get('/emails/test', function() {
    return view('vendor.mail.html.layout')
        ->with(array('header' => 'Please Confirm Your Email - MyKO 1298', 'preview_text' => 'Please confirm your email', 'body' => 'Click on the button below to confirm your email address.',
            'button_text' => 'Confirm', 'button_link' => url('/auth/confirm/'), 'slot' => 'markdown text?'));
});

Auth::routes();
Route::get('/auth/confirm/{token}', 'ConfirmationController@index');

/*
 * Powerup Store Routes
 * */
Route::any('/pus', 'PowerupController@powerupStore');
Route::any('/pus/login', 'PowerupController@postPowerupLogin');

Route::any('recharge/paygol/payment/ipn', 'PayGolController@ipn');

Route::group(['middleware' => 'auth'], function () {
    Route::any('/pus/recharge', 'PowerupController@getPowerupRecharge');

    //Account Routes
    Route::get('account', 'AccountController@index');
    Route::get('account/recharge', 'AccountController@getRecharge');
    Route::get('account/changepassword', 'AccountController@getChangePassword');
    Route::get('account/changepin', 'AccountController@getChangePin');
    Route::get('account/seal_unseal', 'SealController@index');
    Route::get('account/clan_nation_change', 'AccountController@getClanNationChange');
    Route::get('account/clan_leader_change', 'AccountController@getClanLeaderChange');

    Route::post('account/changepassword', 'AccountController@postChangePassword');
    Route::post('account/changepin', 'AccountController@postChangePin');
    Route::post('account/seal_unseal', 'SealController@postSeal');
    Route::any('account/forgotpin', 'AccountController@postForgotPIN');
    Route::post('account/clan_nation_change', 'AccountController@postClanNationChange');
    Route::post('account/clan_leader_change', 'AccountController@postClanLeaderChange');


    //GameMaster Routes
    Route::group(['middleware' => 'gm'], function () {
        Route::get('ban/hardware/{accountid}', 'AdminController@banHardware');
        Route::get('ban/account/{accountid}', 'AdminController@banAccount');
        Route::get('ban/character/{characterid}', 'AdminController@banCharacter');
        Route::get('mute/character/{characterid}', 'AdminController@muteCharacter');

        Route::get('unban/hardware/{accountid}', 'AdminController@unbanHardware');
        Route::get('unban/account/{accountid}', 'AdminController@unbanAccount');
        Route::get('unban/character/{characterid}', 'AdminController@unbanCharacter');
        Route::get('unmute/character/{characterid}', 'AdminController@unmuteCharacter');
    });

    //Administrator Routes
    Route::group(['middleware' => 'admin'], function () {
        Route::get('account/administrator/pus', 'AdminPUSController@getPUSChangeItemsPage');
        Route::get('account/administrator/pus/finances', 'AdminPUSController@getPUSFinancesPage');
        Route::get('account/administrator/pus/items', 'AdminPUSController@getPUSChangeItemsPage');
        Route::post('account/administrator/pus/items', 'AdminPUSController@postPUSChangeItemsPage');
        Route::get('account/administrator/pus/items/delete/{id}', 'AdminPUSController@getPUSDeleteItem');
    });
});