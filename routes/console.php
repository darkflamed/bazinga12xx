<?php

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('db:delete_forgot_pin', function () {
    DB::table('forgot_pin')->truncate();
});

Artisan::command('migrate:nuke', function () {
    $this->comment('Nuking Database...');
    if(!File::exists(base_path('database/backup/myko.bak'))) {
        if(!class_exists('ZipArchive')) {
            $this->comment('Please enable ZIP PHP Extension');
            return;
        }

        $zip = new ZipArchive;
        if ($zip->open(base_path('database/backup/myko.zip')) === TRUE) {
            $zip->extractTo(base_path('database/backup/'));
            $zip->close();
        } else {
            $this->comment('Failed to unzip file!');
            return;
        }
    }

    if(!File::exists(base_path('database/backup/myko.bak'))) {
        $this->comment('Failed to find file after unzipping!');
        return;
    }

    $sql = 'RESTORE DATABASE '.env('DB_DATABASE')
        .' FROM DISK = N\''.base_path('database/backup/myko.bak').'\''
        .' WITH MOVE \'KN_Online1_Data\' TO \'/var/opt/mssql/data/'.env('DB_DATABASE').'.mdf\','
        .' MOVE \'KN_Online1_Log\' TO \'/var/opt/mssql/data/'.env('DB_DATABASE').'_log.ldf\','
        .' REPLACE,RECOVERY';

    dump(shell_exec('sqlcmd -S laradock -U sa -P Erespula4663! -Q "DROP DATABASE IF EXISTS kn_online" -b'));
    dump(shell_exec('sqlcmd -S laradock -U sa -P Erespula4663! -Q "'.$sql.'" -b'));
    dump(shell_exec('sqlcmd -S laradock -U sa -P Erespula4663! -Q "USE kn_online" -b'));

    $this->call('migrate');
    $this->call('db:seed');
    $this->call('passport:install');
})->describe('Restore database backup and run all migrations and seeds');