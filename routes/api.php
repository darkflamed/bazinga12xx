<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::any('pus/knightcash', 'PowerupController@getKnightCash');
    Route::any('pus/search', 'PowerupController@getSearchItemList');
    Route::any('pus/category', 'PowerupController@getCategoryItemList');
    Route::any('pus/main', 'PowerupController@getMainContent');
    Route::any('pus/cart', 'PowerupController@getCart');
    Route::any('pus/cart/add', 'PowerupController@addToCart');
    Route::any('pus/cart/buy', 'PowerupController@buyItems');
    Route::any('pus/cart/delete', 'PowerupController@removeFromCart');
    Route::any('pus/cart/modify/{num}/{qty}', 'PowerupController@getModifyCartItemQuantity');
    Route::any('pus/itemdetail/{num}', 'PowerupController@getItemDetail');

    Route::any('pus/recharge', 'PowerupController@getPowerupRecharge');
    Route::any('pus/recharge/english', 'PowerupController@getEnglishRecharge');
    Route::any('pus/recharge/spanish', 'PowerupController@getSpanishRecharge');
    Route::any('pus/recharge/turkish', 'PowerupController@getTurkishRecharge');

    Route::any('recharge/paypal/payment/create', 'PayPalController@createPayment');
    Route::any('recharge/paypal/payment/execute', 'PayPalController@executePayment');

    Route::any('recharge/paygol/payment/create', 'PayGolController@createPayment');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
