﻿namespace ClanWar
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.txt1 = new System.Windows.Forms.TextBox();
            this.s1 = new System.Windows.Forms.Label();
            this.s2 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.c1 = new System.Windows.Forms.Label();
            this.c2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button7 = new System.Windows.Forms.Button();
            this.s8 = new System.Windows.Forms.Label();
            this.s6 = new System.Windows.Forms.Label();
            this.s7 = new System.Windows.Forms.Label();
            this.s4 = new System.Windows.Forms.Label();
            this.s5 = new System.Windows.Forms.Label();
            this.s3 = new System.Windows.Forms.Label();
            this.c8 = new System.Windows.Forms.Label();
            this.c7 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.c4 = new System.Windows.Forms.Label();
            this.c6 = new System.Windows.Forms.Label();
            this.c3 = new System.Windows.Forms.Label();
            this.c5 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tponetxt = new System.Windows.Forms.TextBox();
            this.rwrdtxt = new System.Windows.Forms.TextBox();
            this.tpalltxt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button8 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.r3 = new System.Windows.Forms.TextBox();
            this.r2 = new System.Windows.Forms.TextBox();
            this.r1 = new System.Windows.Forms.TextBox();
            this.statu = new System.Windows.Forms.Label();
            this.timer5 = new System.Windows.Forms.Timer(this.components);
            this.timer4 = new System.Windows.Forms.Timer(this.components);
            this.timer6 = new System.Windows.Forms.Timer(this.components);
            this.timer7 = new System.Windows.Forms.Timer(this.components);
            this.timer8 = new System.Windows.Forms.Timer(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txt1
            // 
            this.txt1.Location = new System.Drawing.Point(149, 775);
            this.txt1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.txt1.Name = "txt1";
            this.txt1.Size = new System.Drawing.Size(497, 39);
            this.txt1.TabIndex = 0;
            // 
            // s1
            // 
            this.s1.AutoSize = true;
            this.s1.Location = new System.Drawing.Point(195, 207);
            this.s1.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.s1.Name = "s1";
            this.s1.Size = new System.Drawing.Size(31, 32);
            this.s1.TabIndex = 4;
            this.s1.Text = "0";
            // 
            // s2
            // 
            this.s2.AutoSize = true;
            this.s2.Location = new System.Drawing.Point(648, 207);
            this.s2.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.s2.Name = "s2";
            this.s2.Size = new System.Drawing.Size(31, 32);
            this.s2.TabIndex = 4;
            this.s2.Text = "0";
            // 
            // timer1
            // 
            this.timer1.Interval = 153500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 152500;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // c1
            // 
            this.c1.AutoSize = true;
            this.c1.Location = new System.Drawing.Point(163, 48);
            this.c1.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.c1.Name = "c1";
            this.c1.Size = new System.Drawing.Size(106, 32);
            this.c1.TabIndex = 6;
            this.c1.Text = "CLAN1";
            // 
            // c2
            // 
            this.c2.AutoSize = true;
            this.c2.Location = new System.Drawing.Point(613, 48);
            this.c2.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.c2.Name = "c2";
            this.c2.Size = new System.Drawing.Size(106, 32);
            this.c2.TabIndex = 6;
            this.c2.Text = "CLAN2";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 782);
            this.label4.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 32);
            this.label4.TabIndex = 7;
            this.label4.Text = "ClanID:";
            // 
            // timer3
            // 
            this.timer3.Interval = 3605000;
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.tabControl1.ItemSize = new System.Drawing.Size(66, 18);
            this.tabControl1.Location = new System.Drawing.Point(-8, -2);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(106, 3);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(2003, 997);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.button5);
            this.tabPage2.Controls.Add(this.button4);
            this.tabPage2.Controls.Add(this.listView1);
            this.tabPage2.Controls.Add(this.txt1);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Location = new System.Drawing.Point(10, 28);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tabPage2.Size = new System.Drawing.Size(1983, 959);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Clans";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1197, 782);
            this.button2.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(304, 55);
            this.button2.TabIndex = 12;
            this.button2.Text = "Top 8 Clan";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(795, 782);
            this.button3.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(304, 55);
            this.button3.TabIndex = 11;
            this.button3.Text = "Top Online";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(349, 837);
            this.button5.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(304, 55);
            this.button5.TabIndex = 10;
            this.button5.Text = "DELETE";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(29, 837);
            this.button4.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(304, 55);
            this.button4.TabIndex = 10;
            this.button4.Text = "ADD";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listView1.Location = new System.Drawing.Point(16, 14);
            this.listView1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(1961, 741);
            this.listView1.TabIndex = 9;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "ID";
            this.columnHeader1.Width = 313;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "CLAN NAME";
            this.columnHeader2.Width = 423;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.button7);
            this.tabPage3.Controls.Add(this.s8);
            this.tabPage3.Controls.Add(this.s6);
            this.tabPage3.Controls.Add(this.s7);
            this.tabPage3.Controls.Add(this.s4);
            this.tabPage3.Controls.Add(this.s5);
            this.tabPage3.Controls.Add(this.s3);
            this.tabPage3.Controls.Add(this.c8);
            this.tabPage3.Controls.Add(this.c7);
            this.tabPage3.Controls.Add(this.button6);
            this.tabPage3.Controls.Add(this.s1);
            this.tabPage3.Controls.Add(this.button1);
            this.tabPage3.Controls.Add(this.c4);
            this.tabPage3.Controls.Add(this.c6);
            this.tabPage3.Controls.Add(this.c2);
            this.tabPage3.Controls.Add(this.s2);
            this.tabPage3.Controls.Add(this.c3);
            this.tabPage3.Controls.Add(this.c5);
            this.tabPage3.Controls.Add(this.c1);
            this.tabPage3.Location = new System.Drawing.Point(10, 28);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tabPage3.Size = new System.Drawing.Size(1983, 959);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Scores";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(1157, 742);
            this.button7.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(309, 126);
            this.button7.TabIndex = 10;
            this.button7.Text = "STOP";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // s8
            // 
            this.s8.AutoSize = true;
            this.s8.Location = new System.Drawing.Point(1653, 539);
            this.s8.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.s8.Name = "s8";
            this.s8.Size = new System.Drawing.Size(31, 32);
            this.s8.TabIndex = 9;
            this.s8.Text = "0";
            // 
            // s6
            // 
            this.s6.AutoSize = true;
            this.s6.Location = new System.Drawing.Point(648, 539);
            this.s6.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.s6.Name = "s6";
            this.s6.Size = new System.Drawing.Size(31, 32);
            this.s6.TabIndex = 9;
            this.s6.Text = "0";
            // 
            // s7
            // 
            this.s7.AutoSize = true;
            this.s7.Location = new System.Drawing.Point(1149, 539);
            this.s7.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.s7.Name = "s7";
            this.s7.Size = new System.Drawing.Size(31, 32);
            this.s7.TabIndex = 9;
            this.s7.Text = "0";
            // 
            // s4
            // 
            this.s4.AutoSize = true;
            this.s4.Location = new System.Drawing.Point(1653, 207);
            this.s4.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.s4.Name = "s4";
            this.s4.Size = new System.Drawing.Size(31, 32);
            this.s4.TabIndex = 9;
            this.s4.Text = "0";
            // 
            // s5
            // 
            this.s5.AutoSize = true;
            this.s5.Location = new System.Drawing.Point(195, 539);
            this.s5.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.s5.Name = "s5";
            this.s5.Size = new System.Drawing.Size(31, 32);
            this.s5.TabIndex = 9;
            this.s5.Text = "0";
            // 
            // s3
            // 
            this.s3.AutoSize = true;
            this.s3.Location = new System.Drawing.Point(1149, 207);
            this.s3.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.s3.Name = "s3";
            this.s3.Size = new System.Drawing.Size(31, 32);
            this.s3.TabIndex = 9;
            this.s3.Text = "0";
            // 
            // c8
            // 
            this.c8.AutoSize = true;
            this.c8.Location = new System.Drawing.Point(1613, 370);
            this.c8.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.c8.Name = "c8";
            this.c8.Size = new System.Drawing.Size(106, 32);
            this.c8.TabIndex = 8;
            this.c8.Text = "CLAN8";
            // 
            // c7
            // 
            this.c7.AutoSize = true;
            this.c7.Location = new System.Drawing.Point(1109, 370);
            this.c7.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.c7.Name = "c7";
            this.c7.Size = new System.Drawing.Size(106, 32);
            this.c7.TabIndex = 8;
            this.c7.Text = "CLAN7";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(741, 742);
            this.button6.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(309, 126);
            this.button6.TabIndex = 7;
            this.button6.Text = "ABORT";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(323, 742);
            this.button1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(309, 126);
            this.button1.TabIndex = 1;
            this.button1.Text = "START WAR";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // c4
            // 
            this.c4.AutoSize = true;
            this.c4.Location = new System.Drawing.Point(1613, 48);
            this.c4.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.c4.Name = "c4";
            this.c4.Size = new System.Drawing.Size(106, 32);
            this.c4.TabIndex = 6;
            this.c4.Text = "CLAN4";
            // 
            // c6
            // 
            this.c6.AutoSize = true;
            this.c6.Location = new System.Drawing.Point(613, 370);
            this.c6.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.c6.Name = "c6";
            this.c6.Size = new System.Drawing.Size(106, 32);
            this.c6.TabIndex = 6;
            this.c6.Text = "CLAN6";
            // 
            // c3
            // 
            this.c3.AutoSize = true;
            this.c3.Location = new System.Drawing.Point(1109, 48);
            this.c3.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.c3.Name = "c3";
            this.c3.Size = new System.Drawing.Size(106, 32);
            this.c3.TabIndex = 6;
            this.c3.Text = "CLAN3";
            // 
            // c5
            // 
            this.c5.AutoSize = true;
            this.c5.Location = new System.Drawing.Point(163, 370);
            this.c5.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.c5.Name = "c5";
            this.c5.Size = new System.Drawing.Size(106, 32);
            this.c5.TabIndex = 6;
            this.c5.Text = "CLAN5";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.label9);
            this.tabPage4.Controls.Add(this.label8);
            this.tabPage4.Controls.Add(this.label7);
            this.tabPage4.Controls.Add(this.tponetxt);
            this.tabPage4.Controls.Add(this.rwrdtxt);
            this.tabPage4.Controls.Add(this.tpalltxt);
            this.tabPage4.Controls.Add(this.label6);
            this.tabPage4.Controls.Add(this.comboBox1);
            this.tabPage4.Controls.Add(this.pictureBox1);
            this.tabPage4.Controls.Add(this.button8);
            this.tabPage4.Controls.Add(this.label5);
            this.tabPage4.Controls.Add(this.checkBox4);
            this.tabPage4.Controls.Add(this.label3);
            this.tabPage4.Controls.Add(this.label2);
            this.tabPage4.Controls.Add(this.label1);
            this.tabPage4.Controls.Add(this.checkBox3);
            this.tabPage4.Controls.Add(this.checkBox2);
            this.tabPage4.Controls.Add(this.checkBox1);
            this.tabPage4.Controls.Add(this.r3);
            this.tabPage4.Controls.Add(this.r2);
            this.tabPage4.Controls.Add(this.r1);
            this.tabPage4.Location = new System.Drawing.Point(10, 28);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1983, 959);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Properties";
            this.tabPage4.UseVisualStyleBackColor = true;
            this.tabPage4.Click += new System.EventHandler(this.tabPage4_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1253, 470);
            this.label9.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(143, 32);
            this.label9.TabIndex = 11;
            this.label9.Text = "Item Give:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(1200, 384);
            this.label8.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(216, 32);
            this.label8.TabIndex = 11;
            this.label8.Text = "Teleport Player:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1253, 300);
            this.label7.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(168, 32);
            this.label7.TabIndex = 11;
            this.label7.Text = "Teleport All:";
            // 
            // tponetxt
            // 
            this.tponetxt.Location = new System.Drawing.Point(1432, 377);
            this.tponetxt.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tponetxt.Name = "tponetxt";
            this.tponetxt.Size = new System.Drawing.Size(361, 39);
            this.tponetxt.TabIndex = 10;
            this.tponetxt.Text = "summonusera";
            // 
            // rwrdtxt
            // 
            this.rwrdtxt.Location = new System.Drawing.Point(1432, 463);
            this.rwrdtxt.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.rwrdtxt.Name = "rwrdtxt";
            this.rwrdtxt.Size = new System.Drawing.Size(361, 39);
            this.rwrdtxt.TabIndex = 10;
            // 
            // tpalltxt
            // 
            this.tpalltxt.Location = new System.Drawing.Point(1432, 293);
            this.tpalltxt.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tpalltxt.Name = "tpalltxt";
            this.tpalltxt.Size = new System.Drawing.Size(361, 39);
            this.tpalltxt.TabIndex = 10;
            this.tpalltxt.Text = "teleport_users";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1816, 570);
            this.label6.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 32);
            this.label6.TabIndex = 9;
            this.label6.Text = "Minute";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(1432, 563);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(361, 39);
            this.comboBox1.TabIndex = 8;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ClanWar.Properties.Resources._13263763_826402370825680_4629401867499076083_n;
            this.pictureBox1.Location = new System.Drawing.Point(75, 7);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1845, 272);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(757, 723);
            this.button8.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(544, 184);
            this.button8.TabIndex = 6;
            this.button8.Text = "SAVE";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1248, 570);
            this.label5.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(131, 32);
            this.label5.TabIndex = 5;
            this.label5.Text = "Duration:";
            // 
            // checkBox4
            // 
            this.checkBox4.Location = new System.Drawing.Point(347, 556);
            this.checkBox4.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(368, 57);
            this.checkBox4.TabIndex = 3;
            this.checkBox4.Text = "Boss Room";
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(109, 513);
            this.label3.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(172, 32);
            this.label3.TabIndex = 2;
            this.label3.Text = "REWARD-3:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(109, 310);
            this.label2.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(172, 32);
            this.label2.TabIndex = 2;
            this.label2.Text = "REWARD-1:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(109, 410);
            this.label1.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(172, 32);
            this.label1.TabIndex = 2;
            this.label1.Text = "REWARD-2:";
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(728, 477);
            this.checkBox3.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(239, 36);
            this.checkBox3.TabIndex = 1;
            this.checkBox3.Text = "Active/Passive";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(728, 398);
            this.checkBox2.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(239, 36);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "Active/Passive";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(728, 308);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(239, 36);
            this.checkBox1.TabIndex = 1;
            this.checkBox1.Text = "Active/Passive";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // r3
            // 
            this.r3.Enabled = false;
            this.r3.Location = new System.Drawing.Point(307, 470);
            this.r3.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.r3.Name = "r3";
            this.r3.Size = new System.Drawing.Size(361, 39);
            this.r3.TabIndex = 0;
            // 
            // r2
            // 
            this.r2.Enabled = false;
            this.r2.Location = new System.Drawing.Point(307, 393);
            this.r2.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.r2.Name = "r2";
            this.r2.Size = new System.Drawing.Size(361, 39);
            this.r2.TabIndex = 0;
            // 
            // r1
            // 
            this.r1.Enabled = false;
            this.r1.Location = new System.Drawing.Point(307, 303);
            this.r1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.r1.Name = "r1";
            this.r1.Size = new System.Drawing.Size(361, 39);
            this.r1.TabIndex = 0;
            // 
            // statu
            // 
            this.statu.Location = new System.Drawing.Point(11, 992);
            this.statu.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.statu.Name = "statu";
            this.statu.Size = new System.Drawing.Size(1984, 52);
            this.statu.TabIndex = 1;
            this.statu.Text = "Status:";
            // 
            // timer5
            // 
            this.timer5.Interval = 150000;
            this.timer5.Tick += new System.EventHandler(this.timer5_Tick_1);
            // 
            // timer4
            // 
            this.timer4.Tick += new System.EventHandler(this.timer4_Tick);
            // 
            // timer6
            // 
            this.timer6.Interval = 600000;
            this.timer6.Tick += new System.EventHandler(this.timer6_Tick);
            // 
            // timer7
            // 
            this.timer7.Enabled = true;
            this.timer7.Interval = 200;
            this.timer7.Tick += new System.EventHandler(this.timer7_Tick);
            // 
            // timer8
            // 
            this.timer8.Interval = 240000;
            this.timer8.Tick += new System.EventHandler(this.timer8_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2197, 1035);
            this.Controls.Add(this.statu);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "ClanWar";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txt1;
        private System.Windows.Forms.Label s1;
        private System.Windows.Forms.Label s2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label c1;
        private System.Windows.Forms.Label c2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Label statu;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label c4;
        private System.Windows.Forms.Label c6;
        private System.Windows.Forms.Label c3;
        private System.Windows.Forms.Label c5;
        private System.Windows.Forms.Label s8;
        private System.Windows.Forms.Label s6;
        private System.Windows.Forms.Label s7;
        private System.Windows.Forms.Label s4;
        private System.Windows.Forms.Label s5;
        private System.Windows.Forms.Label s3;
        private System.Windows.Forms.Label c8;
        private System.Windows.Forms.Label c7;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox r3;
        private System.Windows.Forms.TextBox r2;
        private System.Windows.Forms.TextBox r1;
        private System.Windows.Forms.Timer timer5;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tponetxt;
        private System.Windows.Forms.TextBox rwrdtxt;
        private System.Windows.Forms.TextBox tpalltxt;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Timer timer4;
        private System.Windows.Forms.Timer timer6;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Timer timer7;
        private System.Windows.Forms.Timer timer8;
    }
}

