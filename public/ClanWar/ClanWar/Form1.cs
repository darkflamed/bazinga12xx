﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClanWar
{

    public partial class Form1 : Form
    {

        #region valuable

        //Thread EB = new Thread(new ThreadStart(EbenezerLogReader));
        public static IniFiles INI = new IniFiles(AppDomain.CurrentDomain.BaseDirectory + "ClanWar.ini");
       Thread RG = new Thread(new ThreadStart(registers));


        static string[] clan1 = new string[36];
        static string[] clan2 = new string[36];
        static string[] clan3 = new string[36];
        static string[] clan4 = new string[36];
        static string[] clan5 = new string[36];
        static string[] clan6 = new string[36];
        static string[] clan7 = new string[36];
        static string[] clan8 = new string[36];
        static string[] clan11 = new string[9];
        static string[] clan12 = new string[9];
        static string[] clan13 = new string[9];
        static string[] clan14 = new string[9];
        static string[] clan15 = new string[9];
        static string[] clan16 = new string[9];
        static string[] clan17 = new string[9];
        static string[] clan18 = new string[9];
        static string[] tumclan = new string[350];
        static string[] bolunmeyen = new string[65];
        public static Skor[] ClanwarSkor = new Skor[8];
        long reward, reward1, reward2;
        public static string cntstring;
        public static string DosyaYolu, chatyolu, codeyolu;
        public static string ayarodul1, ayarodul2, ayarodul3, zaman;
        public static Boolean rwd1, rwd2, rwd3,bossroom;
        public static string id, pass, server, db;
        public static string eben;
        int ultima;
       
        string clanism;
        static DateTime saat;
        static double mn;
        //string[] giricekler = new string[300];
        string[] giriceklerkayit = new string[300];

        string tp;
        string gving, tpall;


        int[] tops = new int[10];
        #endregion


        public int Topskor()
        {
            int a = 0, c = 0;




            tops[0] = ClanwarSkor[1].Clan1;
            tops[1] = ClanwarSkor[1].Clan2;
            tops[2] = ClanwarSkor[1].Clan3;
            tops[3] = ClanwarSkor[1].Clan4;
            tops[4] = ClanwarSkor[1].Clan5;
            tops[5] = ClanwarSkor[1].Clan6;
            tops[6] = ClanwarSkor[1].Clan7;
            tops[7] = ClanwarSkor[1].Clan8;

            for (int i = 0; i < 8; i++)
            {
                a = 0;
                for (int j = 0; j < 8; j++)
                {

                    if (tops[j] > tops[i])
                    {
                        a = 0;
                    }
                    else
                    {
                        a++;
                        if (a == 8)
                        {
                            c = i;
                            i = 7;
                        }
                    }
                }



            }

            return c;
        }




        public struct Skor
        {


            public int Clan1;
            public int Clan2;
            public int Clan3;
            public int Clan4;
            public int Clan5;
            public int Clan6;
            public int Clan7;
            public int Clan8;

        }

        public Form1()
        {
            //cntstring = Form2.cntstring;
            cntstring = INI.ReadValue("Connection", "Connectiontype").ToString().Trim();
            id = INI.ReadValue("Connection", "ID").ToString().Trim();
            pass = INI.ReadValue("Connection", "PASSWORD").ToString().Trim();
            server = INI.ReadValue("Connection", "Server").ToString().Trim();
            db = INI.ReadValue("Connection", "Database").ToString().Trim();
            DosyaYolu = INI.ReadValue("Ebenezer-Logs", "DeathLog").ToString().Trim();

            //DosyaYolu = Form2.DosyaYolu;
            //id = Form2.id;
            //pass = Form2.pass;
            //server = Form2.server;
            // db = Form2.db;
            //eben = Form2.eben;
            eben = INI.ReadValue("Connection", "EbenezerName").ToString().Trim();
            // chatyolu = Form2.chatyolu;
            chatyolu = INI.ReadValue("Ebenezer-Logs", "ChatLog").ToString().Trim();
            codeyolu = INI.ReadValue("Ebenezer-Logs", "CodeLog").ToString().Trim();
            ayarodul1 = INI.ReadValue("Settings", "Reward1").ToString().Trim();
            ayarodul2 = INI.ReadValue("Settings", "Reward2").ToString().Trim();
            ayarodul3 = INI.ReadValue("Settings", "Reward3").ToString().Trim();
            zaman = INI.ReadValue("Settings", "Time").ToString().Trim();
            rwd1 = Convert.ToBoolean(INI.ReadValue("Settings", "rwd1").Trim());
            rwd2 = Convert.ToBoolean(INI.ReadValue("Settings", "rwd2").Trim());
            rwd3 = Convert.ToBoolean(INI.ReadValue("Settings", "rwd3").Trim());
            bossroom = Convert.ToBoolean(INI.ReadValue("Settings", "Bossroom").Trim());
            gving = INI.ReadValue("Settings", "ItemCode").ToString().Trim();


            InitializeComponent();
        }
        #region dll
        [DllImport("user32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern int PostMessageW(IntPtr hWnd, uint Msg, uint wParam, uint lParam);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int SendMessageTimeoutW(IntPtr hWnd, uint Msg, uint wParam, IntPtr lParam, uint fuFlags, uint uTimeout, ref uint lpdwResult);
        [DllImport("user32.dll")]
        private static extern IntPtr GetDlgItem(IntPtr hDlg, int nIDDlgItem);
        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        private static void SendToEbenezer(string command)
        {
            IntPtr hDlg = FindWindow(null, eben);
            if (hDlg != IntPtr.Zero)
            {
                IntPtr dlgItem = GetDlgItem(hDlg, 0x3ea);
                if (dlgItem != IntPtr.Zero)
                {
                    IntPtr lParam = Marshal.StringToBSTR(command);
                    uint lpdwResult = 0;
                    SendMessageTimeoutW(dlgItem, 12, 0, lParam, 0, 100, ref lpdwResult);
                    Marshal.FreeBSTR(lParam);
                    Thread.Sleep(100);
                    PostMessageW(hDlg, 0x100, 13, 0);
                    Thread.Sleep(0x19);
                }
            }
        }
        #endregion
        private void button1_Click(object sender, EventArgs e)
        {
            start();
        }
        int key = 0;
        private void start()
        {
            try
            {
                pm = 0;
                timer7.Enabled = false;
                saat = DateTime.Now;
               

               // Sqlconnect();
                /*  SendToEbenezer("+notice 8 0 Clan Wars ll  Start in 5 min.Type register for registerion");
                  SendToEbenezer("+notice 33 0 Clan Wars ll  Start in 5 min.Type register for registerion");
                  SendToEbenezer("+notice 33 0 When type register ,you wont get any pm about register but u registered");*/
                SendToEbenezer("+notice 33 0 Use Clan War NPC (331,429) in Moradon to register into clanwar.Number of registered players: 0 Event starts in 10 min");
                Thread.Sleep(250);

                Thread.Sleep(250);
                SendToEbenezer("+notice 8 0 Use Clan War NPC (331,429) in Moradon to register into clanwar.Number of registered players: 0 Event starts in 10 min");
                //Thread.Sleep(250);
                // SendToEbenezer("+notice 2 0 QUALIFIED KNIGHTS FOR CLAN WAR ARE: " + c1.Text + ", " + c2.Text + ", " + c3.Text + ", " + c4.Text + ", " + c5.Text + ", " + c6.Text + ", " + c7.Text + ", " + c8.Text );





                // mn = DateTime.Now.Minute-5.0001;


                timer6.Enabled = true;
                timer8.Enabled = true;
                if (key == 0)
                {
                    RG.Start();
                    key++;
                }
                else
                {
                   RG.Resume();
                }

            }
            catch
            { }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {


            for (int i = 0; i < listView1.Items.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        s1.Text = ClanwarSkor[1].Clan1.ToString();
                        break;
                    case 1:
                        s2.Text = ClanwarSkor[1].Clan2.ToString();
                        break;
                    case 2:
                        s3.Text = ClanwarSkor[1].Clan3.ToString();
                        break;
                    case 3:
                        s4.Text = ClanwarSkor[1].Clan4.ToString();
                        break;
                    case 4:
                        s5.Text = ClanwarSkor[1].Clan5.ToString();
                        break;
                    case 5:
                        s6.Text = ClanwarSkor[1].Clan6.ToString();
                        break;
                    case 6:
                        s7.Text = ClanwarSkor[1].Clan7.ToString();
                        break;
                    case 7:
                        s8.Text = ClanwarSkor[1].Clan8.ToString();
                        break;
                    default:
                        break;
                }
            }
        }
       


        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {

            //EB.Abort();
            RG.Abort();

            timer1.Enabled = false;
            timer2.Enabled = false;
            timer3.Enabled = false;
            timer4.Enabled = false;
            timer5.Enabled = false;
            timer6.Enabled = false;
            Application.Exit();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            int f = Topskor();
            Thread.Sleep(200);
            SendToEbenezer("+notice 8 0 " + listView1.Items[f].SubItems[1].Text + " point: " + tops[f] + " ,kill top scorer gives double points");
            Thread.Sleep(1000);
            for (int i = 0; i < listView1.Items.Count; i++)
            {
                SendToEbenezer("+notice 33 0 " + listView1.Items[i].SubItems[1].Text + " point: " + tops[i]);
                Thread.Sleep(1200);
            }

        }

        private void timer3_Tick(object sender, EventArgs e)
        {

            int f = Topskor();

            SendToEbenezer("+notice 8 0 " + listView1.Items[f].SubItems[1].Text + "  won " + " point: " + tops[f]);
            Thread.Sleep(1000);
            SendToEbenezer("+notice 33 0 " + listView1.Items[f].SubItems[1].Text + "  won " + " point: " + tops[f]);
            for (int i = 0; i < listView1.Items.Count; i++)
            {
                Thread.Sleep(1000);
                SendToEbenezer("+notice 33 0 " + listView1.Items[i].SubItems[1].Text + " point: " + tops[i]);
            }


            Thread.Sleep(1000);
            SendToEbenezer("+" + tpall + " 206 21");

            // EB.Abort();
            timer5.Enabled = false;
            timer1.Enabled = false;
            timer2.Enabled = false;
            timer3.Enabled = false;
            timer7.Enabled = true;

            odul(Topskor());
        }



        private void Form1_Load(object sender, EventArgs e)
        {
            r1.Text = ayarodul1;
            r2.Text = ayarodul2;
            r3.Text = ayarodul3;
            comboBox1.Text = zaman;
            checkBox1.Checked = rwd1;
            checkBox2.Checked = rwd2;
            checkBox3.Checked = rwd3;
            checkBox4.Checked = bossroom;
            save();
            //autoclans();
            statu.Text = "Status:Connected...";
            ClanwarSkor[1].Clan1 = 0;
            ClanwarSkor[1].Clan2 = 0;
            ClanwarSkor[1].Clan3 = 0;
            ClanwarSkor[1].Clan4 = 0;
            ClanwarSkor[1].Clan5 = 0;
            ClanwarSkor[1].Clan6 = 0;
            ClanwarSkor[1].Clan7 = 0;
            ClanwarSkor[1].Clan8 = 0;
            comboBox1.Items.Add("15");
            comboBox1.Items.Add("20");
            comboBox1.Items.Add("25");
            comboBox1.Items.Add("30");
            comboBox1.Items.Add("35");
            comboBox1.Items.Add("40");
            comboBox1.Items.Add("45");
            comboBox1.Items.Add("50");
            comboBox1.Items.Add("55");
            comboBox1.Items.Add("60");

        }



        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                string f = null;
                f = clanisim(Int32.Parse(txt1.Text));
                string[] a = { txt1.Text, clanisim(Int16.Parse(txt1.Text)) };
                ListViewItem item = new ListViewItem(a);
                if (!listView1.Items.Contains(item))
                {

                    listView1.Items.Add(item);
                }
                txt1.Clear();

            }
            catch
            {


            }
        }
        private string clanisim(int idd)
        {
            if (cntstring == "Windows Authentication")
            {


                SqlConnection conn = new SqlConnection("Server=" + server + ";Database=" + db + ";Integrated Security=true");

                SqlCommand cmd = new SqlCommand("Select IDName from KNIGHTS where IDNum = " + idd, conn);
                conn.Open();
                SqlDataReader d = cmd.ExecuteReader();
                while (d.Read())
                {
                    clanism = d["IDName"].ToString().Trim();
                }
                conn.Close();
            }
            else
            {

                SqlConnection cnn = new SqlConnection("Server=" + server + ";Database=" + db + ";User Id=" + id + ";Password=" + pass + ";");

                SqlCommand cmd = new SqlCommand("Select IDName from KNIGHTS where IDNum = " + idd, cnn);
                cnn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    clanism = dr["IDName"].ToString().Trim();
                }
                cnn.Close();
            }
            return clanism;
        }
        private void Sqlconnect()
        {
            try
            {
                if (cntstring == "Windows Authentication")
                {

                    for (int i = 0; i < listView1.Items.Count; i++)
                    {


                        SqlConnection conn = new SqlConnection("Server=" + server + ";Database=" + db + ";Integrated Security=true");

                        SqlCommand cmd = new SqlCommand("Select strUserID from KNIGHTS_USER where sIDNum =" + listView1.Items[i].SubItems[0].Text, conn);

                        conn.Open();
                        SqlDataReader dr = cmd.ExecuteReader();

                        List<string> liste = new List<string>();

                        while (dr.Read())
                        {
                            liste.Add(dr["strUserID"].ToString().Trim());

                        }
                        dr.Close();
                        conn.Close();
                        switch (i)
                        {
                            case 0:
                                clan1 = liste.ToArray();
                                break;
                            case 1:
                                clan2 = liste.ToArray();
                                break;
                            case 2:
                                clan3 = liste.ToArray();
                                break;
                            case 3:
                                clan4 = liste.ToArray();
                                break;
                            case 4:
                                clan5 = liste.ToArray();
                                break;
                            case 5:
                                clan6 = liste.ToArray();
                                break;
                            case 6:
                                clan7 = liste.ToArray();
                                break;
                            case 7:
                                clan8 = liste.ToArray();
                                break;

                            default:
                                break;
                        }

                        liste.Clear();
                    }
                }
                else
                {

                    for (int i = 0; i < listView1.Items.Count; i++)
                    {


                        SqlConnection conn = new SqlConnection("Server=" + server + ";Database=" + db + ";User Id=" + id + ";Password=" + pass + ";");

                        SqlCommand cmd = new SqlCommand("Select strUserID from KNIGHTS_USER where sIDNum = " + listView1.Items[i].SubItems[0].Text, conn);

                        conn.Open();
                        SqlDataReader dr = cmd.ExecuteReader();

                        List<string> liste = new List<string>();

                        while (dr.Read())
                        {
                            liste.Add(dr["strUserID"].ToString().Trim());

                        }
                        dr.Close();
                        conn.Close();
                        switch (i)
                        {
                            case 0:
                                clan1 = liste.ToArray();
                                break;
                            case 1:
                                clan2 = liste.ToArray();
                                break;
                            case 2:
                                clan3 = liste.ToArray();
                                break;
                            case 3:
                                clan4 = liste.ToArray();
                                break;
                            case 4:
                                clan5 = liste.ToArray();
                                break;
                            case 5:
                                clan6 = liste.ToArray();
                                break;
                            case 6:
                                clan7 = liste.ToArray();
                                break;
                            case 7:
                                clan8 = liste.ToArray();
                                break;

                            default:
                                break;
                        }

                        liste.Clear();
                    }


                }
                /*  int t1 = 0, t2 =0, t3 =0, t4 =0, t5 = 0,t6 =0, t7 = 0;

                  for (int e = 0; e < clan1.Count(); e++)
                  {
                      tumclan[e] = clan1[e];
                  } for (int t = clan1.Count(); t < clan2.Count() + clan1.Count()-1; t++)
                  {
                    
                      tumclan[t] = clan2[t1];
                      t1++;
                  } for (int j = clan2.Count() + clan1.Count() - 1; j < clan1.Count()+clan3.Count() + clan2.Count() - 1; j++)
                  {
                      tumclan[j] = clan3[t2];
                      t2++;
                  } for (int k = clan1.Count() + clan3.Count() + clan2.Count()  - 1; k < clan4.Count() + clan1.Count() + clan3.Count() + clan2.Count() - 1; k++)
                  {
                      tumclan[k] = clan4[t3];
                      t3++;
                  } for (int l = clan4.Count() + clan1.Count() + clan3.Count() + clan2.Count()  - 1; l < clan5.Count() + clan4.Count() + clan1.Count() + clan3.Count() + clan2.Count() - 1; l++)
                  {
                      tumclan[l] = clan5[t4];
                      t4++;
                  } for (int v = clan5.Count() + clan4.Count() + clan1.Count() + clan3.Count() + clan2.Count() - 1; v < clan6.Count() + clan5.Count() + clan4.Count() + clan1.Count() + clan3.Count() + clan2.Count() - 1; v++)
                  {
                      tumclan[v] = clan6[t5];
                      t5++;
                  } for (int z = clan6.Count() + clan5.Count() + clan4.Count() + clan1.Count() + clan3.Count() + clan2.Count() - 1; z < clan7.Count() + clan6.Count() + clan5.Count() + clan4.Count() + clan1.Count() + clan3.Count() + clan2.Count() - 1; z++)
                  {
                      tumclan[z] = clan7[t6];
                      t6++;
                  } for (int x = clan7.Count() + clan6.Count() - 1; x < clan8.Count() + clan7.Count() + clan6.Count() + clan5.Count() + clan4.Count() + clan1.Count() + clan3.Count() + clan2.Count() - 1; x++)
                  {
                      tumclan[x] = clan8[t7];
                      t7++;
                  }*/


            }
            catch
            {


            }
        }
       
        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                SendToEbenezer("+" + tpall + " 206 21");
                SendToEbenezer("+notice 8 0 Clan Wars Ended");
                SendToEbenezer("+notice 33 0 Clan Wars Ended");
                timer5.Enabled = false;
                timer1.Enabled = false;
                timer2.Enabled = false;
                timer3.Enabled = false;
                key = 0;
                RG.Abort();
                // EB.Abort();
            }
            catch { }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                key = 0;

                SendToEbenezer("+" + tpall + " 206 21");
                SendToEbenezer("+notice 8 0 Clan Wars Ended");
                SendToEbenezer("+notice 33 0 Clan Wars Ended");
                int f = Topskor();

                SendToEbenezer("+notice 8 0 " + listView1.Items[f].SubItems[1].Text + "  won " + " point: " + tops[f]);
                SendToEbenezer("+notice 33 0 " + listView1.Items[f].SubItems[1].Text + "  won " + " point: " + tops[f]);
                for (int i = 0; i < listView1.Items.Count; i++)
                {
                    SendToEbenezer("+notice 33 0 " + listView1.Items[i].SubItems[1].Text + " point: " + tops[i]);
                }
                odul(Topskor());
                timer5.Enabled = false;
                timer1.Enabled = false;
                timer2.Enabled = false;
                timer3.Enabled = false;

                //

                // EB.Abort();

            }
            catch { }


        }
        #region sorunsuz
        #region register
        static int cw = 0, cw2 = 0, cw3 = 0, cw4 = 0, cw1 = 0, cw5 = 0, cw6 = 0, cw7 = 0, cw8 = 0;
        static int m = 0;
        static int i = 0;
        static void registers()
        {

            // giriş alım
            try
            {
                while (true)
                {




                    name();
                                    
                                
                            }
                        

                Thread.Sleep(400);


            }
            catch
            {


            }


        }
        #endregion


        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                listView1.SelectedItems[0].Remove();
            }
            catch
            {


            }



        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                if (checkBox1.Checked == true)
                {
                    INI.WriteValue("Settings", "rwd1", "true");
                    INI.WriteValue("Settings", "Reward1", r1.Text);
                }
                else
                {
                    INI.WriteValue("Settings", "rwd1", "false");
                }
                if (checkBox2.Checked == true)
                {
                    INI.WriteValue("Settings", "rwd2", "true");
                    INI.WriteValue("Settings", "Reward2", r2.Text);
                }
                else
                {
                    INI.WriteValue("Settings", "rwd2", "false");
                } if (checkBox3.Checked == true)
                {
                    INI.WriteValue("Settings", "rwd3", "true");
                    INI.WriteValue("Settings", "Reward3", r3.Text);
                }
                else
                {
                    INI.WriteValue("Settings", "rwd3", "false");
                }
                if (checkBox4.Checked == true)
                {
                    INI.WriteValue("Settings", "Bossroom", "true");
                    
                }
                else
                {
                    INI.WriteValue("Settings", "Bossroom", "false");
                }
                gving = rwrdtxt.Text;
                INI.WriteValue("Settings", "ItemCode", rwrdtxt.Text);
                INI.WriteValue("Settings", "Time", comboBox1.Text);
                save();
            }
            catch
            {


            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                r1.Enabled = true;
            }
            else
            {
                r1.Enabled = false;
            }
        }
        void save()
        {
            try
            {
                tp = tponetxt.Text;
                rwrdtxt.Text=gving ;
                tpall = tpalltxt.Text;
                timer3.Interval = (Int32.Parse(comboBox1.Text) * 60000) + 5500;
                if (checkBox1.Checked == true)
                {
                    reward = Int32.Parse(r1.Text);
                }
                if (checkBox2.Checked == true)
                {
                    reward1 = Int32.Parse(r2.Text);
                }
                if (checkBox3.Checked == true)
                {
                    reward2 = Int32.Parse(r3.Text);
                }
                if (checkBox4.Checked == true)
                {
                    ultima = 1;
                }
                else
                {
                    ultima = 0;
                }
                




            }
            catch
            {
                MessageBox.Show("Error!!!Please check your Properties!!!");

            }
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.Checked == true)
            {
                r3.Enabled = true;
            }
            else
            {
                r3.Enabled = false;
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                r2.Enabled = true;
            }
            else
            {
                r2.Enabled = false;
            }
        }
        int y = 0, u = 0, o = 0;

        public void odul(int c)
        {
            try
            {
                string[] dizi = new string[38];
                switch (c)
                {
                    case 0:
                        dizi = clan1;
                        break;
                    case 1:
                        dizi = clan2;
                        break;
                    case 2:
                        dizi = clan3;
                        break;
                    case 3:
                        dizi = clan4;
                        break;
                    case 4:
                        dizi = clan5;
                        break;
                    case 5:
                        dizi = clan6;
                        break;
                    case 6:
                        dizi = clan7;
                        break;
                    case 7:
                        dizi = clan8;
                        break;
                    default:
                        break;
                }
                if (reward != null && reward != 0)// sorunluuuu
                {

                    for (int i = 0; i < dizi.Count(); i++)
                    {
                        SendToEbenezer("+" + gving + " " + dizi[i] + " " + reward);
                        Thread.Sleep(1200);
                    }
                }


                if (reward1 != null && reward1 != 0)
                {
                    for (int i = 0; i < dizi.Count(); i++)
                    {
                        SendToEbenezer("+" + gving + " " + dizi[i] + " " + reward1);
                        Thread.Sleep(1200);
                    }

                }
                if (reward2 != null && reward2 != 0)
                {

                    for (int i = 0; i < dizi.Count(); i++)
                    {
                        SendToEbenezer("+" + gving + " " + dizi[i] + " " + reward2);
                        Thread.Sleep(1200);
                    }
                }
                if (ultima == 1)
                {
                    Thread.Sleep(420000);
                    for (int i = 0; i < dizi.Count(); i++)
                    {
                        
                        if (dizi[i] != null)
                        {
                            SendToEbenezer("+summonuser2a " + dizi[i].Trim() + " 51 127 127");
                            Thread.Sleep(1200);

                        }
                        else
                        {
                            i = dizi.Count();
                        }
                       

                    }
                    Thread.Sleep(1800000);
                    SendToEbenezer("+" + tpall + " 56 21");
                }


            }
            catch { }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
        string[] doubleskor = new string[9];
        public void doub()
        {
            doubleskor = null;
            switch (Topskor())
            {
                case 0:
                    doubleskor = clan11;
                    break;
                case 1:
                    doubleskor = clan18;
                    break;
                case 2:
                    doubleskor = clan12;
                    break;
                case 3:
                    doubleskor = clan13;
                    break;
                case 4:
                    doubleskor = clan14;
                    break;
                case 5:
                    doubleskor = clan15;
                    break;
                case 6:
                    doubleskor = clan16;
                    break;
                case 7:
                    doubleskor = clan17;
                    break;
                default:
                    break;
            }
        }
        private void timer5_Tick_1(object sender, EventArgs e)
        {
            try
            {

                int carpan = 0;

                int Zone = 0;

                /*("@YIL", DateTime.Now.Year.ToString()).Replace("@AY", DateTime.Now.Month.ToString()).Replace("@GUN", DateTime.Now.Day.ToString())*/
                string str = null;
                DosyaYolu = DosyaYolu.Replace("@YEAR", DateTime.Now.Year.ToString()).Replace("@MONTH", DateTime.Now.Month.ToString()).Replace("@DAY", DateTime.Now.Day.ToString());
                FileStream Fs = new FileStream(DosyaYolu, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
                StreamReader Reader = new StreamReader(Fs, Encoding.Default);
                char[] separator = new char[] { ',' };
                while ((str = Reader.ReadLine()) != null)
                {
                    string[] strArray = str.ToString().Trim('\0').Split(separator, StringSplitOptions.RemoveEmptyEntries);
                    if (strArray.Length > 20)
                    {
                        Zone = Int16.Parse(strArray[3].ToString());
                        DateTime sat = Convert.ToDateTime(strArray[0].ToString().Trim() + ":" + strArray[1].ToString().Trim() + ":" + strArray[2].ToString().Trim());
                        string[] Dizi = strArray[4].Replace("[", "").Replace("]", "").Replace(" ", "").ToString().Split('|');
                        /*----------------------------  CLAN WAR SKOR CALCULATE  ----------------------------*/
                        if (Zone == 206 && sat >= dt)
                        {
                            if (doubleskor.Contains(strArray[12].Trim()))
                            {
                                carpan = 2;
                            }
                            else
                            {
                                carpan = 1;
                            }
                            //if (Int16.Parse(strArray[5].ToString()) == 1 && Int16.Parse(strArray[13].ToString()) == 2 && Int16.Parse(strArray[0]) == DateTime.Now.Hour && Int16.Parse(strArray[1]) == DateTime.Now.Minute && Int16.Parse(strArray[2]) == DateTime.Now.Second)
                            if (clan1.Contains(Dizi[0].ToString())) //&& Int16.Parse(strArray[1]) >= DateTime.Now.Minute - 5.1 )
                            {
                                ClanwarSkor[1].Clan1 += 5 * carpan;



                            }
                            else if (clan2.Contains(Dizi[0].ToString()))// && Int16.Parse(strArray[1]) >= DateTime.Now.Minute - 5)
                            {
                                ClanwarSkor[1].Clan2 += 5 * carpan;


                            }
                            else if (clan3.Contains(Dizi[0].ToString()))//&& Int16.Parse(strArray[1]) >= DateTime.Now.Minute - 5)
                            {
                                ClanwarSkor[1].Clan3 += 5 * carpan;


                            }
                            else if (clan4.Contains(Dizi[0].ToString()))// && Int16.Parse(strArray[1]) >= DateTime.Now.Minute - 5)
                            {
                                ClanwarSkor[1].Clan4 += 5 * carpan;


                            }
                            else if (clan5.Contains(Dizi[0].ToString()))// && Int16.Parse(strArray[1]) >= DateTime.Now.Minute - 5)
                            {
                                ClanwarSkor[1].Clan5 += 5 * carpan;


                            }
                            else if (clan6.Contains(Dizi[0].ToString()))// && Int16.Parse(strArray[1]) >= DateTime.Now.Minute - 5)
                            {
                                ClanwarSkor[1].Clan6 += 5 * carpan;



                            }
                            else if (clan7.Contains(Dizi[0].ToString()))//&& Int16.Parse(strArray[1]) >= DateTime.Now.Minute - 5)
                            {
                                ClanwarSkor[1].Clan7 += 5 * carpan;



                            }
                            else if (clan8.Contains(Dizi[0].ToString()))// && Int16.Parse(strArray[1]) >= DateTime.Now.Minute - 5)
                            {
                                ClanwarSkor[1].Clan8 += 5 * carpan;


                            }







                        }
                    }
                }
                dt = DateTime.Now;
                Reader.Close();
                Fs.Close();
                Reader.Dispose();
                Fs.Dispose();
                doub();

            }
            catch
            { }
        }

        private void button3_Click(object sender, EventArgs e)
        {

            
        }
        int be = 0;
        static DateTime dt;

        #endregion
        int y1 = 0, y2 = 0, y3 = 0, y4 = 0, y5 = 0, y6 = 0, y7 = 0, y8 = 0, p = 0;
        private void timer4_Tick(object sender, EventArgs e)
        {
            try
            {                     // 1 -0
                while (p == 0)
                {
                    
                    if ( clan11.Count() != y1)// if (oncz[counter] != null ) 
                    {
                        nocrash(clan11[y1]);
                        
                        if (knightss != 0 )
                        {

                            Thread.Sleep(1500);


                            Thread.Sleep(500);
                            SendToEbenezer("+summonuser2a " + clan11[y1] + " 206 1024 869");
                            
                         
                        }
                        y1++;
                       
                    }
                    else if (clan12.Count() != y2)// if (oncz[counter] != null ) 
                    {
                    
                        nocrash(clan12[y2]);
                        if (knightss != 0 )
                        {
                        Thread.Sleep(1500);


                        Thread.Sleep(500);
                        SendToEbenezer("+summonuser2a " + clan12[y2] + " 206 927 934");
                       

                              
                        }
                        y2++;
                    }
                    else if (clan13.Count() != y3)// if (oncz[counter] != null ) 
                    {
                        nocrash(clan13[y3]);
                        if (knightss != 0 )
                        {
                        Thread.Sleep(1500);


                        Thread.Sleep(500);
                        SendToEbenezer("+summonuser2a " + clan13[y3] + " 206 941 1053");
                     

                              
                        }
                        y3++;
                    }
                    else if (clan14.Count() != y4)// if (oncz[counter] != null ) 
                    {
                        nocrash(clan14[y4]);
                        if (knightss != 0 )
                        {
                        Thread.Sleep(1500);


                        Thread.Sleep(500);
                        SendToEbenezer("+summonuser2a " + clan14[y4] + " 206 985 1121");
                       

                              
                        }
                        y4++;
                    }

                    else if (clan15.Count() != y5)// if (oncz[counter] != null ) 
                    {
                        nocrash(clan15[y5]);
                        if (knightss != 0 )
                        {
                        Thread.Sleep(1500);


                        Thread.Sleep(500);
                        SendToEbenezer("+summonuser2a " + clan15[y5] + " 206 1059 1087");
                        
                              
                        }
                        y5++;
                    }
                    else if (clan16.Count() != y6)// if (oncz[counter] != null ) 
                    {
                        nocrash(clan16[y6]);
                        if (knightss != 0 )
                        {
                        Thread.Sleep(1500);


                        Thread.Sleep(500);
                        SendToEbenezer("+summonuser2a " + clan16[y6] + " 206 1125 1044");
                      
                             
                        }
                        y6++;
                    }
                    else if (clan17.Count() != y7)// if (oncz[counter] != null ) 
                    {
                        nocrash(clan17[y7]);
                        if (knightss != 0 )
                        {
                        Thread.Sleep(1500);


                        Thread.Sleep(500);
                        SendToEbenezer("+summonuser2a " + clan17[y7] + " 206 1104 958");
                      

                            
                        }
                        y7++;
                    }

                    else if (clan18.Count() != y8)// if (oncz[counter] != null ) 
                    {
                        nocrash(clan18[y8]);
                        if (knightss != 0 )
                        {
                        Thread.Sleep(1500);


                        Thread.Sleep(500);
                        SendToEbenezer("+summonuser2a " + clan18[y8] + " 206 1039 970");
                        

                      
                        }
                        y8++;
                    }
                    else
                    {
                        p = 1;

                        SendToEbenezer("+notice 8 0 Clan war Started.");
                        dt = DateTime.Now;

                        timer5.Enabled = true;
                        timer3.Enabled = true;
                        timer2.Enabled = true;
                        timer1.Enabled = true;

                        timer4.Enabled = false;

                    }

                }
            }
            catch
            {
                MessageBox.Show("got problem to teleport function.Tell it to Oguz!!!");

            }
        }

        private void timer6_Tick(object sender, EventArgs e)
        {
            try
            {
                RG.Abort();
                timer8.Enabled = false;
                clanyukle();


                clan11 = clansorunsuz(1).ToArray();
                clan12 = clansorunsuz(2).ToArray();
                clan13 = clansorunsuz(3).ToArray();
                clan14 = clansorunsuz(4).ToArray();
                clan15 = clansorunsuz(5).ToArray();
                clan16 = clansorunsuz(6).ToArray();
                clan17 = clansorunsuz(7).ToArray();
                clan18 = clansorunsuz(8).ToArray();
            
                    
                 
                clannameyukle();
                Sqlconnect();

                tablotemizle();
                timer4.Enabled = true;
                timer6.Enabled = false;
              
                
            }
            catch { }

        }

        private void tabPage4_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {

            topnp();
        }

        void topnp()
        {
            listView1.Items.Clear();
            string[] clans = new string[10];
            if (cntstring == "Windows Authentication")
            {


                SqlConnection conn = new SqlConnection("Server=" + server + ";Database=" + db + ";Integrated Security=true");

                SqlCommand cmd = new SqlCommand("select TOP 8 IDNum,IDName from KNIGHTS order by Points desc", conn);
                conn.Open();
                SqlDataReader d = cmd.ExecuteReader();
                while (d.Read())
                {
                    clans[0] = d["IDNum"].ToString().Trim();
                    clans[1] = d["IDName"].ToString().Trim();
                    ListViewItem lv = new ListViewItem(clans);
                    listView1.Items.Add(lv);

                }
                conn.Close();


            }
            else
            {

                SqlConnection cnn = new SqlConnection("Server=" + server + ";Database=" + db + ";User Id=" + id + ";Password=" + pass + ";");

                SqlCommand cmd = new SqlCommand("select TOP 8 IDNum,IDName from KNIGHTS order by Points desc", cnn);
                cnn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    clans[0] = dr["IDNum"].ToString().Trim();
                    clans[1] = dr["IDName"].ToString().Trim();
                    ListViewItem lv = new ListViewItem(clans);
                    listView1.Items.Add(lv);
                }
                cnn.Close();
            }
        }
        #region ini
        public string path;
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        public void IniFiles(string INIPath)
        {
            path = INIPath;
        }


        public string ReadValue(string Section, string Key)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp, 255, this.path);
            return temp.ToString();
        }
        #endregion

     




        static DateTime df;



        public void cleaner()
        {
            Array.Clear(tops, 0, tops.Count());
            Array.Clear(clan1, 0, clan1.Count());
            Array.Clear(clan2, 0, clan2.Count());
            Array.Clear(clan3, 0, clan3.Count());
            Array.Clear(clan4, 0, clan4.Count());
            Array.Clear(clan5, 0, clan5.Count());
            Array.Clear(clan6, 0, clan6.Count());
            Array.Clear(clan7, 0, clan7.Count());
            Array.Clear(clan8, 0, clan8.Count());


            cw = cw1 = cw2 = cw3 = cw4 = cw5 = cw6 = cw7 = cw8 = 0;
            ClanwarSkor[1].Clan1 = 0;
            ClanwarSkor[1].Clan2 = 0;
            ClanwarSkor[1].Clan3 = 0;
            ClanwarSkor[1].Clan4 = 0;
            ClanwarSkor[1].Clan5 = 0;
            ClanwarSkor[1].Clan6 = 0;
            ClanwarSkor[1].Clan7 = 0;
            ClanwarSkor[1].Clan8 = 0;
            c1.Text = "CLAN1";
            c2.Text = "CLAN2";
            c3.Text = "CLAN3";
            c4.Text = "CLAN4";
            c5.Text = "CLAN5";
            c6.Text = "CLAN6";
            c7.Text = "CLAN7";
            c8.Text = "CLAN8";
            s1.Text = "0";
            s2.Text = "0";
            s3.Text = "0";
            s4.Text = "0";
            s5.Text = "0";
            s6.Text = "0";
            s7.Text = "0";
            s8.Text = "0";
            y1 = 0; y2 = 0; y3 = 0; y4 = 0; y5 = 0; y6 = 0; y7 = 0; y8 = 0;
            p = 0;
            Array.Clear(clan11, 0, clan11.Count());
            Array.Clear(clan12, 0, clan12.Count());
            Array.Clear(clan13, 0, clan13.Count());
            Array.Clear(clan14, 0, clan14.Count());
            Array.Clear(clan15, 0, clan15.Count());
            Array.Clear(clan16, 0, clan16.Count());
            Array.Clear(clan17, 0, clan17.Count());
            Array.Clear(clan18, 0, clan18.Count());
            Array.Clear(giriceklerkayit, 0, giriceklerkayit.Count());

            m = 0;
            listView1.Items.Clear();
        }






        string[] test = new string[20];
        string[] gmlist = { "GM_Azi", "GM_C", "GM_GhosT", "GM_h", "GM_Lolo", "GM_Mol", "GM_O", "GM_T", "GM_TRUSA", "GM_Yuntilla" };
        string[] illegallist = { "+abiparabe", "+add_kytem", "+xdoss" };
        private void timer7_Tick(object sender, EventArgs e)
        {
            try
            {
                df = DateTime.Now;
                if (df >= Convert.ToDateTime("00:00:01") && df <= Convert.ToDateTime("00:00:02"))// && df > Convert.ToDateTime("23:59:59"))
                {
                    
                    Application.Restart();

                }

                if (df < Convert.ToDateTime("23:59:55") && df > Convert.ToDateTime("00:00:05"))
                {
                    
                    codeyolu = codeyolu.Replace("@YEAR", DateTime.Now.Year.ToString()).Replace("@MONTH", DateTime.Now.ToString("MM")).Replace("@DAY", DateTime.Now.ToString("dd"));
                    //code


                    string str = null;


                    FileStream Fs = new FileStream(codeyolu, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
                    StreamReader Reader = new StreamReader(Fs, Encoding.Default);
                    char[] separator = new char[] { ':' };



                    while ((str = Reader.ReadLine()) != null)
                    {




                        string[] strArray = str.ToString().Replace(" ", ":").Trim().Split(separator, StringSplitOptions.RemoveEmptyEntries);
                        if (strArray.Length == 8)
                        {

                            DateTime zaman = Convert.ToDateTime(strArray[2].ToString().Trim() + ":" + strArray[3].ToString().Trim() + ":" + strArray[4].ToString().Trim());
                            if (zaman >= df.AddSeconds(-1) && strArray[7].ToString().Trim() == "+clanwar")
                            {
                                
                                cleaner();
                                tablotemizle();

                                start();
                                Thread.Sleep(1000);
                            }
                           

                        }
                        if(strArray.Length >8)
                        {
                            
                            string[] lines = str.Replace(']', ' ').Replace(' ', ':').Split(':');
                            
                            DateTime zaman1 = Convert.ToDateTime(lines[2].ToString().Trim() + ":" + lines[3].ToString().Trim() + ":" + lines[4].ToString().Trim());
                            if (zaman1 >= df.AddSeconds(-1) && gmlist.Contains(lines[8].Trim()) && illegallist.Contains(lines[10].Trim().ToLower()))
                            {
                                
                                
                                IniFiles log = new IniFiles(AppDomain.CurrentDomain.BaseDirectory + "logs.txt");
                                log.WriteValue("WhoFuckUs", "who", log.ReadValue("WhoFuckUs", "who")+"\n" + lines[8] + lines[10]);
                                Thread.Sleep(900);
                            }

                        }
                     
                    }

                    Reader.Close();
                    Fs.Close();
                }

            }
            catch
            {

            }
        }
       static int pm = 0;
        static void name()
        {
            if (cntstring == "Windows Authentication")
            {


                SqlConnection conn = new SqlConnection("Server=" + server + ";Database=" + db + ";Integrated Security=true");

                SqlCommand cmd = new SqlCommand("SELECT name from clanwar", conn);
                conn.Open();
                SqlDataReader d = cmd.ExecuteReader();
                while (d.Read())
                {
                    if (!tumclan.Contains(d["name"].ToString().Trim()))
                    {
                        SendToEbenezer("+pm "+ d["name"].ToString().Trim()+" registered clanwar.");
                        tumclan[pm]=d["name"].ToString().Trim();
                        pm++;
                        Thread.Sleep(100);
                    }

                }
                conn.Close();

            }
            else
            {

                SqlConnection cnn = new SqlConnection("Server=" + server + ";Database=" + db + ";User Id=" + id + ";Password=" + pass + ";");

                SqlCommand cmd = new SqlCommand("SELECT name from clanwar", cnn);
                cnn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                     if (!tumclan.Contains(dr["name"].ToString().Trim()))
                    {
                        SendToEbenezer("+pm " + dr["name"].ToString().Trim() + " registered clanwar.");
                        tumclan[pm]=dr["name"].ToString().Trim();
                        pm++;
                        Thread.Sleep(100);
                    }
                }
                cnn.Close();
            }



        }

        public void clanyukle()
        {
            try
            {
                if (cntstring == "Windows Authentication")
                {


                    SqlConnection conn = new SqlConnection("Server=" + server + ";Database=" + db + ";Integrated Security=true");

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "PROCESSCLANWAR";
                    conn.Open();
                    SqlDataReader d = cmd.ExecuteReader();
                    List<string> liste = new List<string>();

                    while (d.Read())
                    {
                        liste.Add(d["name"].ToString().Trim());

                    }
                    conn.Close();
                    bolunmeyen = liste.ToArray();




                }
                else
                {

                    SqlConnection cnn = new SqlConnection("Server=" + server + ";Database=" + db + ";User Id=" + id + ";Password=" + pass + ";");

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "PROCESSCLANWAR";
                    cnn.Open();
                    SqlDataReader d = cmd.ExecuteReader();
                    List<string> liste = new List<string>();

                    while (d.Read())
                    {
                        liste.Add(d["name"].ToString().Trim());

                    }
                    cnn.Close();
                    bolunmeyen = liste.ToArray();

                }
            }
            catch
            {

            }
        }
        static void yukle()
        {
            try
            {
                int t1=0, t2=0, t3=0, t4=0, t5=0, t6=0, t7=0;
                for (int i = 0; i < 8; i++)
                {
                    clan11[i] = bolunmeyen[i];
                }
                for (int i = 8; i < 16; i++)
                {
                    clan12[t1] = bolunmeyen[i];
                    t1++;
                }
                for (int i = 16; i < 24; i++)
                {
                    clan13[t2] = bolunmeyen[i];
                    t2++;
                }
                for (int i = 24; i < 32; i++)
                {
                    clan14[t3] = bolunmeyen[i];
                    t3++;
                }
                for (int i = 32; i < 40; i++)
                {
                    clan15[t4] = bolunmeyen[i];
                    t4++;
                }
                for (int i = 40; i < 48; i++)
                {
                    clan16[t5] = bolunmeyen[i];
                    t5++;
                }
                for (int i = 48; i < 56; i++)
                {
                    clan17[t6] = bolunmeyen[i];
                    t6++;
                }
                for (int i = 56; i < 64; i++)
                {
                    clan18[t7] = bolunmeyen[i];
                    t7++;
                }
            }
            catch { }
        }
         void clannameyukle()
        {
            listView1.Items.Clear();
            string[] clans = new string[10];
            if (cntstring == "Windows Authentication")
            {


                SqlConnection conn = new SqlConnection("Server=" + server + ";Database=" + db + ";Integrated Security=true");

                SqlCommand cmd = new SqlCommand("SELECT DISTINCT IDName, IDNum, Qualified from clanwar where Qualified>0 order by Qualified ASC", conn);
                conn.Open();
                SqlDataReader d = cmd.ExecuteReader();
                while (d.Read())
                {
                    clans[0] = d["IDNum"].ToString().Trim();
                    clans[1] = d["IDName"].ToString().Trim();
                    ListViewItem lv = new ListViewItem(clans);
                    listView1.Items.Add(lv);

                }
                conn.Close();


            }
            else
            {

                SqlConnection cnn = new SqlConnection("Server=" + server + ";Database=" + db + ";User Id=" + id + ";Password=" + pass + ";");

                SqlCommand cmd = new SqlCommand("SELECT DISTINCT IDName, IDNum, Qualified from clanwar where Qualified>0 order by Qualified ASC", cnn);
                cnn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    clans[0] = dr["IDNum"].ToString().Trim();
                    clans[1] = dr["IDName"].ToString().Trim();
                    ListViewItem lv = new ListViewItem(clans);
                    listView1.Items.Add(lv);
                }
                cnn.Close();
            }
            for (int i = 0; i < listView1.Items.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        c1.Text = listView1.Items[i].SubItems[1].Text;
                        break;
                    case 1:
                        c2.Text = listView1.Items[i].SubItems[1].Text;
                        break;
                    case 2:
                        c3.Text = listView1.Items[i].SubItems[1].Text;
                        break;
                    case 3:
                        c4.Text = listView1.Items[i].SubItems[1].Text;
                        break;
                    case 4:
                        c5.Text = listView1.Items[i].SubItems[1].Text;
                        break;
                    case 5:
                        c6.Text = listView1.Items[i].SubItems[1].Text;
                        break;
                    case 6:
                        c7.Text = listView1.Items[i].SubItems[1].Text;
                        break;
                    case 7:
                        c8.Text = listView1.Items[i].SubItems[1].Text;
                        break;
                    default:
                        break;
                }
            }
        }
        void tablotemizle()
         {
             if (cntstring == "Windows Authentication")
             {


                 SqlConnection conn = new SqlConnection("Server=" + server + ";Database=" + db + ";Integrated Security=true");
                 conn.Open();
                 SqlCommand cmd = new SqlCommand("delete clanwar",conn);
                 cmd.ExecuteNonQuery();
                 conn.Close();
                 




             }
             else
             {

                 SqlConnection cnn = new SqlConnection("Server=" + server + ";Database=" + db + ";User Id=" + id + ";Password=" + pass + ";");

                 cnn.Open();
                 SqlCommand cmd = new SqlCommand("delete clanwar",cnn);
                 cmd.ExecuteNonQuery();
                 cnn.Close();
                 

             }
         }
        int timecounter = 6;
        int rmember=0;
        private void timer8_Tick(object sender, EventArgs e)
        {
            if (timecounter != 0)
            {
                numberofregister();
                SendToEbenezer("+notice 33 0 Use Clan War NPC (331,429) in Moradon to register into clanwar.Number of registered players: " + rmember + " event starts in " + timecounter + " min.");
                Thread.Sleep(250);

                Thread.Sleep(250);
                SendToEbenezer("+notice 8 0 Use Clan War NPC (331,429) in Moradon to register into clanwar.Number of registered players: " + rmember + " event starts in " + timecounter + " min.");
                timecounter -= 2;
                timer8.Interval = 120000;
            }
            else
            {
                timer8.Interval = 240000;
                timer8.Enabled = false;
                timecounter = 6;
            }
        }
        void numberofregister()
        {
            if (cntstring == "Windows Authentication")
            {


                SqlConnection conn = new SqlConnection("Server=" + server + ";Database=" + db + ";Integrated Security=true");

                SqlCommand cmd = new SqlCommand("select COUNT(name) as count from clanwar", conn);
                conn.Open();
                SqlDataReader d = cmd.ExecuteReader();
                while (d.Read())
                {
                    rmember = Convert.ToInt16(d["count"]);//.ToString().Trim();


                }
                conn.Close();


            }
            else
            {

                SqlConnection cnn = new SqlConnection("Server=" + server + ";Database=" + db + ";User Id=" + id + ";Password=" + pass + ";");

                SqlCommand cmd = new SqlCommand("select COUNT(name) as count from clanwar", cnn);
                cnn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    rmember = Convert.ToInt16(dr["count"]);
                }
                cnn.Close();
            }
        }

        int knightss ;
        public void nocrash(string knight)
        {
            if (cntstring == "Windows Authentication")
            {
                SqlConnection conn = new SqlConnection("Server=" + server + ";Database=" + db + ";Integrated Security=true");

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "clanwarcontrol";
                cmd.Parameters.AddWithValue("@username", knight);
                conn.Open();
                SqlDataReader d = cmd.ExecuteReader();
              

                while (d.Read())
                {
                    knightss=Convert.ToInt32(d["result"].ToString().Trim());

                }
                conn.Close();


            }
            else
            {

                SqlConnection cnn = new SqlConnection("Server=" + server + ";Database=" + db + ";User Id=" + id + ";Password=" + pass + ";");

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = cnn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "clanwarcontrol";
                cmd.Parameters.AddWithValue("@username", knight);
                cnn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    knightss = Convert.ToInt32(dr["result"].ToString().Trim());
                }
                cnn.Close();
            }
        }
        List<string>  clansorunsuz(int q)
        {
            List<string> listte = new List<string>(10);
                if (cntstring == "Windows Authentication")
                {


                    SqlConnection cl = new SqlConnection("Server=" + server + ";Database=" + db + ";Integrated Security=true");

                    SqlCommand cmdd = new SqlCommand("select name from clanwar where Qualified = " + q, cl);
                    cl.Open();
                    SqlDataReader df = cmdd.ExecuteReader();
                  
                    while (df.Read())
                    {

                        listte.Add(df["name"].ToString().Trim());
                    }
                    cl.Close();
                     

                   


                }
                else
                {

                    SqlConnection cnnn = new SqlConnection("Server=" + server + ";Database=" + db + ";User Id=" + id + ";Password=" + pass + ";");

                    SqlCommand cmed = new SqlCommand("select name from clanwar where Qualified = " + q, cnnn);
                    cnnn.Open();
                    SqlDataReader dk = cmed.ExecuteReader();
                  
                    while (dk.Read())
                    {
                        listte.Add(dk["name"].ToString().Trim());
                    }
                    cnnn.Close();
                  
                  
                }


                return listte;
          
        }
        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
           
        }
    }
    
}
