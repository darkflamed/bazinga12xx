﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClanWar
{
    public  partial class Form2 : Form
    {
        public static string cntstring;
        public static string DosyaYolu ,chatyolu;
        public static string id, pass, server, db;
        public static string eben;
        public Form2()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                cntstring = comboBox2.Text;
                if (cntstring == "Windows Authentication")
                {
                    server = servertxt.Text;
                    db = dbtxt.Text;

                    SqlConnection conn = new SqlConnection("Server=" + server + ";Database=" + db + ";Integrated Security=true");
                    conn.Open();
                    conn.Close();
                }
                else
                {
                    server = servertxt.Text;
                    db = dbtxt.Text;
                    id = idtxt.Text;
                    pass = pstxt.Text;
                    SqlConnection cnn = new SqlConnection("Server=" + server + ";Database=" + db + ";User Id=" + id + ";Password=" + pass + ";");
                    cnn.Open();
                    cnn.Close();
                }
                eben = txt2.Text;
                if (button2.Enabled == false && button1.Enabled == false)
                {
                    Form1 fm = new Form1();
                    this.Hide();
                    fm.Show();
                }
                else
                {
                    MessageBox.Show("Please Choose logs!!!");
                }

            }
            catch
            {
                MessageBox.Show("Sql informations arenot true");
                
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fd = new OpenFileDialog();
                fd.Title = "Choose log";

                fd.ShowDialog();
                DosyaYolu = fd.FileName;
                MessageBox.Show("Logs choosed!!!");
                button2.Enabled = false;

            }
            catch
            { MessageBox.Show("Error!!!Please Choose logs again!!!"); }
        }

        private void Form2_Load(object sender, EventArgs e)
        {

            comboBox2.Items.Add("Windows Authentication");
            comboBox2.Items.Add("SQL Server Authentication");
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            cntstring = comboBox2.Text;
            if (cntstring == "Windows Authentication")
            {
                servertxt.Enabled = true;
                dbtxt.Enabled = true;
                idtxt.Enabled = false;
                pstxt.Enabled = false;
            }
            else
            {
                servertxt.Enabled = true;
                dbtxt.Enabled = true;
                idtxt.Enabled = true;
                pstxt.Enabled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fd = new OpenFileDialog();
                fd.Title = "Choose log";

                fd.ShowDialog();
                chatyolu = fd.FileName;
                MessageBox.Show("Logs choosed!!!");
                button1.Enabled = false;

            }
            catch
            { MessageBox.Show("Error!!!Please Choose logs again!!!"); }
        }
    }
}
