<?php
ob_start();
session_start();
include("db/odbc.php");
include('guvenlik/antisqlinject.php');
function get($deger)
{
	global $_GET;
	$deger = $_GET[$deger];
	$karakterler = array(chr(39),chr(92),chr(34),"-","/",";");
	$deger = str_replace($karakterler,"",$deger);
	return $deger;
}
function pretty_number($n, $floor = true) {
	if ($floor) {
		$n = floor($n);
	}
	return number_format($n, 0, ",", ".");
}
?>