﻿<style>
.info, .success, .warning, .error, .validation {
border: 1px solid;
margin: 10px 0px;
padding:15px 10px 15px 50px;
background-repeat: no-repeat;
background-position: 10px center;
}
.validation {
color: #D63301;
background-color: #FFCCBA;
background-image: url('../images/m_validation.png');
}
.info {
color: #00529B;
background-color: #BDE5F8;
background-image: url('../images/m_info.png');
}
.success {
color: #4F8A10;
background-color: #DFF2BF;
background-image:url('../images/m_success.png');
}
.warning {
color: #9F6000;
background-color: #FEEFB3;
background-image: url('../images/m_warning.png');
}
.error {
color: #D8000C;
background-color: #FFBABA;
background-image: url('../images/m_error.png');
}

</style>
<?php
	include('../eng/settings.php');
	include('../eng/functions.php');
	$transaction_id=$get->t; //754823423
	
	$xml_request='DATA=<?xml version="1.0" encoding="utf-8"?>
	<APIRequest>
	<params>
		<cmd>getOrderResult</cmd>
		<token>94f6d7e04a4d452035300f18b984988c</token>
		<transaction_id>'.$transaction_id.'</transaction_id>
	</params>
	</APIRequest>';
	
	$result=curlPost("https://panel.teckcard.com/api.php", $xml_request);
	$server_response=simplexml_load_string($result);

	if($server_response->params->error == "000"){
		if($server_response->params->error_desc == "İşlem başarılı..."){
			$char = $server_response->params->extra_info;
			$gun = $server_response->params->source_user;
			$islemno = $server_response->params->transaction_id;
			$islemtarih = $server_response->params->charge_date;
			
			
			$bilgiler = explode("|",$char);
			$acc = $bilgiler[1];
			$kc = $bilgiler[0];
	
			$no = $db->col("select count(*) from w_Mobil where ReturnCode = '$transaction_id'");
			if($no != 0){
				msg("Bu işlem zaten yapılmış.",1);
				$db->query("insert into w_Mobil_Error (hata,charid,accountid) values ('İşlem tekrarlama denemesi','{$acc}','{$acc}')");
			}else{
				$db->query("update tb_user set cashpoint = cashpoint + {$kc} where strAccountId ='{$acc}'");
				$db->query("insert into w_Mobil (strAccountId,strCharId,Date,TransId,Day,Price,ReturnCode) values ('{$acc}','{$acc}','{$islemtarih}','{$islemno}','{$gun}',1,'$transaction_id')");
				msg("İşleminiz tamamlanmıştır hesabınıza {$kc} eklenmiştir.",3);
			}
		}
	}
	
    function curlPost($url, $params){
    
        $ch = curl_init();   
        curl_setopt($ch, CURLOPT_URL,$url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
        curl_setopt($ch, CURLOPT_TIMEOUT, 20); // times out after 4s
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params); // add POST fields
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        
        
        if (curl_errno($ch)) {
           print curl_error($ch);
        } else {
           curl_close($ch);
        }
        return $result;
    } 
    
    
 ?>