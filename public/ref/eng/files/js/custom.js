// JavaScript Document
$(document).ready(function(){
	/*$('#header .wrapper').mouseenter(function(){
		$('.shown_header').show();
	});
	$('#header .wrapper').mouseleave(function(){
		$('.shown_header').hide();
	});*/
	$('#header .wrapper').bind('mouseenter', function () {
		$('.shown_header').stop().slideDown();
	});
	$('#header .wrapper').bind('mouseleave', function () {
		$('.shown_header').stop().slideUp();
	});
	if($('.notice_board')){
		$('.notice_board .nav ul li a').click(function(){
			var rel = $(this).attr('rel');
			$('.notice_board').find('.tab_content').hide();
			$('.notice_board').find('#'+rel).show();
			$('.notice_board .nav ul li').removeClass('active');
			$(this).parent('li').addClass('active');
			return false;
		});
	}
	if($('.themis_section')){
		$('.themis_section .tabs ul li a').click(function(){
			var rel = $(this).attr('rel');
			$('.themis_section').find('.themis_tab_content').hide();
			$('.themis_section').find('#'+rel).show();
			$('.themis_section .tabs ul li').removeClass('active');
			$(this).parent('li').addClass('active');
			return false;
		});
	}
});