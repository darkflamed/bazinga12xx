<?PHP
/**
 *	@author		: 	SEVENSTYLE
 * 	@copyright	:	2012
 **/
 if( !defined('SEVENPANEL') ) {
    die("Erisim Engellendi! - <b>SEVENSTYLE</b>");
 }
 
 if(isset($_POST['submit'])):
	$yenisifre      =   $sevendbo->SQLSecurity($_POST['newpass']);
	$yenisifre2     =   $sevendbo->SQLSecurity($_POST['newpass2']);
	$yenimail       =   $sevendbo->SQLSecurity($_POST['mail']);
	$gsoru          =   $sevendbo->SQLSecurity($_POST['gsoru']);
	$gyanit         =   $sevendbo->SQLSecurity($_POST['gyanit']);
	$gkod           =   $sevendbo->SQLSecurity($_POST['gkod']);
	$sevendbo->doquery("select MyKOLSoru, MyKOLCevap from _MYKOL where StrAccountID = '$username'");
	$MyKOLSoru = $sevendbo->result(1);
	$MyKOLCevap = $sevendbo->result(2);
		if(!$yenisifre or !$yenisifre2 or !$yenimail or !$gsoru or !$gyanit or !$gkod):
			$sevendbo->uyari(''.$lang['bosalan'].'');
			$sevendbo->yonlendir(''.$base_url.'usercp/editprofile',0);
		elseif($yenisifre != $yenisifre2):
			$sevendbo->uyari(''.$lang['sifreuyusmuyor'].'');
			$sevendbo->yonlendir(''.$base_url.'usercp/editprofile',0);
		elseif($gkod <> $_SESSION["security_code"]):
			$sevendbo->uyari(''.$lang['gyanlis'].'');
			$sevendbo->yonlendir(''.$base_url.'usercp/editprofile',0);
		elseif(!eregi("^[_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.)+[a-z]{2,3}$", $yenimail)):
			$sevendbo->uyari(''.$lang['gecerlimail'].'');
			$sevendbo->yonlendir(''.$base_url.'usercp/editprofile',0);
		elseif(preg_match("/[^a-z, A-Z, 0-9]/", $yenisifre)):
			$sevendbo->uyari(''.$lang['gecerlisifre'].' '.$lang['gecerlikharf'].' : abcdefghijklmnoprstuvyzxwq '.$lang['gecerlibahrf'].' : ABCDEFGHIJKLMNOPRSTUVYZXWQ '.$lang['gecerlirakam'].': 0123456789');
			$sevendbo->yonlendir(''.$base_url.'usercp/editprofile',0);
		elseif($MyKOLSoru <> $gsoru or $MyKOLCevap <> $gyanit):
			$sevendbo->uyari(''.$lang['gsoruyanlis'].'');
			$sevendbo->yonlendir(''.$base_url.'usercp/editprofile',0);
		else:
			$sevendbo->doquery("UPDATE TB_USER Set StrPasswd='$yenisifre', StrEmail='$yenimail' where StrAccountID='$username'");
			$sevendbo->doquery("UPDATE _MYKOL Set MyKOLMail='$yenimail' where StrAccountID='$username'");
			$sevendbo->uyari(''.$lang['islembasarili'].'');
		endif;
 else:
 
 endif;
 
 echo'<form action="'.$base_url.'usercp/editprofile" method="POST">
										<span style="color:red; font-weight:bold">'.$lang['profilduzenle'].'</span>
										<table cellspacing="5" cellpadding="0">
											<tr>
												<td>'.$lang['yenisifre'].'</td>
												<td>:</td>
												<td><input type="password" class="genel_input" name="newpass" /></td>
											</tr>
											<tr>
												<td>'.$lang['yenisifre2'].'</td>
												<td>:</td>
												<td><input type="password" class="genel_input" name="newpass2" /></td>
											</tr>
											<tr>
												<td>'.$lang['yenimail'].'</td>
												<td>:</td>
												<td><input type="text" class="genel_input" name="mail" /></td>
											</tr>
											<tr>
												<td>'.$lang['gsoru'].'</td>
												<td>:</td>
												<td><select class="genel_select" name="gsoru"><option value="">'.$lang['seciniz'].'</option><option value="anneniz">'.$lang['anneniz'].'</option><option value="eniyicocukluk">'.$lang['eniyicocukluk'].'</option><option value="ilkevcilhayvan">'.$lang['ilkevcilhayvan'].'</option><option value="ogretmen">'.$lang['ogretmen'].'</option><option value="tarihsel">'.$lang['tarihsel'].'</option></select></td>
											</tr>
											<tr>
												<td>'.$lang['gcevap'].'</td>
												<td>:</td>
												<td><input type="text" class="genel_input" name="gyanit" /></td>
											</tr>
											<tr>
												<td>'.$lang['guvenlikkodu'].'</td>
												<td>:</td>
												<td><img src="../font/imgcode.php" alt="" /></td>
											</tr>
											<tr>
												<td>'.$lang['guvenlikkodu2'].'</td>
												<td>:</td>
												<td><input type="text" class="genel_input" name="gkod" /></td>
											</tr>
											<tr>
												<td></td>
												<td></td>
												<td align="center"><input type="submit" name="submit" class="genel_submit" value="'.$lang['kaydet'].'" /></td>
											</tr>
										</table>
									</form>';
 
 ?>