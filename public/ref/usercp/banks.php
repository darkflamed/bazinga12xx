<?PHP
/**
 *	@author		: 	SEVENSTYLE
 * 	@copyright	:	2012
 **/
 if( !defined('SEVENPANEL') ) {
    die("Erisim Engellendi! - <b>SEVENSTYLE</b>");
 }
	
	$banks = $sevendbo->doquery("SELECT Banka, Sube, Hesap, Isim, IBAN FROM _BANKALAR ORDER BY id ASC");
	echo '<span style="color:red; font-weight:bold">'.$lang['hesapno'].'</span>
	<table class="siralama" width="100%" style="text-align:center; padding-left:10px; padding-top:5px" cellspacing="3" cellpadding="0">';
		while($sevendbo->row($banks)):
			echo '<tr>
				<td><b>'.$lang['banka'].':</b></td>
				<td>'.$sevendbo->result('Banka').'</td>
			</tr>
			<tr>
				<td><b>'.$lang['sube'].':</b></td>
				<td>'.$sevendbo->result('Sube').'</td>
			</tr>
			<tr>
				<td><b>'.$lang['hesap'].':</b></td>
				<td>'.$sevendbo->result('Hesap').'</td>
			</tr>
			<tr>
				<td><b>IBAN:</b></td>
				<td>'.$sevendbo->result('IBAN').'</td>
			</tr>
			<tr>
      <td><hr style="border: 1px dotted #C0C0C0" size="1" /></td>
      <td><hr style="border: 1px dotted #C0C0C0" size="1" /></td>
      </tr>';
		endwhile;
	echo '</table>';
 ?>