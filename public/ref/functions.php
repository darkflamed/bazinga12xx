<?php
@ob_start();
	function clan_rankings($count,$nation=null){
		global $db,$site;
		if(empty($nation)){ $nation = "1,2"; }
		$q = $db->results("SELECT TOP {$count} k.IDNum , k.Nation , ltrim(rtrim(k.IDName)) as IDName, k.Members,  ltrim(rtrim(k.Chief)) as Chief, SUM(u.Loyalty) as Points,(Select COUNT(*) from CURRENTUSER where strCharID in(select strUserID from USERDATA where Knights = k.IDNum)) as c FROM knights k, knights_user ku, userdata u WHERE k.IDNum = ku.sIDNum AND ku.strUserID COLLATE DATABASE_DEFAULT = u.strUserID COLLATE DATABASE_DEFAULT AND u.Authority != 255 AND u.Authority != 0 and k.Nation in({$nation}) GROUP BY k.IDNum,k.Nation,k.IDName,k.Members,k.Chief ORDER BY SUM(u.Loyalty) DESC");
		$i=0;
		foreach($q as $r){
			$i++;
			if($i%2){ $c = "_light"; }else{ $c = ""; }
			$p2 .= '
				<li><div class="payment_row'.$c.'">
					<div class="ranks_col_rank"><strong>'.$i.'</strong></div>
					<div class="ranks_col_nation"><img style="padding-top:8px;" src="'.$site["FILES"].'images/rank_'.$r->Nation.'.png"/></div>
					<div class="ranksclan_col_clan"><strong><a href="?act=clanprofile&clan='.$r->IDNum.'">'.$r->IDName.'</a></strong>&nbsp;</div>
					<div class="ranks_col_level"><strong>'.$r->Members.' / <font color="green">'.$r->c.'</font></strong></div>
					<div class="ranks_col_nation"><img style="padding-top:8px;" src="'.$site["FILES"].'images/grade_'.grade($r->Points).'.gif" width="20" height="20"/></div>
					<div class="ranksclan_col_np"><strong>'.number_format($r->Points).'</strong></div>
				</div></li>
			';
		}
		
		$p1 = '
		<div class="main_rank_content" style="margin-left:4px;">
			<div class="news_row">
				<div class="ranks_col_rank">Rank</div>
				<div class="ranks_col_nation">Nation</div>
				<div class="ranksclan_col_clan">Clan</div>
				<div class="ranks_col_level">Members</div>
				<div class="ranks_col_nation">Grade</div>
				<div class="ranksclan_col_np">National Points</div>	
			</div>		
			<img class="divider" src="'.$site["FILES"].'images/divider_line.png"  />
			<ul class="liste">
		';
		$p3 = '</div></ul><div id="sayfala"></div>';
		echo $p1.$p2.$p3;
	}

	function clan_profile($clan){
		global $db,$site;
		$q = $db->results("select strUserId, Level, Nation, Class, Knights,(select IDName from KNIGHTS where IDNum = Knights) as IDName, loyaltymonthly, fame, (select count(*) from CURRENTUSER where strCharID = strUserId) as status FROM userdata where knights in ('{$clan->IDNum}') order by fame asc");
		$i=0;
		foreach($q as $r){
			$i++;
			if($i%2){ $c = "_light"; }else{ $c = ""; }
			
			switch($r->fame){
				case 1:
					$fame = "leader";
				break;
				case 2:
					$fame = "coleader";
				break;
				default:
					$fame = "";
				break;
			}
			
			$p2 .= '
				<li>
					<div class="payment_row_light '.$fame.'">
						<div class="ranks_col_rank"><strong>'.$i.'</strong></div>
						<div class="ranks_col_nation"><img style="padding-top:8px;" src="'.$site["FILES"].'images/rank_'.$r->Nation.'.png"/></div>
						<div class="ranks_col_class"><img style="padding-top:8px;" src="'.$site["FILES"].'images/'.$r->Nation.classimg($r->Class).'.png"/></div>
						<div class="ranks_col_character"><strong><a href="?act=userprofile&user='.$r->strUserId.'">'.$r->strUserId.'</a></strong></div>
						<div class="ranks_col_clan"><strong>'.$r->IDName.'</strong>&nbsp;</div>
						<div class="ranks_col_level"><strong>'.$r->Level.'</strong></div>
						<div class="ranks_col_np"><strong>'.number_format($r->loyaltymonthly).'</strong></div>
					</div>
				</li>
			';
		}
		
		$p1 = '
		<div class="main_rank_content" style="margin-left:4px;">
			<div class="news_row">
				<div class="ranks_col_rank">Rank</div>
				<div class="ranks_col_nation">Nation</div>						
				<div class="ranks_col_class">Class</div>	
				<div class="ranks_col_character">Character</div>
				<div class="ranks_col_clan">Clan</div>
				<div class="ranks_col_level">Level</div>
				<div class="ranks_col_np">National Points</div>	
			</div>		
			<img class="divider" src="'.$site["FILES"].'images/divider_line.png"  />
			<ul class="liste">
		';
		
		$p3 = '</div></ul><div id="sayfala"></div>';
		echo $p1.$p2.$p3;
	}

	function user_rankings($count,$nation=null){
		global $db,$site;
		if(empty($nation)){ $nation = "1,2"; }
		$q = $db->results("select distinct top {$count} strUserId,Race, Nation, Level, Class, loyaltymonthly, (select IDName from KNIGHTS where IDNum = Knights) as KnightsName , Knights from userdata where Authority in(1,11,12) and Nation in({$nation}) order by loyaltymonthly desc");
		$i=0;
		foreach($q as $r){
			$i++;
			if($i%2){ $c = "_light"; }else{ $c = ""; }
			$p2 .= '
				<li>
					<div class="payment_row'.$c.'">
						<div class="ranks_col_rank"><strong>'.$i.'</strong></div>
						<div class="ranks_col_nation"><img style="padding-top:8px;" src="'.$site["FILES"].'images/rank_'.$r->Nation.'.png"/></div>
						<div class="ranks_col_class"><img style="padding-top:8px;" src="'.$site["FILES"].'images/'.$r->Nation.classimg($r->Class).'.png"/></div>
						<div class="ranks_col_character"><strong><a href="?act=userprofile&user='.$r->strUserId.'">'.$r->strUserId.'</a></strong>&nbsp;</div>
						<div class="ranks_col_clan"><strong><a href="?act=clanprofile&clan='.$r->Knights.'">'.$r->KnightsName.'</a></strong>&nbsp;</div>
						<div class="ranks_col_level"><strong>'.$r->Level.'</strong></div>
						<div class="ranks_col_np"><strong>'.number_format($r->loyaltymonthly).'</strong></div>
					</div>
				</li>
			';
		}
		
		$p1 = '
		<div class="main_rank_content" style="margin-left:4px;">
			<div class="news_row">
				<div class="ranks_col_rank">Rank</div>
				<div class="ranks_col_nation">Nation</div>						
				<div class="ranks_col_class">Class</div>
				<div class="ranks_col_character">Character</div>
				<div class="ranks_col_clan">Clan</div>
				<div class="ranks_col_level">Level</div>
				<div class="ranks_col_np">National Points</div>	
			</div>		
			<img class="divider" src="'.$site["FILES"].'images/divider_line.png"  />
			<ul class="liste">			
		';
		
		$p3 = '</div></ul><div id="sayfala"></div>';
		echo $p1.$p2.$p3;
	}

	function banned_users($count,$nation=null){
		global $db,$site;
		if(empty($nation)){ $nation = "1,2"; }
		$q = $db->results("select top  {$count} strUserId, Nation, Level, Class, loyaltymonthly, (select IDName from KNIGHTS where IDNum = Knights) as KnightsName , Knights from userdata where Authority = 255 and Nation in({$nation}) order by loyaltymonthly");
		$i=0;
		foreach($q as $r){
			$i++;
			if($i%2){ $c = "_light"; }else{ $c = ""; }
			$p2 .= '
				<li><div class="payment_row'.$c.'">
					<div class="ranks_col_rank"><strong>'.$i.'</strong></div>
					<div class="ranks_col_nation"><img style="padding-top:8px;" src="'.$site["FILES"].'images/rank_'.$r->Nation.'.png"/></div>
					<div class="ranks_col_class"><img style="padding-top:8px;" src="'.$site["FILES"].'images/'.$r->Nation.classimg($r->Class).'.png"/></div>
					<div class="ranks_col_character"><strong><a href="?act=userprofile&user='.$r->strUserId.'">'.$r->strUserId.'</a></strong>&nbsp;</div>
					<div class="ranks_col_clan"><strong><a href="?act=clanprofile&clan='.$r->Knights.'">'.$r->KnightsName.'</a></strong>&nbsp;</div>
					<div class="ranks_col_level"><strong>'.$r->Level.'</strong></div>
					<div class="ranks_col_np"><strong>'.number_format($r->loyaltymonthly).'</strong></div>
				</div></li>
			';
		}
		
		$p1 = '
		<div class="main_rank_content" style="margin-left:4px;">
			<div class="news_row">
				<div class="ranks_col_rank">Rank</div>
				<div class="ranks_col_nation">Nation</div>						
				<div class="ranks_col_class">Class</div>
				<div class="ranks_col_character">Character</div>
				<div class="ranks_col_clan">Clan</div>
				<div class="ranks_col_level">Level</div>
				<div class="ranks_col_np">National Points</div>	
			</div>		
			<img class="divider" src="'.$site["FILES"].'images/divider_line.png"  />	
			<ul class="liste">
		';
		
		$p3 = '</div></ul><div id="sayfala"></div>';
		echo $p1.$p2.$p3;
	}

	function whosonline($count,$nation=null){
		global $db,$site;
		if(empty($nation)){ $nation = "1,2"; }
		$q = $db->results("select distinct top {$count} strClientIP,strUserId, Nation, Level, Class, loyaltymonthly, (select IDName from KNIGHTS where IDNum = Knights) as KnightsName , Knights from userdata inner join  CURRENTUSER on strCharID = strUserId where  struserid in (select strcharid from currentuser) and Nation in({$nation}) order by Knights desc");
		$i=0;
		foreach($q as $r){
			$i++;
			if($i%2){ $c = "_light"; }else{ $c = ""; }
			$p2 .= '
				<li><div class="payment_row'.$c.'">
					<div class="ranks_col_rank"><strong>'.$i.'</strong></div>
					<div class="ranks_col_nation"><img style="padding-top:8px;" src="'.$site["FILES"].'images/rank_'.$r->Nation.'.png"/></div>
					<div class="ranks_col_class"><img style="padding-top:8px;" src="'.$site["FILES"].'images/'.$r->Nation.classimg($r->Class).'.png"/></div>
					<div class="ranks_col_character"><strong><a href="?act=userprofile&user='.$r->strUserId.'">'.$r->strUserId.'</a></strong>&nbsp;</div>
					<div class="ranks_col_clan"><strong><a href="?act=clanprofile&clan='.$r->Knights.'">'.$r->KnightsName.'</a></strong>&nbsp;</div>
					<div class="ranks_col_level"><strong>'.$r->Level.'</strong></div>
					<div class="ranks_col_np"><strong>'.number_format($r->loyaltymonthly).'</strong></div>
				</div></li>
			';
		}
		
		$p1 = '
		<div class="main_rank_content" style="margin-left:4px;">
			<div class="news_row">
				<div class="ranks_col_rank">Rank</div>
				<div class="ranks_col_nation">Nation</div>						
				<div class="ranks_col_class">Class</div>
				<div class="ranks_col_character">Character</div>
				<div class="ranks_col_clan">Clan</div>
				<div class="ranks_col_level">Level</div>
				<div class="ranks_col_np">National Points</div>	
			</div>		
			<img class="divider" src="'.$site["FILES"].'images/divider_line.png"  />	
			<ul class="liste">
		';
		
		$p3 = '</div></ul><div id="sayfala"></div>';
		echo $p1.$p2.$p3;
	}

	function news_type($type){
		switch($type){
			case 1:
				return '<span class="notice_ttl">[Notice]</span>';
			break;
			case 2:
				return '<span class="update_ttl">[Update]</span>';
			break;
			case 3:
				return '<span class="event_ttl">[Event]</span>';
			break;
		}
	}

	function monthly_rankings($count){
		global $db,$site;
		$td = null;
		
		$q = $db->query("select TOP $count * from USER_PERSONAL_RANK where strElmoUserID is not NULL or strKarusUserID is not NULL order by nRank");
		while($r=odbc_fetch_object($q)){
			$td .= "
			<tr>
				<td width='5%'>{$r->nRank}</td>
				<td width='10%'><span class='nation n2'></span><span class='symbol b".symbol($r->strPosition)."'></td>
				<td width='20%'><a class='sig' href='userprofile/{$r->strElmoUserID}' rel='".$site["URL"]."/signature.php?user={$r->strElmoUserID}'>{$r->strElmoUserID}</a></td>
				<td width='15%'>".number_format($r->nElmoloyaltyMonthly)."</td>
				<td width='10%'><span class='nation n1'></span><span class='symbol b".symbol($r->strPosition)."'></td>
				<td width='20%'><a class='sig' href='userprofile/{$r->strKarusUserID}' rel='".$site["URL"]."/signature.php?user={$r->strKarusUserID}'>{$r->strKarusUserID}</a></td>
				<td width='15%'>".number_format($r->nKarusLoyaltyMonthly)."</td>
			</tr>
			";
		}
		
		$table = "
		<table id='options' class='tablesorter'> 
			<thead>
				<tr>
					<th width='15' scope='col'></th>
					<th scope='col'></th>
					<th scope='col'>Takma �sim</th>
					<th scope='col'>Puanlar</th>
					<th scope='col'></th>
					<th scope='col'>Takma �sim</th>
					<th scope='col'>Puanlar</th>
				</tr>
			</thead>
			<tbody>
			{$td}
			</tbody>
		</table>";	
		echo $table;
	}

	function news($count,$type=null){
		global $db,$site;
		if(empty($type)){ $type = "1,2,3"; }
		$q = $db->results("select top {$count} * from web_News where news_status = 0  and news_type in({$type}) order by news_date desc");
		if(!empty($q)){
			foreach($q as $r){
				echo '
				<div class="notice">
					<div class="left_part">
						<p class="notice_text">'.news_type($r->news_type).'<a href="?act=news&id='.$r->id.'">'.$r->news_title.'</a></p>
					</div>
					<div class="right_part">
						<p class="time">'.substr($r->news_date,0,10).'</p>
					</div>
				</div>
				<img class="divider" src="'.$site["FILES"].'images/divider_line.png" />
				';
			}
		}
	}
	
	function muted_users(){
		global $db,$site; 
		$td = null;
		
		$q = $db->query("select w.Yetkili,DATEDIFF(day,w.Baslangic,w.Bitis) as Gun,u.strUserId,u.Class,w.Sebep,k.IDName,u.Nation from WRC_CEZA w inner join USERDATA u on u.strUserId = w.Cezali inner join KNIGHTS k on k.IDNum = u.Knights order by Baslangic desc");
		$i=0;
		while($r=odbc_fetch_object($q)){
			$i++;
			$td .= "
			<tr>
				<td width='5%'>{$i}</td>
				<td width='8%'><span class='nation n{$r->Nation}'></span><span class='class c{$r->Nation}".classimg($r->Class)."'></span></td>
				<td width='20%'><a class='sig' href='userprofile/{$r->strUserId}' rel='".$site["URL"]."/signature.php?user={$r->strUserId}'>{$r->strUserId}</a></td>
				<td width='20%'><a href='clanprofile/{$r->IDNum}'>{$r->IDName}</a></td>
				<td width='20%'>{$r->Sebep}</td>
				<td width='15%'>".number_format($r->Gun)." g�n</td>
			</tr>
			";
		}
		
		$table = "
		<table id='options' class='tablesorter'> 
			<thead>
				<tr>
					<th width='15' scope='col'></th>
					<th scope='col'></th>
					<th scope='col'>Takma �sim</th>
					<th scope='col'>Klan</th>
					<th scope='col'>Sebeb</th>
					<th scope='col'>S�re</th>
				</tr>
			</thead>
			<tbody>
			{$td}
			</tbody>
		</table>";
		echo $table;
	}
	

	function karus_count(){
		global $db; 
		return $db->col("select count(*) from CURRENTUSER where strcharid in (select struserid from userdata where nation = 1)")*FAKE;
	}
		
	function human_count(){
		global $db; 
		return $db->col("select count(*) from CURRENTUSER where strcharid in (select struserid from userdata where nation = 2)")*FAKE;
	}
	
	function total_count(){
		global $db; 
		return $db->col("select count(*) from CURRENTUSER")*FAKE;
	}

	function king_list(){
		global $db,$site;
		$td = null;
		$hata = null;
		$q = $db->query("select strUserId,Class,Nation,(select COUNT(*) from CURRENTUSER where strCharID = strUserId) as status from USERDATA where Rank = 1");
		$i=0;
		if(!empty($q)){
			while($r=odbc_fetch_object($q)){
				$i++;
				$status = $r->status == 1 ? "<div class='online-icon'></div>" : "<div class='offline-icon'></div>";
				$td .= "
				<tr>
					<td width='5%'>{$i}</td>
					<td width='14%'><span class='nation n{$r->Nation}'></span><span class='class c{$r->Nation}".classimg($r->Class)."'></td>
					<td width='25%'><a class='sig' href='userprofile/{$r->strUserId}' rel='".$site["URL"]."/signature.php?user={$r->strUserId}'>{$r->strUserId}</a></td>
					<td width='3%'><center>{$status}</center></td>
				</tr>
				";
			}
		}else{
			$hata = "<div class='table-uyari'>Kral listesi bo�.</div>";
		}
			$table = "
			<table id='options1' class='tablesorter' style='width:260px;'> 
			<thead>
				<tr>
						<th scope='col'></th>
						<th scope='col'></th>
						<th scope='col'>Kral</th>
						<th scope='col'>Durum</th>
					</tr>
				</thead>
				<tbody>
				{$td}
				</tbody>
			</table>";
			echo $table.$hata;
	}
	
	function gm_list(){
		global $db,$site;
		$td = null;
		$hata = null;
		$q = $db->query("select strUserId,Class,Nation,(select COUNT(*) from CURRENTUSER where strCharID = strUserId) as status from USERDATA where Authority = 0 order by UpdateTime desc");
		$i=0;
		if(!empty($q)){
			while($r=odbc_fetch_object($q)){
				$status = $r->status == 1 ? "<div class='online-icon'></div>" : "<div class='offline-icon'></div>";
				/**if($r->strUserId == "Creator"){
					$status = "<div class='online-icon'></div>";
				}**/
				$i++;
				$td .= "
				<tr>
					<td width='5%'>{$i}</td>
					<td width='8%'><span class='nation n{$r->Nation}'></span><span class='class c{$r->Nation}".classimg($r->Class)."'></td>
					<td width='25%'><a class='sig' href='userprofile/{$r->strUserId}' rel='".$site["URL"]."/signature.php?user={$r->strUserId}'>{$r->strUserId}</a></td>
					<td width='3%'><center>{$status}</center></td>
				</tr>
				";
			}
			}else{
			$hata = "<div class='table-uyari'>Y�netici listesi bo�.</div>";
		}
		$table = "
		<table id='options2' class='tablesorter'> 
			<thead>
				<tr>
					<th scope='col'></th>
					<th scope='col'></th>
					<th scope='col'>Y�netici</th>
					<th scope='col'>Durum</th>
				</tr>
			</thead>
			<tbody>
			{$td}
			</tbody>
		</table>";
		echo $table.$hata;
	}
	
	function BugunMU($gun){
	 if ($gun=="0")
	  return true;
	  
	 if (date("N")==$gun)
	  return true;
	 else
	  return false;
	  
	}
	function events(){
		global $db;
		$td = null;
		$hata = null;
		//$q = $db->query("select * from w_Events order by stime asc");
		$i=0;

		$q = $db->query('SELECT Event, Baslangic, Bitis,Gun, CAST(Bitis as time) as bitis_saat, CAST(Baslangic as time) as baslangic_saat,DATEDIFF(HOUR,CAST(GETDATE() as time), CAST(Baslangic as time)) as saat,DATEDIFF(SECOND,CAST(GETDATE() as time), CAST(Baslangic as time)) as sn  FROM WRC_PANEL_EVENT order by baslangic_saat');
		
			if(!empty($q)){
				while($s=odbc_fetch_object($q)){
				  $i++;
				  $geri = "";
					if ($s->sn > 0){
						if (BugunMU($s->Gun))
							$geri = "geri_sayim";
							$yazi="-";
							
							$renk = " ";
							if ($s->saat >= 0 && $s->saat <= 1)
								$renk = 'style="color:#3D5C00;"';
							if ($s->saat >= 2 && $s->saat <= 3)
								$renk = 'style="color:#5C8A00;"';
							if ($s->saat >= 4 && $s->saat <= 10)
								$renk = 'style="color:#1975FF;"';
							if ($s->saat > 10)
								$renk = 'style="color:#80A396;"';
							}else{
								$renk = 'style="color:#990000;"';
								if (BugunMU($s->Gun)) { $yazi="Bitti"; } else $yazi="-"; 
							}
							
						  $par = $s->Gun;
						  if ($par==0)
						  $gun =  'Herg�n';
						  if ($par=='1')
						  $gun =  'Pazartesi';
						  if ($par=='2')
						  $gun =  'Sal�';
						  if ($par=='3')
						  $gun =  '�ar�amba';
						  if ($par=='4')
						  $gun =  'Per�embe';
						  if ($par=='5')
						  $gun =  'Cuma';
						  if ($par=='6')
						  $gun =  'Cumartesi';
						  if ($par=='7')
						  $gun =  'Pazar';
						
						
						$simdi = intval(date("H"));
						$event = intval($s->saat);
							
							$td .= "
							  <tr>
							   <td width='170'>{$s->Event}</td>
							   <td width='90'>{$gun}</td>
							   <td width='80'>{$s->Baslangic}</td>
							   <td width='80'>{$s->Bitis}</td>
							   <td  width='130' {$renk} class='{$geri}' sure='{$s->Baslangic}:00'>{$yazi}</td>
							  </tr>
							";
						
				}
				}else{
				$hata = "<div class='table-uyari'>Event bulunamad�.</div>";
			}
			$table = "
			<table id='options5' class='tablesorter'> 
				<thead>
					<tr>
						<th scope='col'>Event</th>
						<th scope='col'>G�n</th>
						<th scope='col'>Ba�lang��</th>
						<th scope='col'>Biti�</th>
						<th scope='col'>Kalan S�re</th>
					</tr>
				</thead>
				<tbody>
				{$td}
				</tbody>
			</table>";
			echo $table.$hata;
		}
	
	function top_users($count){
		global $db,$site;
		$td = null;
		$hata = null;
		$q = $db->query("select top $count strUserId,Level,Race,Class,Nation,LoyaltyMonthly from USERDATA where Authority in(1,11,12) order by LoyaltyMonthly desc");
		$i=0;
		if(!empty($q)){
			while($r=odbc_fetch_object($q)){
				$i++;
				$td .= "
				<tr>
					<td width='16%'><span class='nation n{$r->Nation}'></span><span class='class c{$r->Nation}".classimg($r->Class)."'></td>
					<td width='25%'><a class='sig' href='userprofile/{$r->strUserId}' rel='".$site["URL"]."/signature.php?user={$r->strUserId}'>{$r->strUserId}</a></td>
					<td width='25%'>".number_format($r->LoyaltyMonthly)."</td>
				</tr>
				";
			}
			}else{
			$hata = "<div class='table-uyari'>Haftan�n en iyileri listesi bo�.</div>";
		}
		$table = "
		<table id='options3' class='tablesorter'> 
			<thead>
				<tr>
					<th scope='col'></th>
					<th scope='col'>Takma �sim</th>
					<th scope='col'>Puanlar</th>
				</tr>
			</thead>
			<tbody>
			{$td}
			</tbody>
		</table>";
		echo $table.$hata;
	}
	
	function top_clans($count){
		global $db;
		$td = null;
		$hata = null;
		$q = $db->query("SELECT TOP $count k.IDNum , k.Nation , ltrim(rtrim(k.IDName)) as IDName, k.Members, SUM(u.LoyaltyMonthly) as Points FROM knights k, knights_user ku, userdata u WHERE k.IDNum = ku.sIDNum AND ku.strUserID COLLATE DATABASE_DEFAULT = u.strUserID COLLATE DATABASE_DEFAULT AND u.Authority != 255 AND u.Authority != 0 GROUP BY k.IDNum,k.Nation,k.IDName,k.Members,k.Chief ORDER BY SUM(u.LoyaltyMonthly) DESC");
		$i=0;
		if(!empty($q)){
			while($r=odbc_fetch_object($q)){
				$i++;
				$td .= "
				<tr>
					<td width='24%'><span class='nation'><img width='16' height='16' src='".ClanIcon($r->IDNum)."' /></span><span class='nation n{$r->Nation}'></span><span class='class grade".grade($r->Points)."'></span></td>
					<td width='27%'><a href='clanprofile/{$r->IDNum}'>{$r->IDName}</a></td>
					<td width='25%'>".number_format($r->Points)."</td>
				</tr>
				";
			}
			}else{
			$hata = "<div class='table-uyari'>Haftan�n en iyileri listesi bo�.</div>";
		}
		$table = "
		<table id='options4' class='tablesorter'> 
			<thead>
				<tr>
					<th scope='col'></th>
					<th scope='col'>Clan �smi</th>
					<th scope='col'>Puanlar</th>
				</tr>
			</thead>
			<tbody>
			{$td}
			</tbody>
		</table>";
		echo $table.$hata;
	}

	
	function itemicon($num){
		global $site;
		return $site["ITEM_ICON"]."itemicon_". substr($num,0, 1)."_".substr($num,1, 4)."_".substr($num,5, 1)."0_"."0.bmp";
	}
	
	function itemler($user){
		global $db;
		$itemler = "<ul>";
		$i = 1;
		for($x=0;$x<15;$x++){
			if ($x != 14){
				$item = $db->results("SELECT cast(cast(substring(cast(substring(strItem,$i,4) as varbinary(4)), 4, 1)+substring(cast(substring(strItem,$i,4) as varbinary(4)), 3, 1)+substring(cast(substring(strItem, $i,4) as varbinary(4)), 2, 1)+substring(cast(substring(strItem, $i,4) as varbinary(4)), 1, 1) as varbinary(4)) as int) as num FROM USERDATA WHERE strUserId = '{$user->strUserId}'");
				if($item[0]->num != 0){
					$itemler .= "<li class='item-liste'>
					<a rel='tooltip' title='".item($user->strUserId,$item[0]->num)."'>
					<img src='".itemicon($item[0]->num)."'>
					</a>
					</li>";
					}else{
					$itemler .= "<li class='item-liste'></li>";
				}
				$i += 8;
			}
		}
		echo $itemler."</ul>";
	}
	
	function death_logs($user,$count=0){
		global $db;
		$td = null;
		$hata = null;
		$q = $db->query("select top $count Zaman,KesenUserID,KesilenUserID from w_DeathLog where KesenUserID = '{$user->strUserId}' Or KesilenUserID = '{$user->strUserId}' and (KesenNation != 0 and KesilenNation != 0)");
		$i=0;
		if(!empty($q)){
			while($r=odbc_fetch_object($q)){
				$i++;
				$td .= "
				<tr>
					<td width='25%'><a href='userprofile/{$r->KesenUserID}'>{$r->KesenUserID}</a></td>
					<td width='25%'><a href='userprofile/{$r->KesilenUserID}'>{$r->KesilenUserID}</a></td>
					<td width='15%'>".substr($r->Zaman,0,10)."</td>
				</tr>
				";
			}
		}else{
			$hata = "<div class='table-uyari'>�l�m kayd� bulunamad�.</div>";
		}
		$table = "
		<table id='options5' class='tablesorter' width=500> 
			<thead>
				<tr>
					<th scope='col'>Kesen</th>
					<th scope='col'>Kesilen</th>
					<th scope='col'>Tarih</th>
				</tr>
			</thead>
			<tbody>
			{$td}
			</tbody>
		</table>";
		echo $table.$hata;
	}
	
	function clanprofile($clan){
		$mod = null; NPMODE == 1 ? $mod = "loyaltymonthly" : $mod="LoyaltyMonthly"; # {$mod}
		global $db,$site;
		$td = null;
		$hata = null;
		$q = $db->query("select strUserId, Level, Nation, Class, Knights, {$mod}, fame, (select count(*) from CURRENTUSER where strCharID = strUserId) as status FROM userdata where knights in ('{$clan->IDNum}') order by fame asc");
		$i=0;
		if(!empty($q)){
			while($r=odbc_fetch_object($q)){
				
				switch($r->fame){
					case 1:
						$fame = "Lider";
					break;
					case 2:
						$fame = "Asistan";
					break;
					default:
						$fame = "�ye";
					break;
				}
				
				$i++;
				$status = $r->status == 1 ? "<div class='online-icon'></div>" : "<div class='offline-icon'></div>";
				$td .= "
				<tr>
					<td>{$i}</td>
					<td width='14%'><span class='nation n{$r->Nation}'></span><span class='class c{$r->Nation}".classimg($r->Class)."'></span></td>
					<td width='25%'><a class='sig' href='userprofile/{$r->strUserId}' rel='".$site["URL"]."/signature.php?user={$r->strUserId}'>{$r->strUserId}</a></td>
					<td width='15%'>{$r->Level}</td>
					<td width='15%'>".number_format($r->$mod)."</td>
					<td width='15%'>{$fame}</td>
					<td width='3%'><center>{$status}</center></td>
				</tr>
				";
			}
			}else{
			$hata = "<div class='table-uyari'>Clan'da oyuncu bulunamad�.</div>";
		}
		$table = "
		<table id='options' class='tablesorter'> 
			<thead>
				<tr>
					<th width='15' scope='col'></th>
					<th scope='col'></th>
					<th scope='col'>Takma �sim</th>
					<th scope='col'>Seviye</th>
					<th scope='col'>Puanlar</th>
					<th scope='col'>R�tbe</th>
					<th scope='col'>Durum</th>
				</tr>
			</thead>
			<tbody>
			{$td}
			</tbody>
		</table>";
		echo $table.$hata;
	}
	
	function logged_in($user){
		global $site,$db;
		$kc = $db->col("select CashPoint from TB_USER where strAccountId ='{$user}'");
		
		$ret =  '
		<!-- LOGIN -->
		<div class="login_box">
		<img src="'.$site["FILES"].'images/user_cp.png" /></div>			
		<div class="main_cp_user">
		<div class="welcom_text">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="2"> Welcome, <span>'.$user.'</span></td>
				</tr>
				<tr>
					<td class="king_title1"><span>KnightCash</span> </td>
					<td><div class="back_id_div">'.number_format($kc).'</div></td>
				</tr>
			';
			
			$premium = $db->row("select strType,nDays from PREMIUM_SERVICE where strAccountId ='{$user}'");
			
			if(!empty($premium)){
				$ret .= '
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr style="text-align: center;">
					<td colspan="2" class="king_title2"><span>Premium Service</span> </td>
				</tr>
				<tr>
					<td class="king_title1"><span>Type</span></td>
				<td>
					<div class="back_id_div">
					'.premiumtype($premium->strType).'
					</div>
				</td>
                </tr>
				<tr>
					<td class="king_title1"><span>Day</span></td>
					<td><div class="back_id_div">'.$premium->nDays.'</div></td>
				</tr>';				
			}
			
			$ret .= '
				<tr>
					<td colspan="2" class="bull_12"><img src="'.$site["FILES"].'images/tickets_left_bullet.png" /></td>
				</tr>
			</table>
		</div>
		</div>
		
		<div id="forget2" class="forget">
			<ul>
				<li>
					<a href="">Rankings</a>
					<ul>
						<li><a href="?act=user_rankings">Knight Rank</a></li>
						<li><a href="?act=lan_rankings">Clan Rank</a></li>
						<!--<li><a href="?act=whosonline">Whos Online</a></li>--!>
						<li><a href="?act=banned_users">Banned Users</a></li>
					</ul>
					<a href="?act=usercp">User CP</a>
					<ul>
						<li><a href="?act=usercp&p=payments">Payments</a></li>
						<li><a href="?act=usercp&p=security">Security</a></li>
						<li><a href="?act=logout">Logout</a></li>
					</ul>
				</li>
			</ul>
		</div>
		
		<!-- LOGIN -->
		';
		
		return $ret;
	}
	
	function logged_out(){
		return '
		<div class="login_box">
		<form id="login_frm" method="post" name="login_frm" action="?act=login"    autocomplete="off">
		<input name="username" type="text" class="username" value="USERNAME" onfocus="if(this.value==\'USERNAME\'){this.value=\'\'}" onblur="if(this.value==\'\'){this.value=\'USERNAME\'}" />
		<input name="password" type="password" class="password" value="PASSWORD" onfocus="if(this.value==\'PASSWORD\'){this.value=\'\'}" onblur="if(this.value==\'\'){this.value=\'PASSWORD\'}" />
		</form>
		</div>
		
		<div class="login_btn"> <a href="#" onclick="$(\'#login_frm\').submit();">&nbsp;</a> </div>
		<div class="reg_btn"> <a href="?act=register">&nbsp;</a> </div>
		<div class="forget">
		<ul>
		<li><a href="?act=forgotpw">Forgot Your Password?</a></li>
		</ul>
		</div>';
	}
	
	function premiumtype($type){
		switch($type){
			case 1:
				return "Normal P.";
			break;
			case 2:
				return "Ultra P.";
			break;	
			case 3:
				return "Bronze P.";
			break;
			case 4:
				return "Silver P.";
			break;	
			case 5:
				return "Gold P.";
			break;
			case 6:
				return "Prime P.";
			break;	
			case 7:
				return "Platinum P.";
			break;
			case 8:
				return "Royal P.";
			break;
		}
	}	
	
	function pw($len=6){
		$chars = "1234567890qwertyu�opasdfghjklizxcvbnm";
		for($i=0;$i<$len;$i++){
			$pw .= $chars{rand() % 15};
		}
		return $pw;
	}	
	
	function numarakontrol($phone){
		$number = str_replace(" ","",$phone);
		if(is_numeric($number)){
			if(strlen($number) == 11 || strlen($number) == 10){
				$first = substr($number,0,1);
				if($first == 0){
					$phone = substr($number,1,10);
					return $phone;
				}else{
					$phone = $number;
					return $phone;
				}
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}	

	function msg($txt,$type=0,$c=0){
		switch($type){
			case 1:
				$error = "<div class='main_row_div'><div class='error'>{$txt}</div></div>";
			break;
			case 2:
				$error = "<div class='main_row_div'><div class='success'>{$txt}</div></div>";
			break;
			case 3:
				$error = "<div class='main_row_div'><div class='info'>{$txt}</div></div>";
			break;
			default:
				$error = "<div class='main_row_div'><div class='warning'>{$txt}</div></div>";
			break;
		}
		
		if($c == 1) {
			$content = '
			<div class="mid_section">
				<div id="main_content_back">
					<div class="tickets_form">
						<h3>Hata</h3>
						<div class="enter_tick_form2">
							'.$error.'
						</div>
					</div>
				</div>
			</div>';
			echo $content;
		} else {
			echo $error;
		}
	}
	
	function itemname($num){
		global $db;
		$item = $db->col("select ltrim(rtrim(strName)) from ITEM where  Num = '{$num}'");
		if(empty($item)){
			return  " Bulunamad�. ";
		}else{
			return $item;
		}
	}
	
	function chart(){
		global $db;
		$item = $db->row("select top 1 (Select COUNT(Class) from USERDATA where Class in(101,105,106)) as TKW,(Select COUNT(Class) from USERDATA where Class in(102,107,108)) as TKR,(Select COUNT(Class) from USERDATA where Class in(103,109,110)) as TKM,(Select COUNT(Class) from USERDATA where Class in(104,111,112)) as TKP,(Select COUNT(Class) from USERDATA where Class in(201,205,206)) as TEW,(Select COUNT(Class) from USERDATA where Class in(202,207,208)) as TER,(Select COUNT(Class) from USERDATA where Class in(203,209,210)) as TEM,(Select COUNT(Class) from USERDATA where Class in(204,211,212)) as TEP from USERDATA");
		if(!empty($item)){
			return $item;
		}else{
			return false;
		}
	}
	
	function droplist($zone){
		global $db;
		$text = "";
		$drops = $db->results("select distinct k.strName,ki.iItem03,ki.iItem04,ki.iItem05,ki.sPersent01,ki.sPersent02,ki.sPersent03,ki.sPersent03,ki.sPersent04,ki.sPersent05 from K_MONSTER_ITEM ki inner join K_MONSTER k on k.sSid = ki.sIndex inner join K_NPCPOS n on n.NpcID = k.sSid where n.ZoneID = {$zone} and ki.iItem03 != '0.0' and ki.iItem04 != '0.0' and ki.iItem05 != '0.0'");
		if(!empty($drops)){
			$i = 0; $s = 0;
			foreach($drops as $r){
				
				if($i==0 && $s == 0){
					$t = "r1";$s =  1;$i = 1;
				}else if($i==1 && $s == 1){
					$t = "r2";$s =  2;$i = 3;
				}else if($i==3 && $s == 2){
					$t = "r2";$s =  4;$i = 5;
				}else if($i==5 && $s == 4){
					$t = "r1";$s =  0;$i = 0;
				}
				
				$text .=  "
				<div class='monsterler {$t}'>
					<div class='monster_name'>".$r->strName."</div>
					<div class='line'></div>";
					
					if(!empty($r->iItem03) && strlen($r->iItem01) > 9 )
					$text .=  "<div class='drop'><span class='oran'>%".substr($r->sPersent03,0,3)."</span>  <span class='item'><a rel='tooltip' title='".item('',round($r->iItem03))."'>".itemname(round($r->iItem03))."</a></span></div>";
					
					if(!empty($r->iItem03) && strlen($r->iItem02) > 9 )
					$text .=  "<div class='drop'><span class='oran'>%".substr($r->sPersent03,0,3)."</span>  <span class='item'><a rel='tooltip' title='".item('',round($r->iItem03))."'>".itemname(round($r->iItem03))."</a></span></div>";
					
					if(!empty($r->iItem03))
					$text .=  "<div class='drop'><span class='oran'>%".substr($r->sPersent03,0,3)."</span>  <span class='item'><a rel='tooltip' title='".item('',round($r->iItem03))."'>".itemname(round($r->iItem03))."</a></span></div>";
					
					if(!empty($r->iItem04))
					$text .=  "<div class='drop'><span class='oran'>%".substr($r->sPersent04,0,3)."</span>  <span class='item'><a rel='tooltip' title='".item('',round($r->iItem04))."'>".itemname(round($r->iItem04))."</a></span></div>";
					
					if(!empty($r->iItem05))
					$text .=  "<div class='drop'><span class='oran'>%".substr($r->sPersent05,0,3)."</span>  <span class='item'><a rel='tooltip' title='".item('',round($r->iItem05))."'>".itemname(round($r->iItem05))."</a></span></div>";
					
				$text .=  "
				</div>
				";
			}
			echo $text;
		}
	}
	
	function XMLPOST($PostAddress,$xmlData){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$PostAddress);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlData);
		$result = curl_exec($ch);
		return $result;
	}

	function symbol($sym)
	{
		$symbol = 0;
		switch(trim($sym))
		{
			case "Gold Knight":
			$symbol = 1;
			break;
			
			case "Silver Knight":
			$symbol = 2;
			break;
			
			case "Mirage Knight":
			$symbol = 3;
			break;
			
			case "Shadow Knight":
			$symbol = 4;
			break;
			
			case "Mist Knight":
			$symbol = 5;
			break;
			
			case "Training Knight":
			$symbol = 6;
			break;
			
			default:
			$symbol = 0;
			break;
		}
		return $symbol;
	}	
	
	function classimg($class)
	{
		switch ($class)
		{
			case 101:
			return '1';
			case 102:
			return '2';
			case 103:
			return '3';
			case 104:
			return '4';
			case 201:
			return '1';
			case 202:
			return '2';
			case 203:
			return '3';
			case 204:
			return '4';
			case 105:
			return '1';
			case 106:
			return '1';
			case 107:
			return '2';
			case 108:
			return '2';
			case 109:
			return '3';
			case 110:
			return '3';
			case 111:
			return '4';
			case 112:
			return '4';
			case 205:
			return '1';
			case 206:
			return '1';
			case 207:
			return '2';
			case 208:
			return '2';
			case 209:
			return '3';
			case 210:
			return '3';
			case 211:
			return '4';
			case 212:
			return '4';
			default:
			return '0';
		}
	}
	
	function signature2($class)
	{
		switch ($class)
		{
			case 101:
			case 105:
			case 106:
			case 201:
			case 205:
			case 206:
				return 'Warrior';
			case 102:
			case 107:
			case 108:
			case 202:
			case 207:
			case 208:
				return 'Rogue';
			case 103:
			case 109:
			case 110:
			case 203:
			case 209:
			case 210:
				return 'Magician';
			case 104:
			case 111:
			case 112:
			case 204:
			case 211:
			case 212:
				return 'Priest';

		}
	}
	function signature($race,$class){
			
			if($race == 1)
				return 'kmw.png';
				
			if($race == 2 && ($class == 102 || $class == 108))
				return 'kmr.png';
				
			if($race == 3)
				return 'kmm.png';
				
			if($race == 4 && ($class == 104 || $class == 112))
				return 'kfp.png';	
				
			if($race == 2 && ($class == 104 || $class == 112))
				return 'kmp.png';
				
			if($race == 20)
				return 'kfm.png';
				
			if($race == 12 && ($class == 201 || $class == 206))
				return 'hmw.png';
				
			if($race == 13 && ($class == 201 || $class == 206 ))
				return 'hfw.png';
				
			if($race == 11 && ($class == 201 || $class == 206 ))
				return 'hbw.png';
				
			if($race == 12 && ($class == 202 || $class == 208 ))
				return 'hmr.png';
				
			if($race == 13 && ($class == 202 || $class == 208 ))
				return 'hfr.png';
				
			if($race == 12 && ($class == 203 || $class == 210 ))
				return 'hmm.png';
				
			if($race == 13 && ($class == 203 || $class == 210 ))
				return 'hfm.png';
				
			if($race == 12 && ($class == 204 || $class == 212 ))
				return 'hfp.png';
				
			if($race == 13 && ($class == 204 || $class == 212 ))
				return 'hfp.png';	
	}
	
	function ClanIcon($idnum){
		$idnum = intval($idnum);
		if(file_exists("ClanIcon/clan_$idnum.bmp")){
			return "ClanIcon/clan_$idnum.bmp";
		}else{
			return "ClanIcon/noicon.bmp";
		}
	}
	
	function grade($grade) {
		if ($grade <= 1) {$grade = '5'; }
		elseif ($grade <= 50000) {$grade = '4';}
		elseif ($grade <= 140000) {$grade = '3';}
		elseif ($grade <= 400000) {$grade = '2'; }
		elseif ($grade >= 730000) {$grade = '1';}
		
		return $grade;
	}
	
	function tarih() {  
		setlocale(LC_ALL,'turkish');
		$tarih = strftime('%d %B %Y');
		
		date_default_timezone_set('Europe/Istanbul');
		$zaman=date("H:i:s");
		echo $tarih . " saat " . $zaman;
	}   
	
?>