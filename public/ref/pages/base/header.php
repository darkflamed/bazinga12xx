<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-9" />
<title><?php echo $site["TITLE"]; ?></title>
<link rel="stylesheet" type="text/css" href="<?php echo $site["FILES"]; ?>css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo $site["FILES"]; ?>css/validationEngine.jquery.css">
<link rel="stylesheet" type="text/css" href="<?php echo $site["FILES"]; ?>js/themis_selectbox.css" />
<link rel="shortcut icon" href="images/favicon.gif" type="shortcut icon" /> 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js" type="text/javascript" charset="utf-8"></script>	
<script type="text/javascript" src="<?php echo $site["FILES"]; ?>js/custom.js"></script>
<script type="text/javascript" src="<?php echo $site["FILES"]; ?>js/jquery.selectbox-0.5.js"></script>
<script type="text/javascript" src="<?php echo $site["FILES"]; ?>js/languages/jquery.validationEngine-en.js"></script>
<script type="text/javascript" src="<?php echo $site["FILES"]; ?>js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="<?php echo $site["FILES"]; ?>js/bjqs-1.3.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	$('#Items').selectbox();
});
</script>

</head>
<body>
<div id="header">
  <div class="wrapper">
    <div class="menu">
      <ul>
        <li class="first"><a href="?act=news">Knight News</a></li>
        <li><a href="#">GAME RANKS</a></li>
        <li class="clan"><a class="padd_r" href="#">EVENTS</a></li>
        <li class="logo_bg"><a class="logo_padd" href="index.php">&nbsp;</a></li>
        <li><a href="">Community</a></li>
        <li><a href="#">Downloads</a></li>
        <li><a href="#">Support</a></li>
      </ul>
    </div>
	<div class="shown_header">
      <div class="left">
        <ul>
          <li><a href="?act=news&p=notice">Notice</a></li>
		  <li><a href="?act=news&p=update">Update</a></li>
		  <li><a href="?act=news&p=event">Event</a></li>
        </ul>
        <ul>
          <li><a href="?act=user_rankings">User Ranking</a></li>
          <li><a href="?act=clan_rankings">Clan Ranking</a></li>
        </ul>
        <ul>
          <li><a href="#">Border Defance</a></li>
          <li><a href="#">Lunar War</a></li>
        </ul>		
      </div>
      <div class="right">
        <ul>
          <li><a href="http://<?php echo $site["FORUM"]; ?>" target="_blank">Forum</a></li>
          <li><a href="http://<?php echo $site["FACEBOOK"]; ?>" target="_blank">Facebook</a></li>
        </ul>
        <ul>
          <li><a href="?act=downloads">Client</a></li>
        </ul>
        <ul>
          <li><a href="?act=faq">FAQ</a></li>
          <li><a href="?act=ticket">Ticket</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
	#server_status_bg{
		background:url(<?php echo $site["FILES"]; ?>images/stats.png);
		width:438px;
		height:37px;
		float:left;
	}
	#server_status_loc{
		margin-top:13px;
		margin-left:250px;
		float:left;
	}
	#icons{
		color:#fff;
		font-size:11px;
		padding:17px;
		margin-top:1px;
		text-align:center;
	}
	
	#on_karus{
		 background:url(<?php echo $site["FILES"]; ?>images/rank_1.png) no-repeat center -1px #000;
		 float:left;
		 width:16px;
		 height:16px;
	}
	
	#on_human{
		 background:url(<?php echo $site["FILES"]; ?>images/rank_2.png) no-repeat center -1px #000;
		 float:left;
		 width:16px;
		 height:16px;
	}

	#on_total{
		 background:url(<?php echo $site["FILES"]; ?>images/rank_3.png) no-repeat center -1px #000;
		 float:left;
		 width:32px;
		 height:16px;
		 margin-left:10px;
	}
	
	#on_karus_text{
		float:left;
		margin-left:2px;
		color:#efae5a;
		font-family:Arial;
		font-size:11px;
	}
	
	#on_total_text{
		float:left;
		margin-left:2px;
		color:#efae5a;
		font-family:Arial;
		font-size:11px;
	}
	
	#on_human_text{
		float:left;
		margin-left:2px;
		color:#efae5a;
		font-family:Arial;
		font-size:10px;
	}
	
	#on_human_text_b{
		font-family:Arial;
		font-size:12px;
		margin-left:3px;
		color:#b5864a;
		font-weight:bold;
	}
	
	
	#sayfala{
		 float:left;
		 margin-top:30px;
		 text-align:center;
		 width:680px;
		 height:30px;
	}
	
	#sayfala a {
		margin-right:5px;
		padding:5px 10px 5px 10px;
		background-color:#302115;
		color:#c5854f;
	}
	
	#sayfala a.aktif {background:red;color:#fff}
	
</style>
	<script type="text/javascript">
		$(function(){
			var toplam = $("ul.liste li").length, 
			kactane = 20, 
			sayfasay = Math.ceil(toplam/kactane), 
			i = 1;
			
			$.goster = function(nerden,nereye) {
				$("ul.liste li").each(function(index) {
					if(index < nereye && index > nerden - 2) {
						$(this).show(100);
					} else {
						$(this).hide(100);
					}
					
				});	
			}
			
			for(i; i <= sayfasay; i++){
				$("#sayfala").append('<a>' + i + '</a>');
			}
			
			$("#sayfala a").first().addClass("aktif"); 
			$("#sayfala a").on("click",function() { 
				
				var sayfano = $(this).index() + 1, 
				goster = sayfano * kactane;
				
				$.goster(goster - kactane + 1, goster);
				$("#sayfala a").removeClass("aktif");
				$(this).addClass("aktif");
			});
			
			$.goster("1",kactane); 
			
		});
	</script>



<div class="wrapper">
  <div class="body">
    <div id="tickets_back" class="container">
      <div class="wrap">
	  	<div class="left_sidebar">
			<?php 
				if(!empty($_SESSION["loggeduser"])){
					echo logged_in($_SESSION["loggeduser"]);
					}else{
					echo logged_out();
				}
			?>
		
		
		<div class="begin_guide"> <a href="?act=guide">&nbsp;</a> </div>
          <div class="clr"></div>
          <div class="facebook">
            <h2 class="sidebar_title">facebook</h2>
			<div class="wrap1">
			<iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2F<?php echo $site["FACEBOOK"]; ?>&amp;width=180&amp;height=220&amp;show_faces=true&amp;colorscheme=dark&amp;stream=false&amp;show_border=false&amp;header=false&amp;appId=113718568713770" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:180px; height:260px;" allowTransparency="true"></iframe>
			</div>
          </div>
        </div>
		<link rel="stylesheet" href="<?php echo $site["FILES"]; ?>css/bjqs.css">
		<link rel="stylesheet" href="<?php echo $site["FILES"]; ?>css/demo.css">
		
