<?php
	include (BASE."/header.php");
?>

	<div class="mid_section"> 

          <div class="slider">
      <!--  Outer wrapper for presentation only, this can be anything you like -->
      <div id="banner-fade">

        <!-- start Basic Jquery Slider -->
        <ul class="bjqs">
		<?php
			if(!empty($slider)){
				foreach($slider as $slide){
					?>
						 <li><a href="<?php echo $slide["URL"]; ?>"><img src="<?php echo $slide["IMG"]; ?>"></a></li>
					<?php
				}
			}
		?>
        </ul>
        <!-- end Basic jQuery Slider -->

      </div>
      <!-- End outer wrapper -->

      <script class="secret-source">
        jQuery(document).ready(function($) {

          $('#banner-fade').bjqs({
            height      : 293,
            width       : 531,
            responsive  : true,
			nexttext : '<img src="<?php echo $site["FILES"]; ?>images/right_arrow.png">', // Text for 'next' button (can use HTML)
			prevtext : '<img src="<?php echo $site["FILES"]; ?>images/left_arrow.png">',
          });

        });
      </script>
	  
		  </div>
          <div class="notice_board">
            <div class="nav">
              <ul>
                <li class="active"><a rel="tab_01" href="#">ALL</a></li>
                <li><a rel="tab_02" href="#">NEWS</a></li>
                <li><a rel="tab_03" href="#">EVENTS</a></li>
                <li><a rel="tab_04" href="#">UPDATES</a></li>
              </ul>
            </div>
            <div class="clr"></div>
			
<!-- All News -->
            <div id="tab_01" class="tab_content" style="display:block;">
			<?php news(6);?>
				<div class="arrows" style="display:none"> 
					<span class="left_round_arrow"><a href="#"><img src="<?php echo $site["FILES"]; ?>images/left_round_arrow.png" /></a></span> 
					<span class="right_round_arrow"><a href="#"><img src="<?php echo $site["FILES"]; ?>images/right_round_arrow.png" /></a></span> 
				</div>
				<span class="hepsi_button"><a href="?act=news"><img src="<?php echo $site["FILES"]; ?>images/hepsini_btn.png" /></a></span> 
			</div>
<!-- END All News -->
			
<!-- News List -->
			<div id="tab_02" class="tab_content">
				<?php news(6,1);?>
				<div class="arrows" style="display:none"> 
					<span class="left_round_arrow"><a href="#"><img src="<?php echo $site["FILES"]; ?>images/left_round_arrow.png" /></a></span> 
					<span class="right_round_arrow"><a href="#"><img src="<?php echo $site["FILES"]; ?>images/right_round_arrow.png" /></a></span> 
				</div>
				<span class="hepsi_button"><a href="?act=news"><img src="<?php echo $site["FILES"]; ?>images/hepsini_btn.png" /></a></span> 
			</div>
<!-- END News List -->

<!-- Events List -->
			<div id="tab_03" class="tab_content">
				<?php news(6,3);?>
				<div class="arrows" style="display:none"> 
					<span class="left_round_arrow"><a href="#"><img src="<?php echo $site["FILES"]; ?>images/left_round_arrow.png" /></a></span> 
					<span class="right_round_arrow"><a href="#"><img src="<?php echo $site["FILES"]; ?>images/right_round_arrow.png" /></a></span> 
				</div>
				<span class="hepsi_button"><a href="?act=news"><img src="<?php echo $site["FILES"]; ?>images/hepsini_btn.png" /></a></span> 
			</div>
<!-- END Events List -->

<!-- UPDATE List -->
			<div id="tab_04" class="tab_content">
				<?php news(6,2);?>
				<div class="arrows" style="display:none"> 
					<span class="left_round_arrow"><a href="#"><img src="<?php echo $site["FILES"]; ?>images/left_round_arrow.png" /></a></span> 
					<span class="right_round_arrow"><a href="#"><img src="<?php echo $site["FILES"]; ?>images/right_round_arrow.png" /></a></span> 
				</div>
				<span class="hepsi_button"><a href="?act=news"><img src="<?php echo $site["FILES"]; ?>images/hepsini_btn.png" /></a></span> 
			</div>
<!-- END UPDATE List -->

          </div>

	</div>
        <div class="right_sidebar"> 
		
		<span class="clan_rank_btn"><a href="?act=clan_rankings">&nbsp;</a></span>
		<span class="knight_rank_btn"><a href="?act=user_rankings">&nbsp;</a></span>
		<span class="forum_btn"><a href="#">&nbsp;</a></span>

		<span class="support_btn"><a href="#">&nbsp;</a></span>

          <div class="clr"></div>
<br>
			<audio controls style="width:168px">
			  <source src="images/123.mp3" type="audio/mpeg">
			  Your browser does not support this audio format.
			</audio>		  

        </div>	
		
<?php
	include (BASE."/footer.php");
?>