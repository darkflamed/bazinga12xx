<?php
	include (BASE."/header.php");
?>

 <div class="mid_section">
          <div id="main_content_back">
            <div class="tickets_form">
              <h3>Downloads</h3>
                <div class="main_nation_content">
				
				<?php
					if(!empty($downloads)){
						foreach($downloads as $down){
							?>
								<div class="download">
									<div class="download_title"><?php echo $down["TITLE"]; ?></div>
									<div class="download_logo"><img src="<?php echo $down["LOGO"]; ?>"/></div>				
									<div class="download_txt"><?php echo $down["DESCRIPTION"]; ?></div>
									<div class="download_button">
										<span class="your_button"><a target="_blank" href="<?php echo $down["URL"]; ?>">�ndir</a></span>
									</div>
								</div>
							<?php
						}
					}
				?>
			</div>
        </div>
    </div>
</div>
<?php
	include (BASE."/footer.php");
?>