<?php
	include (BASE."/header.php");
	$user = iconv('UTF-8','ISO-8859-1',$post->username);
	$pass = iconv('UTF-8','ISO-8859-1',$post->password);
	$err = null;
	
	if(empty($user) || empty($pass)){
			$err = '<div class="error">You must fill the all blanks!</div>';
	}else{
		if($user = $db->row("select strAccountId,strPasswd from TB_USER where strAccountID ='{$user}' and strPasswd ='{$pass}'")){
			if(!$accounts = $db->row("select strAccountId from ACCOUNT_CHAR where strAccountID ='{$user->strAccountId}'")){
				$err = '<div class="error">You must make a Character!</div>';
			}else{
				$_SESSION["loggeduser"] = $accounts->strAccountID;
				
			}
		}else{
			$err = '<div class="error">Username or Password is wrong!</div>';
		}
	}
	
	if(empty($err)){
		return;
	}
?>
<div class="mid_section">
          <div id="main_content_back_register">
            <div class="tickets_form">
              <h3>Login</h3>
              <form action="?act=login" name="login" id="formLogin" method="POST" autocomplete="off">
                <div class="enter_tick_form">	
				<input type="hidden" name="lp" value="1">
				<?php if(!empty($err)) { echo $err; } ?>
				<div class="main_row_div">
                    <div class="lable_text">Username</div>
                    <div class="text_box_2">
                      <input type="text" name="username" value="" class="validate[required] text_box_tick">
                    </div>
                  </div>
				  
				  <div class="main_row_div">
                    <div class="lable_text">Password</div>
                    <div class="text_box_2">
                      <input type="password" name="password" value="" class="validate[required] text_box_tick">
                    </div>
                  </div>
				  
                  <div class="main_row_div">
                    <div class="lable_text">&nbsp;</div>
                    <div class="text_box_btn">
                      <input type="image" onclick="submit" src="<?php echo $site["FILES"]; ?>/images/send_btn.png">
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
</body>
</html>
		
<?php
	include (BASE."/footer.php");
?>