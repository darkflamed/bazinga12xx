<?php

	class Odbc {
		
		public $dbuser, $dbpass, $dbname, $dbhost;
		private $rval = false;
		
		public function __construct(){
			global $errors,$config;
			$this->dbname = $config['DBNAME'];
			$this->dbuser = $config['DBUSER'];
			$this->dbpass = $config['DBPASS'];
			$this->dbconn = odbc_connect($this->dbname,$this->dbuser,$this->dbpass);
		}

		public function query($query){
			$this->result = @odbc_exec($this->dbconn,$query);
			$this->rows_affected = @odbc_num_rows($this->result);
			if(!$this->rval){
				return $this->rows_affected;
			}else{
				return $this->result;
			}
		}

		
		public function results($query){
			$this->last_result = null;
			$this->rval = true;
			if($this->result = $this->query($query)){
				$num_rows=0;
				while ($row = @odbc_fetch_object($this->result)){
					$this->last_result[$num_rows] = $row;
					$num_rows++;
				}
				if(!empty($this->last_result)){
					return $this->last_result;
				}
			}
		}
		
		public function row($query,$x=0){
			$this->last_result = null;
			$this->rval = true;
			if($this->result = $this->query($query)){
				$num_rows=0;
				while ($row = @odbc_fetch_object($this->result)){
					$this->last_result[$num_rows] = $row;
					$num_rows++;
				}
				if(!empty($this->last_result[$x])){
					return $this->last_result[$x];
				}
			}
		}
		
		public function col($query,$x=0,$y=1){
			$this->last_result = null;
			$this->rval = true;
			if($this->result = $this->query($query)){
				$num_rows=0;
				while (@odbc_fetch_row($this->result)){
					$this->last_result[$num_rows] = @odbc_result($this->result,$y);
					$num_rows++;
				}
				if(!empty($this->last_result[$x])){
					return ($this->last_result[$x]);
				}
			}
		}
	}
?>