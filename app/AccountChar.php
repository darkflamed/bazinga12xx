<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class AccountChar extends Model
{
    protected $table = 'account_char';

    public static function getAccountChar()
    {
        return AccountChar::where('strAccountID', 'LIKE', Auth::user()->strAccountID)->first();
    }
}
