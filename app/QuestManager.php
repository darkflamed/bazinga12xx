<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestManager extends Model
{
    protected $table = 'QuestManager';

    public static function getQuests()
    {
        return QuestManager::join('Zone_Info', 'QuestManager.QuestZone', '=', 'Zone_Info.ZoneNo')
            ->orderBy('QuestManager.QuestZone')
            ->get(['QuestManager.*', 'Zone_Info.bz as QuestZone']);
    }
}
