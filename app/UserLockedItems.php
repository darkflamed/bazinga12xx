<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLockedItems extends Model
{
    protected $table = 'User_LockedItems';
}
