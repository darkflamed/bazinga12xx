<?php

namespace App\Providers;

use Darryldecode\Cart\Cart;
use Illuminate\Support\ServiceProvider;
use App\Libraries\PowerupCartDBStorage;
use Auth;

class PowerupCartStorage extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('cart', function($app)
        {
            $storage = new PowerupCartDBStorage();
            $events = $app['events'];
            $instanceName = 'PowerupCart';
            $session_key = Auth::user()->id . '_' . Auth::user()->strAccountID;
            return new Cart(
                $storage,
                $events,
                $instanceName,
                $session_key,
                config('shopping_cart')
            );
        });
    }
}
