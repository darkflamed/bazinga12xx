<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Authenticatable as UserContract;

class EloquentUserProvider extends \Illuminate\Auth\EloquentUserProvider
{
    public function validateCredentials(UserContract $user, array $credentials)
    {
        if(array_key_exists('password_pus', $credentials)) {
            $plain = $credentials['password_pus'];
            return $this->hasher->check($plain, $user->password_pus);
        } else {
            $plain = $credentials['password'];
            return $this->hasher->check($plain, $user->getAuthPassword());
        }
    }
}
