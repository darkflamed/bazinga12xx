<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\HardwareBanList;
use Auth;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'TB_USER';

    protected $fillable = [
        'name', 'email', 'password', 'email_token', 'verified', 'strAccountID', 'strSocNo', 'idays', 'strAuthority',
        'strIP', 'Premiumtype', 'PremiumDays', 'IP', 'Key', 'giris', 'pin', 'country_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function checkVerified($username)
    {
        $user = User::where('strAccountID', 'LIKE', $username['strAccountID'])->first();
        if(is_null($user) || is_null($user->verified)) return 2;
        if($user->verified != 1) return 1;
        return 0;
    }

    public static function getUserFlag($id)
    {
        $User = User::where('id', $id)->first();
        return $User->country_code;
    }

    public static function purchaseItems($StoreItems, $UserId)
    {
        $TotalPrice = 0;
        foreach($StoreItems as $item){
            $TotalPrice += $item->price * $item->quantity;
        }

        if($TotalPrice > Auth::user()->nKnightCash) return false;
        Auth::user()->nKnightCash -= $TotalPrice;
        Auth::user()->save();

        ItemMall::purchaseItems($StoreItems, $UserId);

        return true;
    }

    public static function checkHardwareBanned($id)
    {
        return HardwareBanList::checkHardwareBan($id);
    }

    public static function getUser($id)
    {
        return User::find($id);
    }
}
