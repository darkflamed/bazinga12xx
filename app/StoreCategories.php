<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreCategories extends Model
{
    protected $table = 'store_categories';

    protected $fillable = [
        'id', 'name'
    ];

    public static function getCategories()
    {
        return StoreCategories::/*orderBy('nPos', 'asc')->*/get();
    }

    public static function getCategoryByName($name)
    {
        return StoreCategories::where('name', 'LIKE', ''.$name.'')->first();
    }
}
