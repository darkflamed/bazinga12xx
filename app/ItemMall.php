<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CurrentUser;
use App\ItemMallLog;
use Auth;
use Carbon\Carbon;

class ItemMall extends Model
{
    protected $table = 'web_itemmall';

    protected $fillable = [
        'strAccountID', 'strCharID', 'ServerNo', 'ItemID', 'ItemCount', 'strItemName', 'price',
        'pay_type', 'itemtime', 'BuyTime'
    ];

    public static function purchaseItems($items, $UserId)
    {
        foreach($items as $item){
            for($i=0;$i<$item->quantity;$i++) {
                $mall = new ItemMall;
                $mall->strAccountID = Auth::user()->strAccountID;
                $mall->strCharID = $UserId;
                $mall->ServerNo = 0;
                $mall->ItemID = $item->num;
                $mall->ItemCount = 1;
                $mall->price = $item->price;
                $mall->save();

                $log = new ItemMallLog;
                $log->strAccountID = Auth::user()->strAccountID;
                $log->strCharID = $UserId;
                $log->ServerNo = 0;
                $log->ItemID = $item->num;
                $log->ItemCount = 1;
                $log->price = $item->price;
                $log->save();
            }
        }
    }
}
