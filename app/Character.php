<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Capsule\Manager as Capsule;
use Auth;
use DB;
use PDO;

class Character extends Model
{
    protected $table = 'userdata';

    protected $casts = [
        'is_admin' => 'boolean',
    ];

    protected $appends = ['pEquipped'];

    public static function getTopUserRankings()
    {
        return Character::orderby('LoyaltyMonthly', 'desc')->limit(10)->get();
    }

    public static function getTopUserRankingsKarus()
    {
        return Character::where('Nation', 1)->orderby('Loyalty', 'desc')->limit(10)->get();
    }

    public static function getTopUserRankingsElmo()
    {
        return Character::where('Nation', 2)->orderby('Loyalty', 'desc')->limit(10)->get();
    }

    public static function getTopIngameKarusRankings()
    {
        return Character::where('userdata.Nation', 1)
            ->leftJoin('KNIGHTS', 'userdata.Knights', '=', 'knights.IDNum')
            ->where('userdata.Zone', 201)->where('userdata.LoyaltyToday', '>', 0)
            ->orderby('userdata.LoyaltyToday', 'DESC')->limit(10)->get();
    }

    public static function getTopIngameElmoRankings()
    {
        return Character::where('userdata.Nation', 2)
            ->leftJoin('KNIGHTS', 'userdata.Knights', '=', 'knights.IDNum')
            ->where('userdata.Zone', 201)->where('userdata.LoyaltyToday', '>', 0)
            ->orderby('userdata.LoyaltyToday', 'DESC')->limit(10)->get();
    }

    public static function getAllKarusMonthlyRankings()
    {
        return Character::orderby('LoyaltyMonthly', 'desc')
            ->where('userdata.Nation', 1)
            ->leftJoin('knights', 'userdata.Knights', '=', 'knights.IDNum')
            ->limit(100)
            ->get(array('userdata.*', 'knights.IDName', 'knights.IDNum'));
    }

    public static function getAllElmoradMonthlyRankings()
    {
        return Character::orderby('LoyaltyMonthly', 'desc')
            ->where('userdata.Nation', 2)
            ->leftJoin('knights', 'userdata.Knights', '=', 'knights.IDNum')
            ->limit(100)
            ->get(array('userdata.*', 'knights.IDName', 'knights.IDNum'));
    }

    public static function getAllTimeKarusRankings()
    {
        return Character::orderby('Loyalty', 'desc')
            ->where('userdata.Nation', 1)
            ->leftJoin('knights', 'userdata.knights', '=', 'knights.IDNum')
            ->limit(100)
            ->get(array('userdata.*', 'knights.IDName', 'knights.IDNum'));
    }

    public static function getAllTimeElmoradRankings()
    {
        return Character::orderby('Loyalty', 'desc')
            ->where('userdata.Nation', 2)
            ->leftJoin('knights', 'userdata.knights', '=', 'knights.IDNum')
            ->limit(100)
            ->get(array('userdata.*', 'knights.IDName', 'knights.IDNum'));
    }

    public static function getCharacter($CharID)
    {
        $CharID = html_entity_decode($CharID);
        $Character = DB::table('USERDATA')
            ->select(DB::raw('*, CAST(strItem as varbinary(max)) as strItem, CAST(strSerial as varbinary(max)) as strSerial'))
            ->where('strUserId', $CharID)->first();
        return $Character;
    }

    public static function getCharList()
    {
        $Characters = DB::table('USERDATA')
            ->select(DB::raw('*, CAST(strItem as varbinary(max)) as strItem, CAST(strSerial as varbinary(max)) as strSerial'))
            ->where('account_id', Auth::user()->id)->get();
        return $Characters;
    }

    public static function getClanCharacters($ClanID)
    {
        return Character::where('Knights', '=', $ClanID)->get();
    }

    public static function getClanCharactersByName($ClanID)
    {
        return Character::where('Knights', '=', $ClanID)->get();
    }
}
