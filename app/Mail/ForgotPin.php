<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgotPin extends Mailable
{
    use Queueable, SerializesModels;

    protected $PIN;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($PIN)
    {
        $this->PIN = $PIN;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.status_update')
            ->with(array('header' => 'Forgot PIN Reminder', 'preview_text' => 'Your PIN for KnightOnline-MyKO', 'body' => 'Your PIN for KnightOnline-MyKO is '.$this->PIN.'.'));
    }
}
