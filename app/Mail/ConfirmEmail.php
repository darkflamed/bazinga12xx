<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConfirmEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $token;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.status_update')
            ->with(array('header' => 'Please Confirm Your Email - MyKO 1298', 'preview_text' => 'Please confirm your email', 'body' => 'Click on the button below to confirm your email address. <br> Aşağıdaki butona basarak email adresinizi onaylayınız.',
                'button_text' => 'Confirm', 'button_link' => url('/auth/confirm/'.$this->token)));
    }
}
