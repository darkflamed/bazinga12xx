<?php

namespace App\Libraries;

use App\ZoneInfo;

class Template
{
    public static $KCOptions = array(
        ['kc' => 500, 'price' => 3.00, 'tl' => 11, 'bonus' => 0],
        ['kc' => 1000, 'price' => 6.00, 'tl' => 22, 'bonus' => 0],
        ['kc' => 1500, 'price' => 8.00, 'tl' => 33, 'bonus' => 0],
        ['kc' => 2150, 'price' => 11.00, 'tl' => 44, 'bonus' => 100],
        ['kc' => 2850, 'price' => 13.00, 'tl' => 55, 'bonus' => 300],
        ['kc' => 6000, 'price' => 26.00, 'tl' => 110, 'bonus' => 600],
        ['kc' => 10000, 'price' => 39.00, 'tl' => 165, 'bonus' => 1200],
        ['kc' => 15000, 'price' => 57.00, 'tl' => 242, 'bonus' => 1500],
        ['kc' => 20000, 'price' => 77.00, 'tl' => 330, 'bonus' => 3000]
    );

    public static function GetRanking($num)
    {
        if($num == 1)
        {
            return '001';
        }
        if($num > 1 && $num < 5)
        {
            return '002';
        }
        if($num > 4 && $num < 10)
        {
            return '003';
        }
        if($num > 9 && $num < 26)
        {
            return '004';
        }
        if($num > 25)
        {
            return '005';
        }
    }

    public static function ResizeBy($x, $y)
    {
        $do = 0;
        if ($x < $y)
        {
            $a = $x;
            $x = $y;
            $y = $a;
            $do = 1;
        }

        $value = 0;
        $count = 0;
        while ($value < $x)
        {
            $value += $y;
            $count++;
        }

        if ($do == 0)
            return 100 / $count;
        else
            return 100 * $count;
    }

    public static function toUTF($string)
    {
        return iconv('CP1257', 'UTF-8//TRANSLIT', $string);
    }

    public static function getClassName($class)
    {
        switch (intval(substr($class, 1)))
        {
            case 1:
            case 5:
            case 6:
                return 'Warrior';//Lang::get('buttons.warrior');
                break;

            case 2:
            case 7:
            case 8:
                return 'Rogue';//Lang::get('buttons.rogue');
                break;

            case 3:
            case 9:
            case 10:
                return 'Magician';//Lang::get('buttons.magician');
                break;

            case 4:
            case 11:
            case 12:
                return 'Priest';//Lang::get('buttons.priest');
                break;
        }
    }

    public static function getZoneName($zone)
    {
        return ZoneInfo::getZoneName($zone);
    }

    public static function getItemTypeName($itemType)
    {
        switch($itemType){
            case 0:
                return "(Upgrade Item)<br />";
            case 1:
                return "(Magic Item)<br />";
            case 2:
                return "(Rare Item)<br />";
            case 3:
                return "(Craft Item)<br />";
            case 4:
                return "(Unique Item)<br />";
            case 5:
                return "(Upgrade Item)<br />";
            case 6:
                return "";
            default:
                return "";
        }
    }

    public static function getItemKindName($Kind)
    {
        switch ($Kind)
        {
            case 11:
                return 'DAGGER';
                break;

            case 21:
                return 'SWORD';
                break;

            case 22:
                return '2HSWORD';
                break;

            case 31:
                return 'AXE';
                break;

            case 32:
                return '2HAXE';
                break;

            case 41:
                return 'MACE';
                break;

            case 42:
                return '2HMACE';
                break;

            case 51:
                return 'SPEAR';
                break;

            case 52:
                return '2HSPEAR';
                break;

            case 60:
                return 'SHIELD';
                break;

            case 70:
                return 'BOW';
                break;

            case 71:
                return 'XBOW';
                break;

            case 91:
                return 'EARRING';
                break;

            case 92:
                return 'NECKLACE';
                break;

            case 93:
                return 'RING';
                break;

            case 94:
                return 'BELT';
                break;

            case 95: // quest items
                return NULL;
                break;

            case 97: // misc
                return NULL;
                break;

            case 98: // scrolls
                return NULL;
                break;

            case 110:
                return 'STAFF';
                break;

            case 210:
                return 'WARRIOR_ARMOR';
                break;

            case 220:
                return 'ROGUE_ARMOR';
                break;

            case 230:
                return 'MAGE_ARMOR';
                break;

            case 240:
                return 'PRIEST_ARMOR';
                break;

            case 255: // misc
                return NULL;
                break;
        }
    }

    public static function getNationName($nation)
    {
        return $nation == 1 ? 'Karus' : 'Elmorad';
    }

    public static function getClanRanking($np)
    {
        if($np < 72000) return "gn_grade_5.gif";
        if($np >= 72000 && $np < 144000) return "gn_grade_4.gif";
        if($np >= 144000 && $np < 360000) return "gn_grade_3.gif";
        if($np >= 360000 && $np < 720000) return "gn_grade_2.gif";
        if($np >= 720000) return "gn_grade_1.gif";
    }

    public static function getClanRankingImage($ranking)
    {
        switch($ranking) {
            case 3:
                return "symbol5.jpg";
            case 4:
                return "symbol4.jpg";
            case 5:
                return "symbol3.jpg";
            case 6:
                return "symbol2.jpg";
            case 7:
                return "symbol1.jpg";
            case 8:
                return "gn_grade_5.gif";
            case 9:
                return "gn_grade_4.gif";
            case 10:
                return "gn_grade_3.gif";
            case 11:
                return "gn_grade_2.gif";
            case 12:
                return "gn_grade_1.gif";
            default:
                return "k006.gif";
        }
    }

    public static function getMapName($ZoneNum)
    {
        $ZoneName = ZoneInfo::getZoneName($ZoneNum);
        switch($ZoneName)
        {
            case 'BDW Room':
                return 'bdw.jpg';
            case 'Forgotten Temple':
            case 'KoMaster':
            case 'Goblin Arena':
            case 'Orc Arena':
            case 'Blood Don Arena':
                return 'forgotten_temple.jpg';
            case 'Battle Arena':
            case 'Private Arenad':
            case 'OldSchoolKO_40.smd':
                return 'arena.jpg';
            case 'Colony Zone':
                return 'old_cz.jpg';
            case 'Bifrost':
                return 'bifrost.jpg';
            case 'Lunar War':
                return 'lunar_1.jpg';
            case 'Lunar War':
                return 'lunar_2.jpg';
            case 'OldSchoolKO_103.smd':
                return 'lunar_3.jpg';
            case 'Moradon':
                return 'moradon.jpg';
            case 'Delos':
                return 'delos.jpg';
            case 'Desesperation Abyss':
                return 'desperation_abyss.jpg';
            case 'Hell Abyss':
                return 'hell_abyss.jpg';
            default:
                return 'arena.jpg';
        }
    }

    public static function doEvalBBTags ($text)
    {
        return preg_replace (
            array (
                '/\[b\](.*?)\[\/b\]/i',
                '/\[i\](.*?)\[\/i\]/i',
                '/\[s\](.*?)\[\/s\]/i',
                '/\[img\](.*?)\[\/img\]/i',
                '/\[url\=(.*?)\](.*?)\[\/url\]/i',
                '/\[align\=(left|center|right)\](.*?)\[\/align\]/i',
                '/\[color\=(.*?)\](.*?)\[\/color\]/i',
                '/\[font\=(.*?)\](.*?)\[\/font\]/i'
            ),
            array (
                '<b>\\1</b>',
                '<i>\\1</i>',
                '<s>\\1</s>',
                '<img src="\\1" alt="Image" />',
                '<a href="\\1">\\2</a>',
                '<div style="text-align: \\1;">\\2</div>',
                '<span style="color: \\1;">\\2</span>',
                '<span style="font-family: \\1;">\\2</span>'
            ),
            $text);
    }

    public static function getIconID($IconID)
    {
        $icon = 'itemicon_';
        $icon .= mb_strlen($IconID) < 9 ? substr($IconID, 0, 1) . '_' . substr($IconID, 1, 4) . '_' . substr($IconID, 5, 2) . '_' . substr($IconID, 7, 1) . '.jpg' : substr($IconID, 0, 2) . '_' . substr($IconID, 2, 4) . '_' . substr($IconID, 6, 2) . '_' . substr($IconID, 8, 1).'.jpg';
        return $icon;
    }

}