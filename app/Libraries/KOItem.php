<?php namespace App\Libraries;

use Auth;
use Cache;
use Carbon\Carbon;
use App;

class KOItem
{
    public static function getPresetItemData($pEquipped, $num = 46, $renderInventory = false)
    {
        $presetItemData = '';
        $testdata = '';

        for ($i = 0; $i < $num; $i++)
        {
            if($renderInventory == false)
                if($i > 14 && $i < 42) continue;

            $pItem = $pEquipped->Get($i);
            if(is_null($pItem)) continue;
            if($pItem->GetNum() <= 0) continue;

            $ItemID = $pItem->GetNum();

            $ItemType = $pItem->GetData('ItemType');
            if ($ItemType == NULL)
                $ItemType = 6;

            $Kind = $pItem->GetData('Kind');
            $Delay = $pItem->GetData('Delay');
            if (substr($ItemID, -3, 3) == '000')
                $ItemType = 6;

            $itemData['itemid'] = $ItemID;
            $itemData['item-name'] = htmlentities($pItem->GetName());
            if (Auth::check() && Auth::user()->strAuthority == 0 || Auth::check() && Auth::user()->strAuthority == 2){
                $itemData['item-serial'] = $pItem->GetNum();
            } else {
                $itemData['item-serial'] = '';
            }
            $itemData['item-type'] = Template::getItemTypeName($ItemType);
            $itemData['item-type2'] = 'itemType-' . $ItemType;
            $itemData['item-kind'] = Template::getItemKindName($Kind);

            $extendedInfo = '';

            //Drop Items
            if($pItem->getDropPercentage() >= 0) {
                $extendedInfo .= '<div style="font-weight: bold; color: #B45F04;">Drop Percentage: '.$pItem->getDropPercentage().'%</div><br />';
            }

            // weapons
            if (($Kind >= 11 && $Kind <= 71 && $Kind != 60) || $Kind == 110)
            {
                $extendedInfo .= 'Attack Power : '.$pItem->GetData('Damage').'<br />';
                $extendedInfo .= 'Attack Speed : '.($Delay < 110 ? 'FAST' : ($Delay > 150 ? 'VERY_SLOW' : 'SLOW')).'<br />';
                $extendedInfo .= 'Effective Range : '.sprintf('%.02f', $pItem->GetData('Range') / 10).'<br />';
            }

            // weapons, armors, misc
            if ((($Kind >= 11 && $Kind <= 71) || $Kind == 110) || ($Kind >= 210 && $Kind <= 240) || ($Kind == 97 || $Kind == 255))
            {
                $extendedInfo .= 'Weight : '.sprintf('%.02f', $pItem->GetData('Weight') / 10).'<br />';
            }

            // no longer misc
            if ((($Kind >= 11 && $Kind <= 71) || $Kind == 110) || ($Kind >= 210 && $Kind <= 240))
            {
                $extendedInfo .= 'Max Durability : '.$pItem->GetData('Duration').'<br />';
                $durability = $pItem->GetDurability();
                $extendedInfo .= 'Current Durability : '.$pItem->GetDurability().'<br />';
            }

            // armors
            if (($Kind >= 210 && $Kind <= 240) || $Kind == 60 || $pItem->GetData('Ac') > 0)
            {
                $extendedInfo .= 'Defense Ability : '.$pItem->GetData('Ac').'<br />';
            }

            $FireDamage = $pItem->GetData('FireDamage');
            $IceDamage = $pItem->GetData('IceDamage');
            $LightningDamage = $pItem->GetData('LightningDamage');
            $PoisonDamage = $pItem->GetData('PoisonDamage');

            $HPDrain = $pItem->GetData('HPDrain');
            $MPDamage = $pItem->GetData('MPDamage');
            $MPDrain = $pItem->GetData('MPDrain');
            $MaxHpB = $pItem->GetData('MaxHpB');
            $MaxMpB = $pItem->GetData('MaxMpB');

            $StrB = $pItem->GetData('StrB');
            $StaB = $pItem->GetData('StaB');
            $DexB = $pItem->GetData('DexB');
            $IntelB = $pItem->GetData('IntelB');
            $ChaB = $pItem->GetData('ChaB');

            $DaggerAc = $pItem->GetData('DaggerAc');
            $SwordAc = $pItem->GetData('SwordAc');
            $MaceAc = $pItem->GetData('MaceAc');
            $AxeAc = $pItem->GetData('AxeAc');
            $SpearAc = $pItem->GetData('SpearAc');
            $BowAc = $pItem->GetData('BowAc');

            $FireR = $pItem->GetData('FireR');
            $ColdR = $pItem->GetData('ColdR');
            $LightningR = $pItem->GetData('LightningR');
            $MagicR = $pItem->GetData('MagicR');
            $PoisonR = $pItem->GetData('PoisonR');
            $CurseR = $pItem->GetData('CurseR');

            if ($FireDamage > 0 || $IceDamage > 0 || $LightningDamage > 0 || $PoisonDamage > 0
                || $StrB > 0 || $StaB > 0 || $DexB > 0 || $IntelB > 0 || $ChaB > 0
                || $DaggerAc > 0 || $SwordAc > 0 || $MaceAc > 0 || $AxeAc > 0 || $SpearAc > 0 || $BowAc > 0
                || $HPDrain > 0 || $MPDamage > 0 || $MPDrain > 0 || $MaxHpB > 0 || $MaxMpB > 0
                || $FireR > 0 || $ColdR > 0 || $LightningR > 0 || $MagicR > 0 || $PoisonR > 0 || $CurseR > 0)
            {
                $bonusInfo = '';

                if ($FireDamage > 0)
                    $bonusInfo .= 'Fire Damage : '.$FireDamage.'<br />';

                if ($IceDamage > 0)
                    $bonusInfo .= 'Ice Damage : '.$IceDamage.'<br />';

                if ($LightningDamage > 0)
                    $bonusInfo .= 'Lightning Damage : '.$LightningDamage.'<br />';

                if ($PoisonDamage > 0)
                    $bonusInfo .= 'Poison Damage : '.$PoisonDamage.'<br />';

                if ($DaggerAc > 0)
                    $bonusInfo .= 'Defense Ability (Dagger) : '.$DaggerAc.'<br />';

                if ($SwordAc > 0)
                    $bonusInfo .= 'Defense Ability (Sword) : '.$SwordAc.'<br />';

                if ($MaceAc > 0)
                    $bonusInfo .= 'Defense Ability (Club) : '.$MaceAc.'<br />';

                if ($AxeAc > 0)
                    $bonusInfo .= 'Defense Ability (Ax) : '.$AxeAc.'<br />';

                if ($SpearAc > 0)
                    $bonusInfo .= 'Defense Ability (Spear) : '.$SpearAc.'<br />';

                if ($BowAc > 0)
                    $bonusInfo .= 'Defense Ability (Bow) : '.$BowAc.'<br />';

                if ($HPDrain > 0)
                    $bonusInfo .= 'HP Absorbed : '.$HPDrain.'<br />';

                if ($MPDrain > 0)
                    $bonusInfo .= 'MP Absorbed : '.$MPDrain.'<br />';

                if ($MPDamage > 0)
                    $bonusInfo .= 'MP Damage : '.$MPDamage.'<br />';

                if ($MaxHpB > 0)
                    $bonusInfo .= 'HP Bonus : '.$MaxHpB.'<br />';

                if ($MaxMpB > 0)
                    $bonusInfo .= 'MP Bonus : '.$MaxMpB.'<br />';

                if ($StrB > 0)
                    $bonusInfo .= 'Strength Bonus : '.$StrB.'<br />';

                if ($StaB > 0)
                    $bonusInfo .= 'Health Bonus : '.$StaB.'<br />';

                if ($DexB > 0)
                    $bonusInfo .= 'Dexterity Bonus : '.$DexB.'<br />';

                if ($IntelB > 0)
                    $bonusInfo .= 'Intelligence Bonus : '.$IntelB.'<br />';

                if ($ChaB > 0)
                    $bonusInfo .= 'Magic Power Bonus : '.$ChaB.'<br />';

                if ($FireR > 0)
                    $bonusInfo .= 'Resistance to Flame : '.$FireR.'<br />';

                if ($ColdR > 0)
                    $bonusInfo .= 'Resistance to Glacier : '.$ColdR.'<br />';

                if ($LightningR > 0)
                    $bonusInfo .= 'Resistance to Lightning : '.$LightningR.'<br />';

                if ($MagicR > 0)
                    $bonusInfo .= 'Resistance to Magic : '.$MagicR.'<br />';

                if ($PoisonR > 0)
                    $bonusInfo .= 'Resistance to Poison : '.$PoisonR.'<br />';

                if ($CurseR > 0)
                    $bonusInfo .= 'Resistance to Curse : '.$CurseR.'<br />';

                $itemData['item-bonuses'] = '<div id="itemBonuses">'.$bonusInfo.'</div>';
            }
            else
            {
                $itemData['item-bonuses'] = NULL;
            }

            $itemData['item-set-bonuses'] = '';

            if($pItem->GetData('ACBonus') > 0)
                $itemData['item-set-bonuses'] .= 'Defense Bonus: '.$pItem->GetData('ACBonus').'<br />';
            if($pItem->GetData('HPBonus') > 0)
                $itemData['item-set-bonuses'] .= 'HP Bonus: '.$pItem->GetData('HPBonus').'<br />';
            if($pItem->GetData('XPBonusPercent') > 0)
                $itemData['item-set-bonuses'] .= 'XP Bonus Percent: %'.($pItem->GetData('XPBonusPercent') * 100).'<br />';
            if($pItem->GetData('CoinBonusPercent') > 0)
                $itemData['item-set-bonuses'] .= 'Coin Bonus Percent: %'.($pItem->GetData('CoinBonusPercent')*100).'<br />';
            if($pItem->GetData('APBonusPercent') > 0)
                $itemData['item-set-bonuses'] .= 'Attack Power Bonus: %'.$pItem->GetData('APBonusPercent').'<br />';
            if($pItem->GetData('APBonusClassPercent') > 0)
                $itemData['item-set-bonuses'] .= 'Class Attack Power Bonus: %'.$pItem->GetData('APBonusClassPercent').'<br />';
            if($pItem->GetData('ACBonusClassPercent') > 0)
                $itemData['item-set-bonuses'] .= 'Class Defense Power Bonus: %'.$pItem->GetData('ACBonusClassPercent').'<br />';

            $ReqStr = $pItem->GetData('ReqStr');
            $ReqSta = $pItem->GetData('ReqSta');
            $ReqDex = $pItem->GetData('ReqDex');
            $ReqIntel = $pItem->GetData('ReqIntel');
            $ReqCha = $pItem->GetData('ReqCha');
            $Desc = str_replace('|', '<br />', $pItem->GetData('strDesc'));

            if ($ReqStr > 0 || $ReqSta > 0 || $ReqDex > 0 || $ReqIntel > 0 || $ReqCha > 0 || !empty($Desc))
            {
                $finalInfo = '';

                if ($ReqStr > 0)
                    'Required Strength : '.$ReqStr.'<br />';

                if ($ReqSta > 0)
                    $finalInfo .= 'Required Health : '.$ReqSta.'<br />';

                if ($ReqDex > 0)
                    $finalInfo .= 'Required Dexterity : '.$ReqDex.'<br />';

                if ($ReqIntel > 0)
                    $finalInfo .= 'Required Intelligence : '.$ReqIntel.'<br />';

                if ($ReqCha > 0)
                    $finalInfo .= 'Required Magic Power : '.$ReqCha.'<br />';

                if (!empty($Desc))
                    $finalInfo .= '<br /><center>' . $Desc . '</center>';

                $itemData['item-final'] = $finalInfo;
            }
            else
                $itemData['item-final'] = NULL;

            $itemData['item-extended'] = $extendedInfo;
            //if (isset($durability) && $durability < 20 && $durability > -1)
            //    $itemData['item-broken'] = 'BROKEN';
            //else
            $itemData['item-broken'] = '';

            $view = view('items.item-data')->with('itemData', $itemData);

            $expiresAt = Carbon::now()->addHours(48);

            Cache::put('presetitem-'.$ItemID, $view->render(), $expiresAt);

            $presetItemData .= $view->render();
        }

        return $presetItemData;
    }
}