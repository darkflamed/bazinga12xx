<?php namespace App\Libraries;

class Item
{
    private $iNum;
    private $iSerial;
    private $sDurability;
    private $sCount;
    private $iIconID;
    private $strName;
    private $getItemData;
    private $itemData;
    private $dropPercent;
    private $itemGroup;

    public function __construct($iNum, $iSerial, $sDurability, $sCount, $getItemData, $dropPercentage = -1, $itemGroup = 0)
    {
        $this->iNum = $iNum;
        $this->iSerial = $iSerial;
        $this->sDurability = $sDurability;
        $this->sCount = $sCount;
        $this->strName = 'N/A';
        $this->getItemData = $getItemData;
        $this->dropPercent = $dropPercentage;
        $this->itemGroup = $itemGroup;
    }

    public function Delete()
    {
        return $this->SetNum(0)->SetSerial(0)->SetDurability(0)->SetCount(0);
    }

    public function GetNum()
    {
        return intval($this->iNum);
    }

    public function SetNum($in)
    {
        $this->iNum = $in;

        if($in == 0)
            return;
        return $this;
    }

    public function LoadItemData($row)
    {
        $this->SetName($row->strName);
        $this->itemData = $row;
    }

    public function GetData($key)
    {
        if(is_null($this->itemData) || is_null($this->itemData->$key))
            return 0;
        return $this->itemData->$key;
    }

    public function GetSerial()
    {
        return $this->iSerial;
    }

    public function SetSerial($in)
    {
        $this->iSerial = $in;
        return $this;
    }

    public function GetDurability()
    {
        return $this->sDurability;
    }

    public function SetDurability($in)
    {
        $this->sDurability = $in;
        return $this;
    }

    public function GetName()
    {
        return $this->strName;
    }

    public function SetName($in)
    {
        $this->strName = $in;
        return $this;
    }

    public function GetCount()
    {
        return $this->sCount;
    }

    public function SetCount($in)
    {
        $this->sCount = $in;
        return $this;
    }

    public function getIconID()
    {
        $IconID = $this->GetData('IconID');
        return mb_strlen($IconID) < 9 ? substr($IconID, 0, 1) . '_' . substr($IconID, 1, 4) . '_' . substr($IconID, 5, 2) . '_' . substr($IconID, 7, 1) : substr($IconID, 0, 2) . '_' . substr($IconID, 2, 4) . '_' . substr($IconID, 6, 2) . '_' . substr($IconID, 8, 1);
    }

    public function getDropPercentage()
    {
        return $this->dropPercent;
    }

    public function getItemGroup()
    {
        return $this->itemGroup;
    }
}

?>