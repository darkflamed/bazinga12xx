<?php
/**
 * Created by PhpStorm.
 * User: darkf
 * Date: 9/14/2017
 * Time: 11:01 AM
 */
namespace App\Libraries;

use Cache;

class PowerupCartDBStorage {

    public function has($key)
    {
        return Cache::has($key);
    }

    public function get($key)
    {
        return Cache::get($key);
    }

    public function put($key, $value)
    {
        Cache::put($key, $value, 30);
        return true;
    }
}