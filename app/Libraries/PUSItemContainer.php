<?php namespace App\Libraries;

use DB;

class PUSItemContainer
{
    var $items, $total, $db, $changed;

    public function __construct($iAllocate, $arrItems)
    {
        $this->items = array();
        $this->total = $iAllocate;
        $this->changed = FALSE;
        $items = array();

        for ($i = 0; $i < $iAllocate; ++$i)
        {
            $this->items[$i] = new Item($arrItems[$i], 00000000, 0, 1, $this, true);
        }

        $itemtable = DB::select(DB::raw('SELECT * FROM ITEM_PUS WHERE Num IN(' . implode(',', $arrItems) . ')'));
        if (sizeof($itemtable) > 0)
        {
            foreach($itemtable as $item)
            {
                $names[$item->Num] = (array) $item;
            }

            for ($i = 0; $i < $iAllocate; ++$i)
            {
                $pItem = $this->items[$i];
                $iNum = $pItem->GetNum();

                if ($iNum > 0)
                {
                    if (@$names[$iNum] != NULL)
                        $pItem->LoadItemData($names[$iNum]);
                }
            }
        }
    }

    public function IsAvailable($iPos)
    {
        if ($iPos < 0 || $iPos > $this->total)
            return false;

        $pItem = $this->items[$iPos];
        if ($pItem->GetNum() == 0)
            return true;
        else
            return false;
    }

    public function GetFirstAvailable()
    {
        for ($i = 0; $i < $this->total; ++$i)
        {
            if ($this->IsAvailable($i))
                return $i;
        }

        return -1;
    }

    public function GetCount()
    {
        $count = 0;
        for ($i = 0; $i < $this->total; ++$i)
        {
            if ($this->items[$i]->GetNum() != 0)
                $count++;
        }

        return $count;
    }

    public function Del($iPos)
    {
        if ($iPos < 0 || $iPos > $this->total)
            return false;

        $pItem = $this->items[$iPos];
        $pItem->SetNum(0);
        $pItem->SetDurability(0);
        $pItem->SetCount(0);
        $pItem->SetSerial(0);

        return true;
    }

    public function Get($iPos)
    {
        if ($iPos < 0 || $iPos > $this->total)
            return NULL;

        return $this->items[$iPos];
    }

    public function Find($itemid)
    {
        $found = false;
        $i = 0;
        while(!$found)
        {
            if($i == ($this->total - 1))
            {
                $pos = -1;
                $found = true;
            }
            $pItem = $this->items[$i];
            if($pItem->GetNum() == $itemid)
            {
                $pos = $i;
                $found = true;
            }
            $i++;
        }
        return $pos;
    }

    public function Set($iPos, $pItem)
    {
        if ($iPos < 0 || $iPos > $this->total)
            return false;

        $this->items[$iPos] = $pItem;
        return true;
    }

    function SetChanged()
    {
        $this->changed = TRUE;
    }

    function __destruct()
    {

    }
}