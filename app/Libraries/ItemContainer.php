<?php namespace App\Libraries;

use App\Items;

define('WAREHOUSE', 192);
define('INVENTORY', 42);

class ItemContainer
{
    var $player, $items, $total, $itemGroup;

    public function __construct($iAllocate, $player, $strItem, $strSerial = NULL, $getNames = TRUE, $getInventory = FALSE, $itemGroup = 0)
    {
        $this->items = array();
        $this->total = 0;
        $this->player = $player;
        //if(!is_array($strItem))
            //$strItem = pack("H*", $strItem);

        $items = array();
        $names = array();
        $this->itemGroup = $itemGroup;

        for ($i = 0; $i < $iAllocate; $i++)
        {
            if($strSerial == NULL && is_array($strItem)) {
                if($strItem[$i] < 100000000) {
                    $items[$i] = 0;
                    $this->items[$i] = new Item(0, $this->ToUInt64(0, 0), 0, 0, $getNames, -1, $strItem[$i]);
                    $this->total++;
                    continue;
                }
                $items[$i] = $strItem[$i];
                $this->items[$i] = new Item($strItem[$i], $this->ToUInt64(0, 0), 0, 1, $getNames, -1);
                $this->total++;
                continue;
            }

            if(strlen(substr($strItem, $i * 8, 8)) != 8) continue;

            if($getInventory == FALSE && ($i > 14 && $i < 42)){
                $items[$i] = 0;
                $this->items[$i] = new Item(0, $this->ToUInt64(0, 0), 0, 0, $getNames);
                $this->total++;
                continue;
            }

            $item = unpack('Vitem/vdurability/vcount', substr($strItem, $i * 8, 8));

            $serial = substr($strSerial, $i * 8, 8);

            $_serial = @unpack("V*", $serial);

            $items[$i] = $item['item'];

            $this->items[$i] = new Item($item['item'], $this->ToUInt64($_serial[2], $_serial[1]), $item['durability'], $item['count'], $getNames);
            $this->total++;
        }

        if($iAllocate == 0) return;
        if ($getNames == FALSE) return;
        if(sizeof($items) < 1) return;

        $itemtable = Items::getItems($items);

        foreach($itemtable as $Item)
        {
            $names[$Item->Num] = $Item;
        }

        for ($i = 0; $i < $iAllocate; ++$i)
        {
            $pItem = $this->items[$i];
            $iNum = $pItem->GetNum();

            if ($iNum > 0)
            {
                if (array_key_exists($iNum, $names) && !is_null($names[$iNum]))
                    $pItem->LoadItemData($names[$iNum]);
            }
        }
    }

    public function IsAvailable($iPos)
    {
        if ($iPos < 0 || $iPos >= $this->total)
            return false;

        $pItem = $this->items[$iPos];
        if ($pItem->GetNum() == 0)
            return true;
        else
            return false;
    }

    public function GetFirstAvailable()
    {
        for ($i = 0; $i < $this->total; $i++)
        {
            if ($this->IsAvailable($i))
                return $i;
        }

        return -1;
    }

    public function GetCount()
    {
        $count = 0;
        for ($i = 0; $i < $this->total; $i++)
        {
            if ($this->items[$i]->GetNum() != 0)
                $count++;
        }

        return $count;
    }

    public function DumpItems()
    {
        $strItem = '';
        for ($i = 0; $i < $this->total; $i++)
        {
            $pItem = $this->items[$i];
            $strItem .= pack("Vvv", $pItem->GetNum(), $pItem->GetDurability(), $pItem->GetCount());
        }
        return $strItem;
    }

    public function DumpSerials()
    {
        $strSerial = '';
        for ($i = 0; $i < $this->total; $i++)
        {
            $pItem = $this->items[$i];
            $strSerial .= $this->FromUInt64($pItem->GetSerial());
        }

        return $strSerial;
    }

    public function Del($iPos)
    {
        if ($iPos < 0 || $iPos > $this->total)
            return false;

        $pItem = $this->items[$iPos];
        $pItem->SetNum(0);
        $pItem->SetDurability(0);
        $pItem->SetCount(0);
        $pItem->SetSerial(0);

        return true;
    }

    public function bankAdd($pItem)
    {
        $this->items[$this->total] = new Item($pItem->GetNum(), $pItem->GetSerial(), $pItem->GetDurability(), $pItem->GetCount(), false);
        $this->total++;
    }

    public function bankDel($iPos)
    {
        if ($iPos < 0 || $iPos >= $this->total)
            return false;

        $pItem = $this->items[$iPos];
        $pItem->SetNum(0);
        $pItem->SetDurability(0);
        $pItem->SetCount(0);
        $pItem->SetSerial(0);
        $this->SortBank($iPos);
        $this->total = $this->total - 1;

        return true;
    }

    public function SortBank($iPos)
    {
        $found = false;
        for($i = 0; $i < $this->total; $i++)
        {
            if($i >= $iPos && ($this->total - 1) != $i)
            {
                $pItem = $this->items[$i];
                $pItem2 = $this->items[$i+1];
                $pItem->SetNum($pItem2->GetNum());
                $pItem->SetDurability($pItem2->GetDurability());
                $pItem->SetCount($pItem2->GetCount());
                $pItem->SetSerial($pItem2->GetSerial());
            }
        }
    }

    public function Get($iPos)
    {
        if ($iPos < 0 || $iPos >= $this->total)
            return NULL;
        return $this->items[$iPos];
    }

    public function GetItemGroup()
    {
        return $this->itemGroup;
    }

    public function Find($itemid)
    {
        $found = false;
        $i = 0;
        while(!$found)
        {
            if($i == ($this->total - 1))
            {
                $pos = -1;
                $found = true;
            }
            $pItem = $this->items[$i];
            if($pItem->GetNum() == $itemid)
            {
                $pos = $i;
                $found = true;
            }
            $i++;
        }
        return $pos;
    }

    public function Set($iPos, $pItem)
    {
        if ($iPos < 0 || $iPos >= $this->total)
            return false;

        $this->items[$iPos] = $pItem;
        return true;
    }

    public function Save($strName)
    {
        if ($this->total == INVENTORY)
        {
            $num_rows = DB::update('UPDATE USERDATA SET strItem = ?, strSerial = ? WHERE strUserId = ?',
                array($this->DumpItems(), $this->DumpSerials(), $strName));
            if ($num_rows > 0)
                return true;
        }
        else
        {
            $num_rows = DB::update('UPDATE WAREHOUSE SET WarehouseData = ?, strSerial = ? WHERE strAccountID = ?',
                array($this->DumpItems(), $this->DumpSerials(), $strName));
            if ($num_rows > 0)
                return true;
        }

        return false;
    }

    public function SaveClanBank($bank)
    {
        $num_rows = DB::update('UPDATE CLANBANKS SET strItems = ?, strSerial = ?, numItems = ? WHERE id = ?',
            array($this->DumpItems(), $this->DumpSerials(), $this->total, $bank));
        if ($num_rows > 0)
            return true;

        return false;
    }

    public static function ToUInt64($hi, $lo)
    {
        if (((int)4294967296) != 0)
            return (((int)$hi) << 32) + ((int)$lo);

        $hi = sprintf("%u", $hi);
        $lo = sprintf("%u", $lo);

        if (function_exists("gmp_mul"))
            return gmp_strval(gmp_add(gmp_mul($hi, "4294967296"), $lo));
        elseif (function_exists("bcmul"))
            return bcadd(bcmul($hi, "4294967296"), $lo);

        trigger_error("\nFor 64 bit integer support, we require that you have either the GNU Multiple Precision or the BC math library.\n\nSince PHP 4.0.4, libbcmath is bundled with PHP. You don't need any external libraries for this extension.\nTo alternatively use the GNU Multiple Precision library, you should enable the php_gmp extension in php.ini.\n", E_USER_ERROR);
    }

    private function FromUInt64($number)
    {
        $hi = 0;
        $lo = 0;

        if (function_exists("gmp_div"))
        {
            $hi = gmp_div($number, "4294967296");
            $lo = gmp_mod($number, "4294967296");
        }
        elseif (function_exists("bcdiv"))
        {
            $hi = bcdiv($number, "4294967296");
            $lo = bcmod($number, "4294967296");
        }
        else
        {
            trigger_error("\nFor 64 bit integer support, we require that you have either the GNU Multiple Precision or the BC math library.\n\nSince PHP 4.0.4, libbcmath is bundled with PHP. You don't need any external libraries for this extension.\nTo alternatively use the GNU Multiple Precision library, you should enable the php_gmp extension in php.ini.\n", E_USER_ERROR);
        }

        return pack("V", $lo) . pack("V", $hi);
    }
}

?>