<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NpcPositions extends Model
{
    protected $table = 'k_npcpos';

    public static function getPositions($ssid)
    {
        return NpcPositions::where('NpcID', '=', $ssid)->orderby('ZoneID', 'asc')->get();
    }
}
