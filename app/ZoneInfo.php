<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cache;
use App;

class ZoneInfo extends Model
{
    protected $table = 'ZONE_INFO';

    public static function getZoneName($num)
    {
        $key = 'Zone_Name_'.$num;

        if(Cache::has($key)) {
            $ZoneName = Cache::get($key);
        } else {
            $ZoneInfo = ZoneInfo::where('ZoneNo', $num)->first();
            if(is_null($ZoneInfo['bz'])) return 'Unknown';
            $ZoneName = $ZoneInfo->bz;
            Cache::put($key, $ZoneName, 1440);
        }

        return $ZoneName;
    }
}
