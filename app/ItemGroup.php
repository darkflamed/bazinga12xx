<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemGroup extends Model
{
    protected $table = 'make_item_group';

    public static function getItemGroups(array $Groups)
    {
        return ItemGroup::whereIn('iItemGroupNum', $Groups)->get();
    }
}
