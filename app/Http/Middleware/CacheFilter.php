<?php

namespace App\Http\Middleware;

use Closure;
use Cache;
use Illuminate\Support\Str;
use Auth;
use App;

class CacheFilter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $minutes)
    {
        $key = $this->makeCacheKey($request->url());

        if((!is_null($request->getSession()) && $request->getSession()->get('errors')) || env('APP_CACHE') != 'true') {

        } else {
            if(Cache::has($key) && !Auth::check())
                return response(Cache::get($key), 200);
        }

        $response = $next($request);

        if((!is_null($request->getSession()) && $request->getSession()->get('errors')) || env('APP_CACHE') != 'true') {

        } else {
            if (!Cache::has($key) && !Auth::check()) Cache::put($key, $response->getContent(), $minutes);
        }

        return $response;
    }

    private function makeCacheKey($url)
    {
        return 'route_' . Str::slug($url);
    }
}
