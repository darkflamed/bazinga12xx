<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EventController extends Controller
{
    public function index()
    {
        return view('events.index');
    }

    public function bifrost()
    {
        return view('events.bifrost');
    }

    public function crlns()
    {
        return view('events.CRLNS');
    }

    public function crhl()
    {
        return view('events.CRHL');
    }

    public function lunar()
    {
        return view('events.lunar');
    }

    public function forgotten_temple()
    {
        return view('events.forgotten_temple');
    }

    public function csw()
    {
        return view('events.csw');
    }

    public function juraid()
    {
        return view('events.juraid');
    }

    public function crcz()
    {
        return view('events.CRCZ');
    }

    public function death_match()
    {
        return view('events.death_match');
    }

    public function bdw()
    {
        return view('events.bdw');
    }
}
