<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SearchController extends Controller
{
    public function index()
    {
        return view('search.index');
    }

    public function jsonSearch(Request $request)
    {
        if($request->term !== NULL) {
            $term = stripslashes($request->term);
            if(strlen($term) > 21 || strlen($term) < 2)
                return 0;

            $returncharacter = array();
            $UserSearch = DB::table('USERDATA')->select('strUserId')->where('strUserId', 'LIKE', "%".$term."%")->get();
            $ClanSearch = DB::table('KNIGHTS')->select('IDName')->where('IDName', 'LIKE', "%".$term."%")->get();

            foreach ($UserSearch as $result)
            {
                array_push($returncharacter,array('label'=>trim($result->strUserId), 'category' => 'CHARACTER', 'value'=>trim($result->strUserId)));
            }

            foreach ($ClanSearch as $result)
            {
                array_push($returncharacter,array('label'=>trim($result->IDName), 'category' => 'CLAN', 'value'=>trim($result->IDName)));
            }

            return json_encode($returncharacter);
        }

        return 0;
    }
}
