<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ConfirmationController extends Controller
{
    public function index($token)
    {
        $user = User::where('email_token', $token)->first();
        if(is_null($user)) {
            return view('auth.confirmed_error');
        }
        $user->verified = 1;

        if($user->save()){
            return view('auth.confirmed',['user'=>$user]);
        }

        return view('auth.confirmed_error');
    }
}
