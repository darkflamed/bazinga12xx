<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Items;

class ItemController extends Controller
{
    public function getItems(Request $request)
    {
        $Items = explode('|', $request->items);

        $ItemTable = Items::getItems($Items);
        $ItemTable->each(function($item, $key){
            $item->strName = trim($item->strName);
            $item->strDescription = str_replace('|', '<br />', trim($item->strDescription));

            if($item->isUnique == 0) unset($item->isUnique);
            if($item->Race == 0) unset($item->Race);
            if($item->Class == 0) unset($item->Class);
            if($item->Damage == 0) unset($item->Damage);
            if($item->Delay == 0) unset($item->Delay);
            if($item->Range == 0) unset($item->Range);
            if($item->Weight == 0) unset($item->Weight);
            if($item->Duration == 0) unset($item->Duration);
            if($item->Ac == 0) unset($item->Ac);
            if($item->Countable == 0) unset($item->Countable);
            if($item->ReqLevel == 0) unset($item->ReqLevel);
            if($item->ReqLevelMax == 100) unset($item->ReqLevelMax);
            if($item->ReqRank == 0) unset($item->ReqRank);
            if($item->ReqTitle == 0) unset($item->ReqTitle);
            if($item->ReqStr == 0) unset($item->ReqStr);
            if($item->ReqSta == 0) unset($item->ReqSta);
            if($item->ReqDex == 0) unset($item->ReqDex);
            if($item->ReqIntel == 0) unset($item->ReqIntel);
            if($item->ReqCha == 0) unset($item->ReqCha);
            if($item->Hitrate == 0) unset($item->Hitrate);
            if($item->Evasionrate == 0) unset($item->Evasionrate);
            if($item->DaggerAc == 0) unset($item->DaggerAc);
            if($item->SwordAc == 0) unset($item->SwordAc);
            if($item->MaceAc == 0) unset($item->MaceAc);
            if($item->AxeAc == 0) unset($item->AxeAc);
            if($item->SpearAc == 0) unset($item->SpearAc);
            if($item->BowAc == 0) unset($item->BowAc);
            if($item->FireDamage == 0) unset($item->FireDamage);
            if($item->IceDamage == 0) unset($item->IceDamage);
            if($item->LightningDamage == 0) unset($item->LightningDamage);
            if($item->PoisonDamage == 0) unset($item->PoisonDamage);
            if($item->HPDrain == 0) unset($item->HPDrain);
            if($item->MPDamage == 0) unset($item->MPDamage);
            if($item->MPDrain == 0) unset($item->MPDrain);
            if($item->MirrorDamage == 0) unset($item->MirrorDamage);
            if($item->StrB == 0) unset($item->StrB);
            if($item->StaB == 0) unset($item->StaB);
            if($item->DexB == 0) unset($item->DexB);
            if($item->IntelB == 0) unset($item->IntelB);
            if($item->ChaB == 0) unset($item->ChaB);
            if($item->MaxHpB == 0) unset($item->MaxHpB);
            if($item->MaxMpB == 0) unset($item->MaxMpB);
            if($item->FireR == 0) unset($item->FireR);
            if($item->ColdR == 0) unset($item->ColdR);
            if($item->LightningR == 0) unset($item->LightningR);
            if($item->MagicR == 0) unset($item->MagicR);
            if($item->PoisonR == 0) unset($item->PoisonR);
            if($item->CurseR == 0) unset($item->CurseR);
        });

        return ($ItemTable->toJson());
    }
}
