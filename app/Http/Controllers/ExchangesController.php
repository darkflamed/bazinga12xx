<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ItemExchange;
use App\Libraries\ItemContainer;
use App\Libraries\KOItem;
use App\Items;

class ExchangesController extends Controller
{
    public function index()
    {
        $itemFilter = [379106000, 379154000, 379156000, 379157000, 379158000, 810043000, 389196000, 389197000, 389198000, 389199000, 389201000, 389205000,
            389217000, 389160000, 389166000, 389161000, 389165000, 389164000, 389162000, 389163000, 978155000, 502573034, 503573034, 508122000, 508121000];
        return view('exchanges.index')
            ->with('Exchanges', ItemExchange::whereIn('nOriginItemNum1', $itemFilter)
            ->orWhere('strNote', 'LIKE', '%Trivia%')
            ->leftJoin('item', 'nOriginItemNum1', '=', 'Num')->get());
    }

    public function getItemData($nNpcNum, $Item)
    {
        $Items = collect([]);
        $Exchanges = ItemExchange::where('nNpcNum', $nNpcNum)->where('nOriginItemNum1', $Item)->get();

        foreach($Exchanges as $exc)
            for ($i = 1; $i < 6; $i++) {
                if ($exc['nExchangeItemNum' . $i] >= 100000000 && $exc['nExchangeItemNum' . $i] < 1000000000)
                    if (!$Items->contains($exc['nExchangeItemNum' . $i])) $Items->push($exc['nExchangeItemNum' . $i]);
            }

        return $Items->toJson();
    }
}
