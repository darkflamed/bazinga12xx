<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HardwareLog;
use App\HardwareBanList;
use App\User;
use App\Character;

class AdminController extends Controller
{
    public static function banHardware($account_id)
    {
        $HWLOG = HardwareLog::getAccountLogs($account_id);
        if(is_null($HWLOG) || $HWLOG->count() <= 0) return 'User Does not Have a Hardware Log';
        foreach($HWLOG as $log)
            HardwareBanList::banHardware($account_id, $log->HWID);
        return redirect()->back();
    }

    public static function unbanHardware($account_id)
    {
        HardwareBanList::unbanHardware($account_id);
        return redirect()->back();
    }

    public static function banAccount($AccountID)
    {
        $User = User::getUser($AccountID);
        $User->strAuthority = 255;
        $User->timestamps = false;
        $User->save();
        return redirect()->back();
    }

    public static function unbanAccount($AccountID)
    {
        $User = User::getUser($AccountID);
        $User->strAuthority = 1;
        $User->timestamps = false;
        $User->save();
        return redirect()->back();
    }

    public static function banCharacter($CharacterID)
    {
        $Character = Character::find($CharacterID);
        $Character->Authority = 255;
        $Character->timestamps = false;
        $Character->save();
        return redirect()->back();
    }

    public static function unbanCharacter($CharacterID)
    {
        $Character = Character::find($CharacterID);
        $Character->Authority = 1;
        $Character->timestamps = false;
        $Character->save();
        return redirect()->back();
    }

    public static function muteCharacter($CharacterID)
    {
        $Character = Character::find($CharacterID);
        $Character->Authority = 11;
        $Character->timestamps = false;
        $Character->save();
        return redirect()->back();
    }

    public static function unmuteCharacter($CharacterID)
    {
        $Character = Character::find($CharacterID);
        $Character->Authority = 1;
        $Character->timestamps = false;
        $Character->save();
        return redirect()->back();
    }
}
