<?php

namespace App\Http\Controllers;

use App\Clan;
use App\Libraries\ItemContainer;
use Illuminate\Http\Request;
use Mail;
use Auth;
use DB;
use App\Mail\PasswordChanged;
use App\Character;
use App\CurrentUser;
use App\User;
use App\Mail\ForgotPin;
use App\ForgotPIN as DBForgotPIN;
use App\Libraries\Template;

class AccountController extends Controller
{
    public function index()
    {
        return view('account.main');
    }

    public function getRecharge()
    {
        $token = Auth::user()->createToken('PowerupStore')->accessToken;
        $kcoptions = '';

        foreach(Template::$KCOptions as $key => $kcoption) {
            $kcoptions .= '<option value="'.$key.'" id="kc-'.$key.'">'.$kcoption['kc'].'KC (€'.$kcoption['price'].' - '.$kcoption['tl'].'TL)';
            if($kcoption['bonus'] > 0) $kcoptions .= ' '.$kcoption['bonus'].' BONUS';
            $kcoptions .= ' </option>';
        }

        return view('account.recharge')->with('KCOptions', $kcoptions)->with('oAuth', $token);
    }

    public function getChangePassword()
    {
        return view('account.changepassword');
    }

    public function postChangePassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|min:6|max:18',
            'newpassword' => 'required|min:6|max:18|confirmed',
            'pin'   => 'required|digits_between:4,8|numeric',
        ]);

        $credentials = array('strAccountID' => Auth::user()->strAccountID, 'password' => $request->password);
        if (!Auth::validate($credentials)) return redirect()->back()->withErrors(['Incorrect Password was entered!'])->withInput();
        if (Auth::user()->pin != $request->pin) return redirect()->back()->withErrors(['Incorrect Pin was entered!'])->withInput();

        $newPassword = bcrypt($request->newpassword);
        $user = Auth::user();
        $user->password = $newPassword;
        $user->save();

        Mail::to(Auth::user()->email)->queue(new PasswordChanged());

        return redirect('/account')->with('Message', 'Your password has been successfully changed!');
    }

    public function getChangePIN()
    {
        return view('account.changepin');
    }

    public function postChangePIN(Request $request)
    {
        $this->validate($request, [
            'pin' => 'required|digits_between:4,8|numeric',
            'newpin' => 'required|digits_between:4,8|numeric|confirmed',
        ]);

        if($request->pin != Auth::user()->pin)
            return redirect()->back()->withErrors(['PIN is incorrect.'])->withInput();

        Auth::user()->pin = $request->newpin;
        Auth::user()->save();

        return redirect('/account');
    }

    public function postForgotPIN()
    {
        $rows = DBForgotPIN::where('account_id', Auth::user()->id)->count();
        if($rows >= 3)
            return view('account.forgotpin')->with('Message', 'You can not request PIN more then 3 times in one day.');
        Mail::to(Auth::user()->email)->queue(new ForgotPin(Auth::user()->pin));
        $fp = new DBForgotPIN;
        $fp->account_id = Auth::user()->id;
        $fp->save();
        return view('account.forgotpin')->with('Message', 'PIN has been sent to your email.');
    }

    public function getClanNationChange()
    {
        $Characters = Character::getCharList();
        return view('account.clannationchange')->with(['Characters' => $Characters]);
    }

    public function postClanNationChange(Request $request)
    {
        $Character = Character::getCharList();
        $CurrentCharacter = Character::find($request->character);
        $ClanID = 0;

        if($CurrentCharacter->account_id != Auth::user()->id) return redirect()->back()->withErrors(['Invalid Character Selection.'])->withInput();
        if(Auth::user()->pin != $request->pin) return redirect()->back()->withErrors(['Invalid PIN.'])->withInput();
		if(Auth::user()->nKnightCash < 3000) return redirect()->back()->withErrors(['You do not have enough Knight Cash for this transaction.'])->withInput();
		if($CurrentCharacter->Race >= 20 && $CurrentCharacter->Race < 30) return redirect()->back()->withErrors(['You cannot NT because some of your clan members have Transformation Scroll active.'])->withInput();

        foreach($Character as $char) {
            if($char->id == $request->character) {
                $ClanID = $char->Knights;
            }
        }

        $Clan = Clan::getClan($ClanID);
        if(is_null($Clan)) return redirect()->back()->withErrors(['You need to be part of a clan to perform this action.'])->withInput();

        if(strcmp($Clan->Chief, $CurrentCharacter->strUserId) != 0) return redirect()->back()->withErrors(['You need to be Chief to perform this action.'])->withInput();

        $CharList = [];
        $ClanCharacters = Character::getClanCharacters($ClanID);
        foreach($ClanCharacters as $char) {
            array_push($CharList, $char->strUserId);
        }

        $NumOnline = CurrentUser::whereIn('strCharID', $CharList)->count();
        if($NumOnline > 0) return redirect()->back()->withErrors(['Clan members are online, please log off to perform this function.'])->withInput();

        /**
         * Checks are done time to do the dirty work.
         */
		 
		 Auth::user()->nKnightCash -= 3000;
		 Auth::user()->save();

        $Clan->Nation = $Clan->Nation == 1 ? 2 : 1;
        $Clan->timestamps = false;
        $Clan->save();

        foreach($ClanCharacters as $member) {
            $user = User::find($member->account_id);
            DB::statement("EXEC [dbo].[ACCOUNT_NATION_CHANGE] @strAccountID = ?, @Nation = ?", [$user->strAccountID, $member->Nation == 1 ? 2 : 1]);
        }

        return redirect('/account')->with('Message', 'Your clan has successfully been transferred');
    }

    public function getClanLeaderChange()
    {
        $Characters = Character::getCharList();
        $ClanID = $Characters->first()->Knights;
        return view('account.clanleaderchange')->with(['Characters' => $Characters, 'ClanCharacters' => Character::getClanCharacters($ClanID)]);
    }
	
	public function postClanLeaderChange(Request $request)
	{
		$Character = Character::getCharList();
        $CurrentCharacter = Character::find($request->character);
		$newChiefCharacter = Character::find($request->chief_character);
        $ClanID = 0;
		
		if(is_null($CurrentCharacter) || $CurrentCharacter->account_id != Auth::user()->id) return redirect()->back()->withErrors(['Invalid Character Selection.'])->withInput();
		if(is_null($newChiefCharacter) || $newChiefCharacter->Knights != $CurrentCharacter->Knights) return redirect()->back()->withErrors(['Invalid New Leader Character Selection.'])->withInput();
        if(Auth::user()->pin != $request->pin) return redirect()->back()->withErrors(['Invalid PIN.'])->withInput();
		if(Auth::user()->nKnightCash < 250) return redirect()->back()->withErrors(['You do not have enough Knight Cash for this transaction.'])->withInput();
		
		foreach($Character as $char) {
            if($char->id == $request->character) {
                $ClanID = $char->Knights;
            }
        }
		
		$Clan = Clan::getClan($ClanID);
        if(is_null($Clan)) return redirect()->back()->withErrors(['You need to be part of a clan to perform this action.'])->withInput();
		
		if(strcmp($Clan->Chief, $CurrentCharacter->strUserId) != 0) return redirect()->back()->withErrors(['You need to be Chief to perform this action.'])->withInput();

        $CharList = [];
        $ClanCharacters = Character::getClanCharacters($ClanID);
        foreach($ClanCharacters as $char) {
            array_push($CharList, $char->strUserId);
        }
		
		$NumOnline = CurrentUser::whereIn('strCharID', $CharList)->count();
        if($NumOnline > 0) return redirect()->back()->withErrors(['Clan members are online, please log off to perform this function.'])->withInput();
		
		Auth::user()->nKnightCash -= 250;
		Auth::user()->save();
		
		$CurrentCharacter->Fame = 0;
		$CurrentCharacter->timestamps = false;
		$CurrentCharacter->save();
		
		$newChiefCharacter->Fame = 1;
		$newChiefCharacter->timestamps = false;
		$newChiefCharacter->save();
		
		$Clan->Chief = $newChiefCharacter->strUserId;
        $Clan->timestamps = false;
        $Clan->save();
		
		return redirect('/account')->with('Message', 'Your clan has a new leader');
	}
}
