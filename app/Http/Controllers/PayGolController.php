<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\Template;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use DB;
use App\StoreTransactions;
use Auth;
use App\User;

class PayGolController extends Controller
{
    public function createPayment(Request $request)
    {
        if (!array_key_exists($request->KCSelect, Template::$KCOptions))
            return 'Option not found!';

        $kcoption = Template::$KCOptions[$request->KCSelect];
        $kcoption['id'] = $request->KCSelect;

        $id = StoreTransactions::storeNewTransaction($request, $kcoption, 0);

        $params = array(
            'pg_serviceid' => 350098,
            'pg_currency' => 'EUR',
            'pg_name'  => $kcoption['kc'].'KC (€'.$kcoption['price'].') Non-Refundable',
            'pg_price' => $kcoption['price'],
            'pg_custom' => $id,
            'pg_return_url' => url('/account'),
            'pg_cancel_url' => url('/account'),
        );

        return json_encode($params);
    }

    public function ipn(Request $request)
    {
        if($request->key != env('PAYGOL_KEY', '0')) {
            $this->logPayGol('Unkown Key.', $request);
            die();
        }

        if($request->service_id != 350098) {
            $this->logPayGol('Service ID was incorrect.', $request);
            die();
        }

        if(!isset($request->custom)) {
            $this->logPayGol('Custom field was not set.', $request);
            die();
        }

        $trans = StoreTransactions::where('id', '=', $request->custom)->first();

        if(is_null($trans) || $trans->count() <= 0) {
            $this->logPayGol('Could not find transaction in table.', $request);
            die();
        }

        if(strcmp($trans->status, 'approved') == 0) {
            $this->logPayGol('Transaction is already approved.', $request);
            die();
        }

        $validPrice = (floatval($request->frmprice) == floatval($trans->kc_price));
        if(!$validPrice) {
            $this->logPayGol('Price field was not valid.', $request);
            die();
        }

        $trans->transaction_id = $request->transaction_id;
        $trans->country = $request->country;
        $trans->payment_gross = floatval($request->frmprice);
        $trans->payment_fee = floatval($request->frmprice) - floatval($request->price);
        $trans->payment_status = 'Approved';
        $trans->save();

        $user = User::find($trans->account_id);

        DB::update('UPDATE TB_USER SET nKnightCash = nKnightCash + ? WHERE id = ?', array($trans->kc_amount, $trans->account_id));

        event(new \App\Events\KnightCashBroadcast($trans->account_id, $trans->kc_amount + $user->nKnightCash));
    }

    public function logPayGol($message, Request $request)
    {
        $view_log = new Logger('PayGol Logs',
            [new StreamHandler(base_path('storage/logs/PayGol.log'), Logger::INFO)]);
        $view_log->addInfo($message);
        $view_log->addInfo(json_encode($request->toArray(), true));
    }
}
