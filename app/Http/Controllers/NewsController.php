<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class NewsController extends Controller
{
    public function index()
    {
        return view('news.feed')->with(['NewsArticles' => News::getNewsArticles()]);
    }

    public function getPage($Article)
    {
        $News = News::getArticle($Article);
        return view('news.page')->with(['News' => $News]);
    }
}
