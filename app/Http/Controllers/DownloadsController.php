<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Downloads;

class DownloadsController extends Controller
{
    public function index()
    {
        return view('downloads')->with(['Downloads' => Downloads::getDownloads()]);
    }
}
