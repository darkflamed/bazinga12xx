<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Mail;
use App\Mail\ConfirmEmail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        if(env('APP_ENV') == 'local') {
            return Validator::make($data, [
                'accountid' => 'required|max:12|alpha_num|unique:tb_user,strAccountID',
                'email' => 'required|email|max:255|unique:tb_user',
                'password' => 'required|min:6|max:12|alpha_num|confirmed',
                'pin' => 'required|digits_between:4,8|numeric|confirmed',
                'country_code' => 'required|min:2|max:4',
                'tos' => 'accepted',
            ]);
        }

        return Validator::make($data, [
            'accountid' => 'required|max:12|alpha_num|unique:tb_user,strAccountID',
            'email' => 'required|email|max:255|unique:tb_user',
            'password' => 'required|min:6|max:12|alpha_num|confirmed',
            'pin' => 'required|digits_between:4,8|confirmed',
            'country_code' => 'required|min:2|max:4',
            'g-recaptcha-response' => 'required|captcha',
            'tos' => 'accepted',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'strAccountID' => $data['accountid'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'email_token' => base64_encode($data['email'].$data['accountid']),
            'pin' => $data['pin'],
            'country_code' => $data['country_code'],
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return $this->registered($request, $user)
            ?: view('auth.registered');
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    public function registered(Request $request, $user)
    {
        Mail::to($user->email)->queue(new ConfirmEmail($user->email_token));
    }
}
