<?php

namespace App\Http\Controllers;

use App\CurrentUser;
use Illuminate\Http\Request;
use App\Character;
use App\Libraries\ItemContainer;
use App\Libraries\KOItem;
use App\Libraries\Template;
use App\UserLockedItems;
use Auth;
use Redirect;
use Carbon\Carbon;

class SealController extends Controller
{
    public function index()
    {
        $Characters = Character::getCharList();
        $CharacterInfo = [];
        $CharList = [];

        if(CurrentUser::checkCharList($Characters))
            return Redirect::to('/account')->withErrors(['Please log out before using Seal/Unseal!']);

        foreach ($Characters as $char) {
            session(['character-'.$char->strUserId.'-last_updated' => Carbon::createFromFormat('Y-m-d H:i:s', $char->UpdateTime)->timestamp]);
            $CharacterInfo[trim($char->strUserId)] = [];
            $CharacterInfo[trim($char->strUserId)]['items'] = [];
            $CharacterInfo[trim($char->strUserId)]['serials'] = [];
            $CharacterInfo[trim($char->strUserId)]['last_updated'] = session('character-'.$char->strUserId.'-last_updated');
            array_push($CharList, $char->strUserId);

            for ($i = 0; $i < 42; $i++) {
                $item = unpack('Vitem/vdurability/vcount', substr($char->strItem, $i * 8, 8));
                $serial = substr($char->strSerial, $i * 8, 8);
                $_serial = @unpack("V*", $serial);

                array_push($CharacterInfo[trim($char->strUserId)]['items'], $item['item']);
                array_push($CharacterInfo[trim($char->strUserId)]['serials'], (string)ItemContainer::ToUInt64($_serial[2], $_serial[1]));
            }
        }

        $SealedItems = UserLockedItems::wherein('strUserID', $CharList)->get();
        foreach($SealedItems as $sealed)
            $sealed->strUserID = trim($sealed->strUserID);

        return view('account.sealunseal')->with('CharacterInfo', json_encode($CharacterInfo))->with('SealedItems', $SealedItems->toJson());
    }

    public function postSeal(Request $request)
    {
        $add = $request->add;
        $remove = $request->remove;
        $pin = $request->pin;
        $last_updated = $request->last_updated;

        foreach(explode('|', $last_updated) as $updated){
            $tmp = explode(',', $updated);
            if(sizeof($tmp) < 2) continue;
            $LastUpdatedList[$tmp[0]] = $tmp[1];
        }

        if($pin != Auth::user()->pin)
            return json_encode(['message' => 'PIN is incorrect!', 'error' => true]);

        $Characters = Character::getCharList();
        $CharacterList = [];
        $SerialList = [];

        if(CurrentUser::checkCharList($Characters))
            return json_encode(['message' => 'Please log out before using Seal/Unseal!', 'error' => true]);

        foreach ($Characters as $char) {
            if(Carbon::createFromFormat('Y-m-d H:i:s', $char->UpdateTime)->timestamp != session('character-'.$char->strUserId.'-last_updated'))
                return json_encode(['message' => 'Characters data has modified since page load, refreshing page.', 'error' => true, 'reload' => true]);
        }

        foreach ($Characters as $char) {
            array_push($CharacterList, $char->strUserId);
            for ($i = 0; $i < 42; $i++) {
                $serial = substr($char->strSerial, $i * 8, 8);
                $_serial = @unpack("V*", $serial);

                array_push($SerialList, (string)ItemContainer::ToUInt64($_serial[2], $_serial[1]));
            }
        }

        foreach($add as $ad) {
            if(in_array($ad['Serial'], $SerialList)) {
                $locked_item = new UserLockedItems;
                $locked_item->strUserID = $ad['Character'];
                $locked_item->ItemNum = $ad['Num'];
                $locked_item->ItemSerial = $ad['Serial'];
                $locked_item->timestamps = false;
                $locked_item->save();
            }
        }

        foreach($remove as $rem) {
            $item = UserLockedItems::wherein('strUserID', $CharacterList)->where('ItemSerial', $rem['Serial'])->first();
            if(!is_null($item) && $item->count() > 0)
                UserLockedItems::wherein('strUserID', $CharacterList)->where('ItemSerial', $rem['Serial'])->delete();
        }
    }
}
