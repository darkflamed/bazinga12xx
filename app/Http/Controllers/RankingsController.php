<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Character;
use App\Clan;
use App\CurrentUser;

class RankingsController extends Controller
{
    public function index()
    {
        Clan::updateClanPoints();
        return view('rankings')
            ->with(array('KarusUserRankings' => Character::getAllTimeKarusRankings(),
                'ElmoradUserRankings' => Character::getAllTimeElmoradRankings(),
                'KarusClanRankings' => Clan::getAllKarusClanRankings(),
                'ElmoradClanRankings' => Clan::getAllElmoradClanRankings(),
                'KarusMonthlyRankings' => Character::getAllKarusMonthlyRankings(),
                'ElmoradMonthlyRankings' => Character::getAllElmoradMonthlyRankings()));
    }

    public function ingame(Request $request)
    {
        $User = null;
        if(!is_null($request->mgid)) {
            $currentUser = CurrentUser::getAccount($request->mgid);
            $User = Character::where('strUserId', $currentUser->strCharID)->leftJoin('KNIGHTS', 'userdata.Knights', '=', 'knights.IDNum')->first();
        }

        return view('rankings.ingame')
            ->with(array(
                'KarusUserRankings' => Character::getTopIngameKarusRankings(),
                'ElmoradUserRankings' => Character::getTopIngameElmoRankings(),
                'User' => $User
            ));
    }
}
