<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Monsters;
use App\MonsterItems;
use App\ItemGroup;
use App\Libraries\ItemContainer;
use App\Libraries\KOItem;

class DropListController extends Controller
{
    public function index()
    {
        return view('droplist.index')->with(array('Monsters' => Monsters::getMonsters()));
    }

    public function getItemData($sItem)
    {
        $Items = collect([]);
        $Groups = array();

        $Drops = MonsterItems::getDrops($sItem);
        for($i=1;$i<6;$i++){
            if($Drops['iItem0'.$i] < 100000000 && $Drops['iItem0'.$i] > 0) {
                if(!in_array($Drops['iItem0'.$i], $Groups, true))
                    array_push($Groups, $Drops['iItem0'.$i]);
            }
            elseif($Drops['iItem0'.$i] >= 100000000 && $Drops['iItem0'.$i] < 1000000000) {
                if(!$Items->contains($Drops['iItem0'.$i]))
                    $Items->push($Drops['iItem0'.$i]);
            }
        }

        $ItemGroups = ItemGroup::getItemGroups($Groups);

        foreach($ItemGroups as $group) {
            for($i=1;$i<31;$i++)
                if($group['iItem_'.$i] > 0) {
                    if(!$Items->contains($group['iItem_'.$i]))
                        $Items->push($group['iItem_'.$i]);
                }
        }

        return $Items->toJson();
    }
}
