<?php

namespace App\Http\Controllers;

use http\Exception\InvalidArgumentException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\StoreTransactions;
use Auth;
use PayPal;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\PaymentExecution;
use DB;
use App\Libraries\Template;

class PayPalController extends Controller
{
    private $_apiContext;

    public function __construct()
    {
        if(filter_var(env('PAYPAL_SANDBOXED', 'false'), FILTER_VALIDATE_BOOLEAN) == true) {
            $ClientID = env('PAYPAL_SANDBOX_CLIENT_ID', '');
            $Secret = env('PAYPAL_SANDBOX_SECRET', '');
            $Mode = 'sandbox';
            $Endpoint = 'https://api.sandbox.paypal.com';
        } else {
            $ClientID = env('PAYPAL_CLIENT_ID', '');
            $Secret = env('PAYPAL_SECRET', '');
            $Mode = 'live';
            $Endpoint = 'https://api.paypal.com';
        }

        $this->_apiContext = // After Step 1
        $apiContext = new PayPal\Rest\ApiContext(
            new PayPal\Auth\OAuthTokenCredential(
                $ClientID, // ClientID
                $Secret // ClientSecret
            )
        );

        $this->_apiContext->setConfig(array(
            'mode' => $Mode,
            'service.EndPoint' => $Endpoint,
            'http.ConnectionTimeOut' => 30,
            'log.LogEnabled' => true,
            'log.FileName' => storage_path('logs/paypal.log'),
            'log.LogLevel' => 'FINE'
        ));
    }

    public function createPayment(Request $request)
    {
        if (!array_key_exists($request->KCSelect, Template::$KCOptions))
            return new JsonResponse('error', 422);

        $kcoption = Template::$KCOptions[$request->KCSelect];

        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        $item = new Item();
        $item->setName($kcoption['kc'].'KC (€'.$kcoption['price'].')')
            ->setDescription('Non-Refundable Virtual Product ' . $kcoption['kc'] . 'KC')
            ->setCurrency('EUR')
            ->setQuantity(1)
            ->setSku($kcoption['kc']) // Similar to `item_number` in Classic API
            ->setPrice($kcoption['price']);

        $itemList = new ItemList();
        $itemList->setItems(array($item));

        $details = new Details();
        $details->setShipping(0)
            ->setTax(0)
            ->setSubtotal($kcoption['price']);

        $amount = new Amount();
        $amount->setCurrency("EUR")
            ->setTotal($kcoption['price'])
            ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription('Non-Refundable Virtual Product ')
            ->setInvoiceNumber(uniqid("KC_", true));

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(url('/account'))
                    ->setCancelUrl(url('/account'));

        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($this->_apiContext);
        } catch (\Http\Exception $ex) {
            //PayPal\Exception\PayPalConnectionException::printError("Created Payment Using PayPal. Please visit the URL to Approve.", "Payment", null, $request, $ex);
            return new JsonResponse('error', 422);
        }

        return $payment;
    }

    public function executePayment(Request $request)
    {
        $payment = Payment::get($request->paymentID, $this->_apiContext);

        $execution = new PaymentExecution();
        $execution->setPayerId($request->payerID);

        try {
            $result = $payment->execute($execution, $this->_apiContext);

            try {
                $payment = Payment::get($result->id, $this->_apiContext);

                if (strcmp($payment->getState(), 'approved') == 0) {
                    $trans = new StoreTransactions;
                    $trans->account_id = Auth::user()->id;
                    $trans->character_id = '';
                    $trans->client_ip = $request->ip();
                    $trans->kc_option = 0;
                    $trans->kc_amount = $payment->getTransactions()[0]->getItemList()->getItems()[0]->getSku();
                    $trans->kc_price = $payment->getTransactions()[0]->getRelatedResources()[0]->getSale()->getAmount()->getTotal();
                    $trans->provider = 0;
                    $trans->transaction_id = $payment->getId();
                    $trans->country = $payment->getPayer()->getPayerInfo()->getCountryCode();
                    $trans->first_name = $payment->getPayer()->getPayerInfo()->getFirstName();
                    $trans->last_name = $payment->getPayer()->getPayerInfo()->getLastName();
                    $trans->email = $payment->getPayer()->getPayerInfo()->getEmail();
                    $trans->payment_gross = $payment->getTransactions()[0]->getRelatedResources()[0]->getSale()->getAmount()->getTotal();
                    $trans->payment_fee = $payment->getTransactions()[0]->getRelatedResources()[0]->getSale()->getTransactionFee()->getValue();
                    $trans->payment_status = $payment->getState();
                    $trans->payment_method = $payment->getTransactions()[0]->getRelatedResources()[0]->getSale()->getPaymentMode();
                    $trans->save();

                    DB::update('UPDATE TB_USER SET nKnightCash = nKnightCash + ? WHERE id = ?',
                        array($payment->getTransactions()[0]->getItemList()->getItems()[0]->getSku(), Auth::user()->id));

                    event(new \App\Events\KnightCashBroadcast(Auth::user()->id, $payment->getTransactions()[0]->getItemList()->getItems()[0]->getSku() + Auth::user()->nKnightCash));

                    return new JsonResponse('success', 200);
                }

                return new JsonResponse('success', 200);
            } catch (\Http\Exception $ex) {
                //ResultPrinter::printError("Get Payment", "Payment", null, null, $ex);
                return new JsonResponse('error', 422);
            }
        } catch (\Http\Exception $ex) {
            //ResultPrinter::printError("Executed Payment", "Payment", null, null, $ex);
            return new JsonResponse('error', 422);
        }
    }
}
