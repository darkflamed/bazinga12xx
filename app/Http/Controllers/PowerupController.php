<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Input;
use App\User;
use App\Character;
use App\CurrentUser;
use App\StoreItems;
use Cart;
use App\Libraries\Template;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Cache\RateLimiter;

class PowerupController extends Controller
{
    private $m_settings = array();

    public function powerupStore(Request $request)
    {
        if(Auth::check() && strcasecmp(Auth::user()->strAccountID,  is_null($request->mgid)? session('mgid') : $request->mgid)) {
            Auth::guard()->logout();
        }

        if(!Auth::check()) {
            if(is_null($request->mgid)) return view('powerup.error');
            Auth::attempt(['strAccountID' => $request->mgid, 'password_pus' => $request->mgmc]);
            if(!Auth::check()) {
                return view('powerup.login')->with('request', $request);
            }
        }

        if(filter_var(env('PUS_CLOSED', 'false'), FILTER_VALIDATE_BOOLEAN) == true && Auth::user()->is_super_admin != 1) return "PUS is currently closed!";

        $CurrentUser = CurrentUser::getAccount($request->mgid);
        if(is_null($CurrentUser) && Auth::user()->is_super_admin) {
            $CurrentUser = Character::where('account_id', Auth::user()->id)->firstOrFail();
        }
        if (is_null($CurrentUser) || $CurrentUser->count() < 1) return "Please log in game to use Powerup Store!";

        $params = explode(',', is_null($request->param) ? session('param') : $request->param);

        Auth::user()->inventory_slots = $params[2];
        Auth::user()->save();

        return view('powerup.index')->with(array(
            'KnightCash' => Auth::user()->nKnightCash,
            'FreeSlots' => Auth::user()->inventory_slots,
            'KCOptions' => $this->loadKCOptions(),
            'Recommended' => StoreItems::getRecommendedItems(),
            'New' => StoreItems::getNewItems(),
            'oAuth' => Auth::user()->createToken('PowerupStore')->accessToken
        ));
    }

    public function postPowerupLogin(Request $request)
    {
        return app('App\Http\Controllers\Auth\LoginController')->powerupLogin($request);
    }

    public function hasTooManyLoginAttempts(Request $request, LoginController $login)
    {
        return $login->app(RateLimiter::class)->tooManyAttempts(
            $login->throttleKey($request), $login->maxAttempts(), $login->decayMinutes()
        );
    }

    public function getKnightCash(Request $request)
    {
        return json_encode(['nKnightCash' => Auth::user()->nKnightCash, 'Slots' => Auth::user()->inventory_slots]);
    }

    public function getMainContent(Request $request)
    {
        return view('powerup.content.main')->with(array(
            'Recommended' => StoreItems::getRecommendedItems(),
            'New' => StoreItems::getNewItems()
        ));
    }

    public function getSearchItemList(Request $request)
    {
        if(is_null($request->category))
            $request->category = 0;

        if(is_null($request->nowPage))
            $request->nowPage = 1;

        if(is_null($request->listCount) ||  $request->listCount <= 0)
            $request->listCount = 10;

        $StoreItems = StoreItems::getSearchItems($request->searchText, $request->namedesc, $request->category, $request->sortType, $request->nowPage, $request->listCount);
        $TotalItems = StoreItems::getTotalSearchItems($request->searchText, $request->namedesc, $request->category);
        $TotalPages = ceil($TotalItems/$request->listCount);

        return view('powerup.content.itemlist')->with('StoreItems', $StoreItems)->with('ListCount', $request->listCount)->with('TotalCount', $TotalItems)
            ->with('Page', $request->nowPage)->with('TotalPages', $TotalPages)->with('Category', $request->category)
            ->with('SearchPage', 'true')->with('SearchText', $request->searchText);
    }

    public function getCategoryItemList(Request $request)
    {
        if(is_null($request->nowPage))
            $request->nowPage = 1;
        if(is_null($request->sortType))
            $request->sortType = 0;
        if(is_null($request->listCount) ||  $request->listCount <= 0)
            $request->listCount = 10;

        $TotalCount = StoreItems::getTotalCategoryItems($request->category);
        $StoreItems = StoreItems::getCategoryItems($request->category, $request->sortType, "TODO", $request->nowPage,  $request->listCount);

        $TotalPages = ceil($TotalCount/ $request->listCount);

        return view('powerup.content.itemlist')->with('StoreItems', $StoreItems)->with('ListCount', $request->listCount)->with('TotalCount', $TotalCount)
            ->with('Page', $request->nowPage)->with('TotalPages', $TotalPages)->with('Category', $request->category)->with('SearchPage', 'false');

    }

    public function addToCart(Request $request)
    {
        $Items = explode('|', $request->items);

        foreach($Items as $_item) {
            $item = explode(',', $_item);
            if(sizeof($item) < 2) return "Failed to be";
            if(!$storeitem = StoreItems::getItem($item[0])) return "Failed to be";

            Cart::add($item[0], $storeitem->name, $storeitem->price, $item[1],
                ['item_type' => $storeitem->item_type, 'icon' => Template::getIconID($storeitem->icon_id), 'desc1' => $storeitem->description_1, 'desc2' => $storeitem->description_2]);
        }

        return "been Successfully";
    }

    public function removeFromCart(Request $request)
    {
        $Items = explode('|', $request->items);
        foreach($Items as $key => $_item)
            $Items[$key] = explode(',', $_item);

        foreach($Items as $item) {
            Cart::remove($item[0]);
        }
    }

    public function buyItems(Request $request)
    {
        $Items = explode('|', $request->items);
        $StoreItems = array();
        $TotalPrice = 0;
        $TotalItems = 0;

        foreach($Items as $_item) {
            $item = explode(',', $_item);
            $StoreItem = StoreItems::getItem($item[0]);
            $StoreItem->quantity = $item[1];
            $TotalItems += $item[1];
            $TotalPrice += $item[1] * $StoreItem->price;
            array_push($StoreItems, $StoreItem);
        }

        if(Auth::user()->nKnightCash < $TotalPrice) return 'Not enough KC!';

        $CurrentUser = CurrentUser::getAccount($request->mgid);
        if(is_null($CurrentUser) && Auth::user()->is_super_admin) {
            $CurrentUser = Character::where('account_id', Auth::user()->id)->firstOrFail();
        }
        if (is_null($CurrentUser) || $CurrentUser->count() < 1) return "Please log in game to use Powerup Store!";

        if($TotalItems > Auth::user()->inventory_slots) return "Too many items, please free up more inventory slots.";

        if(!User::purchaseItems($StoreItems, $CurrentUser->strUserId)) return 'Could not purchase items!';

        Auth::user()->inventory_slots -= $TotalItems;
        Auth::user()->save();

        //Only needed for Cart Buy
        if(!is_null($request->cartbuy) && intval($request->cartbuy) == 1) {
            foreach ($Items as $_item) {
                $i = explode(',', $_item);
                Cart::remove($i[0]);
            }
        }

        return "Successfully purchased Items!";
    }

    public function getModifyCartItemQuantity($num, $qty)
    {
        Cart::update($num, ['quantity' => ['relative' => false, 'value' => $qty]]);
        return json_encode('success');
    }

    public function getCart(Request $request)
    {
        $Cart = Cart::getContent();
        return view('powerup.content.shoppingcart')->with('Cart', $Cart)->with('CartCount', $Cart->count())->with('CartTotal', Cart::getTotal());
    }

    public function getItemDetail(Request $request, $num)
    {
        return view('powerup.content.itemdetail')->with(array(
            'Item' => StoreItems::getItem($num)
        ));
    }
    //Below is old PUS

    public function loadKCOptions()
    {
        $this->m_settings["bStoreEnabled"] = "true";
        $content = '';

        foreach(Template::$KCOptions as $key => $kcoption) {
            $content .= '<option value="'.$key.'" id="kc-'.$key.'">'.$kcoption['kc'].'KC ($'.$kcoption['price'].')</option>';
        }

        return $content;
    }

    public function getPowerupRecharge(Request $request)
    {
        return view('powerup.recharge.index');
    }

    public function getEnglishRecharge(Request $request)
    {
        return view('powerup.recharge.english');
    }

    public function getSpanishRecharge(Request $request)
    {
        return view('powerup.recharge.spanish');
    }

    public function getTurkishRecharge(Request $request)
    {
        return view('powerup.recharge.turkish');
    }
}
