<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ItemGroup;
use App\MonsterItems;
use App\NpcPositions;
use App\Monsters;
use App\Character;
use App\Clan;
use App\CurrentUser;
use App\User;
use App\Libraries\Template;
use App\Libraries\ItemContainer;
use App\Libraries\KOItem;
use Auth;
use DB;

class CharacterController extends Controller
{
    private $Maps = array
    (
        1	=> 2048,
        2	=> 2048,
        11	=> 1024,
        12	=> 1024,
        21	=> 1024,
        30	=> 1024,
        31	=> 1024,
        32	=> 512,
        33	=> 512,
        48	=> 256,
        51	=> 256,
        52	=> 256,
        53	=> 256,
        54	=> 256,
        55	=> 256,
        61  => 512,
        62  => 512, // UNKNOWN
        63  => 1024,
        68	=> 1024,
        69	=> 1024, // UNKNOWN
        71  => 2048,
        72  => 512,
        73  => 512,
        86	=> 1024,
        87	=> 512,
        96  => 1024,
        101	=> 1024,
        102	=> 1024,
        103	=> 1024,
        201	=> 2048,
        202	=> 1024,
        210 => 512 // UNKNOWN
    );

    public function showClan($ClanName)
    {
        $Knights = Clan::getClanByName($ClanName);
        $Users = Character::getClanCharacters($Knights->IDNum);
        return view('clan.main', array('Knights' => $Knights, 'Users' => $Users));
    }

    public function getMonsterSearchPage()
    {
        return view('droplist')->with('monsters', Monsters::getMonsters());
    }

    public function getMonsterDrops($ssid)
    {
        $Monster = Monsters::getMonster($ssid);
        $ItemDrops = MonsterItems::getDrops($Monster->sItem);
        if(is_null($ItemDrops))
            $strItem = array([0,0],[0,0],[0,0],[0,0],[0,0]);
        else
            $strItem = array(
                [$ItemDrops->iItem01,$ItemDrops->sPersent01],
                [$ItemDrops->iItem02,$ItemDrops->sPersent02],
                [$ItemDrops->iItem03,$ItemDrops->sPersent03],
                [$ItemDrops->iItem04,$ItemDrops->sPersent04],
                [$ItemDrops->iItem05,$ItemDrops->sPersent05]);

        $groups = array();
        if(!is_null($ItemDrops)) {
            if ($ItemDrops->iItem01 > 0 && $ItemDrops->iItem01 < 100000000) array_push($groups, $ItemDrops->iItem01);
            if ($ItemDrops->iItem02 > 0 && $ItemDrops->iItem02 < 100000000) array_push($groups, $ItemDrops->iItem02);
            if ($ItemDrops->iItem03 > 0 && $ItemDrops->iItem03 < 100000000) array_push($groups, $ItemDrops->iItem03);
            if ($ItemDrops->iItem04 > 0 && $ItemDrops->iItem04 < 100000000) array_push($groups, $ItemDrops->iItem04);
            if ($ItemDrops->iItem05 > 0 && $ItemDrops->iItem05 < 100000000) array_push($groups, $ItemDrops->iItem05);
        } else
            $groups = array(0, 0, 0, 0, 0);

        $ItemGroups = ItemGroup::getItemGroups($groups);

        $pEquipped = new ItemContainer(5, $Monster->strName, $strItem);
        $presetItemData = KOItem::getPresetItemData($pEquipped, 5, TRUE);

        $items = array();
        $pGroupEquipped = array();
        foreach($ItemGroups as $key => $ig) {
            $items = array(
                [$ig->iItem_1, 333],[$ig->iItem_2, 333],[$ig->iItem_3, 333],[$ig->iItem_4, 333],[$ig->iItem_5, 333],
                [$ig->iItem_6, 333],[$ig->iItem_7, 333],[$ig->iItem_8, 333],[$ig->iItem_9, 333],[$ig->iItem_10, 333],
                [$ig->iItem_11, 333],[$ig->iItem_12, 333],[$ig->iItem_13, 333],[$ig->iItem_14, 333],[$ig->iItem_15, 333],
                [$ig->iItem_16, 333],[$ig->iItem_17, 333],[$ig->iItem_18, 333],[$ig->iItem_19, 333],[$ig->iItem_20, 333],
                [$ig->iItem_21, 333],[$ig->iItem_22, 333],[$ig->iItem_23, 333],[$ig->iItem_24, 333],[$ig->iItem_25, 333],
                [$ig->iItem_26, 333],[$ig->iItem_27, 333],[$ig->iItem_28, 333],[$ig->iItem_29, 333],[$ig->iItem_30, 333]
            );
            $pGroupEquipped[$key] = new ItemContainer(30, $Monster->strName, $items, NULL, TRUE, FALSE, $ig->iItemGroupNum);
            $presetItemData .= KOItem::getPresetItemData($pGroupEquipped[$key], 30, TRUE);
        }

        return view('monsterdrops')->with('Monster', $Monster)->with('Positions', NpcPositions::getPositions($ssid))->with('Maps', $this->Maps)->with('PresetItemData', $presetItemData)
            ->with('pEquipped', $pEquipped)->with('pGroupEquipped', $pGroupEquipped);
    }

    public function jsonSearch()
    {
        if(Input::get('term') !== NULL) {
            $term = stripslashes(Input::get('term'));
            if(strlen($term) > 21 || strlen($term) < 2)
                return 0;

            $returncharacter = array();
            $search = DB::select('SELECT strUserID FROM USERDATA WHERE strUserID LIKE ?', array('%'.$term.'%'));

            foreach ($search as $result)
            {
                array_push($returncharacter,array('label'=>$result->strUserID, 'value'=>$result->strUserID));
            }
            return json_encode($returncharacter);
        }
    }

    public function clanJsonSearch()
    {
        if(Input::get('term') !== NULL) {
            $term = stripslashes(Input::get('term'));
            if(strlen($term) > 21 || strlen($term) < 2)
                return 0;

            $returncharacter = array();
            $search = DB::select('SELECT IDName, IDNum FROM KNIGHTS WHERE IDName LIKE ?', array('%'.$term.'%'));

            foreach ($search as $result)
            {
                array_push($returncharacter,array('label'=>Template::toUTF($result->IDName), 'value'=>$result->IDNum));
            }
            return json_encode($returncharacter);
        }
    }

    public function showCharacter($CharacterName)
    {
        if(is_null($Character = Character::getCharacter($CharacterName)))
            return '<div class="textred text-center col-xs-12"><h1>Character does not exist!</h1></div>';

        if(intval($Character->Knights) > 0)
            $Character->Knights = Clan::getClan($Character->Knights);
        else
            $Character->Knights = NULL;

        if(is_null($Online = CurrentUser::getCharacter($CharacterName)))
            $Character->Online = 'offline';
        else
            $Character->Online = 'online';

        $Character->MapHeight = array_key_exists($Character->Zone, $this->Maps) ? $this->Maps[$Character->Zone] : 512;
        $Character->MapWidth = array_key_exists($Character->Zone, $this->Maps) ? $this->Maps[$Character->Zone] : 512;
        if (is_null($Character->MapWidth)) $Character->MapWidth = 512;
        if (is_null($Character->MapHeight)) $Character->MapHeight = 512;
        $resizeMultiplier = Template::ResizeBy($Character->MapWidth, 512) / 100;

        $Character->MapWidth *= $resizeMultiplier;
        $Character->MapHeight *= $resizeMultiplier;

        $Character->PX = round(round($Character->PX / 100) * $resizeMultiplier) - 7;
        $Character->PZ = round($Character->MapHeight - (round($Character->PZ / 100) * $resizeMultiplier)) - 14;

        if(Auth::check() && Auth::user()->is_admin) {
            $pEquipped = new ItemContainer(46, $CharacterName, $Character->strItem, $Character->strSerial, true, true);
            $presetItemData = KOItem::getPresetItemData($pEquipped, 46, true);
            $isHardwareBanned = User::checkHardwareBanned($Character->account_id);
        } else { $pEquipped = null; $presetItemData = null; $isHardwareBanned = null; }

        $User = User::find($Character->account_id);
        return view('character.main', array('presetItemData' => $presetItemData, 'Character' => $Character, 'pEquipped' => $pEquipped, 'Flag' => $User->country_code, 'User' => $User,
            'isHardwareBanned' => $isHardwareBanned, 'isAccountBanned' => $User->strAuthority == 255));
    }
}
