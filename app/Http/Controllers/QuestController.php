<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\QuestManager;
use App\Libraries\ItemContainer;
use App\Libraries\KOItem;

class QuestController extends Controller
{
    public function index()
    {
        $Quests = QuestManager::getQuests();
        $ItemList = array();
        foreach($Quests as $quest)
        {
            for($i=1; $i<6; $i++)
            {
                if($quest['QuestRewardItem'.$i] > 0 && !in_array($quest['QuestRewardItem'.$i], $ItemList))
                    array_push($ItemList, $quest['QuestRewardItem'.$i]);
            }
        }

        $pEquipped = new ItemContainer(count($ItemList), '', $ItemList);
        $presetItemData = KOItem::getPresetItemData($pEquipped);

        //dd($pEquipped->Get($pEquipped->Find('379107000'))->GetIconID());

        return view('quests.index')
            ->with('Quests', QuestManager::getQuests())
            ->with('pEquipped', $pEquipped)
            ->with('presetItemData', $presetItemData);
    }
}
