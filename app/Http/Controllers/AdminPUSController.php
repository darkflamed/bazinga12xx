<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\StoreTransactions;
use App\StoreItems;
use App\StoreCategories;
use App\Items;

class AdminPUSController extends Controller
{
    public function getPUSChangeItemsPage()
    {
        return view('account.administrator.pus.main')->with('Items', StoreItems::orderby('name', 'asc')->get())->with('Categories', StoreCategories::get());
    }

    public function getPUSFinancesPage()
    {
        return view('account.administrator.pus.finances')->with('TransactionSummary', StoreTransactions::getStoreTransactionHistorySum())->with('Transactions', StoreTransactions::getStoreTransactions());
    }

    public function postPUSChangeItemsPage(Request $request)
    {
        $this->validate($request, [
            'num'       => 'required|digits:9|numeric',
            'name'      => 'required|min:6|max:50',
            'price'     => 'required|digits_between:1,10|numeric',
            'count'     => 'required|digits_between:1,10|numeric',
            'expiredays'     => 'required|digits_between:1,10|numeric',
            'desc1'     => 'max:250',
            'desc2'     => 'max:250',
            'category'  => 'required|digits_between:1,10|numeric',
            'hotitem'   => 'required|digits_between:1,10|numeric',
            'newitem'   => 'required|digits_between:1,10|numeric',
            'recomitem' => 'required|digits_between:1,10|numeric',
        ]);

        $items = Items::getItems(array($request->num));
        if($items->isEmpty()) return redirect()->back()->withErrors(['Incorrect Num!'])->withInput();

        if($request->id == 0) {
            StoreItems::create([
                'name' => $request->name,
                'num' => $request->num,
                'icon_id' => $items[0]->IconID,
                'price' => $request->price,
                'event_price' => 0,
                'count' => $request->count,
                'expire_days' => $request->expiredays,
                'desc_1' => $request->desc1,
                'desc_2' => $request->desc2,
                'limit_total' => 0,
                'category_id' => $request->category,
                'hot_item' => $request->hotitem,
                'new_item' => $request->newitem,
                'recommending_item' => $request->recomitem,
            ]);
        } else {
            $Item = StoreItems::find($request->id);
            if(!$Item) return redirect()->back()->withErrors(['Store Item does not Exist!'])->withInput();

            $Item->name = $request->name;
            $Item->num = $request->num;
            $Item->icon_id = $items[0]->IconID;
            $Item->price = $request->price;
            $Item->count = $request->count;
            $Item->expire_days = $request->expiredays;
            $Item->desc_1 = $request->desc1;
            $Item->desc_2 = $request->desc2;
            $Item->category_id = $request->category;
            $Item->hot_item = $request->hotitem;
            $Item->new_item = $request->newitem;
            $Item->recommending_item = $request->recomitem;
            $Item->save();
            return redirect()->back()->with('Success', 'Item has been modified!');
        }

        return redirect()->back()->with('Success', 'Item Added!');
    }

    public function getPUSDeleteItem($id)
    {
        $Item = StoreItems::find($id);
        if(!$Item) return redirect()->back()->withErrors(['Store Item does not Exist!'])->withInput();

        $Item->delete();
        return redirect()->back()->with('Success', 'Item has been removed!');
    }
}
