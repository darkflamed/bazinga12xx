<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Character;
use App\Clan;
use App\CurrentUser;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        Clan::updateClanPoints();
        return view('index')->with(array('UserRankingsElmo' => Character::getTopUserRankingsElmo(), 'UserRankingsKarus' => Character::getTopUserRankingsKarus(),
                                                'KarusClanRankings' => Clan::getTopClanRankingsKarus(), 'ElmoClanRankings' => Clan::getTopClanRankingsElmo(),
                                                'Online' => CurrentUser::getUserCount(), 'GMList' => Character::where('Authority', 0)->get()));
    }

    public function getUpgradeRates()
    {
        return view('upgraderates');
    }

    public function getTAC()
    {
        return view('tac');
    }
}
