<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';

    protected $fillable = ['image', 'title', 'short_text', 'long_text'];

    public static function getNewsArticles()
    {
        return News::get();
    }

    public static function getArticle($Article)
    {
        $Article = str_replace('-', '%', $Article);

        return News::where('title', 'LIKE', $Article)->first();
    }
}
