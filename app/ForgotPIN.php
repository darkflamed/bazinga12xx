<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForgotPIN extends Model
{
    protected $table = 'forgot_pin';
}
