<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class StoreTransactions extends Model
{
    protected $table = 'STORE_TRANSACTIONS';

    protected $fillable = [
        'account_id', 'character_id', 'client_ip', 'kc_option', 'kc_amount', 'kc_price', 'provider', 'transaction_id',
        'country', 'first_name', 'last_name', 'email',
        'payment_gross', 'payment_fee', 'payment_status', 'payment_method'
    ];

    public static function storeNewTransaction($request, $kcoption, $provider)
    {
        return DB::table('STORE_TRANSACTIONS')->insertGetId(
            ['account_id' => Auth::user()->id, 'client_ip' => $request->ip(), 'kc_amount' => $kcoption['kc'], 'kc_price' => $kcoption['price'], 'provider' => $provider,
                'payment_status' => 'Initialized', 'character_id' => 0, 'kc_option' => $kcoption['id'], 'transaction_id' => '', 'country' => '', 'first_name' => '', 'last_name' => '',
                'email' => Auth::user()->email, 'payment_gross' => 0, 'payment_fee' => 0, 'payment_method' => 'None']);
    }

    public static function getStoreTransactionHistorySum()
    {
        $selects = array(
            'SUM(payment_gross) AS gross_income',
            'COUNT(*) AS num_transactions',
            'SUM(payment_fee) AS total_fees'
        );
        return StoreTransactions::selectRaw(implode(',', $selects))->limit(1)->first();
    }

    public static function getStoreTransactions()
    {
        return StoreTransactions::where('status', '=', 3)->orwhere('status', '=', '4')->join('tb_user', 'store_transactions.account_id', '=', 'tb_user.id')->orderby('payment_processed', 'desc')->get();
    }
}
