<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class HardwareBanList extends Model
{
    protected $table = 'HardwareBanList';

    protected $fillable = ['account_id', 'HWID'];

    public static function checkHardwareBan($account_id)
    {
        $HWBAN = HardwareBanList::where('account_id', '=', $account_id)->get();
        if(!is_null($HWBAN) && $HWBAN->count() > 0)
            return true;
        else
            return false;
    }

    public static function banHardware($account_id, $HWID)
    {
        $HW = new HardwareBanList;
        $HW->account_id = $account_id;
        $HW->HWID = $HWID;
        $HW->save();

        return true;
    }

    public static function unbanHardware($account_id)
    {
        $HWBAN = HardwareBanList::where('account_id', '=', $account_id)->get();
        foreach($HWBAN as $ban)
            if (!is_null($ban)) {$ban->delete();}


        return true;
    }
}
