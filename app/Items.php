<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    protected $table = 'item';

    protected $hidden = ['strExtensionDescription', 'BuyPrice', 'SellingGroup', 'Extension', 'Slot', 'SellPrice', 'Effect2', 'Effect1', 'Droprate', 'strExtensionName'];

    public static function getItems(array $Items)
    {
        return Items::whereIn('Num', $Items)->get();
    }
}
