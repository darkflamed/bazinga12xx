<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use auth;

class CurrentUser extends Model
{
    protected $table = 'currentuser';

    public static function getCharacter($UserID)
    {
        return CurrentUser::where('strCharID', '=', $UserID)->first();
    }

    public static function getAccount($AccountID)
    {
        return CurrentUser::where('strAccountID', '=', $AccountID)->first();
    }

    public static function getUserCount()
    {
        return CurrentUser::selectRaw(
            '(SELECT count(userdata.Nation) FROM currentuser INNER JOIN userdata ON currentuser.strCharID = userdata.strUserId WHERE userdata.Nation = 1) as KARUS, 
                        (SELECT count(userdata.Nation) FROM currentuser INNER JOIN userdata ON currentuser.strCharID = userdata.strUserId WHERE userdata.Nation = 2) as ELMORAD')
            ->first();

    }

    public static function getOnlineUsers()
    {
        return CurrentUser::join('userdata', 'currentuser.CharID', '=', 'userdata.id')
            ->leftJoin('knights', 'userdata.knights', '=', 'knights.IDNum')->get();
    }

    public static function checkCharList($characters)
    {
        $query = CurrentUser::where('strAccountID', '=', Auth::user()->strAccountID);

        foreach($characters as $char)
        {
            $query->orWhere('strCharID', '=', $char->strUserId);
        }

        return $query->get()->count() > 0;
    }
}
