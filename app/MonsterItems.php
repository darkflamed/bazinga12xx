<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MonsterItems extends Model
{
    protected $table = 'k_monster_item';

    public static function getDrops($index)
    {
        return MonsterItems::where('sIndex', '=', $index)->first();
    }
}
