<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Downloads extends Model
{
    protected $table = 'downloads';

    protected $fillable = ['link', 'name', 'image'];

    public static function getDownloads()
    {
        return Downloads::get();
    }
}
