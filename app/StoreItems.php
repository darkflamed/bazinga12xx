<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreItems extends Model
{
    protected $table = 'store_items';

    protected $fillable = [
        'name', 'num', 'icon_id', 'price', 'event_price', 'count', 'expire_days', 'desc_1', 'desc_2',
        'limit_total', 'category_id', 'hot_item', 'new_item', 'recommending_item'
    ];

    public static function getItem($itemid)
    {
        return StoreItems::where('num', '=', $itemid)->firstOrFail();
    }

    public static function getNewItems()
    {
        return StoreItems::where('new_item', '>', 0)->orderBy('new_item', 'asc')->get();
    }

    public static function getRecommendedItems()
    {
        return StoreItems::where('recommending_item', '>', 0)->orderBy('recommending_item', 'asc')->get();
    }

    public static function getWeeklyHotItems()
    {
        return StoreItems::where('hot_item', '>', 0)->orderBy('hot_item', 'asc')->get();
    }

    public static function getCategoryItems($category, $sorttype, $layouttype, $page, $listcount)
    {
        $order = 'desc';
        switch($sorttype){
            case 'priceLow':
                $sorttype = 'price';
                break;
            case 'priceHigh':
                $sorttype = 'price';
                $order = 'asc';
                break;
            case 'name':
                $sorttype = 'name';
                $order = 'asc';
                break;
            case 0:
            default:
                $sorttype = 'id';
                break;
        }
        return StoreItems::where('category_id', '=', $category)->orderBy($sorttype, $order)->skip(($page-1)*$listcount)->take($listcount)->get();
    }

    public static function getSearchItems($text, $type, $categoryid, $sortType, $page, $listCount)
    {
        if($type == 0) $type = "name";
        else $type ="desc_1";

        if($categoryid > 0)
            return StoreItems::where($type, 'LIKE', '%'.$text.'%')->where('category_id', '=', $categoryid)->skip(($page-1)*$listCount)->take($listCount)->get();
        else
            return StoreItems::where($type, 'LIKE', '%'.$text.'%')->skip(($page-1)*$listCount)->take($listCount)->get();
    }

    public static function getTotalSearchItems($text, $type, $categoryid)
    {
        if($type == 0) $type = "name";
        else $type ="desc_1";

        if($categoryid > 0)
            return StoreItems::where($type, 'LIKE', '%'.$text.'%')->where('category_id', '=', $categoryid)->count();
        else
            return StoreItems::where($type, 'LIKE', '%'.$text.'%')->count();
    }

    public static function getTotalCategoryItems($category)
    {
        return StoreItems::where('category_id', '=', $category)->count();
    }
}
