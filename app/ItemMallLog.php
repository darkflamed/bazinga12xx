<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemMallLog extends Model
{
    protected $table = 'web_itemmall_log';

    protected $fillable = [
        'strAccountID', 'strCharID', 'ServerNo', 'ItemID', 'ItemCount', 'strItemName', 'price',
        'pay_type', 'BuyTime'
    ];
}
