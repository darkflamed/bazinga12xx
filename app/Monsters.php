<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Monsters extends Model
{
    protected $table = 'k_monster';

    public static function getMonster($ssid)
    {
        return Monsters::where('sSid', '=', $ssid)->first();
    }

    public static function getMonsters()
    {
        return Monsters::join('k_npcpos', 'k_monster.sSid', '=', 'k_npcpos.NpcID')
            ->groupBy('k_monster.sSid','k_monster.sItem', 'k_monster.strName', 'k_npcpos.ZoneID')
            ->orderBy('k_monster.strName', 'Asc')
            ->get(['k_monster.sSid','k_monster.sItem', 'k_monster.strName', 'k_npcpos.ZoneID']);
    }
}
