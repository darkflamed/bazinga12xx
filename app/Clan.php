<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Clan extends Model
{
    protected $table = 'knights';

    protected $primaryKey = 'IDNum';

    public static function getTopClanRankings()
    {
        return Clan::orderby('Flag', 'desc')->orderby('Ranking', 'desc')->orderby('Points', 'desc')->limit(10)->get();
    }

    public static function getTopClanRankingsKarus()
    {

        return Clan::where('Nation', 1)->orderby('Points', 'desc')->limit(10)->get();
    }

    public static function getTopClanRankingsElmo()
    {
        return Clan::where('Nation', 2)->orderby('Points', 'desc')->limit(10)->get();
    }

    public static function getAllKarusClanRankings()
{
    return Clan::where('Nation', 1)->orderby('Points', 'desc')->limit(100)->get();
}

    public static function getAllElmoradClanRankings()
    {
        return Clan::where('Nation', 2)->orderby('Points', 'desc')->limit(100)->get();
    }

    public static function getClan($id)
    {
        return Clan::where('IDNum', '=', $id)->first();
    }

    public static function getClanByName($name)
    {
        $name = html_entity_decode($name);
        return Clan::where('IDName', '=', $name)->first();
    }

    public static function updateClanPoints()
    {
        DB::update('EXEC [RANK_KNIGHTS]');
    }
}
