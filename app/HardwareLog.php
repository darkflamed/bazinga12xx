<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class HardwareLog extends Model
{
    protected $table = 'HardwareLog';

    public static function getAccountLogs($account_id)
    {
        return HardwareLog::where('account_id', '=', $account_id)->get();
    }
}
